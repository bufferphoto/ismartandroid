package com.handmark.pulltorefresh.library;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.view.View;

public class SpaceItemDecoration extends ItemDecoration
{
	public final static int TYPE_NORMAL = 0;
	public final static int TYPE_NO_SHOW = 1;
	public final static int TYPE_SPECIAL_SHOW = 2;

	private int space;

	private int noShowPosition = 0;

	private int showSpecialTxtPosition = 0;
	private String txt = "";
	private int specialPositionHight = 0;

	private int type = TYPE_NORMAL;

	public SpaceItemDecoration(int space)
	{
		type = TYPE_NORMAL;
		this.space = space;
	}

	public SpaceItemDecoration(int space, int noShowPosition)
	{
		type = TYPE_NO_SHOW;
		this.space = space;
		this.noShowPosition = noShowPosition;
	}

	public SpaceItemDecoration(int space, int showSpecialTxtPosition, String txt, int specialPositionHight)
	{
		type = TYPE_SPECIAL_SHOW;
		this.space = space;
		this.showSpecialTxtPosition = showSpecialTxtPosition;
		this.txt = txt;
		this.specialPositionHight = specialPositionHight;
	}

	@Override
	public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
	{

		switch (type)
		{
		case TYPE_NORMAL:
			if (parent.getChildPosition(view) != 0)
			{
				outRect.top = space;
			}
			break;
		case TYPE_NO_SHOW:
			if (parent.getChildPosition(view) != 0 && parent.getChildPosition(view) != noShowPosition)
			{
				outRect.top = space;
			}
			break;
		case TYPE_SPECIAL_SHOW:
			if (parent.getChildPosition(view) != 0)
			{
				if (parent.getChildPosition(view) == showSpecialTxtPosition)
				{
					outRect.top = specialPositionHight;
				}
				else
				{
					outRect.top = space;
				}
			}
			break;
		}
	}
}
