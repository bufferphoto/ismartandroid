package com.handmark.pulltorefresh.library.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.guogee.tablet.utils.LruCacheManager;
import com.example.mt.mediaplay.R;

/**
 * Created by Administrator on 2016/6/20.
 */
public class SwellView extends ImageView {

    private Context context = null;

    private int normalResource = R.drawable.zq_sound_a;
    private int pressResourceId = R.drawable.zq_sound_b;
    private boolean imageviewAnimation = false;

    private Paint mPaint;
    private Paint mSelcetPaint;
    private Bitmap normalBitmap;
    private Bitmap selectBitmap;
    private int width = 0;
    private int hight = 0;

    private float density = 1;
    private int progress = 0;

    public SwellView(Context context) {
        super(context);
        this.context = context;
    }

    public SwellView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        if (null != attrs) {
            TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.headImageview);
            if (t.hasValue(R.styleable.headImageview_imageviewRssource)) {
                normalResource = t.getResourceId(R.styleable.headImageview_imageviewRssource, R.drawable.zq_sound_a);
            }
            if (t.hasValue(R.styleable.headImageview_imageviewPressResource)) {
                pressResourceId = t.getResourceId(R.styleable.headImageview_imageviewPressResource, R.drawable.zq_sound_b);
            }

            if(t.hasValue(R.styleable.headImageview_imageviewAnimation)){
                imageviewAnimation = t.getBoolean(R.styleable.headImageview_imageviewAnimation, false);
            }

            t.recycle();
        }
        density = context.getResources().getDisplayMetrics().density;
        width = getBitmap(normalResource).getWidth();
        hight = getBitmap(normalResource).getHeight();
//        Log.i("SwellView", "----width= " + width + "-------hight=" + hight + "---backgroundAnimation=" + imageviewAnimation);
    }

    public void setOwnAnimation(boolean isTure) {
        this.imageviewAnimation = isTure;
    }

    public boolean getIsOwnAnimation(){
        return imageviewAnimation;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(width, hight);
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        Log.i("DrawBitmapView", "-onMeasure---widthMeasureSpec= " + widthMeasureSpec + "-------heightMeasureSpec=" + heightMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public void setProgress(int progress) {
        this.progress = progress;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!imageviewAnimation) {
            return;
        }
        if (null == mPaint) {
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setFlags(Paint.FILTER_BITMAP_FLAG);
        }

        if (null == mSelcetPaint) {
            mSelcetPaint = new Paint();
            mSelcetPaint.setAntiAlias(true);
            mSelcetPaint.setFlags(Paint.FILTER_BITMAP_FLAG);
//            mSelcetPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        }

        if (normalBitmap == null || normalBitmap.isRecycled()) {
            normalBitmap = getBitmap(normalResource);
        }
        canvas.drawBitmap(normalBitmap, 0, 0, mPaint);

        if (selectBitmap == null || selectBitmap.isRecycled()) {
            selectBitmap = getBitmap(pressResourceId);
        }
        int showHight = hight;
        if (progress <= 100 && progress >= 0) {
            showHight = hight * (100 - progress) / 100;
        } else if (progress > 100) {
            showHight = 0;
        }
//        Rect rect = new Rect(0, 0, width, showHight);
        Rect rect = new Rect(0, showHight, width, hight);
        canvas.drawBitmap(selectBitmap, rect, rect, mSelcetPaint);
    }

    private Bitmap getBitmap(int resId) {
        String path = "";
        path = resId + "";
//		GLog.i("", "onDraw,path:"+path);
        return LruCacheManager.getInstance(context).get(path);
    }
}
