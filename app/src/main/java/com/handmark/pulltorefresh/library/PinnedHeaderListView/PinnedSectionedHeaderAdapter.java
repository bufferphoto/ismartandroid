package com.handmark.pulltorefresh.library.PinnedHeaderListView;

import android.view.View;
import android.view.ViewGroup;

public interface PinnedSectionedHeaderAdapter {
	public boolean isSectionHeader(int position);

	public int getSectionForPosition(int position);

	public int getSectionHeaderViewType(int section);

	public int getCount();

	public View getSectionHeaderView(int section, int position, View convertView, ViewGroup parent);

	public abstract View getItemView(int section, int position, View convertView, ViewGroup parent);

}