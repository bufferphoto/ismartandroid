package com.handmark.pulltorefresh.library.PinnedHeaderListView;

import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class SectionedBaseAdapter extends BaseAdapter implements PinnedSectionedHeaderAdapter {

    private static int HEADER_VIEW_TYPE = 0;
    private static int ITEM_VIEW_TYPE = 0;

    /**
     * Holds the calculated values of @{link getPositionInSectionForPosition}
     */
    private SparseArray<Integer> mSectionPositionCache;
    /**
     * Holds the calculated values of @{link getSectionForPosition}
     */
    private SparseArray<Integer> mSectionCache;
    /**
     * Holds the calculated values of @{link getCountForSection}
     */
    private SparseArray<Integer> mSectionCountCache;

    /**
     * Caches the item count
     */
    private int mCount;
    /**
     * Caches the section count
     */
    private int mSectionCount;

    public SectionedBaseAdapter() {
        super();
        mSectionCache = new SparseArray<Integer>();
        mSectionPositionCache = new SparseArray<Integer>();
        mSectionCountCache = new SparseArray<Integer>();
        mCount = -1;
        mSectionCount = -1;
    }

    @Override
    public void notifyDataSetChanged() {
        mSectionCache.clear();
        mSectionPositionCache.clear();
        mSectionCountCache.clear();
        mCount = -1;
        mSectionCount = -1;
        super.notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetInvalidated() {
        mSectionCache.clear();
        mSectionPositionCache.clear();
        mSectionCountCache.clear();
        mCount = -1;
        mSectionCount = -1;
        super.notifyDataSetInvalidated();
    }

    // @Override
    // public final Object getItem(int position) {
    // return getItem(getSectionForPosition(position),
    // getPositionInSectionForPosition(position));
    // }
    //
    // @Override
    // public final long getItemId(int position) {
    // return getItemId(getSectionForPosition(position),
    // getPositionInSectionForPosition(position));
    // }

    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        if (isSectionHeader(position)) {
            return getSectionHeaderView(getSectionForPosition(position), position, convertView, parent);
        }
        return getItemView(getSectionForPosition(position), position, convertView, parent);
    }

    @Override
    public final int getItemViewType(int position) {
        if (isSectionHeader(position)) {
            return getItemViewTypeCount() + getSectionHeaderViewType(getSectionForPosition(position));
        }
        return getItemViewType(getSectionForPosition(position), getPositionInSectionForPosition(position));
    }

    @Override
    public final int getViewTypeCount() {
        return getItemViewTypeCount() + getSectionHeaderViewTypeCount();
    }

    public final int getSectionForPosition(int position) {
        // first try to retrieve values from cache
        Integer cachedSection = mSectionCache.get(position);
        if (cachedSection != null) {
            return cachedSection;
        }
        int count = -1;
        if (isSectionHeader(position)) {
//            count = mSectionCache.size();
            count = position;
            mSectionCache.put(position, count);
        } else {
            for (int i = position; i >= 0; i--) {
                if (isSectionHeader(i)) {
                    Integer postion = mSectionCache.get(i);
                    if (postion != null) {
                        count = postion;
                    }
                    break;
                }
            }
        }
        return count;
    }

    public final int getPositionInSectionForPosition(int position) {
        // first try to retrieve values from cache
        Integer cachedPosition = mSectionPositionCache.get(position);
        if (cachedPosition != null) {
            return cachedPosition;
        }

        int positionInSection = 0;
        if (!isSectionHeader(position)) {
            int sectionCount = -1;
            for (int i = position; i >= 0; i--) {
                if (isSectionHeader(i)) {
                    Integer postion = mSectionCache.get(i);
                    if (postion != null) {
                        sectionCount = postion;
                    }
                    break;
                }
            }
            positionInSection = position - sectionCount - 1;
            mSectionPositionCache.put(position, positionInSection);
        }
        return positionInSection;
    }

    public int getItemViewType(int section, int position) {
        return ITEM_VIEW_TYPE;
    }

    public int getItemViewTypeCount() {
        return 1;
    }

    public int getSectionHeaderViewType(int section) {
        return HEADER_VIEW_TYPE;
    }

    public int getSectionHeaderViewTypeCount() {
        return 1;
    }

    // public abstract Object getItem(int section, int position);
    //
    // public abstract long getItemId(int section, int position);

	/*
     * public abstract int getCount();
	 * 
	 * public abstract boolean isSectionHeader();
	 */

    // public abstract View getItemView(int section, int position, View
    // convertView, ViewGroup parent);
    //
    // public abstract View getSectionHeaderView(int section, View convertView,
    // ViewGroup parent);

    // public abstract View getItemView(int position, View convertView,
    // ViewGroup parent);

    // public abstract View getSectionHeaderView(int position, View convertView,
    // ViewGroup parent);

}
