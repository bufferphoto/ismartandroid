package com.ndt.ppcs_api;

@SuppressWarnings("JniMissingFunction")
public class NDT_API {

    public static final int DID_ALREAD_SUBSCRIBLE = 1000; //已订阅
    public static final int DID_ALREAD_UNSUBSCRIBLE = 1001; //未订阅
    public static final int NDT_ERROR_NoError = 0;   // API function is successfully returned and no error occurs
    public static final int NDT_ERROR_AlreadyInitialized = -1;  // Initialize function is already successfully called
    public static final int NDT_ERROR_NotInitialized = -2;  // The NDT lib is not initialize yet.
    public static final int NDT_ERROR_TimeOut = -3;  // Time out

    public static final int NDT_ERROR_ScketCreateFailed = -4;  // Socket create error
    public static final int NDT_ERROR_ScketBindFailed = -5;  // Socket bind error
    public static final int NDT_ERROR_HostResolveFailed = -6;  // Calling gethostbyname is failed
    public static final int NDT_ERROR_ThreadCreateFailed = -7;  // Thread create error
    public static final int NDT_ERROR_MemoryAllocFailed = -8;  // Failed to allocate memory
    public static final int NDT_ERROR_NotEnoughBufferSize = -9; // The size of data buffer is not enough

    public static final int NDT_ERROR_InvalidInitString = -10; // Not a legal InitString
    public static final int NDT_ERROR_InvalidAES128Key = -11; // the AES128Key require exectly 16 Byte
    public static final int NDT_ERROR_InvalidDataOrSize = -12; // The specified Data buffer or Data size is invalid
    public static final int NDT_ERROR_InvalidDID = -13; // The DID is invalid, Maybe invalid Prefix or CheckCode, or exceed max SN, or DID is not specified in Initialize()
    public static final int NDT_ERROR_InvalidNDTLicense = -14; // The DID string don't include valid NDT License, it should looks like "AAAA-123456-ABCDE$FGHIJK"

    public static final int NDT_ERROR_InvalidHandle = -15; // The specified Handle is invalid
    public static final int NDT_ERROR_ExceedMaxDeviceHandle = -16; // Exceed Max number of Device handle
    public static final int NDT_ERROR_ExceedMaxClientHandle = -17; // Exceed Max number of Client handle

    public static final int NDT_ERROR_NetworkDetectRunning = -18; // Network Detect function is on going
    public static final int NDT_ERROR_SendToRunning = -19; // The SendTo() or SendToByServer() for the same DID is running
    public static final int NDT_ERROR_RecvRunning = -20; // Recv function is running
    public static final int NDT_ERROR_RecvFromRunning = -21; // RecvFrom()for the same DeviceHandle is running
    public static final int NDT_ERROR_SendBackRunning = -22; // SendBack()for the same ClientHandle is running

    public static final int NDT_ERROR_DeviceNotOnRecv = -23; // The Target device is not calling Recv()
    public static final int NDT_ERROR_ClientNotOnRecvFrom = -24; // The Client is not calling RecvFrom()
    public static final int NDT_ERROR_NoAckFromCS = -25; // None of 3 CS acks, it's very likely due to local network problem
    public static final int NDT_ERROR_NoAckFromPushServer = -26; // No Push Server acks, it's very likely due to local network problem
    public static final int NDT_ERROR_NoAckFromDevice = -27; // Device doesn't ack, it's very likely due to device is not online now (but device did online in last minite)
    public static final int NDT_ERROR_NoAckFromClient = -28; // Client doesn't ack, it's very likely due to Client is not online now
    public static final int NDT_ERROR_NoPushServerKnowDevice = -29; // Device doesn't send alive to any PS, it's very likely due to device is not online now
    public static final int NDT_ERROR_NoPushServerKnowClient = -30; // Cleint doesn't send message PS, it's very likely due to Client is not online now

    public static final int NDT_ERROR_UserBreak = -31; // Recv_Break(), SendTo_Break(), SendBack_Break() or RecvFrom_Break() is called
    public static final int NDT_ERROR_SendToNotRunning = -32; // The SendTo() or SendToByServer() for the same DID is not running
    public static final int NDT_ERROR_RecvNotRunning = -33; // Recv function is not running
    public static final int NDT_ERROR_RecvFromNotRunning = -34; // RecvFrom()for the same DeviceHandle is not running
    public static final int NDT_ERROR_SendBackNotRunning = -35; // SendBack()for the same ClientHandle is not running
    public static final int NDT_ERROR_RemoteHandleClosed = -36; //Remote Device or Client already call CloseHandle()

    public static final int NDT_ERROR_FAESupportNeeded = -99; // When this Error occurs, please contact CS2' FAE for

    public static final int NDT_ERROR_InvalidParameter = -100; //Parameter is invalid

    public static native String NDT_PPCS_GetAPIVersion(int[] Version);

    public static native int NDT_PPCS_Initialize(String InitString, int PortNo, String DIDString, String AES128Key);

    public static native int NDT_PPCS_DeInitialize();

    public static native int NDT_PPCS_NetworkDetect(st_NDT_NetInfo NetInfo, int TimeOut_ms);

    public static native int NDT_PPCS_Recv(byte[] Data, int[] Size, int TimeOut_ms, int Option);

    public static native int NDT_PPCS_SendBack(int ClientHandle, byte[] Data, int Size);

    public static native int NDT_PPCS_SendTo(String DID, byte[] Data, int Size, int Mode);

    public static native int NDT_PPCS_SendToByServer(String DID, byte[] Data, int Size, int Mode, String InitString, String AES128Key);

    public static native int NDT_PPCS_RecvFrom(int DeviceHandle, byte[] Data, int[] Size, int TimeOut_ms);

    public static native int NDT_PPCS_CloseHandle(int Handle);

    public static native int NDT_PPCS_SendTo_Break(String DID);

    public static native int NDT_PPCS_Recoov_Break();

    public static native int NDT_PPCS_SendBack_Break(int ClientHandle);

    public static native int NDT_PPCS_RecvFrom_Break(int DeviceHandle);

    static {
        try {
            System.loadLibrary("NDT_API_PPCS");
        } catch (UnsatisfiedLinkError ule) {
            System.out.println("!!!!! loadLibrary(NDT_API_PPCS), Error:" + ule.getMessage());
        }
    }

}

