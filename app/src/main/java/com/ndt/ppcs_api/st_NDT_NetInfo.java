package com.ndt.ppcs_api;

public class st_NDT_NetInfo {
    public byte[] LanIP = new byte[16];
    public byte[] LanIPv6 = new byte[40];
    public byte[] WanIP = new byte[16];
    public byte[] WanIPv6 = new byte[40];
    public int LanPort;
    public int WanPort;
    public byte bServerHelloAck;
    public byte[] bReserved = new byte[3];

    {
        bServerHelloAck = 0;
        LanPort = 0;
        WanPort = 0;
    }
    public static String bytes2Str(byte[] byts)
    {
        String str="";
        int iLen=0;
        for(iLen=0; iLen<byts.length; iLen++){
            if(byts[iLen]==(byte)0) break;
        }
        if(iLen==0) str="";
        else str=new String(byts,0,iLen);
        return str;
    }

    public String getLanIP()	{ return bytes2Str(LanIP); 	}
    public int	  getLanPort()	{ return LanPort;			}
    public String getWanIP()		{ return bytes2Str(WanIP); 	}
    public int	  getWanPort()	{ return WanPort;				}

}
