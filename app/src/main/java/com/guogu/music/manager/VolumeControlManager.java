package com.guogu.music.manager;

import android.content.Context;
import android.media.AudioManager;

import com.guogee.ismartandroid2.utils.GLog;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2017/5/25.
 */
public class VolumeControlManager {

    private volatile static VolumeControlManager instance;
    private List<VolumeReusltListener> listenerList;
    private AudioManager mAudioManager;
    private int maxVolume = 100;
    private int minVolume = 100;

    private VolumeControlManager(Context context) {
        //音量控制,初始化定义
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        //最大音量
        maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        GLog.i("  maxVolume:" + maxVolume);
        minVolume = (int) ((maxVolume / 5.0) == 0 ? 1 : (maxVolume / 5.0));
//        //当前音量
//        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        listenerList = new ArrayList<>();
    }

    public static VolumeControlManager getInstance(Context context) {
        if (null == instance) {
            synchronized (VolumeControlManager.class) {
                if (null == instance) {
                    instance = new VolumeControlManager(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    public void addVolume() {
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        currentVolume += maxVolume / 5.0;
        currentVolume = currentVolume > maxVolume ? maxVolume : currentVolume;
        currentVolume = currentVolume < minVolume ? minVolume : currentVolume;
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, AudioManager.FX_FOCUS_NAVIGATION_UP); //tempVolume:音量绝对值
//        mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE,
//                AudioManager.FX_FOCUS_NAVIGATION_UP);
        callback(getVolume());
    }

    public void reduceVolume() {
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        currentVolume -= maxVolume / 5.0;
        currentVolume = currentVolume > maxVolume ? maxVolume : currentVolume;
        currentVolume = currentVolume < minVolume ? minVolume : currentVolume;
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, AudioManager.FX_FOCUS_NAVIGATION_UP); //tempVolume:音量绝对值
//        mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER,
//                AudioManager.FX_FOCUS_NAVIGATION_UP);
        callback(getVolume());
    }

    public void maxVolume() {
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, AudioManager.FX_FOCUS_NAVIGATION_UP); //tempVolume:音量绝对值
        callback(getVolume());
    }

    public void minVolume() {
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, minVolume, AudioManager.FX_FOCUS_NAVIGATION_UP); //tempVolume:音量绝对值
        callback(getVolume());
    }

    public void setVolume(int vaule) {
        if (vaule > 100 || vaule < 0) {
            return;
        }
        vaule = (int) (vaule / 100.0 * maxVolume);
//        vaule = vaule > maxVolume ? maxVolume : vaule;
//        vaule = vaule < minVolume ? minVolume : vaule;
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, vaule, AudioManager.FX_FOCUS_NAVIGATION_UP); //tempVolume:音量绝对值
        callback(getVolume());
    }

    public int getVolume() {
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        //最大音量
       // return (int) (currentVolume * 100.0 / maxVolume);
        return currentVolume;
    }

    private void callback(int volume) {
        for (int i = 0; i < listenerList.size(); i++) {
            listenerList.get(i).volumeResult(volume);
        }
    }

    public void registerVolumeListener(VolumeReusltListener listener) {
        if (!listenerList.contains(listener))
            listenerList.add(listener);
    }

    public void unregisterVolumeListener(VolumeReusltListener listener) {
        if (listenerList.contains(listener))
            listenerList.remove(listener);
    }

    public interface VolumeReusltListener {
        public void volumeResult(int volume);
    }

}
