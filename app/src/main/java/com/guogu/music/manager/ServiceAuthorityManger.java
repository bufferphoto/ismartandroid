package com.guogu.music.manager;

import android.content.Context;

import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.ismartandroid2.utils.PublishHelper;
import com.guogee.pushclient.SDKUtils;
import com.guogu.music.http.RemoteServerManger;
import com.guogu.music.http.imp.ICallback;
import com.guogu.music.play.PlayerControl;
import com.guogu.music.util.AuthorityErrorUtil;
import com.guogu.music.util.CommonUtil;
import com.guogu.music.util.Constants;

/**
 * Created by Administrator on 2017/6/5.
 */
public class ServiceAuthorityManger {

    private volatile static ServiceAuthorityManger instance;
    private Context context;
    private int authorityID = -1;

    private ServiceAuthorityManger(Context context) {
        this.context = context;
    }

    public static ServiceAuthorityManger getInstance(Context context) {
        if (null == instance) {
            synchronized (ServiceAuthorityManger.class) {
                if (null == instance) {
                    instance = new ServiceAuthorityManger(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    public void checkVendor() {
        String sn = SDKUtils.getSN(context);
        String deviceID = SDKUtils.getCID(context);
        RemoteServerManger.getInstance().serviceAuthority(getVendor(), sn, deviceID, new ICallback() {
            @Override
            public void callbackSuccess(String jsonString) {
                authorityID = 0;
            }

            @Override
            public void callbackFail(int code, String err) {
                authorityID = code;
            }
        });
        GLog.i("ServiceAuthorityManger","---sn----"+sn);
    }

    private String getVendor() {
        if (PublishHelper.getBuildType() == PublishHelper.BuildType.Release) {
            return Constants.VENDORID_PUBLIC;
        } else {
            return Constants.VENDORID;
        }
    }

    public int getAuthorityID() {
        return authorityID;
    }

    public boolean authorityCheck() {
        boolean result = false;
        GLog.i("olife", "---id:" + authorityID);
        if (authorityID == 0) {
            result = true;
        } else {
            switch (authorityID) {
                case AuthorityErrorUtil.VENDOR_NOT_REGISTER_OR_INVALID:
                    PlayerControl.getInstance(context).speak("厂商未注册");
                    break;
                case AuthorityErrorUtil.SN_NOT_REGISTER_OR_INVALID:
                    PlayerControl.getInstance(context).speak("SN未注册");
                    break;
                case AuthorityErrorUtil.SN_HAD_USED:
                    PlayerControl.getInstance(context).speak("SN已被使用");
                    break;
                case -1:
                    PlayerControl.getInstance(context).speak("未验证厂商");
                    break;
                case AuthorityErrorUtil.NETWORK_ERROR:
                default:
                    PlayerControl.getInstance(context).speak("网络错误");
                    break;
            }
        }
        return result;
    }
}
