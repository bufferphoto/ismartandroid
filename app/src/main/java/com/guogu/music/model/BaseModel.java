package com.guogu.music.model;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/*
 * 反射得到类中所有对象
 * 
 * 定义的私有变量需要和数据库中的字段名一致
 * 
 */
public abstract class BaseModel implements Serializable, Cloneable {

	/**
	 * 
	 */
	public static final long serialVersionUID = 6868435884517835898L;

	/**
	 * 
	 */

	public Map<String, String> getModelMap() {
		return getValueMap(this);
	}

	public Map<String, Object> getDBMap() {
		return getObjectMap(this);
	}

	public JSONObject getModelJSONObj() {
		return getValueJSONObj(this);
	}

	/**
	 * @author Smither
	 * @param obj
	 * @return
	 */
	private JSONObject getValueJSONObj(Object obj) {

		JSONObject json = new JSONObject();
		// System.out.println(obj.getClass());
		// 获取f对象对应类中的所有属性域
		Field[] fields = obj.getClass().getDeclaredFields();
		for (int i = 0, len = fields.length; i < len; i++) {
			String varName = fields[i].getName();
			try {
				// 获取原来的访问控制权限
				boolean accessFlag = fields[i].isAccessible();
				// 修改访问控制权限
				fields[i].setAccessible(true);
				// 获取在对象f中属性fields[i]对应的对象中的变量
				Object o = fields[i].get(obj);
				if (o != null)
					try {
						json.put(varName, o.toString());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				// System.out.println("传入的对象中包含一个如下的变量：" + varName + " = " + o);
				// 恢复访问控制权限
				fields[i].setAccessible(accessFlag);
			} catch (IllegalArgumentException ex) {
				ex.printStackTrace();
			} catch (IllegalAccessException ex) {
				ex.printStackTrace();
			}
		}
		return json;

	}

	private Map<String, Object> getObjectMap(Object obj) {

		Map<String, Object> map = new HashMap<String, Object>();
		Class<?> clazz = obj.getClass();

		for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
			// 获取f对象对应类中的所有属性域
			Field[] fields = clazz.getDeclaredFields();
			for (int i = 0, len = fields.length; i < len; i++) {
				String varName = fields[i].getName();
				if (varName.equals("serialVersionUID")) {
					continue;
				}
				if (Modifier.toString(fields[i].getModifiers()).equals("public"))
					continue;
				try {
					// 获取原来的访问控制权限
					boolean accessFlag = fields[i].isAccessible();
					// 修改访问控制权限
					fields[i].setAccessible(true);
					// 获取在对象f中属性fields[i]对应的对象中的变量
					Object o = fields[i].get(obj);

					if (o != null) {
						if (o.toString().equals("-1")) {// 定义的Model中int类型数据初始化为-1,若为-1则跳过
							continue;
						} else if (varName.equals("id") && o.toString().equals("0"))
							continue;
						else {
							map.put(varName, o);
						}
					}
					// System.out.println("传入的对象中包含一个如下的变量：" + varName + " = " +
					// o);
					// 恢复访问控制权限
					fields[i].setAccessible(accessFlag);
				} catch (IllegalArgumentException ex) {
					ex.printStackTrace();
				} catch (IllegalAccessException ex) {
					ex.printStackTrace();
				}
			}

		}
		return map;

	}

	private Map<String, String> getValueMap(Object obj) {

		Map<String, String> map = new HashMap<String, String>();
		Class<?> clazz = obj.getClass();

		for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
			// 获取f对象对应类中的所有属性域
			Field[] fields = clazz.getDeclaredFields();
			for (int i = 0, len = fields.length; i < len; i++) {
				String varName = fields[i].getName();
				try {
					// 获取原来的访问控制权限
					boolean accessFlag = fields[i].isAccessible();
					// 修改访问控制权限
					fields[i].setAccessible(true);
					// 获取在对象f中属性fields[i]对应的对象中的变量
					Object o = fields[i].get(obj);
					if (o != null)
						map.put(varName, o.toString());
					// System.out.println("传入的对象中包含一个如下的变量：" + varName + " = " +
					// o);
					// 恢复访问控制权限
					fields[i].setAccessible(accessFlag);
				} catch (IllegalArgumentException ex) {
					ex.printStackTrace();
				} catch (IllegalAccessException ex) {
					ex.printStackTrace();
				}
			}
		}

		return map;

	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
