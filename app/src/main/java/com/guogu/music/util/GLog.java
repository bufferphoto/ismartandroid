package com.olife.music.util;

/**
 * 替换原来android的Log类
 */
public class GLog {
	/** 是否为发布模式 */
	private static boolean IS_RELEASE = false;

	public static void setDebug(boolean value) {
		if (value) {
			IS_RELEASE = false;
		} else {
			IS_RELEASE = true;
		}
	}

	/*
	 * 自动获取当前类名以及行数
	 */
	public static void d(String msg) {
		if (IS_RELEASE)
			return;
		android.util.Log.d(generateTag(getCallerStackTraceElement()), msg);
	}

	/**
	 * Send a DEBUG log message.
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void d(String tag, String msg) {
		if (IS_RELEASE)
			return;
		android.util.Log.d(tag, msg);
	}

	public static void v(String msg) {
		android.util.Log.v(generateTag(getCallerStackTraceElement()), msg);
	}

	/**
	 * Send a VERBOSE log message.
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void v(String tag, String msg) {
		if (IS_RELEASE)
			return;
		android.util.Log.v(tag, msg);
	}

	public static void i(String msg) {
		if (IS_RELEASE)
			return;
		android.util.Log.i(generateTag(getCallerStackTraceElement()), msg);
	}

	/**
	 * Send an INFO log message.
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void i(String tag, String msg) {
		if (IS_RELEASE)
			return;
		android.util.Log.i(tag, msg);
	}

	public static void e(String msg) {
		android.util.Log.e(generateTag(getCallerStackTraceElement()), msg);
	}

	/**
	 * Send an ERROR log message.
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void e(String tag, String msg) {
		android.util.Log.e(tag, msg);
	}

	/**
	 * Send a WARN log message.
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void w(String tag, String msg) {
		tag = generateTag(getCallerStackTraceElement());
		android.util.Log.w(tag, msg);
	}

	public static String generateTag(StackTraceElement caller) {
		String tag = "%s.%s(Line:%d)"; // 占位符
		String callerClazzName = caller.getClassName(); // 获取到类名
		callerClazzName = callerClazzName.substring(callerClazzName.lastIndexOf(".") + 1);
		tag = String.format(tag, callerClazzName, caller.getMethodName(), caller.getLineNumber()); // 替换
		// tag = TextUtils.isEmpty(customTagPrefix) ? tag : customTagPrefix +
		// ":" + tag;
		return tag;
	}

	private static StackTraceElement getCallerStackTraceElement() {
		return Thread.currentThread().getStackTrace()[4];
	}

	public static boolean isRelease() {
		return IS_RELEASE;
	}
}
