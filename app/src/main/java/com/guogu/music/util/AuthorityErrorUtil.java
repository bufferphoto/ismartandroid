package com.guogu.music.util;

public class AuthorityErrorUtil {
	public static final int NETWORK_ERROR = 100000;//网络错误
	public static final int VENDOR_NOT_REGISTER_OR_INVALID = 100027;//厂商未注册或无效
	public static final int SN_NOT_REGISTER_OR_INVALID = 100028;//SN未注册或无效
	public static final int SN_HAD_USED = 100029;//SN已被使用

}
