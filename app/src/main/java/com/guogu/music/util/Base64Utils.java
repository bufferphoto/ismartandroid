package com.guogu.music.util;

import android.util.Base64;

public class Base64Utils {
	/**
	 * 解码
	 * 
	 * @param bytes
	 * @return
	 */
	public static String decode(final byte[] bytes) {
		return new String(Base64.decode(bytes, Base64.DEFAULT));
	}

	/**
	 * 编码 二进制数据编码为BASE64字符串
	 * 
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	public static String encode(final byte[] bytes) {
		return new String(Base64.encode(bytes, Base64.DEFAULT));
	}
}
