/**
 *
 */
package com.guogu.music.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.guogee.ismartandroid2.utils.GConstant;

/**
 * @author xieguangwei
 * @ClassName: Actions.java
 * @Description: 动作定义类
 * @date 2015年11月9日
 */
public class MusicFilterSetting {

	public static int LAN = GConstant.LAN_CN;
	public final static int ACTION_TYPE_PLAY = 0;// 音乐搜索播放指令
	public final static int ACTION_TYPE_CONTROL_LAST = 1;// 上一首
	public final static int ACTION_TYPE_CONTROL_NEXT = 2;// 下一首
	public final static int ACTION_TYPE_CONTROL_STOP = 3;// 暂停
	public final static int ACTION_TYPE_CONTROL_CONTINUE = 4;// 继续播放
	public final static int ACTION_TYPE_CONTROL_REPLACE = 5;// 换一首
	public final static int ACTION_TYPE_CONTROL_VOICE_ADD = 6;// 音量增加
	public final static int ACTION_TYPE_CONTROL_VOICE_REDUCE = 7;// 音量减少
	public final static int ACTION_TYPE_CONTROL_VOICE_MAX = 8;// 音量最大
	public final static int ACTION_TYPE_CONTROL_VOICE_MIN = 9;// 音量最小
	public final static int ACTION_TYPE_CONTROL_VOICE_VAULE = 10;// 音量设置值
	public final static int ACTION_TYPE_CONTROL_PLAY_NAME = 11;// 播报名字
	public final static int ACTION_TYPE_CONTROL_SINGER = 12;// 单曲循环
	public final static int ACTION_TYPE_CONTROL_RANDOM = 13;// 随机播放
	public final static int ACTION_TYPE_CONTROL_ORDER = 14;// 顺序播放
	public final static int ACTION_TYPE_CONTROL_ADD_LOVE = 15;// 加入喜欢列表
	public final static int ACTION_TYPE_CONTROL_PLAY_LOVE = 16;// 播放喜欢列表
	public final static int ACTION_TYPE_CONTROL_PLAY_MUSIC = 17;// 播放喜欢列表
	public final static int ACTION_TYPE_CONTROL_PLAY_HOT = 18;// 热门推荐

	/**
	 * 第一个string 正则过滤
	 * <p/>
	 * 第二个string 控制指令\或者搜索的词
	 * <p/>
	 * 第三个int 指令类型
	 */
	private static final Map<String, ControlKeyword> ACTIONS_CN = new LinkedHashMap<String, ControlKeyword>() {
		private static final long serialVersionUID = 2672348697412559451L;

		{
			put(".*上(一|1)(首|曲).*", new ControlKeyword(ACTION_TYPE_CONTROL_LAST, "上一首"));
			put(".*下(一|1)(首|曲).*", new ControlKeyword(ACTION_TYPE_CONTROL_NEXT, "下一首"));
			put(".*(停止播放|暂停播放|停止|暂停).*", new ControlKeyword(ACTION_TYPE_CONTROL_STOP, "暂停"));
			put(".*继续播放.*", new ControlKeyword(ACTION_TYPE_CONTROL_CONTINUE, "继续播放"));
			put(".*换(.*)(首|曲)(.*).*", new ControlKeyword(ACTION_TYPE_CONTROL_REPLACE, "换一首"));
			put(".*音量(增加|增大|变大|调大|调高).*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_ADD, "音量增加"));
			put(".*音量(减少|减小|变小|调小|调低).*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_REDUCE, "音量减小"));
			put(".*音量最大.*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_MAX, "音量最大"));
			put(".*音量最小.*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_MIN, "音量最小"));
			put(".*(音量)(.*).*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_VAULE, "音量值"));
			put(".*(.*)(当前|现在)(.*)播放(.*)什么歌.*", new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_NAME, "播报歌名"));
			put(".*单曲循环.*", new ControlKeyword(ACTION_TYPE_CONTROL_SINGER, "单曲循环"));
			put(".*随机(|播放).*", new ControlKeyword(ACTION_TYPE_CONTROL_RANDOM, "随机播放"));
			put(".*顺序(|播放).*", new ControlKeyword(ACTION_TYPE_CONTROL_ORDER, "顺序播放"));
			put(".*列表循环.*", new ControlKeyword(ACTION_TYPE_CONTROL_ORDER, "顺序播放"));
			put(".*列表循环.*", new ControlKeyword(ACTION_TYPE_CONTROL_ORDER, "顺序播放"));
			put(".*(把|将)这首歌(加入|添加)(.*)喜(欢|爱)(.*)列表.*", new ControlKeyword(ACTION_TYPE_CONTROL_ADD_LOVE, "加入喜欢列表"));
			put(".*喜(欢|爱)(.*)(歌|列表).*", new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_LOVE, "播放喜欢列表"));
			put(".*播放(.*)我(.*)喜(欢|爱)(.*)(歌|列表).*", new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_LOVE, "播放喜欢列表"));
			put(".*热门推荐.*", new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_HOT, "热门推荐"));
			put(".*播放音乐.*", new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_MUSIC, "播放音乐"));
			put(".*(播放一首|播放|放一首|放)(.*).*", new ControlKeyword(ACTION_TYPE_PLAY, "播放歌曲"));
		}
	};

	private static final Map<String, ControlKeyword> ACTIONS_EN = new LinkedHashMap<String, ControlKeyword>() {
		private static final long serialVersionUID = 2672348697412559451L;

		{
			put(".*(previous|Previous) song.*", new ControlKeyword(ACTION_TYPE_CONTROL_LAST, "previous song"));
			put(".*(next|Next) song.*", new ControlKeyword(ACTION_TYPE_CONTROL_NEXT, "next song"));
			put(".*(stop|Stop) play.*", new ControlKeyword(ACTION_TYPE_CONTROL_STOP, "stop"));
			put(".*(continue|Continue) to play.*", new ControlKeyword(ACTION_TYPE_CONTROL_CONTINUE, "continue"));
			put(".*(change|Change)(.*)song.*", new ControlKeyword(ACTION_TYPE_CONTROL_REPLACE, "change song"));
			put(".*(volume|Volume) up.*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_ADD, "volume up"));
			put(".*(volume|Volume) down.*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_REDUCE, "volum down"));
			put(".*(max|Max) volume.*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_MAX, "max volum"));
			put(".*(min|Min) volume.*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_MIN, "min volum"));
			put(".*(volume|Volume)(.*).*", new ControlKeyword(ACTION_TYPE_CONTROL_VOICE_VAULE, "volume value"));
			put(".*(What|what) song is playing now.*",
					new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_NAME, "plau music name"));
			put(".*(Single|single) loop.*", new ControlKeyword(ACTION_TYPE_CONTROL_SINGER, "single loop"));
			put(".*(random|Random) play.*", new ControlKeyword(ACTION_TYPE_CONTROL_RANDOM, "random play"));
			put(".*(List|list) loop.*", new ControlKeyword(ACTION_TYPE_CONTROL_ORDER, "list loop"));
			put(".*(add|Add) (this|the) music to (favorite|love) list.*",
					new ControlKeyword(ACTION_TYPE_CONTROL_ADD_LOVE, "add favorite list"));
			put(".*(favorite|Favorite|love|Love) (.*)(song|list).*",
					new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_LOVE, "favorite list"));
			put(".*(play|Play)(.*)(My|I)(.*)(favorite|love)(.*)(song|list).*",
					new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_LOVE, "favorite list"));
			put(".*(Hot|hot).*", new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_HOT, "hot"));
			put(".*(play|Play) music.*", new ControlKeyword(ACTION_TYPE_CONTROL_PLAY_MUSIC, "play music"));
			put(".*(play|Play) (.*).*", new ControlKeyword(ACTION_TYPE_PLAY, "play"));
		}
	};

	public static Map<String, ControlKeyword> getActions(int lan) {
		switch (lan) {
		case GConstant.LAN_EN:
			return ACTIONS_EN;
		case GConstant.LAN_CN:
		default:
			return ACTIONS_CN;
		}
	}

	public static ControlKeyword filterKeyword(String text) {
		List<Integer> lanList = new ArrayList<>();
		lanList.add(GConstant.LAN_CN);
		lanList.add(GConstant.LAN_EN);
		for (Integer lan : lanList) {
			Map<String, ControlKeyword> actions = MusicFilterSetting.getActions(lan);
			Iterator<String> keys = actions.keySet().iterator();
			while (keys.hasNext()) {
				String key = keys.next();
				// Log.i(" match:" + text.matches(key) + " value:" +
				// actions.get(key).action);
				if (text.matches(key)) {
					ControlKeyword controlKeyword = actions.get(key);
					if (controlKeyword.type == MusicFilterSetting.ACTION_TYPE_PLAY) {
						controlKeyword.action = filterMatchs(text, key);
					} else if (controlKeyword.type == MusicFilterSetting.ACTION_TYPE_CONTROL_VOICE_VAULE) {
						controlKeyword.action = filterMatchs(text, key);
					}
					return controlKeyword;
				}
			}
		}
		return null;
	}

	private static String filterMatchs(String testString, String sceneRegex) {
		Pattern p = Pattern.compile(sceneRegex);
		Matcher matcher = p.matcher(testString);
		String keyword = "";
		while (matcher.find()) {
			System.out.println(matcher.groupCount());
			for (int i = 0; i <= matcher.groupCount(); i++) {
				// System.out.println("group" + i + " : " + matcher.start(i) + "
				// - " + matcher.end(i));
				// System.out.println(matcher.group(i));
			}
			keyword = matcher.group(matcher.groupCount());
		}
		System.out.println(" keyword:" + keyword);
		return keyword;
	}

	/**
	 * @author Administrator
	 *         <p/>
	 *         type
	 *         <p/>
	 *         action
	 */
	public static class ControlKeyword {
		public int type;
		public String action;

		public ControlKeyword(int type, String action) {
			this.type = type;
			this.action = action;
		}
	}
}
