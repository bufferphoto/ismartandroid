package com.guogu.music.http.imp;

public interface ICallback {

	void callbackSuccess(String jsonString);

	void callbackFail(int code, String err);
}
