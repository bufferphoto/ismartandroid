package com.guogu.music.http;

import android.content.Context;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class HttpManger {
	private volatile static HttpManger instance;
	private boolean isWrite = true;
	private Context context;

	private RequestQueue mQueue;

	private HttpManger(Context context) {
		mQueue = Volley.newRequestQueue(context);
	}

	public static HttpManger getInstance(Context context) {
		if (instance == null) {
			synchronized (HttpManger.class) {
				if (instance == null) {
					instance = new HttpManger(context.getApplicationContext());
				}
			}
		}
		return instance;
	}

	public <T> void requestGet(String url, Listener<String> listener, ErrorListener errorListener) {
		StringRequest stringRequest = new StringRequest(Method.GET, url, listener, errorListener);
		mQueue.add(stringRequest);
	}

	public void requestPost(String url, Listener<String> listener, ErrorListener errorListener) {
		StringRequest stringRequest = new StringRequest(Method.POST, url, listener, errorListener);
		mQueue.add(stringRequest);
	}
}
