package com.guogu.music.http.imp;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogu.music.model.Music;

import java.util.ArrayList;
import java.util.List;

public class ParserSet {

    public static class MusicParser extends JsonParser<List<Music>> {
        @Override
        public List<Music> parse(String data) {
            GLog.i("olife", "---data:" + data);
            List<Music> musics = new ArrayList<Music>();
            GLog.i("music time:" + JSONObject.parseObject(data).getString("time"));
            JSONArray jsonArray = JSONObject.parseObject(data).getJSONArray("value");
            if (null != jsonArray && jsonArray.size() > 0) {
                int length = jsonArray.size();
                for (int index = 0; index < length; index++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(index);
                    String songMid = jsonObject.getString("songMid");
                    GLog.i("olife", "---songMid:" + songMid);
                    if (null != songMid && !songMid.isEmpty()) {
                        Music music = new Music();
                        music.setAlbumName(jsonObject.getString("albumName"));
                        music.setSingerName(jsonObject.getString("singerName"));
                        music.setSongName(jsonObject.getString("songName"));
                        music.setSongMid(songMid);
                        musics.add(music);
                    }
                }
            }
            GLog.i("olife", "---size:" + musics.size());
            return musics;
        }
    }

}
