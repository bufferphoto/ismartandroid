package com.guogu.music.http.imp;

import com.guogee.ismartandroid2.utils.GLog;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class OkHttpUtils {

	private final static String TAG = OkHttpUtils.class.getSimpleName();

	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	private static OkHttpUtils mInstance;
	private OkHttpClient mOkHttpClient;

	private OkHttpUtils() {
		okhttp3.OkHttpClient.Builder ClientBuilder = new okhttp3.OkHttpClient.Builder();
		ClientBuilder.readTimeout(30, TimeUnit.SECONDS);// 读取超时
		ClientBuilder.connectTimeout(10, TimeUnit.SECONDS);// 连接超时
		ClientBuilder.writeTimeout(60, TimeUnit.SECONDS);// 写入超时
		mOkHttpClient = ClientBuilder.build();
	}

	public static OkHttpUtils getInstance() {
		if (mInstance == null) {
			synchronized (OkHttpUtils.class) {
				if (mInstance == null) {
					mInstance = new OkHttpUtils();
				}
			}
		}
		return mInstance;
	}

	public void sendGetRequest(final String url, final Map<String, String> params, ICallback callback) {
		okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder();
		requestBuilder.url(url + setUrlParams(params));// 添加URL地址
		// requestBuilder.headers(setHeaders(headersParams));// 添加请求头
		Request request = requestBuilder.build();
		GLog.d(TAG, "get_url===" + request.url());
		Call call = mOkHttpClient.newCall(request);
		call.enqueue(new ResponseCallback(callback));
	}

	public void sendPostRequest(final String url, final Map<String, String> params, ICallback callback) {
		okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder();
		requestBuilder.url(url);// 添加URL地址
		// requestBuilder.headers(setHeaders(headersParams));// 添加请求头
		requestBuilder.post(setRequestBody(params));
		Request request = requestBuilder.build();
		GLog.d(TAG, "get_url===" + request.url());
		Call call = mOkHttpClient.newCall(request);
		call.enqueue(new ResponseCallback(callback));
	}

	/**
	 * 设置请求头
	 * 
	 * @param headersParams
	 * @return
	 */
	private Headers setHeaders(Map<String, String> headersParams) {
		Headers headers = null;
		okhttp3.Headers.Builder headersbuilder = new okhttp3.Headers.Builder();

		if (headersParams != null) {
			Iterator<String> iterator = headersParams.keySet().iterator();
			String key = "";
			while (iterator.hasNext()) {
				key = iterator.next().toString();
				headersbuilder.add(key, headersParams.get(key));
				GLog.d(TAG, "get_headers===" + key + "====" + headersParams.get(key));
			}
		}
		headers = headersbuilder.build();

		return headers;
	}

	/**
	 * post请求参数
	 * 
	 * @param BodyParams
	 * @return
	 */
	private RequestBody setRequestBody(Map<String, String> BodyParams) {
		RequestBody body = null;
		okhttp3.FormBody.Builder formEncodingBuilder = new okhttp3.FormBody.Builder();
		if (BodyParams != null) {
			Iterator<String> iterator = BodyParams.keySet().iterator();
			String key = "";
			while (iterator.hasNext()) {
				key = iterator.next().toString();
				formEncodingBuilder.add(key, BodyParams.get(key));
				GLog.d(TAG, "post_Params===" + key + "====" + BodyParams.get(key));
			}
		}
		body = formEncodingBuilder.build();
		return body;

	}

	/**
	 * get方法连接拼加参数
	 * 
	 * @param mapParams
	 * @return
	 */
	private String setUrlParams(Map<String, String> mapParams) {
		String strParams = "";
		if (mapParams != null) {
			Iterator<String> iterator = mapParams.keySet().iterator();
			String key = "";
			while (iterator.hasNext()) {
				key = iterator.next().toString();
				strParams += "&" + key + "=" + mapParams.get(key);
			}
		}
		return strParams;
	}

}
