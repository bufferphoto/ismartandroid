package com.guogu.music.http.imp;

public interface Parser<Result, Convert> {
	public Result parse(Convert data);
}
