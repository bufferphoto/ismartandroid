package com.guogu.music.http.imp;

import com.alibaba.fastjson.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ResponseCallback<T> implements Callback {

    private ICallback object;

    public ResponseCallback(ICallback object) {
        this.object = object;
    }

    @Override
    public void onFailure(Call arg0, IOException arg1) {
        object.callbackFail(ResponseError.ERROR_IOEXCEPTION, arg1.toString());
    }

    @Override
    public void onResponse(Call arg0, Response arg1) {
        if (arg1.code() == 200) {
            try {
                String result = arg1.body().string();
                JSONObject jsonObject = JSONObject.parseObject(result);
                if (null != jsonObject) {
                    if (jsonObject.getBoolean("result")) {
                        object.callbackSuccess(jsonObject.toJSONString());
                    } else {
                        object.callbackFail(jsonObject.getInteger("errno"), jsonObject.getString("err"));
                    }
                } else {
                    object.callbackFail(ResponseError.ERROR_JSONCEPTION, "");
                }
            } catch (IOException e) {
                e.printStackTrace();
                object.callbackFail(ResponseError.ERROR_IOEXCEPTION, e.toString());
            }
        } else {
            object.callbackFail(arg1.code(), arg1.message());
        }
    }

}
