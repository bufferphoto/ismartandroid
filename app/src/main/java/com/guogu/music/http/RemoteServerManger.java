package com.guogu.music.http;


import com.guogee.ismartandroid2.utils.PublishHelper;
import com.guogu.music.http.imp.ICallback;
import com.guogu.music.http.imp.OkHttpUtils;

import java.util.HashMap;
import java.util.Map;

import com.guogee.pushclient.SDKUtils;


public class RemoteServerManger {

    private OkHttpUtils okHttpManager;

    private volatile static RemoteServerManger instance;

    private RemoteServerManger() {
        okHttpManager = OkHttpUtils.getInstance();
    }

    /**
     * Double check lock 模式能够保证大部分情况下线程安全 。。但是在并发场景复杂的情况下有可能出现线程不安全
     *
     * @return
     */
//    public static RemoteServerManger getInstance() {
//        if (instance == null) {
//            synchronized (RemoteServerManger.class) {
//                if (instance == null) {
//                    instance = new RemoteServerManger();
//                }
//            }
//        }
//        return instance;
//    }
    public static RemoteServerManger getInstance() {
        return SingletonHolder.instance;
    }

    /**
     * 静态内部内更能确定单例模式的唯一性
     */
    private static class SingletonHolder {
        private final static RemoteServerManger instance = new RemoteServerManger();
    }

    /*
     * 使用音乐权限验证
	 */
    public void serviceAuthority(String vendorId, String sn, String deviceId, ICallback callback) {
        String url = PublishHelper.getDataServerUrl() + "api/sn/auth?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("vendorId", vendorId);
        params.put("SN", sn);
        params.put("deviceId", deviceId);
        params.put("type", String.valueOf(2));
        okHttpManager.getInstance().sendGetRequest(url, params, callback);
    }

    public void register(String userName, String cid, ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/register?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("userName", userName);
        params.put("cid", cid);
        okHttpManager.sendGetRequest(url, params, callback);
    }

    public void unregister(String cid, ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/unregister?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("cid", cid);
        okHttpManager.sendGetRequest(url, params, callback);
    }

    public void searchMusic(String name, ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/search?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("text", name);
        okHttpManager.sendGetRequest(url, params, callback);
    }

    public void getPlayUrl(String songId, ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/play?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("songMid", songId);
        okHttpManager.sendGetRequest(url, params, callback);
    }

    public void getType(int type, ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/category?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("type", String.valueOf(type));
        okHttpManager.sendGetRequest(url, params, callback);
    }

    public void random(ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/random";
        okHttpManager.sendGetRequest(url, null, callback);
    }

    public void hot(ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/hot";
        okHttpManager.sendGetRequest(url, null, callback);
    }

    public void addLove(String userName, String songMid, ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/addLove?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("userName", userName);
        params.put("songMid", songMid);
        okHttpManager.sendGetRequest(url, params, callback);
    }

    public void getLove(String userName, ICallback callback) {
        String url = PublishHelper.getMusicServerIP() + "api/music/getLove?";
        Map<String, String> params = new HashMap<String, String>();
        params.put("userName", userName);
        okHttpManager.sendGetRequest(url, params, callback);
    }
}
