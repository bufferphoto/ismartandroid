package com.guogu.music.play;

import com.guogu.music.model.Music;

import java.util.List;

/**
 * Created by Administrator on 2017/5/10.
 */
public interface MusicListener {
    /**
     * @param process   : 0: search 1:playurl
     * @param musicList : 返回的music数据  0得到的是没有url的数据 1：得到的是有播放了的url
     */
    public void musicResultSuccess(int process, List<Music> musicList);

    /**
     * @param process 0: search 1:playurl
     * @param error   错误码
     * @param err     错误提示
     */
    public void musicResultFail(int process, int error, String err);

}
