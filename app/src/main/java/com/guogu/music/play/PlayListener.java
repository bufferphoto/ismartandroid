package com.guogu.music.play;

/**
 * Created by Administrator on 2017/5/4.
 */
public interface PlayListener {

    /**
     * @param status   -1:播放错误 0：准备中 1：开始播放 2：播放完成
     * @param position 开始播放状态有效 position 播放的位置
     */
    public void playStatus(int status, int position);
}
