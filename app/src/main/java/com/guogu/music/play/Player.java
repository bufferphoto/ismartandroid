package com.guogu.music.play;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;

import com.guogee.ismartandroid2.utils.GLog;
import com.guogu.music.util.Base64Utils;
import com.guogu.music.util.Handler_Background;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Player {

    public final static int PLAY_ERROR = -1;
    public final static int PLAY_PREPARE = 0;
    public final static int PLAYING = 1;
    public final static int PLAY_FINISH = 2;


    private MediaPlayer mediaPlayer; // 媒体播放器
    private List<PlayListener> playListenerList;
    private Timer mTimer = new Timer(); // 计时器
    private int seq = 0;

    // 初始化播放器
    public Player() {
        super();
        try {
            playListenerList = new ArrayList<>();
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);// 设置媒体流类型
            // 每一秒触发一次
            mTimer.schedule(timerTask, 0, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registerListener(PlayListener playListener) {
        if (!playListenerList.contains(playListener)) {
            playListenerList.add(playListener);
        }
    }

    public void unregisterListener(PlayListener playListener) {
        if (!playListenerList.contains(playListener)) {
            playListenerList.remove(playListener);
        }
    }

    /**
     * @param currentPosition 当前位置
     * @param url             url地址
     */
    public void playUrl(final int currentPosition, String url) {
        try {
            GLog.i(" url:" + Base64Utils.decode(url.getBytes()));
            url = Base64Utils.decode(url.getBytes());
//            url = "http://yinyueshiting.baidu.com/data2/music/137009070/73306141493931661128.mp3?xcode=a4e264c0e63556cf2e999d51bc758b81";
            mediaPlayer.reset();
            mediaPlayer.setDataSource(url); // 设置数据源
            mediaPlayer.prepareAsync();
//            mediaPlayer.prepare(); // prepare自动播放
            playStatus(PLAY_PREPARE, 0);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    GLog.d("onPrepared");
                    mediaPlayer.seekTo(currentPosition);
                    mediaPlayer.start();
                    playStatus(PLAYING, currentPosition);
                }
            });

            mediaPlayer.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    GLog.d("percent:" + percent);
//                    playStatus(PLAYING, percent);
                }
            });

            mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    GLog.d("onCompletion");
                    playStatus(PLAY_FINISH, 0);
                }
            });

            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    GLog.d("onError");
                    playStatus(PLAY_ERROR, 0);
                    return false;
                }
            });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            playStatus(PLAY_ERROR, 0);
        } catch (SecurityException e) {
            e.printStackTrace();
            playStatus(PLAY_ERROR, 0);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            playStatus(PLAY_ERROR, 0);
        } catch (IOException e) {
            e.printStackTrace();
            playStatus(PLAY_ERROR, 0);
        }
    }

    public void seekTo(int position) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(position);
        }
    }

    public int getCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    public boolean isPlayer() {
        return mediaPlayer.isPlaying();
    }

    // 暂停
    public int pause() {
        int curIndex = -1;
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            curIndex = mediaPlayer.getCurrentPosition();
            mediaPlayer.pause();
        }

        return curIndex;
    }

    // 停止
    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    //关闭
    public void close() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (null != mTimer)
            mTimer.cancel();
    }

    private void playStatus(final int status, final int position) {
        Handler_Background.execute(new Runnable() {
            @Override
            public void run() {
                for (PlayListener playListener : playListenerList) {
                    if (null != playListener) {
                        playListener.playStatus(status, position);
                    }
                }
            }
        });
    }

    // 计时器
    private TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            if (mediaPlayer == null)
                return;
            if (mediaPlayer.isPlaying() && mediaPlayer.getCurrentPosition() > 0 && mediaPlayer.getDuration() > 0) {
                playStatus(PLAYING, mediaPlayer.getCurrentPosition());
            }
        }
    };


}
