package com.guogu.music.play;

import com.guogu.music.model.Music;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 2017/5/25.
 */
public class PlayModeManager {
    public static final int PLAY_MODEL_ORDER = 0;//顺序播放
    public static final int PLAY_MODEL_SINGER = 1;//单曲循环
    public static final int PLAY_MODEL_RANDOM = 2;//随机播放

    private volatile static PlayModeManager instance;
    private int curModel = PLAY_MODEL_ORDER;
    private List<Music> musicListOriginal;//原始数据
    private List<Music> musicListOp;//操作数据
    private int curMusicPosition = 0;//操作数据的中音乐的位置

    private PlayModeManager() {
        musicListOriginal = new ArrayList<>();
        musicListOp = new ArrayList<>();
    }

    public static PlayModeManager getInstance() {
        if (null == instance) {
            synchronized (PlayModeManager.class) {
                if (null == instance) {
                    instance = new PlayModeManager();
                }
            }
        }
        return instance;
    }

    public int getCurModel() {
        return curModel;
    }

    public void setCurModel(int model) {
        this.curModel = model;
    }

    /**
     * 打乱数据
     *
     * @param musicDataModel
     * @param musicList
     */
    public void setMusicDataModel(int musicDataModel, List<Music> musicList) {
        if (null == musicList)
            return;
        musicListOriginal.clear();
        musicListOriginal.addAll(musicList);
        switch (musicDataModel) {
            case PLAY_MODEL_RANDOM:
                musicListOp.clear();
                musicListOp.addAll(musicList);
                Collections.shuffle(musicListOp);//随机算法打乱
                curMusicPosition = 0;
                break;
            case PLAY_MODEL_SINGER:
            case PLAY_MODEL_ORDER:
            default:
                musicListOp.clear();
                musicListOp.addAll(musicList);
                break;
        }
    }

    public void updateMusic(List<Music> musicList) {
        musicListOriginal.clear();
        musicListOriginal.addAll(musicList);
    }

    public void setCurMusicPosition(String songMid) {
        for (int i = 0; i < musicListOp.size(); i++) {
            Music music = musicListOp.get(i);
            if (music.getSongMid().equalsIgnoreCase(songMid)) {
                curMusicPosition = i;
            }
        }
    }

    public Music getCurMusic() {
        return getPosition(curMusicPosition);
    }

    public Music getLastMusic() {
        if (musicListOriginal.size() == 0) {
            return null;
        }
        curMusicPosition--;
        if (curMusicPosition < 0) {
            curMusicPosition = musicListOriginal.size() - 1;
        }
        return getPosition(curMusicPosition);
    }

    public Music getNextMusic() {
        if (musicListOriginal.size() == 0) {
            return null;
        }
        curMusicPosition++;
        if (curMusicPosition >= musicListOriginal.size()) {
            curMusicPosition = 0;
        }
        return getPosition(curMusicPosition);
    }

    private Music getPosition(int curMusicPosition) {
        if (curMusicPosition >= musicListOp.size())
            return null;
        Music musicTemp = musicListOp.get(curMusicPosition);
        for (int i = 0; i < musicListOriginal.size(); i++) {
            Music music = musicListOriginal.get(i);
            if (music.getSongMid().equalsIgnoreCase(musicTemp.getSongMid())) {
                return music;
            }
        }
        return null;
    }
}
