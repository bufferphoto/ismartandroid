package com.guogu.music.play;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogu.music.http.RemoteServerManger;
import com.guogu.music.http.imp.ICallback;
import com.guogu.music.http.imp.ParserSet;
import com.guogu.music.iat.IatSpeech;
import com.guogu.music.manager.ServiceAuthorityManger;
import com.guogu.music.manager.VolumeControlManager;
import com.guogu.music.model.Music;
import com.guogu.music.util.MusicFilterSetting;
import com.guogu.music.util.Base64Utils;
import com.guogu.music.util.CommonUtil;
import com.guogu.music.util.Handler_UiThread;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/5/9.
 */
public class PlayerControl implements PlayListener {

    private volatile static PlayerControl instance;

    public static final int search = 0;
    public static final int playurl = 1;
    private static final int PLAY_MODEL_ORDER = 0;//顺序播放
    private static final int PLAY_MODEL_SINGER = 1;//单曲循环
    private static final int PLAY_MODEL_RANDOM = 2;//随机播放

    private List<Music> musics;
    private IatSpeech iatSpeech;
    private Player player;
    private int curPlayPosition = 0;
    private Context context;

    private PlayerControl(Context context) {
        this.context = context;
        iatSpeech = new IatSpeech(context);
        player = new Player();
        player.registerListener(this);
        musics = new ArrayList<Music>();
        listenerList = new ArrayList<>();
    }

    public static PlayerControl getInstance(Context context) {
        if (null == instance) {
            synchronized (PlayerControl.class) {
                if (null == instance) {
                    instance = new PlayerControl(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    public void registerListener(PlayListener playListener) {
        player.registerListener(playListener);
    }

    public void unregisterListener(PlayListener playListener) {
        player.unregisterListener(playListener);
    }

    public Player getPlayer() {
        return player;
    }

    public List<Music> getMusics() {
        return musics;
    }

    public boolean setCurMusic(String songMid) {
        PlayModeManager.getInstance().setCurMusicPosition(songMid);
        return playMusic(PlayModeManager.getInstance().getCurMusic());
    }

    public Music getCurMusic() {
        return PlayModeManager.getInstance().getCurMusic();
    }

    public void speak(String text) {
        iatSpeech.startSpeaking(text, null);
    }

    public void playMusic(String text) {
        if (!ServiceAuthorityManger.getInstance(context).authorityCheck()) {
            return;
        }
        if (null != text) {
            MusicFilterSetting.ControlKeyword controlKeyword = MusicFilterSetting.filterKeyword(text);
            GLog.i("  controlKeyword:" + controlKeyword);
            if (null != controlKeyword) {
                GLog.i("  controlKeyword.type:" + controlKeyword.type);
                switch (controlKeyword.type) {
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_LAST:
                        playLast();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_NEXT:
                        playNext();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_STOP:
                        pause();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_CONTINUE:
                        resume();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_REPLACE:
                        random();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_PLAY_HOT:
                        playHot();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_VOICE_ADD:
                        VolumeControlManager.getInstance(context).addVolume();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_VOICE_REDUCE:
                        VolumeControlManager.getInstance(context).reduceVolume();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_VOICE_MAX:
                        VolumeControlManager.getInstance(context).maxVolume();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_VOICE_MIN:
                        VolumeControlManager.getInstance(context).minVolume();
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_VOICE_VAULE:
                        String content = CommonUtil.getNumbers(controlKeyword.action);
                        if (null != content && CommonUtil.isDigit(content)) {
                            VolumeControlManager.getInstance(context).setVolume(Integer.valueOf(content));
                        }
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_PLAY_NAME:
                        Music curMusic = PlayModeManager.getInstance().getCurMusic();
                        if (null == curMusic) {
                            iatSpeech.startSpeaking("未找到数据", null);
                        } else {
                            String voice = "正在播放";
                            voice += curMusic.getSingerName() + "的" + curMusic.getSongName();
                            iatSpeech.startSpeaking(voice, null);
                        }
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_SINGER:
                        PlayModeManager.getInstance().setCurModel(PLAY_MODEL_SINGER);
                        PlayModeManager.getInstance().setMusicDataModel(PLAY_MODEL_SINGER, musics);
                        iatSpeech.startSpeaking("设置为单曲循环", null);
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_RANDOM:
                        PlayModeManager.getInstance().setCurModel(PLAY_MODEL_RANDOM);
                        PlayModeManager.getInstance().setMusicDataModel(PLAY_MODEL_RANDOM, musics);
                        iatSpeech.startSpeaking("设置为随机播放", null);
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_ORDER:
                        PlayModeManager.getInstance().setCurModel(PLAY_MODEL_ORDER);
                        PlayModeManager.getInstance().setMusicDataModel(PLAY_MODEL_ORDER, musics);
                        iatSpeech.startSpeaking("设置为顺序播放", null);
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_ADD_LOVE:
                        SharedPreferences sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE); //私有数据
                        String userName = sharedPreferences.getString("userName", "");
                        if (!userName.isEmpty()) {
                            Music music = PlayModeManager.getInstance().getCurMusic();
                            if (null == music) {
                                Toast.makeText(context.getApplicationContext(), "没有获取到当前歌曲", Toast.LENGTH_SHORT).show();
                            } else {
                                addLove(userName, music);
                            }
                        } else {
                            Toast.makeText(context.getApplicationContext(), "请先注册", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_PLAY_LOVE:
                        SharedPreferences sharedPreferences1 = context.getSharedPreferences("user", Context.MODE_PRIVATE); //私有数据
                        String userName1 = sharedPreferences1.getString("userName", "");
                        if (!userName1.isEmpty()) {
                            getLove(userName1);
                        } else {
                            Toast.makeText(context.getApplicationContext(), "请先注册", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case MusicFilterSetting.ACTION_TYPE_CONTROL_PLAY_MUSIC://针对播放音乐这个词做特殊处理
                        if (player.isPlayer()) {//正在播放就换一首
                            random();
                        } else {
                            Music music = PlayModeManager.getInstance().getCurMusic();
                            if (null != music && null != music.getUrl()) {//继续播放
                                player.playUrl(curPlayPosition, music.getUrl());
                            } else {
                                random();
                            }
                        }
                        break;
                    case MusicFilterSetting.ACTION_TYPE_PLAY:
                    default:
                        searchMusics(controlKeyword.action);
                        break;
                }
            }
        }
    }

    public void pause() {
        if (player.isPlayer())
            curPlayPosition = player.pause();
        callback();
    }

    public void resume() {
        Music curMusic = PlayModeManager.getInstance().getCurMusic();
        if (!player.isPlayer() && null != curMusic && null != curMusic.getUrl()) {
            player.playUrl(curPlayPosition, curMusic.getUrl());
        }
    }

    public void playLast() {
        playMusic(PlayModeManager.getInstance().getLastMusic());
    }

    public void playNext() {
        playMusic(PlayModeManager.getInstance().getNextMusic());
    }

    public void playSinger() {
        playMusic(PlayModeManager.getInstance().getCurMusic());
    }

    private boolean playMusic(Music music) {
        if (null == music)
            return false;

        GLog.i("   music.getUrl():" + music.getUrl());
        if (null != music.getUrl() && !music.getUrl().isEmpty() && music.getPlayStatus() == 1) {
            curPlayPosition = 0;
            player.playUrl(curPlayPosition, music.getUrl());
        } else {
            getPlayUri(music);
        }
        return true;
    }

    private void addLove(String userName, Music music) {
        if (null == music)
            return;
        RemoteServerManger.getInstance().addLove(userName, music.getSongMid(), new ICallback() {
            @Override
            public void callbackSuccess(String jsonString) {
                String voice = "成功添加到喜欢列表";
                iatSpeech.startSpeaking(voice, null);
            }

            @Override
            public void callbackFail(int code, String err) {
                iatSpeech.startSpeaking(err, null);
            }
        });
    }

    private void getLove(String userName) {
        if (!ServiceAuthorityManger.getInstance(context).authorityCheck()) {
            return;
        }
        RemoteServerManger.getInstance().getLove(userName, new ICallback() {
            @Override
            public void callbackSuccess(String jsonString) {
                musics = new ParserSet.MusicParser().parse(jsonString);
                musicCallbackSuccess(search, musics);
                if (musics.size() > 0) {
                    PlayModeManager.getInstance().setMusicDataModel(PLAY_MODEL_RANDOM, musics);
//                    PlayModeManager.getInstance().setCurMusicPosition(musics.get(0).getSongMid());
                    playMusic(PlayModeManager.getInstance().getCurMusic());
                }
            }

            @Override
            public void callbackFail(int code, String err) {
                musicCallbackFail(search, code, err);
                Handler_UiThread.runTask("ui", new Runnable() {
                    @Override
                    public void run() {
                        iatSpeech.startSpeaking("未找到数据", null);
                    }
                }, 0);
            }
        });
    }

    private void random() {
        if (!ServiceAuthorityManger.getInstance(context).authorityCheck()) {
            return;
        }
        RemoteServerManger.getInstance().random(new ICallback() {
            @Override
            public void callbackSuccess(String jsonString) {
                musics = new ParserSet.MusicParser().parse(jsonString);
                musicCallbackSuccess(search, musics);
                if (musics.size() > 0) {
                    PlayModeManager.getInstance().setMusicDataModel(PLAY_MODEL_RANDOM, musics);
                    playMusic(PlayModeManager.getInstance().getCurMusic());
                }
            }

            @Override
            public void callbackFail(int code, String err) {
                musicCallbackFail(search, code, err);
                Handler_UiThread.runTask("ui", new Runnable() {
                    @Override
                    public void run() {
                        iatSpeech.startSpeaking("未找到数据", null);
                    }
                }, 0);
            }
        });
    }

    private void playHot() {
        if (!ServiceAuthorityManger.getInstance(context).authorityCheck()) {
            return;
        }
        RemoteServerManger.getInstance().hot(new ICallback() {
            @Override
            public void callbackSuccess(String jsonString) {
                musics = new ParserSet.MusicParser().parse(jsonString);
                musicCallbackSuccess(search, musics);
                if (musics.size() > 0) {
                    PlayModeManager.getInstance().setMusicDataModel(PLAY_MODEL_RANDOM, musics);
                    playMusic(PlayModeManager.getInstance().getCurMusic());
                }
            }

            @Override
            public void callbackFail(int code, String err) {
                musicCallbackFail(search, code, err);
                Handler_UiThread.runTask("ui", new Runnable() {
                    @Override
                    public void run() {
                        iatSpeech.startSpeaking("未找到数据", null);
                    }
                }, 0);
            }
        });
    }

    public void searchMusics(final String text) {
        if (!ServiceAuthorityManger.getInstance(context).authorityCheck()) {
            return;
        }
        RemoteServerManger.getInstance().searchMusic(text, new ICallback() {
            @Override
            public void callbackSuccess(String jsonString) {
                musics = new ParserSet.MusicParser().parse(jsonString);
                musicCallbackSuccess(search, musics);
                if (musics.size() > 0) {
                    if (text.contains(musics.get(0).getSongName()) || text.contains(musics.get(0).getSingerName())
                            || musics.get(0).getSongName().contains(text) || musics.get(0).getSongName().contains(text)) {
                        PlayModeManager.getInstance().setMusicDataModel(PLAY_MODEL_ORDER, musics);
                        PlayModeManager.getInstance().setCurMusicPosition(musics.get(0).getSongMid());
                    } else {
                        PlayModeManager.getInstance().setMusicDataModel(PLAY_MODEL_RANDOM, musics);
                    }
                    playMusic(PlayModeManager.getInstance().getCurMusic());
                }
            }

            @Override
            public void callbackFail(int code, final String err) {
                musicCallbackFail(search, code, err);
                Handler_UiThread.runTask("ui", new Runnable() {
                    @Override
                    public void run() {
                        iatSpeech.startSpeaking("未找到数据", null);
                    }
                }, 0);
            }
        });
    }

    private void getPlayUri(final Music music) {
        RemoteServerManger.getInstance().getPlayUrl(music.getSongMid(), new ICallback() {
            @Override
            public void callbackSuccess(String jsonString) {
                GLog.i("url time:" + JSONObject.parseObject(jsonString).getString("time"));
                JSONObject jsonObject = JSONObject.parseObject(jsonString).getJSONObject("value");
                final int playStatus = jsonObject.getIntValue("playStatus");
                String url = jsonObject.getString("url");
                String pictureUrl= jsonObject.getString("songPicture");
                GLog.i("  playStatus:" + playStatus);
                GLog.i(" get url:" + Base64Utils.decode(url.getBytes()));
                music.setPlayStatus(playStatus);
                music.setUrl(url);
                music.setPictureUrl(pictureUrl);
                if (playStatus == 0) {
                    playNext();
                } else {
                    playMusic(music);
                }
                PlayModeManager.getInstance().updateMusic(musics);
                musicCallbackSuccess(playurl, musics);
            }

            @Override
            public void callbackFail(int code, final String err) {
                GLog.i("   geturl    err:" + err);
                musicCallbackFail(playurl, code, err);
//                Handler_UiThread.runTask("ui", new Runnable() {
//                    @Override
//                    public void run() {
//                        iatSpeech.startSpeaking("未找到数据", null);
//                    }
//                }, 0);
                playNext();
            }
        });
    }

    @Override
    public void playStatus(final int status, final int position) {
        GLog.i("  status:" + status + "   position:" + position);
        Handler_UiThread.runTask("播报", new Runnable() {
            @Override
            public void run() {
                switch (status) {
                    case Player.PLAY_PREPARE:
                        GLog.i(" 准备播放  ");
//                        Music music = PlayModeManager.getInstance().getCurMusic();
//                        String text = "准备播放";
//                        if (curPlayPosition > 0) {
//                            text = "继续播放";
//                        }
//                        text += music.getSingerName() + "的" + music.getSongName();
//                        iatSpeech.startSpeaking(text, null);
                        break;
                    case Player.PLAYING:
                        break;
                    case Player.PLAY_FINISH:
                        GLog.i(" curMode:" + PlayModeManager.getInstance().getCurModel());
                        if (PlayModeManager.getInstance().getCurModel() == PlayModeManager.PLAY_MODEL_SINGER) {
                            playSinger();
                        } else {
                            playNext();
                        }
                        break;
                    case Player.PLAY_ERROR:
                    default:
                        playNext();
                        break;
                }
            }
        }, 0);
    }

    public void destory() {
        if (player != null) {
            player.close();
            player = null;
        }
        if (null != iatSpeech) {
            iatSpeech.destory();
            iatSpeech = null;
        }
    }

    private MusicListener musicListener = null;

    public void setMusicListener(MusicListener musicListener) {
        this.musicListener = musicListener;
    }

    private void musicCallbackSuccess(int process, List<Music> musicList) {
        if (null != musicListener)
            musicListener.musicResultSuccess(process, musicList);
    }

    private void musicCallbackFail(int process, int error, String err) {
        if (null != musicListener)
            musicListener.musicResultFail(process, error, err);
    }

    private List<PauseReusltListener> listenerList;

    private void callback() {
        for (int i = 0; i < listenerList.size(); i++) {
            listenerList.get(i).pauseResult();
        }
    }

    public void registerPauseListener(PauseReusltListener listener) {
        if (!listenerList.contains(listener))
            listenerList.add(listener);
    }

    public void unregisterPauseListener(PauseReusltListener listener) {
        if (listenerList.contains(listener))
            listenerList.remove(listener);
    }

    public interface PauseReusltListener {
        public void pauseResult();
    }

}
