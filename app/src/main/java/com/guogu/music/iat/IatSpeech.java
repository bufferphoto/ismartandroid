package com.guogu.music.iat;

import android.content.Context;
import android.os.Environment;

import com.example.mt.mediaplay.R;
import com.guogee.ismartandroid2.utils.GLog;
import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.SynthesizerListener;

public class IatSpeech {

	private Context context;
	// 语音听写对象（语音变文字）
	private SpeechRecognizer mIat;
	// 语音合成对象（文字变语音）
	private SpeechSynthesizer mTts;

	public IatSpeech(Context context) {
		this.context = context;
		initIflyTek();
	}

	/*
	 * 开始监听说话
	 * 
	 * return: 返回值ErrorCode.SUCCESS(0)成功 其他失败
	 * 
	 */
	public int startListening(RecognizerListener recognizer) {
		if (isSpeaking()) {
			stopSpeaking();
		}
		return mIat.startListening(recognizer);
	}

	public boolean isListening() {
		return mIat.isListening();
	}

	/*
	 * 停止监听说话
	 */
	public void stopListening() {
		mIat.stopListening();
	}

	/*
	 * 语音合成 开始播报
	 * 
	 * return: 返回值ErrorCode.SUCCESS(0)成功 其他失败
	 */
	public int startSpeaking(String text, SynthesizerListener synthesizerlistener) {
		// stopListening();
		return mTts.startSpeaking(text, synthesizerlistener);
	}

	/*
	 * 语音合成 是否存在播报
	 */
	public boolean isSpeaking() {
		return mTts.isSpeaking();
	}

	/*
	 * 语音合成 暂停播报
	 */
	public void pauseSpeaking() {
		mTts.pauseSpeaking();
	}

	/*
	 * 语音合成 恢复播报
	 */
	public void resumeSpeaking() {
		mTts.resumeSpeaking();
	}

	/*
	 * 语音合成 停止播报
	 */
	public void stopSpeaking() {
		mTts.stopSpeaking();
	}

	/*
	 * 销毁对象
	 */
	public void destory() {
		mIat.cancel();
		mIat.destroy();
		mTts.stopSpeaking();
		mTts.destroy();
	}

	private void initIflyTek() {// 初始化讯飞
		// 设置你申请的应用appid
		SpeechUtility.createUtility(context, "appid=" + context.getString(R.string.app_id));
		mIat = SpeechRecognizer.createRecognizer(context, mInitRecognizerListener);
		mTts = SpeechSynthesizer.createSynthesizer(context, mInitSynthesizerListener);
		setParam();
	}

	/**
	 * 参数设置
	 *
	 * @return
	 */
	@SuppressWarnings("SdCardPath")
	public void setParam() {
		String locale = context.getResources().getConfiguration().locale.getCountry();
		if (locale.equals("CN")) {
			mIat.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
			mIat.setParameter(SpeechConstant.ACCENT, "mandarin");
			mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");
		} else {
			mIat.setParameter(SpeechConstant.LANGUAGE, "en_us");
			mTts.setParameter(SpeechConstant.VOICE_NAME, "Catherine");
		}

		mIat.setParameter(SpeechConstant.VAD_BOS, "4000");
		mIat.setParameter(SpeechConstant.VAD_EOS, "1000");
		mIat.setParameter(SpeechConstant.ASR_PTT, "1");
		mIat.setParameter(SpeechConstant.ASR_AUDIO_PATH, Environment.getExternalStorageDirectory() + "/msc/iat.wav");

		mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);

		mTts.setParameter(SpeechConstant.SPEED, "50");
		mTts.setParameter(SpeechConstant.PITCH, "50");
		mTts.setParameter(SpeechConstant.VOLUME, "50");
		mTts.setParameter(SpeechConstant.STREAM_TYPE, "3");
	}

	/**
	 * 初始化监听器。
	 */
	private InitListener mInitRecognizerListener = new InitListener() {
		@Override
		public void onInit(int code) {
			GLog.d("tag", "SpeechRecognizer init() code = " + code);
			if (code == ErrorCode.SUCCESS) {

			}
		}
	};

	private InitListener mInitSynthesizerListener = new InitListener() {
		@Override
		public void onInit(int code) {
			GLog.d("tag", "Synthesizer init() code = " + code);
			if (code != ErrorCode.SUCCESS) {
				// 初始化失败,错误码 code
			} else {
				// 初始化成功，之后可以调用startSpeaking方法
				// 注：有的开发者在onCreate方法中创建完合成对象之后马上就调用startSpeaking进行合成，
				// 正确的做法是将onCreate中的startSpeaking调用移至这里
			}
		}
	};

}
