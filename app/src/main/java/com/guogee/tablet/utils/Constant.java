package com.guogee.tablet.utils;

import android.content.Context;
import android.widget.Toast;


import com.example.mt.mediaplay.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constant {

	/**
	 * @author Smither
	 * @date 2014/12/9
	 */
	public static final int CAMERA_CONNECTING = 0x1;
	public static final int CAMERA_CONNECTED_FAILED = 0x2;
	public static final int CAMERA_CONNECTED = 0x3;
	public static final int CAMERA_INITIALING = 0x4;
	public static final int CAMERA_INVALID_ID = 0x5;
	public static final int CAMERA_ON_LINE = 0x6;
	public static final int CAMERA_NOT_ON_LINE = 0x7;
	public static final int CAMERA_CONNECT_TIMEOUT = 0x8;
	public static final int CAMERA_WRONG_PASSWORD = 0x9;
	public static final int CAMERA_UNKNOW = 0xA;
	public static final int CAMERA_DISCONNECTED = 0xB;
	public static final int CAMERA_UNSUPPORT = 0xC;

	
	public static final int RESET_CAMERA_PASSWORD_SUCCESS = 1;
	public static final int RESET_CAMERA_PASSWORD_FEILED = 2;
	public static final int RESET_CAMERA_PASSWORD_SUCCESS_WITHOUT_REBOOT = 3;
	public final static int UP = 0;
	public final static int DOWN = 1;
	public final static int LEFT = 2;
	public final static int RIGHT = 3;
	

	

	
	public static final String[] airTagStr = {"18","20","22","100"};  // 0-"锟斤拷", 1-"锟斤拷", 2-"18 C",3-"20 C", 4-"22 C"
	public static final String[] tvTagStr = {"100","100","101"}; // 0-"锟斤拷", 1-"锟斤拷", 2-"锟斤拷锟斤拷"
	public static final String[] tvBoxTagStr = {"100","100","101"}; // 0-"锟斤拷", 1-"锟斤拷", 2-"锟斤拷锟斤拷"
	public static final String[] dvdTagStr = {"100","100","101"}; // 0-"锟斤拷", 1-"锟斤拷", 2-"锟斤拷锟斤拷"
	public static final String[] fanTagStr = {"100","100"};



	public static final int BORADCAST_VOICE_BOX_PORT = 3300;
	
	public static final String GATEWAY_STATUS_CHANGE_ACTION = "com.guogee.gateway.statuschange";
	public static final String GATEWAY_FOUND_ACTION = "com.guogee.gateway.found";
	public static final String UI_SCENE_CHANGE_ACTION = "com.guogee.ui.scenechange";
	public final static String ACTION_KEEP_ALIVE = "com.guogee.keepalive";
	public static final String ACTION_PASSWORD_CHANGE = "com.guogee.user.passwordchange";


	/**
	 * 错误代码
	 * Smither 2015/1/4
	 */
	
	public static final int USER_EXIST = 100001;
	public static final int EMAIL_EXIST = 100002;
	public static final int MOBILE_EXIST = 100005;
	public static final int PASSWORD_USERNAME_WRONG = 100006;
	public static final int VERIFICATION_ERROR = 100009;
	public static final int SEND_VERIFICATION_CODE_FAILED = 100011;
	public static final int USER_NOT_EXIST = 100012;
	public static final int EMAIL_NOT_EXIST = 100013;
	public static final int CELLPHONE_NOT_EXIST = 100014;
	public static final int JAVA_ERROR = 100010;//服务器内部错误
	public static final int VERIFICATION_CODE_OVERTIME = 100007;
	/**
	 * Smither 2015/1/15 套餐错误代码
	 */
	public static final int PACKAGE_NOT_EXIST= 100001;
	public static final int PACKAGE_PWD_WRONG = 100002;
	public static final int PACKAGE_SET_REALITION_FAILED = 100003;
	public static final int NOT_LOGIN = 100004;


	
	
	
	public static boolean isChineseDeviceName(String str){
      //  String reg = "[\u4e00-\u9fa5]*[0-9]*";
     //   return Pattern.compile(reg).matcher(str).find();
        boolean result=false;
        str=str.toLowerCase();
        Pattern pattern = Pattern.compile("(dvd|vcd|[\u4e00-\u9fa50-9]*)"); // [\u4e00-\u9fa5]*[0-9]*
        Matcher isNum = pattern.matcher(str);
        if(isNum.matches()){
        	result=true;
        }
        return  result;
    }
	
	
	public static boolean isEnglishDeviceName(String str){
	      //  String reg = "[\u4e00-\u9fa5]*[0-9]*";
	     //   return Pattern.compile(reg).matcher(str).find();
	        boolean result=false;
	        Pattern pattern = Pattern.compile("[A-Za-z ]*[0-9]*");
	        Matcher isNum = pattern.matcher(str);
	        if(isNum.matches()){
	        	result=true;
	        }
	        return  result;
	    }
	
	
	public static boolean checkDeviceName(Context context, String str){
		boolean result=false;
		String local = context.getResources().getConfiguration().locale
				.getCountry();
		if(local.equals("CN")){
			result=isChineseDeviceName(str);
			if(!result){
				Toast.makeText(context, R.string.device_name_rule_cn, Toast.LENGTH_LONG).show();
			}
		}else{
			result=isEnglishDeviceName(str);
			if(!result){
				Toast.makeText(context,R.string.device_name_rule_en, Toast.LENGTH_LONG).show();
			}
		}
		
		if(NumberUtil.isNumber(str)){
			Toast.makeText(context,R.string.no_number, Toast.LENGTH_LONG).show();
			result = false;
		}
		return result;
	}
	
	/**
	  *锟街伙拷锟斤拷锟斤拷锟斤拷
	  * @author sky
	  * @param mobiles
	  * @return
	  */
	 public static boolean isMobileNO(String mobiles){
	  boolean flag = false;
	  try{
	   Pattern p = Pattern.compile("^((13[0-9])|(15[0-9])|(18[0-9]))\\d{8}$");
	   Matcher m = p.matcher(mobiles);
	   flag = m.matches();
	  }catch(Exception e){
	   flag = false;
	  }
	  return flag;
	 }	
	 
	
	 
}
