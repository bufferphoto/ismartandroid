/**
 *
 */
package com.guogee.tablet.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.guogee.ismartandroid2.manager.HttpManager;
import com.guogee.ismartandroid2.utils.GLog;
import com.example.mt.mediaplay.R;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;

/**
 * @author xieguangwei
 * @ClassName: SystemUtil.java
 * @Description: TODO
 * @date 2015年7月27日
 */
public class SystemUtil {

    private static final String TAG = "SystemUtil";

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }


    /**
     * 通过图片名字获取res id
     *
     * @param name
     * @return
     */
    public static int getDrawableId(String name) {
        int resID = 0;
        try {
            Class<R.drawable> res = R.drawable.class;
            Field field = res.getField(name);
            resID = field.getInt(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resID;
    }

    public static String getStringByName(Context ctx, String name) {
        if (null != name && name.length() > 1) {
            if (NumberUtil.isNumber(name.substring(0, 1))) {
                name = "_" + name;
            }
        }
        String packageName = ctx.getPackageName();
        int resId = ctx.getResources()
                .getIdentifier(name, "string", packageName);
        if (resId == 0) {
            return name;
        } else {
            return ctx.getString(resId);
        }
    }


    public static void turnOnGPS(Context context) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(intent);
    }

    public static boolean isChineseLanguage(Context context) {
        Locale locale = context.getResources().getConfiguration().locale;
        String language = locale.getLanguage();
        if (language.endsWith("zh"))
            return true;
        else
            return false;
    }

    /**
     * 跳转到WIFI设置页面
     */
    public static void turnOnWIFI(Context context) {
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        context.startActivity(intent);
    }

    public static void toast(final Activity a, final int msg, final int time) {
        a.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(a, msg, time).show();
            }
        });
    }

    public static void toast(final Activity a, final String msg, final int time) {
        a.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(a, msg, time).show();
            }
        });
    }

    /**
     * 安装apk
     *
     * @param ctx
     * @param path 安装包路径
     */
    public static void installApp(Context ctx, String path) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.fromFile(new File(path)),
                "application/vnd.android.package-archive");
        ctx.startActivity(intent);
    }


    /**
     * 发送即时通知
     *
     * @param context 上下文
     * @param pIntent 数据
     * @param title   标题
     * @param text    通知正文
     */
    public static void sendNotification(Context context, PendingIntent pIntent, String title, String text) {
        sendNotification(context, pIntent, title, text, 0);
    }

    /**
     * 发送即时通知
     *
     * @param context 上下文
     * @param pIntent 数据
     * @param title   标题
     * @param text    通知正文
     * @param soundId 通知铃声.自定义
     */
    public static void sendNotification(Context context, PendingIntent pIntent, String title, String text, int soundId) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Notification.Builder builder = new Notification.Builder(context).setTicker(text)//显示于屏幕顶端状态栏的文本
                    .setSmallIcon(R.drawable.ic_launcher);
            builder.setPriority(Notification.PRIORITY_MAX);
            builder.setContentTitle(title);
            builder.setContentText(text);
            builder.setContentIntent(pIntent);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder.setCategory(Notification.CATEGORY_MESSAGE);
                builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);//在任何情况下都显示，不受锁屏影响。
            }
            notification = builder.build();
        } else {
            notification = new Notification();
            notification.icon = R.drawable.ic_launcher;// 设置通知的图标
            notification.tickerText = title; // 显示在状态栏中的文字
            // 设置通知的标题和内容
            notification.setLatestEventInfo(context, title, text, pIntent);
            notification.contentIntent = pIntent;
        }
        long when = System.currentTimeMillis();
        notification.when = when; // 设置来通知时的时间
        notification.flags |= Notification.FLAG_AUTO_CANCEL; // 点击清除按钮或点击通知后会自动消失
        if (soundId > 0) {
            notification.sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + soundId);
        } else {
            notification.defaults = Notification.DEFAULT_ALL; // 把所有的属性设置成默认
        }

        // 发出通知
        notificationManager.notify((int) when, notification);
    }

    /**
     * 是否在前台运行
     *
     * @param context
     * @return
     */
    public static boolean isRunningForeground(Context context) {

//		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//		List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
//		for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
//			if (appProcess.processName.equals(context.getPackageName())) {
//				if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
//					//GLog.i("后台", appProcess.processName);
//					return false;
//				}else{
//					//GLog.i("前台", appProcess.processName);
//					return true;
//				}
//			}
//		}
//		return false;

        //需要添加权限
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (tasks != null && !tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            GLog.i(TAG, "top activity:" + topActivity.getClassName());
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            boolean isScreenOn = pm.isScreenOn();//如果为true，则表示屏幕“亮”了，否则屏幕“暗”了。
            GLog.i(TAG, "isScreenOn:" + isScreenOn);
            if (topActivity.getPackageName().equals(context.getPackageName()) && isScreenOn) {
                return true;
            }
        }
        return false;
    }


    public static int getAppVersionCode(Context context) {
        PackageManager pm = context.getPackageManager();
        int code = 0;
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            code = pi.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return code;
    }

    public static String getAppVersionName(Context context) {
        PackageManager pm = context.getPackageManager();
        String name = "";
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            name = pi.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return name;
    }


    /**
     * 是否是Debug模式
     *
     * @param context
     * @return
     */
    public static boolean isDebugable(Context context) {
        try {
            ApplicationInfo info = context.getApplicationInfo();
            return (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取进程名称
     *
     * @param context
     * @param pid
     * @return
     */
    public static String getProcessName(Context context, int pid) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps != null && !runningApps.isEmpty()) {
            for (ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
                if (procInfo.pid == pid) {
                    return procInfo.processName;
                }
            }
        }
        return null;
    }

    public static String getWifiSSID(Context context) {
        String ssid = null;
        boolean isConnected = isWifiConnected(context);
        if (isConnected) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo != null)
                ssid = wifiInfo.getSSID();
        }
        if (null != ssid) {
            ssid = ssid.replace("\"", "");
        }
        return ssid;
    }

    public static boolean isWifiConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetworkInfo.isConnected()) {
            return true;
        }

        return false;
    }

    public static void errorReminder(Activity context, JSONObject jo) {
        switch (jo.getIntValue("errno")) {
            case HttpManager.ERROR_NOT_LOGIN:
                SystemUtil.toast(context, R.string.relogin, Toast.LENGTH_SHORT);
                com.guogee.tablet.manager.ActivityManager.gotoActivity(context, com.guogee.tablet.manager.ActivityManager.ACTIVITY_LOGIN, null);
                break;
            default:
                SystemUtil.toast(context, jo.getString("err"), Toast.LENGTH_SHORT);
                break;
        }
    }

}
