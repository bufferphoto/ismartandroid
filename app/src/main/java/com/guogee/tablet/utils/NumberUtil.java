/**
 * 
 */
package com.guogee.tablet.utils;

import java.util.regex.Pattern;

/**
 *@ClassName:     NumberUtil.java
 * @Description:   TODO
 * @author xieguangwei
 * @date 2015年12月5日
 * 
 */
public class NumberUtil
{
	public static boolean isNumber(String data){
		
		Pattern pattern = Pattern.compile("^[0-9一二三四五六七八九壹贰叁肆伍陆柒捌玖十百千拾佰仟万亿○Ｏ零]+$");
	    return pattern.matcher(data).matches();  
		
	}
}
