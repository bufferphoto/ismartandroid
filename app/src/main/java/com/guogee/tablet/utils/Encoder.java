package com.guogee.tablet.utils;

import android.util.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class Encoder {
	/**
	     * <p>将文件转成base64 字符串</p>
	     * @param path 文件路径
	     * @return
	     * @throws Exception
	     */
	    public static String encodeBase64File(String path) throws Exception {
	        File  file = new File(path);
	        FileInputStream inputFile = new FileInputStream(file);
	        byte[] buffer = new byte[(int)file.length()];
	        inputFile.read(buffer);
	        inputFile.close();
	        return Base64.encodeToString(buffer,Base64.DEFAULT);
	    }
	    /**
	     * <p>将base64字符解码保存文件</p>
	     * @param base64Code
	     * @param targetPath
	     * @throws Exception
	     */
	    public static void decoderBase64File(String base64Code,String targetPath) throws Exception {
	     //   Log.e("dd2", "dd=>"+base64Code);
	        byte[] buffer = Base64.decode(base64Code,Base64.DEFAULT);
	        FileOutputStream out = new FileOutputStream(targetPath);
	        out.write(buffer);
	        out.close();
	    }
	    /**
	     * <p>将base64字符保存文本文件</p>
	     * @param base64Code
	     * @param targetPath
	     * @throws Exception
	     */
	    public static void toFile(String base64Code,String targetPath) throws Exception {
	        byte[] buffer = base64Code.getBytes();
	        FileOutputStream out = new FileOutputStream(targetPath);
	        out.write(buffer);
	        out.close();
	    }




	private static byte[] iv = { 1, 2, 3, 4, 5, 6, 7, 8 };
	private static String encryptKey = "21150915";
	/**
	 * DES加密
	 * @param encryptString
	 * @return
	 * @throws Exception
	 */
	public static String encryptDES(String encryptString)
		throws Exception {
		IvParameterSpec zeroIv = new IvParameterSpec(iv);
		SecretKeySpec key = new SecretKeySpec(encryptKey.getBytes(), "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
		byte[] encryptedData = cipher.doFinal(encryptString.getBytes());
		return Base64.encodeToString(encryptedData, Base64.DEFAULT);
	}

	/**
	 * DES 解密
	 * @param decryptString
	 * @return
	 * @throws Exception
	 */
	public static String decryptDES(String decryptString)
			throws Exception {
		byte[] byteMi = Base64.decode(decryptString,Base64.DEFAULT);
		IvParameterSpec zeroIv = new IvParameterSpec(iv);
		SecretKeySpec key = new SecretKeySpec(encryptKey.getBytes(), "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
		byte decryptedData[] = cipher.doFinal(byteMi);

		return new String(decryptedData);
	}

}
