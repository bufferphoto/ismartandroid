package com.guogee.tablet.utils;

import android.content.Context;
import android.content.SharedPreferences;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by mt on 2017/6/1.
 */

public class ListDataSave {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public ListDataSave(Context mContext, String preferenceName) {
        preferences = mContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    /**
     * 保存List
     *
     * @param tag
     * @param datalist
     */
    public <T> void setDataList(String tag, List<T> datalist) {
        if (null == datalist || datalist.size() <= 0)
            return;

        Gson gson = new Gson();
        //转换成json数据，再保存
        String strJson = gson.toJson(datalist);
        editor.clear();
        editor.putString(tag, strJson);
        editor.commit();

    }

    /**
     * 获取List
     *
     * @param tag
     * @return
     */
    public <T> List<T> getDataList(String tag) {
        List<T> datalist = new ArrayList<T>();
        String strJson = preferences.getString(tag, null);
        if (null == strJson) {
            return datalist;
        }
        Gson gson = new Gson();
        datalist = gson.fromJson(strJson, new TypeToken<List<T>>() {
        }.getType());
        return datalist;

    }


    public void saveInfo(String key,
                                Map<String, List<Integer>> datas) {
        JSONArray mJsonArray = new JSONArray();
        Iterator<Map.Entry<String, List<Integer>>> iterator = datas.entrySet()
                .iterator();
        JSONObject object = new JSONObject();
        while (iterator.hasNext()) {
            Map.Entry<String, List<Integer>> entry = iterator.next();
            List<Integer> value = entry.getValue();
            JSONArray array = new JSONArray();
            for (int j = 0; j < value.size(); j++) {
                array.put(value.get(j));
            }
            try {
                object.put(entry.getKey(), array);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        mJsonArray.put(object);

        editor.putString(key, mJsonArray.toString());
        editor.commit();
    }

    public Map<String, List<Integer>> getInfo(String key) {
        Map<String, List<Integer>> datas = new HashMap<String, List<Integer>>();
        String result = preferences.getString(key, null);
        if (null == result) {
            return datas;
        }
        System.out.println("result" + result);
        try {
            JSONArray jsonArray = new JSONArray(result);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONArray names = jsonObject.names();
                if (names != null) {
                    for (int j = 0; j < names.length(); j++) {
                        String name = names.getString(j);
                        JSONArray jsonArray2 = jsonObject.getJSONArray(name);
                        List<Integer> list2 = new ArrayList<Integer>();
                        for (int k = 0; k < jsonArray2.length(); k++) {
                            int s = jsonArray2.getInt(k);
                            list2.add(s);
                        }
                        datas.put(name, list2);
                    }
                    System.out.println("=====" + datas.toString());
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return datas;
    }
}
