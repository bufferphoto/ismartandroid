package com.guogee.tablet.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.mining.app.zxing.util.QRCodeUtil;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.DiscCacheUtil;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

/**
 * @author yuming 图片下载工具类
 */
public class ImageLoaderUtil {
    /**
     * 初始化ImageLoader
     *
     * @param context
     * @param dir
     */
    public static void initImageLoader(Context context, String dir) {
        // 创建缓存的目录
        File cacheDir = StorageUtils.getOwnCacheDirectory(context, dir);

        // 默认配置
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisc(false).build();
        // 初始化ImageLoader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).memoryCache(new LruMemoryCache(3 * 1024 * 1024)).memoryCacheSize(3 * 1024 * 1024)
                .discCacheSize(10 * 1024 * 1024).discCacheFileCount(100).discCache(new UnlimitedDiscCache(cacheDir)).threadPriority(Thread.NORM_PRIORITY - 2)
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();

        ImageLoader.getInstance().init(config);
    }

    public static DisplayImageOptions getImageOption(int resId) {
        // DisplayImageOptions options = new
        // DisplayImageOptions.Builder().showStubImage(resId).showImageForEmptyUri(resId).showImageOnFail(resId).cacheInMemory(true).cacheOnDisc(true)
        // .bitmapConfig(Bitmap.Config.RGB_565).imageScaleType(ImageScaleType.EXACTLY_STRETCHED).build();
        DisplayImageOptions options = new DisplayImageOptions.Builder().showStubImage(resId).showImageForEmptyUri(resId).showImageOnFail(resId).cacheInMemory(true).cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565).imageScaleType(ImageScaleType.IN_SAMPLE_INT).build();
        return options;
    }

    public static DisplayImageOptions getImageOptionBig(int resId) {
        DisplayImageOptions options = new DisplayImageOptions.Builder().showStubImage(resId).showImageForEmptyUri(resId).showImageOnFail(resId).cacheInMemory(true).cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565).imageScaleType(ImageScaleType.NONE).build();
        return options;
    }

    public static Bitmap getDiscCacheImage(String uri) {//这里的uri一般就是图片网址
        File file = DiscCacheUtil.findInCache(uri, ImageLoader.getInstance().getDiscCache());
        try {
            String path = file.getPath();
            return BitmapFactory.decodeFile(path);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    public static boolean savePicture(Context context, String path) {
        Bitmap bitmap = ImageLoaderUtil.getDiscCacheImage(path);
        if (null != bitmap) {
            final String fileName = "qr_" + System.currentTimeMillis() + ".jpg";
            QRCodeUtil.savePicture(context, bitmap, fileName);
            return true;
        }
        return false;
    }
}
