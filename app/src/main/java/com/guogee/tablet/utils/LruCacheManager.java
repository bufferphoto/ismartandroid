/**
 * 
 */
package com.guogee.tablet.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.util.LruCache;

import com.guogee.ismartandroid2.utils.GLog;


/**
 *@ClassName:     LruCacheManager.java
 * @Description:   TODO
 * @author xieguangwei
 * @date 2015年7月13日
 * 
 */
public class LruCacheManager {

	private static final String TAG = "LruCacheManager";
	private LruCache<String,Bitmap> mLruCache ;
	private Context mContext;
	private int mTotalSize = (int) Runtime.getRuntime().totalMemory();

	private volatile static LruCacheManager mInatance ;

	public static LruCacheManager getInstance(Context ctx){
		if (mInatance == null) {
			synchronized (LruCacheManager.class) {
				mInatance = new LruCacheManager(ctx);
			}
		}
		return mInatance;
	}

	private LruCacheManager(Context ctx){
		mContext = ctx;
		GLog.i(TAG, "totalMemory:"+mTotalSize +"b");
		mLruCache = new LruCache<String, Bitmap>(mTotalSize/2){
			/*当缓存大于我们设定的最大值时，会调用这个方法，我们可以用来做内存释放操作*/
			@Override
			protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
				super.entryRemoved(evicted, key, oldValue, newValue);
				if (evicted && oldValue != null){
					GLog.i(TAG, "recycle img, resid:"+key +" oldValueModel:"+oldValue.toString());
					oldValue.recycle();
					mLruCache.remove(key);
				}
			}
			/*创建 bitmap*/
			@Override
			protected Bitmap create(String key) {
				String[] kes = key.split(",");
				//int resId = Integer.parseInt(kes[0]);
				String path = kes[0];
				GLog.i(TAG, "create img, resid:"+path );
				BitmapFactory.Options opts = new BitmapFactory.Options();
				Bitmap bitmap = null;
				if (kes.length == 2) {//key 值包含了图片宽度
					int width = Integer.parseInt(kes[1]);
					opts.inJustDecodeBounds = true;
					bitmap = getBitmap(path, opts);
					int outWidth = opts.outWidth;

					opts.inSampleSize = outWidth/width;
					GLog.i(TAG,"outWidth:"+outWidth + " width:"+width+"sample size:"+ outWidth/width);
				}

				opts.inDither=false;    /*不进行图片抖动处理*/
				opts.inPreferredConfig=null;  /*设置让解码器以最佳方式解码*/
            	/* 下面两个字段需要组合使用 */
				opts.inPurgeable = true;
				opts.inInputShareable = true;
				opts.inJustDecodeBounds = false;
				bitmap = getBitmap(path, opts);
				if (bitmap == null) {
					GLog.i(TAG, "create bitmap failed" );
				}else {
					GLog.i(TAG, "create img, bitmapID:"+bitmap.toString() );
				}

				return bitmap;
			}
			/*获取每个 value 的大小*/
			@SuppressLint("NewApi")
			@Override
			protected int sizeOf(String key, Bitmap value) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
					return value.getByteCount();
				}
				int size = value.getRowBytes() * value.getHeight();
				return size;
			}
		};
	}


	public Bitmap get(String key){
		Bitmap bm = mLruCache.get(key);
		//GLog.v("LruCacheManager", "使用:"+bm.toString() + "isRecyle:"+bm.isRecycled());
		if (bm != null &&bm.isRecycled()) {
			GLog.v("LruCacheManager", "删除对该bitmap的引用，重新生成bitmap");
			mLruCache.remove(key);
			bm = mLruCache.get(key);
		}


		return bm;
	}

	public void remove(String keyString){
		mLruCache.remove(keyString);
	}

	private Bitmap getBitmap(String path,BitmapFactory.Options opts){
		Bitmap bt = null;
		if(path.startsWith("/")){
			bt = BitmapFactory.decodeFile(path,opts);
		}else{
			int resId = Integer.parseInt(path);
			bt = BitmapFactory.decodeResource(mContext.getResources(), resId, opts);
		}
		return bt;
	}
}