/**
 *
 */
package com.guogee.tablet.utils;

import com.guogee.ismartandroid2.networkingProtocol.DeviceType;
import com.guogee.ismartandroid2.utils.GLog;
import com.example.mt.mediaplay.R;

/**
 * @author xieguangwei
 * @ClassName: ImageUtil.java
 * @Description: TODO
 * @date 2015年7月28日
 */
public class ImageUtil {
    public final static int STATUS_NORMAL = 0;
    public final static int STATUS_HIGHLIGHT = 1;
    public final static int STATUS_PRESSED = 2;
    private static final String TAG = "ImageUtil";

    /**
     * 设备管理和场景设备图标
     *
     * @param deviceType
     * @return
     */
    public static int getDeviceIconByType(String deviceType) {
        GLog.i(TAG, "get icon:" + deviceType);
        switch (deviceType) {
            case DeviceType.TV_FALG:
                return R.drawable.zq_setting_icon_tv;
            case DeviceType.TVBOX_FALG:
                return R.drawable.zq_setting_icon_tvbox;
            case DeviceType.FAN_FALG:
                return R.drawable.zq_setting_icon_fan;
            case DeviceType.DVD_FALG:
                return R.drawable.zq_setting_icon_cd;
            case DeviceType.AIR_FALG:
                return R.drawable.zq_setting_icon_aircond;
            case DeviceType.CUSTOM_FALG:
                return R.drawable.zq_setting_icon_z;
            case DeviceType.LIGHT_FALG:
                return R.drawable.zq_setting_icon_light;
            case DeviceType.OUTLET_FALG:
            case DeviceType.OUTLET_V2_FLAG:
                return R.drawable.zq_setting_icon_socket;

            case DeviceType.IRMOTE_FALG:
                return R.drawable.zq_setting_icon_box;
            case DeviceType.ENVSENSER_FALG:
                return R.drawable.zq_setting_icon_em;
            case DeviceType.GATEWAY_FALG:
            case DeviceType.GATEWAY_INTEL_BOX:
                return R.drawable.zq_setting_icon_gw;
            case DeviceType.LIGHT_GROUP_RGBLIGHT_FALG:
                return R.drawable.zq_setting_icon_light_group;
            case "NODEVICE":
                return R.drawable.zq_setting_icon_no;
            case DeviceType.LAMPHOLDER_FALG:
                return R.drawable.zq_setting_icon_lampholder;
            case DeviceType.LIGHTCONTROL_FALG:
                return R.drawable.zq_setting_icon_light;
            case DeviceType.SWITCH_ONES_FALG:
            case DeviceType.SWITCH_ONE_FALG:
            case DeviceType.SMART_SWITCH_ONES_FALG:
            case DeviceType.SMART_SWITCH_ONE_FALG:
                return R.drawable.zq_setting_icon_socket1;
            case DeviceType.SWITCH_TWOS_FALG:
            case DeviceType.SWITCH_TWO_FALG:
            case DeviceType.SMART_SWITCH_TWOS_FALG:
            case DeviceType.SMART_SWITCH_TWO_FALG:
                return R.drawable.zq_setting_icon_switch2_1;
            case DeviceType.SWITCH_THREES_FALG:
            case DeviceType.SWITCH_THREE_FALG:
            case DeviceType.SMART_SWITCH_THREES_FALG:
            case DeviceType.SMART_SWITCH_THREE_FALG:
                return R.drawable.zq_setting_icon_socket3_1;
            case DeviceType.SWITCH_FOURS_FALG:
            case DeviceType.SWITCH_FOUR_FALG:
            case DeviceType.SMART_SWITCH_FOURS_FALG:
            case DeviceType.SMART_SWITCH_FOUR_FALG:
                return R.drawable.zq_setting_icon_socket4_1;
            case DeviceType.YWLIGHT_FALG:
                return R.drawable.zq_setting_icon_light;
            case DeviceType.LIGHT_GROUP_YWLIGHT_FALG:
                return R.drawable.zq_setting_icon_light_group;
            case DeviceType.CAMERA_FLAG:
                return R.drawable.zq_setting_icon_webcam;// Smither 2014/9/22
            case DeviceType.CURTAIN_FALG:
                return R.drawable.zq_setting_icon_curtains;
            case DeviceType.IRRFMOTE_FLAG:
                return R.drawable.zq_setting_icon_box;
            case DeviceType.SECURITY:
                return R.drawable.zq_setting_icon_security;
            case DeviceType.YWLIGHTCONTROL_FALG:
                return R.drawable.zq_setting_icon_light;
            case DeviceType.POWER_CONTROL_1_FLAG:
                return R.drawable.zq_setting_icon_diany;
            case DeviceType.CELLING_LAMP:
                return R.drawable.zq_setting_icon_xilight1;
            case DeviceType.CELLING_LAMP_SINGLE:
                return R.drawable.zq_setting_icon_xilight;
            case DeviceType.CURTAIN_ELEC:
                return R.drawable.zq_setting_icon_curtains;
            case DeviceType.CURTAIN_OPENCLOSE_DOUBLE:
                return R.drawable.zq_setting_icon_curtains;
            case DeviceType.CURTAIN_OPENCLOSE_SINGLE:
                return R.drawable.zq_setting_icon_curtains;
            case DeviceType.CURTAIN_ROLL_DOUBLE:
                return R.drawable.zq_setting_icon_curtains;
            case DeviceType.CURTAIN_ROLL_SINGLE:
                return R.drawable.zq_setting_icon_curtains;
            case DeviceType.ROBOT:
                return R.drawable.zq_setting_icon_rebot;
            case DeviceType.SECURITY_DOOR_FLAG:
                return R.drawable.zq_setting_icon_magnetic;
            case DeviceType.SECURITY_WINDOW_FLAG:
                return R.drawable.zq_setting_icon_window;
            case DeviceType.SECURITY_HUMAN_FLAG:
                return R.drawable.zq_setting_icon_human;
            case DeviceType.SECURITY_SMOKE_FLAG:
                return R.drawable.zq_setting_icon_smoke;
            case DeviceType.SECURITY_GAS_FLAG:
                return R.drawable.zq_setting_icon_gas;
            case DeviceType.SECURITY_INFRARED_FLAG:
                return R.drawable.zq_setting_icon_infrared;
            case DeviceType.SECURITY_SHOCK_FLAG:
                return R.drawable.zq_setting_icon_shock;
            case DeviceType.SECURITY_LIQUID_FLAG:
                return R.drawable.zq_setting_icon_liquid;
            case DeviceType.SECURITY_RAIN_FLAG:
                return R.drawable.zq_setting_icon_rain;
            case DeviceType.POWER_CONTROL_VER2_FLAG:
                return R.drawable.zq_setting_icon_diany;
            case DeviceType.IRMOTE_V2_FLAG:
                return R.drawable.zq_setting_icon_box;
            case DeviceType.PROJECTOR:
                return R.drawable.zq_setting_icon_projector;
            case DeviceType.IPTV:
                return R.drawable.ip_tv_red;
            case DeviceType.ENVSENSER_V2_FALG:
                return R.drawable.zq_setting_icon_em;
            case DeviceType.IRMOTE_PLUG_FLAG:
                return R.drawable.smart_gateway_manager;
            case DeviceType.GATEWAY_PLUG_FLAG:
                return R.drawable.zq_device_manager_gateway_outlet;
            case DeviceType.SECURITY_MAGNET_DEVICE_FLAG:
                return R.drawable.zq_setting_icon_magnetic;
            case DeviceType.OPEN_DOOR_MACHINE:
                return  R.drawable.zq_setting_icon_door_machine;
            case DeviceType.GATEWAY_LOCK:
            case DeviceType.SMART_LOCK:
                return R.drawable.gateway_lock;
            case DeviceType.LIGHT_CONTROL_2_4g_ONE:
                return R.drawable.zq_setting_icon_light;
            case DeviceType.LIGHT_CONTROL_2_4g_TWO:
                return R.drawable.zq_setting_icon_light;
            case DeviceType.LIGHT_CONTROL_2_4g_FIVE:
                return R.drawable.zq_setting_icon_light;
            case DeviceType.CHAIN_WINDOW_SLIDING_DEV_V1:
            case DeviceType.TRANS_WINDOW_SLIDING_DEV_V1:
                return R.drawable.zq_setting_icon_window_sliding;
            case DeviceType.SMART_ROOM_SENSOR_V1:
                return R.drawable.zq_setting_icon_em;
            case DeviceType.MINI_DOORBELL:
                return R.drawable.ic_setting_doorbell_mini;
            case DeviceType.RT_DOORBELL:
                return R.drawable.ic_setting_doorbell_rt;
            default:
                return R.drawable.zq_setting_icon_box;
        }
    }




}
