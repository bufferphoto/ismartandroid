package com.guogee.tablet.utils;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.guogee.ismartandroid2.device.RGBLight;
import com.guogee.ismartandroid2.device.RGBYWLightControl;
import com.guogee.ismartandroid2.device.YWLight;
import com.guogee.ismartandroid2.manager.DataModifyHandler;
import com.guogee.ismartandroid2.manager.SceneManager;
import com.guogee.ismartandroid2.model.Action;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.model.Room;
import com.guogee.ismartandroid2.model.Scene;
import com.guogee.ismartandroid2.model.SceneAction;
import com.guogee.ismartandroid2.networkingProtocol.DeviceType;
import com.guogee.ismartandroid2.response.CurtainStatus;
import com.guogee.ismartandroid2.response.RGBYWLightControlStatus;
import com.guogee.ismartandroid2.response.RgbLightStatus;
import com.guogee.ismartandroid2.response.Status;
import com.guogee.ismartandroid2.response.YWLightControlStatus;
import com.guogee.ismartandroid2.response.YwLightStatus;
import com.guogee.ismartandroid2.utils.ConvertUtils;
import com.guogee.ismartandroid2.utils.GLog;
import com.example.mt.mediaplay.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/5/23.
 */
public class SceneActionUtil {

    public static Scene createScene(final Context context, String sceneName) {
        GLog.i("--sceneName" + sceneName);
        if (!Constant.checkDeviceName(context, sceneName)) {
            return null;
        }
        if (sceneName.isEmpty()) {
            Toast.makeText(context, R.string.edit_scene_name, Toast.LENGTH_SHORT).show();
            return null;
        }

        List<Scene> list = SceneManager.getInstance(context).getScenes();
        Scene model = new Scene();
        model.setName(sceneName);
        model.setIcon("zq_scene_icon1");
        int orders = 1;
        if (list.size() > 0) {
            orders = list.get(list.size() - 1).getOrders() + 1;
        }
        model.setOrders(orders);
        final byte[] lock = new byte[1];
        synchronized (lock) {
            SceneManager.getInstance(context).addScene(model, new DataModifyHandler<List<Scene>>() {
                @Override
                public void onResult(List<Scene> obj, Exception e) {
                    if (e != null)
                        throw new RuntimeException(e);
                    synchronized (lock) {
                        Intent it = new Intent(Constant.UI_SCENE_CHANGE_ACTION);
                        context.sendBroadcast(it);
                        lock.notify();
                    }
                }
            });
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return SceneManager.getInstance(context).getSceneByName(sceneName);
    }

    public static List<Device> loadCurrentRoomQueryDevice(Context context, Room mCurrentRoom) {
        List<Device> sourceDevies = new ArrayList<>();
        insertDevice(sourceDevies, mCurrentRoom.getDeviceByType(context, DeviceType.LIGHT));
        insertDevice(sourceDevies, mCurrentRoom.getDeviceByType(context, DeviceType.IRMOTE_PLUG_FLAG));
        insertDevice(sourceDevies, mCurrentRoom.getDeviceByType(context, DeviceType.CURTAIN));
        List<Device> switchs = (mCurrentRoom.getDeviceByType(context, DeviceType.SWITCH));
        for (Device swth : switchs) {
            addSwitch(sourceDevies, swth);
        }
        GLog.i("device count:" + sourceDevies.size());
        return sourceDevies;
    }

    public static void saveAction(Context context, Scene mScene, Device device, Status status) {
        GLog.i(" status:" + status.getStatus());
        List<Action> actions = loadData(context, device);
        Action action = null;
        if (device.getDevicetype() == DeviceType.DEVICE_RGBLIGHT) {
            if (status.getStatus() == Status.STATUS_OPEN) {
                action = actions.get(2);
                if (status instanceof RGBYWLightControlStatus) {
                    int red = ((RGBYWLightControlStatus) status).getRed();
                    int green = ((RGBYWLightControlStatus) status).getGreen();
                    int blue = ((RGBYWLightControlStatus) status).getBlue();
                    int rgbBrightness = ((RGBYWLightControlStatus) status).getRGBBrightness();
                    int ywRatio = ((RGBYWLightControlStatus) status).getYWRatio();
                    int yWBrightness = ((RGBYWLightControlStatus) status).getYWBrightness();
                    GLog.i("  red:" + red + "  green:" + green + "  blue:" + blue + "  rgbBrightness:" + rgbBrightness + "  ywRatio:" + ywRatio + "  yWBrightness:" + yWBrightness);
                    action.setData(ConvertUtils.boxAddrByteArrayToString(RGBYWLightControl.calcData(red, green, blue, rgbBrightness, ywRatio, yWBrightness)));
                } else if (status instanceof RgbLightStatus) {
                    int red = ((RgbLightStatus) status).getRed();
                    int green = ((RgbLightStatus) status).getGreen();
                    int blue = ((RgbLightStatus) status).getBlue();
                    int rgbBrightness = ((RgbLightStatus) status).getBrightness();
                    action.setData(ConvertUtils.boxAddrByteArrayToString(RGBLight.calcData(red, green, blue, rgbBrightness + 11)));
                } else if (status instanceof YwLightStatus) {
                    action.setData(ConvertUtils.boxAddrByteArrayToString(YWLight.calcData(((YwLightStatus) status).getYellow(), ((YwLightStatus) status).getWhite() + 11)));
                } else if (status instanceof YWLightControlStatus) {
                    action.setData(ConvertUtils.boxAddrByteArrayToString(RGBYWLightControl.calcData(0, 0, 0, 0, ((YWLightControlStatus) status).getYWRatio(), ((YWLightControlStatus) status).getYWBrightness())));
                }
            } else {
                action = actions.get(1);
            }
        } else if (DeviceType.CURTAIN_ELEC.equalsIgnoreCase(device.getRctype())) {
            String[] addrs = device.getAddr().split("&");
            GLog.i("legth:" + addrs.length);
            for (int i = 0; i < addrs.length; i++) {
                action = actions.get(0);
                int postion = ((CurtainStatus) status).getDestLocation();
                if (i == 1) {
                    postion = ((CurtainStatus) status).getDestLocation2();
                }
                action.setCmd(1);//打开
                action.setData(String.valueOf(postion));
                if (postion == 0) {
                    action.setName(context.getString(R.string.open));
                } else if (postion == 100) {
                    action.setName(context.getString(R.string.close));
                } else {
                    action.setName(context.getString(R.string.open) + " " + (100 - postion) + "%");
                }
                SceneAction model = new SceneAction();
                model.setDevMac(addrs[i]);
                model.setActionId(action.getId());
                model.setSceneId(mScene.getId());
                model.setActionName(action.getName());
                model.setCmd(action.getCmd());
                model.setDevId(device.getId());
                model.setDevType(device.getDevicetype());
                model.setDevVer(device.getVer());
                model.setData(action.getData());
                mScene.addAction(context, model, null);
            }
            return;
        } else if (device.getDevicetype() == DeviceType.DEVICE_SWITCH_FOUR || device.getDevicetype() == DeviceType.DEVICE_SWITCH_THREE || device.getDevicetype() == DeviceType.DEVICE_SWITCH_TWO ||
                device.getDevicetype() == DeviceType.DEVICE_SWITCH_FOURS || device.getDevicetype() == DeviceType.DEVICE_SWITCH_THREES || device.getDevicetype() == DeviceType.DEVICE_SWITCH_TWOS) {
            if (device.isOpen) {
                action = actions.get(0);
            } else {
                action = actions.get(1);
            }
        } else {
            if (status.getStatus() == Status.STATUS_OPEN) {
                action = actions.get(0);
            } else {
                action = actions.get(1);
            }
        }
        SceneAction model = new SceneAction();
        model.setDevMac(device.getAddr());
        model.setActionId(action.getId());
        model.setSceneId(mScene.getId());
        model.setActionName(action.getName());
        model.setCmd(action.getCmd());
        model.setDevId(device.getId());
        model.setDevType(device.getDevicetype());
        model.setDevVer(device.getVer());
        model.setData(action.getData());
        mScene.addAction(context, model, null);

        GLog.i(" addAction onSuccess deviceRctype:" + device.getRctype() + "   name:" + device.getName());
    }

    /**
     * 设备类型来选 设备
     */
    private static List<Action> loadData(Context context, Device deviceInfo) {
        List<Action> actionModelList = new ArrayList<>();
        int deviceType = deviceInfo.getDevicetype();

        if (deviceType == DeviceType.DEVICE_SWITCH_FOUR || deviceType == DeviceType.DEVICE_SWITCH_THREE || deviceType == DeviceType.DEVICE_SWITCH_TWO ||
                deviceType == DeviceType.DEVICE_SWITCH_FOURS || deviceType == DeviceType.DEVICE_SWITCH_THREES || deviceType == DeviceType.DEVICE_SWITCH_TWOS) {
            List<Action> actions = SceneManager.getInstance(context).getActionByDeviceType(deviceType);

            for (Action action : actions) {
                action.setData("" + deviceInfo.switchNum);
                actionModelList.add(action);
            }
        } else if ((deviceType & 0xff) == DeviceType.DEVICE_CURTAIN_ELEC) {
            actionModelList.addAll(SceneManager.getInstance(context).getActionByDeviceType(DeviceType.DEVICE_CURTAIN_ELEC));

        } else if (deviceType == DeviceType.DEVICE_GATEWAY) {
            List<Action> actions = SceneManager.getInstance(context).getActionByDeviceType(DeviceType.DEVICE_GATEWAY);
            for (Action action : actions) {
                int ver = Integer.parseInt(action.getData());
                if (ver == deviceInfo.getVer()) {
                    actionModelList.add(action);
                }
            }
        } else if (deviceInfo.getRctype().equals(DeviceType.LIGHT_CONTROL_2_4g_TWO) || deviceInfo.getRctype().equals(DeviceType.LIGHT_CONTROL_2_4g_FIVE)) { //2.4g一路
            List<Action> actions = SceneManager.getInstance(context).getActionByDeviceType(deviceType);
            if (null != actions && actions.size() == 3) {
                actions.get(2).setData("00-00-00-7F-00");//2.4g调光控制颜色
            }
            actionModelList.addAll(actions);
        } else if (deviceInfo.getRctype().equals(DeviceType.LIGHT_CONTROL_2_4g_ONE)) { //2.4g一路
            List<Action> actions = SceneManager.getInstance(context).getActionByDeviceType(deviceType);
            if (null != actions && actions.size() == 3) {
                actions.remove(2);
            }
            actionModelList.addAll(actions);
        } else if (deviceInfo.getRctype().equals(DeviceType.TV_FALG) || deviceInfo.getRctype().equals(DeviceType.DVD_FALG) || deviceInfo.getRctype().equals(DeviceType.IPTV) || deviceInfo.getRctype().equals(DeviceType.TVBOX_FALG) || deviceInfo.getRctype().equals(DeviceType.FAN_FALG)) {
            List<Action> actions = SceneManager.getInstance(context).getActionByDeviceType(deviceType);
            if (null != actions) {
                if (actions.size() == 3) {
                    actions.remove(1);
                } else if (actions.size() == 2) {
                    actions.remove(1);
                }
            }
            actionModelList.addAll(actions);
        } else {
            actionModelList.addAll(SceneManager.getInstance(context).getActionByDeviceType(deviceType));
        }
        return actionModelList;
    }

    private static void insertDevice(List<Device> sourceDevies, List<Device> devices) {
        for (Device device : devices) {
            addDevice(sourceDevies, device);
        }
    }

    /**
     * 添加多路开关
     *
     * @param swth
     */
    private static void addSwitch(List<Device> sourceDevies, Device swth) {
        addDevice(sourceDevies, swth);
    }

    /**
     * 插入排序
     *
     * @param device
     */
    private static void addDevice(List<Device> mDeviceList, Device device) {
        if (mDeviceList.size() == 0) {
            mDeviceList.add(device);
        } else {
            for (int i = mDeviceList.size() - 1; i >= 0; i--) {
                Device dev = mDeviceList.get(i);
                if (device.getOrders() >= dev.getOrders()) {//升序
                    mDeviceList.add(i + 1, device);
                    break;
                } else if (i == 0) {
                    mDeviceList.add(0, device);
                }
            }
        }
    }

}
