package com.guogee.tablet.utils;

import android.content.Context;

/**
 * Created by Administrator on 2017/4/20.
 */
public class FirVersionConstant {

    public final static String TOKEN = "b7293d222991910b372a37e6e59cb65f";

    public final static String ZHIQU = "com.guogu.ismartandroid2";
    public final static String DIMANSI = "com.dimansi.ismartandroid2";
    public final static String MILLINK = "com.millink.ismartandroid2";
    public final static String LET_SMART = "com.letsmart.ismartandroid2";
    public final static String RL_NET = "com.RLNet.ismartandroid2";
    public final static String LUXNA = "com.guogu.ismartandroid2ByLuxna";
    public final static String GUANG_YING = "com.guangying.ismartandroid2";
    public final static String FANG_ZHENG = "com.fangzheng.ismartandroid2";
    public final static String AO_ZHUO = "com.aozhuo.ismartandroid2";
    public final static String LE_JIA_AN = "com.lejiaan.ismartandroid2";
    public final static String GUOGU_LITE = "com.guogulite.ismartandroid2";
    public final static String GUOGU_MINI= "com.guogumini.ismartandroid2";
    public final static String ZONGTENG= "com.zonteng.ismartandroid2";
    public final static String MILLINK_MINIDOORBELL= " com.Millinkmini.ismartandroid2";
    public final static String GUOGU_TABLET= "com.guogee.tablet.iSmartAndroid";



    public static String getAppID(Context context) {
        String appPackageName = context.getApplicationContext().getPackageName();
        String appId = "";
        switch (appPackageName) {
            case ZHIQU:
                appId = "57a1a3bbca87a840d400005d";
                break;
            case DIMANSI:
                appId = "556edfe52b4495c13f00055a";
                break;
            case MILLINK:
                appId = "57469285e75e2d7e1000000a";
                break;
            case LET_SMART:
                appId = "57515bf7f2fc4248d2000036";
                break;
            case LUXNA:
                appId = "57346f87f2fc425892000007";
                break;
            case RL_NET:
                appId = "58da6fd0ca87a8097300041f";
                break;
            case GUANG_YING:
                appId = "56ea6022f2fc4247e200001f";
                break;
            case FANG_ZHENG:
                appId = "569f05d6e75e2d6aaa00001d";
                break;
            case AO_ZHUO:
                appId = "584e21f7959d694b610026fc";
                break;
            case LE_JIA_AN:
                appId = "57c6ab94959d69276d000ee5";
                break;
            case GUOGU_LITE:
                appId = "59031cbc959d69472b000021";
                break;
            case GUOGU_MINI:
                appId = "590c2f37ca87a856d900046e";
                break;
            case ZONGTENG:
                appId = "56e28ce9e75e2d53f8000047";
                break;
            case MILLINK_MINIDOORBELL:
                appId="591c3483548b7a7e5e00042e";
                break;
            case GUOGU_TABLET:
                appId="59369e3bca87a86dd3000879";
                break;
            default:
                break;
        }
        return appId;
    }

}
