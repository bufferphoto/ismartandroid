package com.guogee.tablet.ui.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.guogee.tablet.adapter.VoiceArrayAdapter;
import com.guogee.tablet.adapter.VoiceCmdData;
import com.guogee.tablet.adapter.VoiceItemAdapter;
import com.guogee.tablet.iSmartApplication;
import com.guogee.tablet.interaction.AddRoomActionToSceneControl;
import com.guogee.tablet.manager.DeviceManager;
import com.guogee.tablet.manager.SceneActionManager;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.model.SmartLockModel;
import com.guogee.tablet.ui.widge.CustomButton;
import com.guogee.tablet.ui.widge.ListViewRebound;
import com.guogee.tablet.utils.Encoder;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.device.Aircondition;
import com.guogee.ismartandroid2.manager.DataModifyHandler;
import com.guogee.ismartandroid2.manager.KeyManager;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.manager.SceneManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.model.DeviceInfo;
import com.guogee.ismartandroid2.model.IRKey;
import com.guogee.ismartandroid2.model.IRKeyTag;
import com.guogee.ismartandroid2.model.Room;
import com.guogee.ismartandroid2.model.RoomInfo;
import com.guogee.ismartandroid2.model.Scene;
import com.guogee.ismartandroid2.model.SceneInfo;
import com.guogee.ismartandroid2.music.MusicSelector;
import com.guogee.ismartandroid2.networkingProtocol.DeviceCMD;
import com.guogee.ismartandroid2.networkingProtocol.DeviceType;
import com.guogee.ismartandroid2.networkingProtocol.WallSwitchCMD;
import com.guogee.ismartandroid2.remoteControlService.AsyncHttpResponseHandler;
import com.guogee.ismartandroid2.remoteControlService.RemoteDeviceControlService;
import com.guogee.ismartandroid2.utils.GConstant;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.sdk.scene.ActionData;
import com.guogee.sdk.voicecontrol.VoiceControl;
import com.guogee.sdk.voicecontrol.VoiceControlAdapter;
import com.guogee.sdk.voicecontrol.VoiceControlListener;
import com.guogee.sdk.voicecontrol.VoiceControlResult;
import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.LexiconListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.resource.Resource;
import com.iflytek.speech.util.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mt on 2017/5/23.
 */

public class VoiceActivity extends BaseActivity implements View.OnClickListener, VoiceControlListener
        , VoiceControlAdapter {

    private class SceneView {
        public LinearLayout parentView;
        public Button image;
        public TextView text;
        public String title;
        public boolean isSet = false;
        public Scene scene;
    }

    private static final String TAG = "VoiceActivity";
    private final static String sceneRegexCN = ".*(把|将)(.*)(状态保存为|状态保存成)(.*)(场景).*";
    private final static String sceneRegexEN = ".*(save the current status of|Save the current status of)(.*)(to)(.*)(scene).*";
    private List<Scene> mSceneList;
    private List<Room> mRoomList;
    private List<Device> mDeviceList;
    private boolean isAnimation = false;
    private boolean isVoiceBtn = false;
    private Toast mToast;
    private LinearLayout linText;
    private ImageView imgLoading;
    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private CustomButton btnVoice;
    private ListViewRebound listview;
    private List<VoiceItemAdapter> itemList = new ArrayList<VoiceItemAdapter>();
    private List<SmartLockModel> passwordList = new ArrayList<SmartLockModel>();
    private VoiceArrayAdapter adapter;
    private StringBuffer voiceString = new StringBuffer("");
    // 语音听写对象
    private SpeechRecognizer mIat;
    private SharedPreferences spf;
    private SharedPreferences mSharedPreferences;
    //	private TextUnderstander mTextUnderstander;
    // 语音合成对象
    private SpeechSynthesizer mTts;
    //语音发音队
    private List<VoiceCmdData> voiceQueueSpeek = new ArrayList<VoiceCmdData>();
    //	private String[] resutlArray;
    private int mLan = GConstant.LAN_EN;

    private Context mContext;

    private VoiceControl mVoiceControl;
    private Handler mHandler;

    private iSmartApplication isapp;
    private String userName = "";

    /**
     * 初始化监听器。
     */
    private InitListener mInitListener = new InitListener() {

        @Override
        public void onInit(int code) {
            GLog.e("SpeechRecognizer init() code = " + code);
            if (code == ErrorCode.SUCCESS) {
                btnVoice.setClickable(true);
            }
        }
    };

    private InitListener mInitListener1 = new InitListener() {
        @Override
        public void onInit(int code) {
            GLog.e("SpeechRecognizer init() code = " + code);
            if (code == ErrorCode.SUCCESS) {
//				btnVoice.setClickable(true);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voice_control_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        mHandler = new Handler();
        init();
        if (!getIntent().getBooleanExtra("showSceneButton", false))
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    voiceOnClick(btnVoice);
                }
            }, 200);

        initSenceAction();
        SharedPreferences loginPreferences = getSharedPreferences("CONFIG", MODE_PRIVATE);
        userName = loginPreferences.getString("login_name", "").trim();
    }

    private void initSenceAction() {
        mContext = VoiceActivity.this;
        isapp = (iSmartApplication) getApplication();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSharedPreferences.edit().putBoolean("isVoiceControlActive", false).commit();
    }

    @Override
    public void onClick(View arg0) {
        GLog.v("LZP", "onClick");
        switch (arg0.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_voice:
                voiceOnClick(arg0);
            default:
                break;
        }
    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
//		SpeechUtility.getUtility().checkServiceInstalled();
        super.onResume();
        String userName = mSharedPreferences.getString("login_name", "");
        String userPwd = mSharedPreferences.getString("login_password", "");
        // 网络。用户名和密都要为真
        if ((userName == null) || (userPwd == null) || ("".equals(userName) || ("".equals(userPwd)))) {
            GLog.d("sxx", "checklogin null");
            Intent intent;
            intent = new Intent(this, LoginActivity.class);
            intent.putExtra("hideForgetPassword", true);
            this.startActivityForResult(intent, 1);
        }
        mSharedPreferences.edit().putBoolean("isVoiceControlActive", true).commit();

    }


    @Override
    protected void onActivityResult(int arg0, int resultCode, Intent arg2) {
        // TODO Auto-generated method stub
        super.onActivityResult(arg0, resultCode, arg2);
        switch (resultCode) {
            case RESULT_CANCELED:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();


        mIat.cancel();
        mIat.destroy();
//		if(mTextUnderstander.isUnderstanding())
//    		mTextUnderstander.cancel();
//    	mTextUnderstander.destroy();
        mTts.stopSpeaking();
        mTts.destroy();
        mContext = null;
    }

    /**
     * voiceBtn
     *
     * @param v
     */
    private void voiceOnClick(View v) {

        isVoiceBtn = !isVoiceBtn;
        if (isVoiceBtn) { //开始
            if (!Resource.getLanguage().equals(getResources().getConfiguration().locale)) {
                Resource.setUILanguage(getResources().getConfiguration().locale);
            }
            int ret = mIat.startListening(recognizerListener);
            if (ret != ErrorCode.SUCCESS) {
                showTip(getString(R.string.ifly_init_fail));
                isVoiceBtn = !isVoiceBtn;
            } else {
                startImageScaleAnimation();
            }

            if (mTts.isSpeaking()) {
                mTts.stopSpeaking();
            }
//			if(mTextUnderstander.isUnderstanding())
//	    		mTextUnderstander.cancel();
        } else {//结束
            stopImageScaleAnimation();
            mIat.stopListening();
            imageLoading();
        }

    }


    private void init() {

        mSceneList = SceneManager.getInstance(this).getScenes();
        spf = getSharedPreferences("com.iflytek.setting", MODE_PRIVATE);
        mSharedPreferences = getSharedPreferences("CONFIG", MODE_PRIVATE);
        linText = (LinearLayout) findViewById(R.id.lin_text);

        imgLoading = (ImageView) findViewById(R.id.img_circle_loading);
        img1 = (ImageView) findViewById(R.id.img_circle1);
        img2 = (ImageView) findViewById(R.id.img_circle2);
        img3 = (ImageView) findViewById(R.id.img_circle3);
        btnVoice = (CustomButton) findViewById(R.id.btn_voice);
//		btnVoice.setBackgroundDrawable(new BitmapDrawable(bitmap3));
        listview = (ListViewRebound) findViewById(R.id.listview);
        adapter = new VoiceArrayAdapter(getApplicationContext(), R.layout.voice_item_left, R.layout.voice_item_right, itemList);
        mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
//		resutlArray				= getResources().getStringArray(R.array.voice_result_array);
        ImageButton backBtn = (ImageButton) findViewById(R.id.btn_back);

        iSmartApplication isapp = (iSmartApplication) getApplication();
        Room currentRoom = isapp.getCurrentRoom();
        if (currentRoom == null) {
            List<Room> roomList = RoomManager.getInstance(this).getRooms();
            if (roomList.size() > 0) {
                currentRoom = roomList.get(0);
            }
            if (currentRoom == null) {
                finish();
                return;
            }
        }
        listview.setAdapter(adapter);
        btnVoice.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        mVoiceControl = new VoiceControl(currentRoom.getId());
        mVoiceControl.setListener(this);
        mVoiceControl.setAdapter(this);
        setIflyTek();

        DeviceManager.getInstance().getSmartLockInfoList(new DataModifyHandler<List<SmartLockModel>>() {

            @Override
            public void onResult(List<SmartLockModel> obj, Exception e) {
                passwordList = obj;
            }
        });

    }

    /**
     * 替换，房间和场景有些是数据库中
     *
     * @return
     */
    private String replaceString(String name) {
        if (name.startsWith("guogee_"))
            return SystemUtil.getStringByName(this, name);
        return name;
    }

    /**
     * 初始化讯飞
     */
    private void setIflyTek() {
        // 设置你申请的应用appid
//		SpeechUtility.createUtility(VoiceActivity.this, "appid="+getString(R.string.app_id));
        mIat = SpeechRecognizer.createRecognizer(this, mInitListener);
        mTts = SpeechSynthesizer.createSynthesizer(this, mInitListener1);
//		mTextUnderstander 	= new TextUnderstander(this, mInitListener2);
        setParam();
        getDataAndUploadWords();
    }


    /**
     * 初始化数据并上传用户词汇
     */
    private void getDataAndUploadWords() {
        JSONArray wordJsonArray = new JSONArray();
        String[] ationStr = getResources().getStringArray(R.array.voice_device_action_array);
        String[] typeStr = getResources().getStringArray(R.array.voice_device_type_array);
        String[] ktStr = getResources().getStringArray(R.array.voice_kt_array);
        try {
            mRoomList = RoomManager.getInstance(this).getRooms();
            mDeviceList = new ArrayList<Device>();
            for (Room room : mRoomList) {
                String roomName = repalceRoomNameForDatabase(room.getName());
                room.setName(roomName);

                wordJsonArray.put(roomName); //所有房间名
                List<Device> devices = getDevicesAtRoom(room);
                for (Device device : devices) {
                    String[] names = device.getName().split("&&");
                    if (names.length == 1) {
                        wordJsonArray.put(device.getName());
                        addDevice(device);
                    } else
                        for (int i = 0; i < names.length; i++) {
                            if (names[i].equals(" ")) {//智能墙面开关的非本地开关不显示
                                continue;
                            }
                            wordJsonArray.put(names[i]); //所有设备名
                            Device model = new Device();
                            model.setName(names[i]);
                            model.setId(device.getId());
                            model.setSystemid(device.getSystemid());
                            model.setVisible(device.getVisible());
                            model.setAddr(device.getAddr());
                            model.setVer(device.getVer());
                            model.setOrders(device.getOrders());
                            model.setDevicetype(device.getDevicetype());
                            model.setRctype(device.getRctype());
                            model.setRoomId(device.getRoomId());
                            model.switchNum = i;
                            addDevice(model);
                        }
                }

            }

            for (String str : ationStr) {

                wordJsonArray.put(str.replace(".*", ""));
            }

            for (String str : typeStr) {
                wordJsonArray.put(str.replace(".*", ""));
            }

            for (String str : ktStr) {
                wordJsonArray.put(str.replace(".*", ""));
            }
            wordJsonArray.put("撤防");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("words", wordJsonArray);
            jsonObject.put("name", "guogee");
            JSONArray userJsonArray = new JSONArray();
            userJsonArray.put(jsonObject);
            JSONObject resultJsonObject = new JSONObject();
            resultJsonObject.put("userword", userJsonArray);

            mIat.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
            mIat.setParameter(SpeechConstant.TEXT_ENCODING, "utf-8");
            mIat.updateLexicon("userword", resultJsonObject.toString(), lexiconListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 插入设备，名字长的放前面
     *
     * @param device
     */
    private void addDevice(Device device) {
        if (mDeviceList.size() == 0) {
            mDeviceList.add(device);
        } else {
            for (int i = 0; i < mDeviceList.size(); i++) {
                Device dev = mDeviceList.get(i);
                if (device.getName().length() >= dev.getName().length()) {
                    mDeviceList.add(i, device);
                    break;
                } else if (i == mDeviceList.size() - 1) {
                    mDeviceList.add(device);
                    break;
                }
            }
        }
    }

    private List<Device> getDevicesAtRoom(Room room) {
        List<Device> devices = new ArrayList<Device>();
        List<Device> security = room.getDeviceByType(this, DeviceType.SECURITY);
        if (security.size() > 0) {
            Device safe = security.get(0);
            safe.setName("1a2d3c");
            safe.setRctype(DeviceType.SECURITY);
//			safe.setDevicetype(DeviceType.DEVICE_SECURITY);
            devices.add(safe);
        }

        devices.addAll(room.getDeviceByType(this, DeviceType.LIGHT));
        devices.addAll(room.getDeviceByType(this, DeviceType.SWITCH));
        devices.addAll(room.getDeviceByType(this, DeviceType.CELLING_LAMP));
        devices.addAll(room.getDeviceByType(this, DeviceType.CELLING_LAMP_SINGLE));
        devices.addAll(room.getDeviceByType(this, DeviceType.GATEWAY_LOCK));
        devices.addAll(room.getDeviceByType(this, DeviceType.IR));
        devices.addAll(room.getDeviceByType(this, DeviceType.CURTAIN));
        devices.addAll(room.getDeviceByType(this, DeviceType.SMART_LOCK));
        devices.addAll(room.getDeviceByType(this, DeviceType.GATEWAY_PLUG_FLAG));
        return devices;
    }

    /**
     * 上传联系人/词表监听器。
     */
    private LexiconListener lexiconListener = new LexiconListener() {

        @Override
        public void onLexiconUpdated(String lexiconId, SpeechError error) {
            if (error != null) {
                //showTip(error.toString());
                GLog.e("LexiconListener===>fail");
            } else {
                //	showTip(getString(R.string.text_upload_success));
//				GGLog.e("tag", "LexiconListener===>ok");
            }
        }
    };

    /**
     * 替房间名，因为预存的是别的
     *
     * @param name
     * @return
     */
    private String repalceRoomNameForDatabase(String name) {

        if (name.contains("guogee_")) {
            name = SystemUtil.getStringByName(this, name);
        }
        return name;
    }

    /**
     * 听写监听器。
     */
    private RecognizerListener recognizerListener = new RecognizerListener() {

        @Override
        public void onBeginOfSpeech() {
//			showTip("开始说话");
        }


        @Override
        public void onError(SpeechError error) {
            showTip(error.getPlainDescription(true));
            stopAnitiom();
            imageLoadStop();
        }

        @Override
        public void onEndOfSpeech() {
//			showTip("结束说话");

        }


        @Override
        public void onResult(RecognizerResult results, boolean isLast) {
            final String text = JsonParser.parseIatResult(results.getResultString());
            GLog.i("--------onResult()------text:" + text);
            voiceString.append(text);
            if (isLast) {
                stopAnitiom();
                imageLoading();
                setListViewScrollTop();
                //item
                VoiceItemAdapter item = new VoiceItemAdapter(voiceString.toString(), false, true);
                itemList.add(item);
                adapter.notifyDataSetChanged();
                new GetDataTask().execute(voiceString.toString());
                voiceString.setLength(0);
            }
        }


        @Override
        public void onEvent(int arg0, int arg1, int arg2, Bundle arg3) {
            // TODO Auto-generated method stub

        }


        @Override
        public void onVolumeChanged(int arg0, byte[] arg1) {
            // TODO Auto-generated method stub

        }

    };

    /**
     * 设置list到底 部
     */
    private void setListViewScrollTop() {
        int totalHeight = 0;
        int oldH = listview.getLastMaxY();
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, listview);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        listview.setLastMaxY(totalHeight);
        listview.snapBy(totalHeight - oldH, -100);
//		adapter.notifyDataSetChanged();
    }


    /**
     * 设置list到底 部
     */
    private void setListViewScrollMaxY() {
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount() - 1; i++) {
            View listItem = adapter.getView(i, null, listview);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        totalHeight += listview.getDividerHeight();
        listview.setMaxY(totalHeight);
    }

    /**
     * 参数设置
     *
     * @return
     */
    @SuppressLint("SdCardPath")
    public void setParam() {

        String lag = spf.getString("iat_language_preference", "mandarin");
        String locale = getResources().getConfiguration().locale.getCountry();
        if (locale.equals("CN")) {
            mLan = GConstant.LAN_CN;
            mIat.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
            mIat.setParameter(SpeechConstant.ACCENT, lag);
            mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");
        } else {
            mLan = GConstant.LAN_EN;
            mIat.setParameter(SpeechConstant.LANGUAGE, "en_us");
            mTts.setParameter(SpeechConstant.VOICE_NAME, "Catherine");
        }
        mVoiceControl.setLan(mLan);

        mIat.setParameter(SpeechConstant.VAD_BOS, spf.getString("iat_vadbos_preference", "4000"));
        mIat.setParameter(SpeechConstant.VAD_EOS, spf.getString("iat_vadeos_preference", "1000"));
        mIat.setParameter(SpeechConstant.ASR_PTT, spf.getString("iat_punc_preference", "1"));
        mIat.setParameter(SpeechConstant.ASR_AUDIO_PATH, "/sdcard/iSmartAndroid/wavaudio.pcm");

        mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);

        mTts.setParameter(SpeechConstant.SPEED, spf.getString("speed_preference", "50"));
        mTts.setParameter(SpeechConstant.PITCH, spf.getString("pitch_preference", "50"));
        mTts.setParameter(SpeechConstant.VOLUME, spf.getString("volume_preference", "50"));
        mTts.setParameter(SpeechConstant.STREAM_TYPE, spf.getString("stream_preference", "3"));
        GLog.i("----------setParam():");
    }

    /**
     * 停止动画
     */
    private void stopAnitiom() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (linText.getVisibility() != View.GONE) {
                    linText.setVisibility(View.GONE);
                }
                if (isAnimation) { //动画没关，关
                    isVoiceBtn = !isVoiceBtn;
                    stopImageScaleAnimation();
                }
            }
        });
    }

    /**
     * show
     *
     * @param str
     */
    private void showTip(final String str) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mToast.setText(str);
                mToast.show();
            }
        });
    }

    /**
     * 开始识别 Loading 动画
     */
    private void imageLoading() {
        if (imgLoading.getVisibility() == View.VISIBLE) return;

        btnVoice.setClickable(false); //不能点了，
        imgLoading.setVisibility(View.VISIBLE);
        imgLoading.startAnimation(getRotateAnimation());
    }

    /**
     * 识别结束 Loading 动画
     */
    private void imageLoadStop() {
        btnVoice.setClickable(true); //能点了
        imgLoading.clearAnimation();
        imgLoading.setVisibility(View.INVISIBLE);
//		img1.setVisibility(View.VISIBLE);
    }

    /**
     * ontouch up or cancle
     */
    private void stopImageScaleAnimation() {
        isAnimation = false;
        img1.clearAnimation();
        img2.clearAnimation();
        img3.clearAnimation();
        img1.setVisibility(View.INVISIBLE);
        img2.setVisibility(View.INVISIBLE);
        img3.setVisibility(View.INVISIBLE);
    }

    /**
     * ontouch down
     */
    private void startImageScaleAnimation() {
        isAnimation = true;
        imgLoading.clearAnimation();
        imgLoading.setVisibility(View.INVISIBLE);

        img1.setAnimation(getAnimationSet());
        img1.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (isAnimation) {
                    img2.setAnimation(getAnimationSet());
                    img2.setVisibility(View.VISIBLE);
                }
            }
        }, 800);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (isAnimation) {
                    img3.setAnimation(getAnimationSet());
                    img3.setVisibility(View.VISIBLE);
                }
            }
        }, 1600);
    }

    /**
     * 开始动画
     *
     * @return
     */
    private AnimationSet getAnimationSet() {
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(getAlphaAnimation());
        animationSet.addAnimation(getScaleAnimation());

        return animationSet;
    }

    /**
     * 转转
     *
     * @return
     */
    private Animation getRotateAnimation() {
        Animation rotateAnimation = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(1000);
        rotateAnimation.setRepeatCount(-1);
        rotateAnimation.setRepeatMode(Animation.RESTART);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        return rotateAnimation;
    }

    /**
     * 缩放
     *
     * @return
     */
    private Animation getScaleAnimation() {
        Animation scaleAnimation = new ScaleAnimation(1.0f, 4.0f, 1.0f, 4.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(2400);
        scaleAnimation.setRepeatCount(-1);
        scaleAnimation.setRepeatMode(Animation.RESTART);
        scaleAnimation.setInterpolator(new LinearInterpolator());
        return scaleAnimation;
    }

    /***
     * 透明
     *
     * @return
     */
    private Animation getAlphaAnimation() {
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.1f);
        alphaAnimation.setDuration(2400);
        alphaAnimation.setRepeatCount(-1);
        alphaAnimation.setRepeatMode(Animation.RESTART);

        return alphaAnimation;
    }


    /**
     * 语义理解错误
     */
    private void understanderErr(String txt) {
        VoiceCmdData cmdData = new VoiceCmdData(txt);
        cmdData.setIsLeft(true);
        cmdData.setIsOk(false);
        addSpeekList(cmdData);
    }

    /**
     * 异步数据处理
     *
     * @author lunge
     */
    private class GetDataTask extends AsyncTask<String, String, String[]> {

        @Override
        protected String[] doInBackground(String... params) {
            // Simulates a background job.   不要做ui操作
            String temp = params[0];
            if (temp == null) return null;  //没有结果， 返回

            GLog.d("听写内容==>" + temp);

            if (temp != null) {
                //temp = "Save the current devices of room living room to home scene";//注意要修改
                if (isExecuteSenceAction(temp)) {
                    params[0] = "voiceSceneAction";
                    return params;
                }

                MusicSelector.getInstance().setUserName(userName);
                if (MusicSelector.getInstance().mactchMusic(temp)) {//匹配音乐并且推送
                    params[0] = "voiceMusic";
                    return params;
                }

                uploadVoiceRecognizeResult(temp);
                boolean result = mVoiceControl.matchAndExec(temp);
                if (!result) { //识别失败
                    GLog.d(TAG, "识别失败==>");
                    return null;
                }

            }
            GLog.i("-------doInBackground-----params:" + params.length);
            return params;

        }

        @Override
        protected void onPostExecute(String[] result) {
            // Do some stuff here
            if (result != null) {
                mTts.startSpeaking(getResources().getString(R.string.voice_OK), null);
                if (result[0].equalsIgnoreCase("voiceSceneAction") || result[0].equalsIgnoreCase("voiceMusic")) {
                    imageLoadStop();
                    understanderErr(getResources().getString(R.string.voice_OK));
                }
            } else {
                understanderErr(getResources().getString(R.string.dont_understand));
                imageLoadStop();
            }

            super.onPostExecute(result);
            GLog.i("-------onPostExecute-----result:" + result);
        }

    }

    /**
     * 是否为自动保存空间的状态为**场景的语音
     *
     * @param temp
     * @return
     */
    private boolean isExecuteSenceAction(String temp) {
        String local = getResources().getConfiguration().locale.getCountry();
        String sceneRegex = sceneRegexEN;
        if (local.equals("CN")) {
            sceneRegex = sceneRegexCN;
        }
        GLog.i("matches:" + temp.matches(sceneRegex));
        if (temp.matches(sceneRegex)) {
            Pattern p = Pattern.compile(sceneRegex);
            Matcher matcher = p.matcher(temp);
            String roomName = "";
            String sceneName = "";
            while (matcher.find()) {
                for (int i = 0; i <= matcher.groupCount(); i++) {
                    System.out.println("group" + i + " : " + matcher.start(i) + " - " + matcher.end(i));
                    System.out.println(matcher.group(i));
                    if (i == 2) {
                        roomName = matcher.group(i);
                    }

                    if (i == 4) {
                        sceneName = matcher.group(i);
                    }
                }
            }

            List<Room> roomList = new ArrayList<>();
            for (Room room : mRoomList) {
                GLog.i(" roomName:" + room.getName());
                if (roomName.contains(replaceString(room.getName()))) {
                    roomList.add(room);
                }
            }

            GLog.i(" size:" + roomList.size());
            if (roomList.size() > 0) {
                boolean isExistScene = false;
                for (Scene model : mSceneList) {
                    GLog.i(" sceneName:" + model.getName());
                    if (sceneName.equalsIgnoreCase(replaceString(model.getName()))) {
                        isExistScene = true;
                        break;
                    }
                }
                showTipDialog(roomName, sceneName, roomList, isExistScene);
            } else {
                SystemUtil.toast(VoiceActivity.this, R.string.no_room_message, Toast.LENGTH_SHORT);
            }
            return true;
        }
        return false;

    }

    private void showTipDialog(final String roomName, final String sceneName, final List<Room> roomList, final boolean isExistScene) {
        if (null == mContext) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mContext, R.style.dialog_transparent);
                View view = LayoutInflater.from(mContext).inflate(R.layout.sence_dialog, null);
                dialog.setContentView(view);
                TextView tipTv = ((TextView) (view.findViewById(R.id.tip_text)));
                LinearLayout etLayout = (LinearLayout) view.findViewById(R.id.ll_sence);
                EditText mSenceName = (EditText) view.findViewById(R.id.wifi_name_edit);
                Button btnSave = (Button) view.findViewById(R.id.btn_sure);
                Button btnNext = (Button) view.findViewById(R.id.btn_next);
                Button btnCancle = (Button) view.findViewById(R.id.btn_cancel);
                String tip = getString(R.string.is_Save_Sence_message).replace("**", roomName).replace("##", sceneName);
                if (!isExistScene) {//不存在该场景
                    tip = getString(R.string.is_Create_Sence_message).replace("**", sceneName).replace("##", roomName);
                    btnSave.setText(R.string.save);
                    btnNext.setVisibility(View.GONE);
                    etLayout.setVisibility(View.VISIBLE);
                    mSenceName.setText(sceneName);
                }
                tipTv.setText(tip);

                final AddRoomActionToSceneControl sceneControl = new AddRoomActionToSceneControl(VoiceActivity.this);
                dialog.show();
                dialog.getWindow().setGravity(Gravity.CENTER);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sceneControl.saveAction(roomList, sceneName, false);
                        mSceneList.clear();
                        mSceneList = SceneManager.getInstance(VoiceActivity.this).getScenes();
                        dialog.dismiss();
                    }
                });

                btnNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sceneControl.saveAction(roomList, sceneName, true);
                        dialog.dismiss();
                    }
                });

                btnCancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    private void uploadVoiceRecognizeResult(String voiceStr) {
        SharedPreferences mPreferences = getSharedPreferences("CONFIG",
                MODE_PRIVATE);
        String userName = mPreferences.getString("login_name", "");
        String deviceType = "andr_" + android.os.Build.BRAND;
        RemoteDeviceControlService.getinstance().uploadVoiceRecognizeResult(voiceStr, userName, deviceType, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, String content) {
                // TODO Auto-generated method stub
                GLog.v("VoiceActivity", "onSuccess:" + content.toString());
            }

            @Override
            public void onFailure(Throwable error, String content) {
                // TODO Auto-generated method stubget
                GLog.v("VoiceActivity", "onFailure:" + content.toString());
            }
        });
        GLog.i("-------uploadVoiceRecognizeResult-----voiceStr:" + voiceStr);
    }

    /**
     * 读队列
     *
     * @param cmdData
     */
    private void addSpeekList(VoiceCmdData cmdData) {


        VoiceItemAdapter item = new VoiceItemAdapter(cmdData.getResult(), cmdData.getIsLeft(), cmdData.getIsOk());
        itemList.add(item);
        int code = mTts.startSpeaking(cmdData.getResult(), null);
        if (code != ErrorCode.SUCCESS) {
            showTip(getString(R.string.voice_error) + code);

            for (VoiceCmdData cmd : voiceQueueSpeek) {
                VoiceItemAdapter itemTemp = new VoiceItemAdapter(cmdData.getResult(), cmdData.getIsLeft(), cmdData.getIsOk());
                itemList.add(itemTemp);
            }
            voiceQueueSpeek.clear();
        }
        adapter.notifyDataSetChanged();

        setListViewScrollMaxY();
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.voice_activity_exit_anim);

    }

    @Override
    public int getRoomCount() {
        return mRoomList.size();
    }

    @Override
    public RoomInfo getRoomInfo(int i) {
        return mRoomList.get(i);
    }

    @Override
    public RoomInfo getRoomById(int id) {
        return RoomManager.getInstance(VoiceActivity.this).getRoomById(id);
    }

    @Override
    public int getDeviceCount() {
        return mDeviceList.size();
    }

    @Override
    public DeviceInfo getDeviceInfo(int i) {
//		GLog.i("olife", "name=" + mDeviceList.get(i).getName() + "---rom=" + mDeviceList.get(i).getRoomId());
        return mDeviceList.get(i);
    }

    @Override
    public int getSceneCount() {
        return mSceneList.size();
    }

    @Override
    public SceneInfo getSceneInfo(int i) {
        return mSceneList.get(i);
    }

    @Override
    public int getActionCount(SceneInfo scene) {
        Scene scn = (Scene) scene;
        return scn.getActions(VoiceActivity.this).size();
    }

    private List<PerformSceneActivity.ActionView> actions;

    @Override
    public ActionData getActionData(SceneInfo scene, int index) {
        if (actions == null || actions.get(0).action.getSceneId() != scene.getId()) {
            actions = SceneActionManager.getInstance(this).loadActions((Scene) scene);
        }
        return actions.get(index).status;
    }

    @Override
    public String getGatewayForDevice(int id) {
        String gws = null;
        Device gw = RoomManager.getInstance(VoiceActivity.this).searchGatewayByDeviceId(id);
        if (gw != null) {
            gws = gw.getAddr();
        }
        return gws;
    }

    @Override
    public DeviceInfo getIRBox(DeviceInfo dev) {
        Room room = RoomManager.getInstance(VoiceActivity.this).getRoomById(dev.getRoomId());
        List<Device> devs = room.getDeviceByType(this, DeviceType.IR_BOX);
        if (devs.size() > 0) {
            return devs.get(0);
        }
        return null;
    }

    @Override
    public IRKey getIrKey(int id, int key) {
        Device dev = RoomManager.getInstance(this).searchDevice(id);
        if (dev.getDevicetype() == DeviceType.DEVICE_AIRCOND) {
            String mac = dev.getAddr().toLowerCase(Locale.CHINA);
            if (key == IRKeyTag.POWER_ON) {
                key = mSharedPreferences.getInt(mac + "airTemperature", 25);
                mSharedPreferences.edit().putInt(mac + "airPowerStatus", Aircondition.AirconditionStatus.POWER_ON).commit();

            }
            if (key < 99) {//温度
                mSharedPreferences.edit().putInt(mac + "airTemperature", key).commit();
                int mode = mSharedPreferences.getInt(mac + "airMode", Aircondition.AirconditionStatus.MODE_COLD);
                if (mode == Aircondition.AirconditionStatus.MODE_COLD || mode == Aircondition.AirconditionStatus.MODE_AUTO || mode == Aircondition.AirconditionStatus.MODE_DEHUMINIFIER || mode == Aircondition.AirconditionStatus.MODE_WIND) {
                    key += 200;
                } else {
                    key += 100;
                }

            }
            if (key == IRKeyTag.POWER_OFF) {
                mSharedPreferences.edit().putInt(mac + "airPowerStatus", Aircondition.AirconditionStatus.POWER_OFF).commit();

            }
            GLog.i("ir key:" + key + " mac:" + mac + " state:" + mSharedPreferences.getInt(mac + "airPowerStatus", 0));

        }
        GLog.i("-------getIrKey-----");
        return KeyManager.getInstance(VoiceActivity.this).getIRKey(id, key);
    }


    @Override
    public void onCommandFinish(VoiceControlResult result) {
        String txt = "";
        GLog.i("-------onCommandFinish-----" + result);
        if (result.getResult() == VoiceControlResult.NO_ACTION) {
            txt = getResources().getString(R.string.no_action);
        } else {

            DeviceInfo device = RoomManager.getInstance(this).searchDevice(result.getDeviceMac());
            //设置设备名
            String devName = "";
            GLog.i("----cmd=" + result.getCmd());
            //多路开关
            if (result.getDeviceType() == DeviceType.DEVICE_SWITCH_ONE || result.getDeviceType() == DeviceType.DEVICE_SWITCH_TWO
                    || result.getDeviceType() == DeviceType.DEVICE_SWITCH_THREE || result.getDeviceType() == DeviceType.DEVICE_SWITCH_FOUR ||
                    result.getDeviceType() == DeviceType.DEVICE_SWITCH_ONES || result.getDeviceType() == DeviceType.DEVICE_SWITCH_TWOS
                    || result.getDeviceType() == DeviceType.DEVICE_SWITCH_THREES || result.getDeviceType() == DeviceType.DEVICE_SWITCH_FOURS) {
                String[] names = device.getName().split("&&");
                int cmd = result.getCmd();
                switch (cmd) {
                    case WallSwitchCMD.TURN_ON_ROAD_ONE:
                    case WallSwitchCMD.TURN_OFF_ROAD_ONE:
                        devName = names[0];
                        break;
                    case WallSwitchCMD.TURN_ON_ROAD_TWO:
                    case WallSwitchCMD.TURN_OFF_ROAD_TWO:
                        devName = names[1];
                        break;
                    case WallSwitchCMD.TURN_ON_ROAD_THREE:
                    case WallSwitchCMD.TURN_OFF_ROAD_THREE:
                        devName = names[2];
                        break;
                    case WallSwitchCMD.TURN_ON_ROAD_FOUR:
                    case WallSwitchCMD.TURN_OFF_ROAD_FOUR:
                        devName = names[3];
                        break;
                    default:
                        break;
                }

                //安防
            } else if (result.getDeviceType() == DeviceType.DEVICE_SECURITY_MAGNET_DOOR || result.getDeviceType() == DeviceType.DEVICE_SECURITY_MAGNET_WIN
                    || result.getDeviceType() == DeviceType.DEVICE_SECURITY || result.getDeviceType() == DeviceType.DEVICE_PIR_SENSOR) {
                int roomId = 0;
                if (device == null) {
                    if (result.getDeviceMac().indexOf("-") < 0)//错误数据
                        roomId = Integer.parseInt(result.getDeviceMac());
                } else {
                    roomId = device.getRoomId();
                }
                if (roomId > 0)
                    devName = replaceString(RoomManager.getInstance(this).getRoomById(roomId).getName());

            } else if (result.getDeviceType() == DeviceType.DEVICE_CURTAIN_OPENCLOSE_DOUBLE || result.getDeviceType() == DeviceType.DEVICE_CURTAIN_ROLL_DOUBLE) {
                if (null == device) {//窗帘拆分的情况根据mac地址搜索不到设备
                    List<Device> deviceList = new ArrayList<>();
                    deviceList.addAll(RoomManager.getInstance(this).getDeviceByType(DeviceType.CURTAIN_ELEC));
                    for (Device device1 : deviceList) {
                        if (device1.getAddr().contains(result.getDeviceMac())) {
                            String[] addrs = device1.getAddr().split("&");
                            if (addrs.length == 2) {
                                if (addrs[0].equalsIgnoreCase(result.getDeviceMac())) {
                                    devName = device1.getName() + "(" + mContext.getString(R.string.curtain_cloth_string) + ")";
                                } else if (addrs[1].equalsIgnoreCase(result.getDeviceMac())) {
                                    devName = device1.getName() + "(" + mContext.getString(R.string.curtain_sha_string) + ")";
                                }
                            }
                            break;
                        }
                    }
                } else {
                    devName = device.getName();
                }
            } else {
                devName = device.getName();
            }
            if (result.getDeviceType() == DeviceType.DEVICE_SMART_LOCK) {//更新控制日期
                final DeviceManager dm = DeviceManager.getInstance();
                dm.getSmartLockInfo(device.getId(), new DataModifyHandler<SmartLockModel>() {

                    @Override
                    public void onResult(SmartLockModel obj, Exception e) {
                        if (obj == null)
                            return;
                        obj.setLastTime(System.currentTimeMillis() / 1000);
                        dm.updateSmartLockInfo(obj, null);

                    }
                });
            }

            //获取指令名 和执行结果
            DeviceCMD cmd = DeviceCMD.getDeviceCMD(result.getDeviceType(), result.getDeviceVer());
            txt += devName;
            if (result.getResult() == VoiceControlResult.CONTROL_SUCCESS) {
                txt += " " + cmd.getCMDName(mLan, result.getCmd());

                txt += " " + getResources().getString(R.string.successfully);
            } else if (result.getResult() == VoiceControlResult.CONTROL_FAIL) {
                txt += " " + cmd.getCMDName(mLan, result.getCmd());

                txt += " " + getResources().getString(R.string.failed);
            } else if (result.getResult() == VoiceControlResult.NO_IRBOX) {
                txt += " " + getResources().getString(R.string.room_no_irmote);
            } else if (result.getResult() == VoiceControlResult.NO_IRKEY) {
                txt += " " + getResources().getString(R.string.current_button_has_no_ircode);
            } else if (result.getResult() == VoiceControlResult.PASSWORD_WRONG) {
                txt += " " + cmd.getCMDName(mLan, result.getCmd());
                txt += " " + getResources().getString(R.string.failed) + "," + getString(R.string.pppp_status_pwd_error);
            } else if (result.getResult() == VoiceControlResult.TURN_ON_MAIN_FIRST) {
                txt += " " + cmd.getCMDName(mLan, result.getCmd());
                txt += " " + getResources().getString(R.string.failed) + "," + getString(R.string.turn_on_main_first);
            }
        }

        final VoiceItemAdapter itemTemp = new VoiceItemAdapter(txt, true, false);
        GLog.i("----txt=" + txt);
//		itemList.add(itemTemp);
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                itemList.add(itemTemp);

                adapter.notifyDataSetChanged();

                setListViewScrollMaxY();

                if (!mVoiceControl.isRunning()) {
                    imageLoadStop();
                }
            }
        });
    }


    @Override
    public String getPwd(int i) {
        if (passwordList == null) {
            return "";
        }
        for (SmartLockModel pwd : passwordList) {
            if (i == pwd.getDeviceId())
                try {
                    return Encoder.decryptDES(pwd.getPwd());
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return "";
    }

    @Override
    public int getUserId(int i) {
        if (passwordList == null) {
            return 1000;
        }
        for (SmartLockModel pwd : passwordList) {
            if (i == pwd.getDeviceId())
                return pwd.getUserId();
        }
        return 1000;
    }


}
