package com.guogee.tablet.ui.widge;


import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class MenuHorizontalScrollView extends HorizontalScrollView {

	private static final String TAG = MenuHorizontalScrollView.class.getSimpleName();
	private MenuHorizontalScrollViewListener mListener;
	private int menuId;
	private int menuWidth;
	private int x,y;
	public MenuHorizontalScrollView(Context context, AttributeSet attrs,
                                    int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public MenuHorizontalScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}

	public MenuHorizontalScrollView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}


	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev)
	{
		
		return super.onInterceptTouchEvent(ev);
	}
	
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		switch (event.getAction())  
        {  
            case MotionEvent.ACTION_UP:  
                  
                //获得ViewHolder  
                  
                //获得HorizontalScrollView滑动的水平方向值.  
                int scrollX = getScrollX();  
                  
                //获得操作区域的长度  
                if(menuWidth == 0)
                    menuWidth = findViewById(menuId).getWidth();
                //注意使用smoothScrollTo,这样效果看起来比较圆滑,不生硬  
                //如果水平方向的移动值<操作区域的长度的一半,就复原  
                if (scrollX < menuWidth / 2)  
                {  
                    smoothScrollTo(0, 0);  
                }  
                else//否则的话显示操作区域  
                {  
                   smoothScrollTo(menuWidth, 0);                                    
               	   if(mListener != null)  {
               		   mListener.onShowMenu(this);
               		   
               	   }
                }  
        }  
		
		return true;
	}

	public void setMenuId(int id)
	{
		menuId = id;
	}
	
	public void setListener(MenuHorizontalScrollViewListener listener){
		mListener = listener;
	}
	
	public static interface MenuHorizontalScrollViewListener{
		void onShowMenu(MenuHorizontalScrollView view);
	}
	
	public void hideMenu(){
		smoothScrollTo(0, 0);
	}
}
