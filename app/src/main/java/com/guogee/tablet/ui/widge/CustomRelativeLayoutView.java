package com.guogee.tablet.ui.widge;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mt.mediaplay.R;
import com.guogee.ismartandroid2.utils.GLog;

/**
 * Created by mt on 2017/6/12.
 */

public class CustomRelativeLayoutView extends RelativeLayout {

    private TextView mTvAddState;
    private Context mContext;

    private String customtext;
    private boolean state;
    private int bg;

    public CustomRelativeLayoutView(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public CustomRelativeLayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
        setCustomAttributes(attrs);
    }

    public CustomRelativeLayoutView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
        setCustomAttributes(attrs);
    }

    private void initView(Context context) {
        View.inflate(context, R.layout.item_relative, CustomRelativeLayoutView.this);
        mTvAddState = (TextView) findViewById(R.id.tv_AddState);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

    }

    private void setCustomAttributes(AttributeSet attrs) {
        TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.CustomReView);
        customtext = a.getString(R.styleable.CustomReView_customtext);
        state = a.getBoolean(R.styleable.CustomReView_customstate,false);
        mTvAddState.setText(customtext);
        changeState(state);
    }

    @Override
    protected void measureChildWithMargins(View child, int parentWidthMeasureSpec, int widthUsed, int parentHeightMeasureSpec, int heightUsed) {
        super.measureChildWithMargins(child, parentWidthMeasureSpec, widthUsed, parentHeightMeasureSpec, heightUsed);

    }

    public void changeState(boolean addstate){
        if(addstate){
            mTvAddState.setVisibility(View.GONE);
        }else{
            mTvAddState.setVisibility(View.VISIBLE);
        }
    }


}
