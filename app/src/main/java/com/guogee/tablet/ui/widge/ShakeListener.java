package com.guogee.tablet.ui.widge;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.guogee.tablet.iSmartApplication;


/**
 * 检测手机摇晃的监听器
 * @author lunge
 *
 */
public class ShakeListener implements SensorEventListener {
	// 速度阈值，当摇晃速度达到这值后产生作用
	private static final int SPEED_SHRESHOLD = 3000;
	public final static int SHAKE_LIMIT = 12;
	// 两次检测的时间间隔
	private static final int UPTATE_INTERVAL_TIME = 70;
	// 传感器管理器
	private SensorManager sensorManager;
	// 传感器
	private Sensor sensor;
	// 重力感应监听器
	private OnShakeListener onShakeListener;
	// 上下文
	private Context mContext;
	// 手机上一个位置时重力感应坐标
	private float lastX;
	private float lastY;
	private float lastZ;
	// 上次检测时间
	private long lastUpdateTime;
	
	private iSmartApplication isapp;
	
	private float mAccel = 0.00f;
	private float mAccelCurrent = SensorManager.GRAVITY_EARTH;
	private float mAccelLast = SensorManager.GRAVITY_EARTH;
	
	public ShakeListener(Context c, iSmartApplication isapp){
		this.mContext = c;
		this.isapp = isapp;
		start();
	}
	
	/**
	 * 开启
	 */
	public void start(){
		
		if(!isapp.enableShake){
			return;
		}
		
		sensorManager = (SensorManager)mContext.getSystemService(Context.SENSOR_SERVICE);
		if(sensorManager != null){
			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			if(sensor != null){
				sensorManager.registerListener(this, sensor,
						sensorManager.SENSOR_DELAY_GAME);
			}
		}
	}
	/**
	 * 关闭
	 */
	public void stop(){
		if(!isapp.enableShake){
			return;
		}
		
		if(sensorManager != null){
			sensorManager.unregisterListener(this);
		}
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent se) {
		// TODO Auto-generated method stub
		long currentUpdateTime = System.currentTimeMillis();
		//间隔
		long timeInterval = currentUpdateTime - lastUpdateTime;
		//达到检测时间
		if(timeInterval < UPTATE_INTERVAL_TIME){
			return ;
		}
		
		lastUpdateTime = currentUpdateTime;
		
		// x , y , z 
//		float x = event.values[0];
//		float y = event.values[1];
//		float z = event.values[2];
//		
//		// 获得x,y,z的变化值
//		float deltaX = x - lastX;
//		float deltaY = y - lastY;
//		float deltaZ = z - lastZ;
//
//		// 将现在的坐标变成last坐标
//		lastX = x;
//		lastY = y;
//		lastZ = z;
//		
//		double speed = Math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ
//				* deltaZ)/ timeInterval * 10000;
//		//达到速度阀值，发出提示
//		if(speed >= SPEED_SHRESHOLD){
//			if(onShakeListener != null)
//				onShakeListener.onShake();
//		}
		
		float x = se.values[0];
	    float y = se.values[1];
	    float z = se.values[2];
	    mAccelLast = mAccelCurrent;
	    mAccelCurrent = (float) Math.sqrt(x*x + y*y + z*z);
	    float delta = mAccelCurrent - mAccelLast;
	    mAccel = mAccel * 0.9f + delta;
	    if(mAccel > SHAKE_LIMIT)
	    	onShakeListener.onShake();
	  
	}
	/**
	 * 设置监听器
	 * @param listener
	 */
	public void setOnShakeListener(OnShakeListener listener){
		this.onShakeListener = listener;
	}

	// 摇晃监听接口
	public interface OnShakeListener {
		public void onShake();
	}
}
