package com.guogee.tablet.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.guogee.pushclient.SDKUtils;
import com.guogee.tablet.SysData;
import com.guogee.tablet.adapter.AdapterDirectPop;
import com.guogee.tablet.adapter.GallayAdapter;
import com.guogee.tablet.adapter.SceneItem;
import com.guogee.tablet.iSmartApplication;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.ui.activity.PerformSceneActivity;
import com.guogee.tablet.ui.widget.FancyCoverFlow;
import com.guogee.tablet.utils.ListDataSave;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.tablet.utils.ViewUtil;
import com.guogee.ismartandroid2.manager.SceneManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.model.Scene;
import com.guogee.ismartandroid2.model.SceneAction;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.sdk.scene.ActionData;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by mt on 2017/5/22.
 */

public class SceneFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "SceneFragment";
    private String SCENTAG = "SCENECHECK";
    private GallayAdapter adapter;
    AdapterDirectPop adapterDirectPop;
    private FancyCoverFlow gallayView;
    private ImageButton BtnPop;
    private List<SceneItem> list = new ArrayList<SceneItem>();
    private List<SceneItem> gaList;
    private List<Scene> dragGridViewItems = new ArrayList<Scene>();
    private PopupWindow window, popupWindow;
    int mScreenWidth, mScreenHeight;
    private int popupX;
    private SharedPreferences mSceneSharedPreferences;
    private SharedPreferences loginPreferences;
    private iSmartApplication isapp;
    private List<Integer> listString;
    Map<String, List<Integer>> checkScene;
    private SysData sysData;
    ListDataSave dataSave;
    private TextView location;
    private ImageView cloud;
    private TextView date;//日期
    private TextView time;//时间
    private TextView week;//星期

    //----------------------用到的常量-----------------------------
    private int showingIndex = -1;
    private static final int TIME_OUT_DISPLAY =300;
    private int toShowIndex = 0;
    DisplayImageOptions options;
    private ImageLoader imageLoader;

    public static SceneFragment instantiation(int position) {
        SceneFragment fragment = new SceneFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (this.getView() != null)
            this.getView().setVisibility(menuVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scene_layout, container, false);

    }

    @Override
    public void onResume() {
        super.onResume();
        loginPreferences = getActivity().getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
        GLog.i(TAG,"onResume"+loginPreferences.getString("login_name", ""));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isapp = (iSmartApplication) getActivity().getApplication();
        sysData = new SysData(getActivity());


        loadData();
        initView();
//        gallayView.setSelection(list.size() / 2);
//		fancyCoverFlow.setActionDistance(10);

        initEvent();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;
        window = new PopupWindow(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new BitmapDrawable());
        window.setAnimationStyle(R.style.alphaStyle);
        window.setFocusable(true);
        window.setOutsideTouchable(false);
        window.setSoftInputMode(PopupWindow.INPUT_METHOD_NEEDED);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        initPopWindows();
        initUpdateReceiver();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }

    private void initUpdateReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction("WEATHER_UPDATE_ACTION");
        getActivity().registerReceiver(receiver, filter);
        updateTime();
        updateWeather();
    }

    private void updateWeather() {
        location.setText(mSceneSharedPreferences.getString("city", getString(R.string.unknown_city)));
        cloud.setImageResource(mSceneSharedPreferences.getInt("weatherIcon", R.drawable.cloud));
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_TIME_TICK)) {
                updateTime();
                GLog.i(TAG, "updateTime()");
                //do what you want to do ...13
            }
            if (action.equals("WEATHER_UPDATE_ACTION")) {
                updateWeather();
                GLog.i(TAG, "updateWeather()");
            }
        }
    };

    /**
     * 更新时间
     */
    private void updateTime() {
        Date date = new Date();

        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒 EEEE");
        format = new SimpleDateFormat("MM-dd");
        String dateStr = format.format(date);
        format = new SimpleDateFormat("EEEE");
        String weekStr = format.format(date);
        format = new SimpleDateFormat("HH:mm");
        String timeStr = format.format(date);

        this.date.setText(dateStr);//显示出日期
        this.time.setText(timeStr);//显示出时间
        this.week.setText(weekStr);
        GLog.i(TAG, "未知:" + mSceneSharedPreferences.getString("city", getString(R.string.unknown_city)));
        if (getString(R.string.unknown_city).equals(location.getText())) {
            updateWeather();
            GLog.i(TAG, "未知:" + mSceneSharedPreferences.getString("city", getString(R.string.unknown_city)));
        }
        //GLog.i(TAG,"Shareprefence:strLoginName:"+loginPreferences.getString("login_name", "").trim());
    }

    public void onPause() {
        super.onPause();

    }

    public void saveScenCheck() {

        if (0 != checkScene.size() && null != checkScene) {
            if (checkScene.containsKey(isapp.getIsmartuser().getUserName())) {
                checkScene.remove(isapp.getIsmartuser().getUserName());
            }
        }
        listString.clear();
        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i).getCheckMode()) {
                listString.add(list.get(i).getId());
            }
            GLog.i("mtmt", "save--" + list.get(i).getListItemTitle() + "--isCheck:" + list.get(i).getCheckMode());
        }
        if (listString.size() != 0) {
            checkScene.put(isapp.getIsmartuser().getUserName(), listString);
            dataSave.saveInfo(SCENTAG, checkScene);
        }

    }

    private void initEvent() {
        BtnPop.setOnClickListener(this);
        gallayView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                showingIndex = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // 通过handler来更新主界面
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if(showingIndex != toShowIndex){
                    showingIndex = toShowIndex;

//做你的业务逻辑处理
                }
            }
        };
        Thread checkChange = new Thread() {
                    @Override
                    public void run() {
                        int myIndex = toShowIndex;
                        try {
                            sleep( TIME_OUT_DISPLAY );
                            if( myIndex == toShowIndex ){
                                handler.sendEmptyMessage(0);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };

                checkChange.start();

                // 点击事件
        gallayView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //SystemUtil.toast(getActivity(), position + "", Toast.LENGTH_SHORT);
                Intent intent = new Intent(getActivity(), PerformSceneActivity.class);
                intent.putExtra("scene", SceneManager.getInstance(getActivity()).getSceneById(gaList.get(position).getId()));
                getActivity().startActivity(intent);
                GLog.i("GallayActivity", gaList.get(position).getListItemTitle());
            }
        });
    }

    private void initView() {
        BtnPop = (ImageButton) getView().findViewById(R.id.popup);
        date = (TextView) getView().findViewById(R.id.date);
        time = (TextView) getView().findViewById(R.id.time);
        week = (TextView) getView().findViewById(R.id.week);

        mSceneSharedPreferences = getActivity().getSharedPreferences("weather", Context.MODE_PRIVATE);
        location = (TextView) getView().findViewById(R.id.location);
        cloud = (ImageView) getView().findViewById(R.id.cloud);

        gallayView = (FancyCoverFlow) getView().findViewById(R.id.fancyCoverFlow);
        // 配置option
        options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

        imageLoader = ImageLoader.getInstance();
        adapter = new GallayAdapter(getActivity(), gaList);
        gallayView.setAdapter(adapter);

        // item之间的间隙可以近似认为是imageview的宽度与缩放比例的乘积的一半
        gallayView.setSpacing(-ViewUtil.Dp2Px(getActivity(), 22));
        gallayView.setReflectionEnabled(false);
        gallayView.setAdapter(adapter);
        gallayView.setSelection(0);
        gallayView.setUnselectedScale(0.85f);
        gallayView.setActionDistance(FancyCoverFlow.ACTION_DISTANCE_AUTO);
    }

    private void loadData() {
        mSceneSharedPreferences = getActivity().getSharedPreferences(SCENTAG, Context.MODE_PRIVATE);
        dataSave = new ListDataSave(getActivity(), SCENTAG);
        gaList = new ArrayList<SceneItem>();
        listString = new ArrayList<Integer>();

        checkScene = new HashMap<>();
        checkScene = dataSave.getInfo(SCENTAG);
        if (0 != checkScene.size() && null != checkScene) {
            loginPreferences = getActivity().getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
            String strLoginName = loginPreferences.getString("login_name", "").trim();
            GLog.i(TAG,"Shareprefence:strLoginName:"+strLoginName);
            if(null != strLoginName){
                if (checkScene.containsKey(strLoginName)) {
                    listString = (List<Integer>) (List) dataSave.getInfo(SCENTAG).get(strLoginName);
                }
            }
        }
        //listString = (List<Integer>) (List) dataSave.getDataList(SCENTAG);
        dragGridViewItems.clear();
        dragGridViewItems.addAll(SceneManager.getInstance(getActivity()).getScenes());
        for (Scene sn : dragGridViewItems) {
            SceneItem item = new SceneItem();
            item.setId(sn.getId());
            item.setListItemBg(getBgByIcon(sn));
            item.setCheckMode(isShowScene(sn.getId()));
            item.setListItemCbIcon(R.drawable.scene_check_btn_a);
            item.setListItemIcon(getIcon(sn));
            item.setListItemTitle(replaceName(sn.getName()));
            if (isShowScene(sn.getId())) {
                gaList.add(item);
            }
            list.add(item);
        }

    }

    public boolean isShowScene(int id) {

        return !listString.contains(id);
    }

    /**
     * 替换名字
     *
     * @param name
     * @return
     */
    private String replaceName(String name) {
        if (name.startsWith("guogee_")) {
            name = SystemUtil.getStringByName(getContext(), name);
        }
        return name;
    }

    private int getIcon(Scene sn) {
        String icon = sn.getIcon();
        int iconId = SystemUtil.getDrawableId(icon);
        if (0 == iconId) {
            iconId = R.drawable.zq_scene_icon1;
        }
        return iconId;
    }

    private int getBgByIcon(Scene sn) {
        String iconbg = sn.getIcon();
        iconbg += "_bg";
        int iconId = SystemUtil.getDrawableId(iconbg);
        if (0 == iconId) {
            iconId = R.drawable.zq_scene_icon1_bg;
        }
        return iconId;
    }


    private void initPopWindows() {
        if (popupWindow == null) {
            // int width = getResources().getDisplayMetrics().widthPixels;

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
            View popLayout = inflater.inflate(R.layout.scene_menu_pop_windows, null);

            ListView lvPop = (ListView) popLayout.findViewById(R.id.lvPopwindow_direct);
            adapterDirectPop = new AdapterDirectPop(getActivity().getApplication(), list);
            lvPop.setAdapter(adapterDirectPop);
            lvPop.setOnItemClickListener(new ItemClickListener());

            popupWindow = new PopupWindow(popLayout, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            popupWindow.setAnimationStyle(R.style.scaleStyle);
            popupWindow.setOutsideTouchable(true);
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.popup:
                showPopWindown();
                break;
        }
    }

    private void showPopWindown() {
        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.popup_window_bg);
        popupX = -mBitmap.getWidth() - BtnPop.getMeasuredWidth() / 8 * 7;
        GLog.i("mtmt", "popup:" + BtnPop.getMeasuredWidth());
        if (popupWindow != null) {
            if (!popupWindow.isShowing()) {
                int[] location = new int[2];
                BtnPop.getLocationOnScreen(location);
                GLog.i(TAG, "location[1]:" + location[1] + "  location[0]:" + location[0]);
                popupWindow.showAsDropDown(BtnPop, popupX, 0);
            } else {
                popupWindow.dismiss();
            }
        }
    }

    private class ItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            GLog.d(TAG, "ItemClickListener");

            if (list.get(position).getCheckMode()) {
                list.get(position).setCheckMode(false);
                GLog.i("mtmt", "title:" + list.get(position).getListItemTitle() + "----length:" + list.size() + "---isCheck:" + list.get(position).getCheckMode());
            } else {
                list.get(position).setCheckMode(true);
            }
            adapterDirectPop.notifyDataSetChanged();
            LoadList();
            adapter.notifyDataSetChanged();
        }
    }

    private void LoadList() {
        gaList.clear();
        Iterator<SceneItem> it = list.iterator();
        while (it.hasNext()) {
            SceneItem item = (SceneItem) it.next();
            if (item.getCheckMode()) {
                gaList.add(item);
            }
        }
        saveScenCheck();
    }

    public static class ActionView {
        public Device deviceInfo;
        public SceneAction action;
        public ActionData status;
        public boolean editing;
        public String cmdData;
        public boolean notStudy;
        public boolean noIRBox;
    }
}
