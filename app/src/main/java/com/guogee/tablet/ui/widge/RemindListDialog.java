package com.guogee.tablet.ui.widge;

import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import com.example.mt.mediaplay.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/4/24.
 */
public class RemindListDialog extends Dialog {
    private View view;
    private TextView textView;
    private ListView listView;
    private Button button;
    private BaseAdapter adapter;
    private Context context;
    private List<String> data;


    public RemindListDialog(Context context) {
        super(context, R.style.dialog_transparent);
        this.context = context;
        view = LayoutInflater.from(context).inflate(R.layout.dialog_remindlist_layout, null);
        setContentView(view);
        setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display d = wm.getDefaultDisplay(); // 获取屏幕宽、高用
        lp.dimAmount = 0.5f;// lp.alpha透明度，黑暗度为lp.dimAmount=1.0f;
        lp.width = (int) (d.getWidth() * 0.8); // 宽度
        lp.height = lp.width; // 高度
        getWindow().setAttributes(lp);

        initView();
    }

    private void initView() {
        data = new ArrayList<>();
        textView = (TextView) view.findViewById(R.id.title);
        listView = (ListView) view.findViewById(R.id.listview);
        button = (Button) view.findViewById(R.id.btn_sure);
        button.setOnClickListener(onClickListener);

        adapter = new ArrayAdapter<String>(context, R.layout.activity_doorbell_videolist_item, data);
        listView.setAdapter(adapter);
    }

    public void setData(List<String> data) {
        this.data.clear();
        this.data.addAll(data);
        adapter.notifyDataSetChanged();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismiss();
        }
    };

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }
}
