package com.guogee.tablet.ui.widge;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.widget.ListView;
import android.widget.Scroller;

public class ListViewRebound extends ListView{

	private boolean mIsTouching = false;
	private float mPrevScrollY 	= 0;
	private int mLastMaxY 		= 0;//上一次最大
	private int mMaxY			= 0;//顶部y最大不能超过他
	private Scroller mScroller;
	private VelocityTracker mVelocityTracker;
	
	public ListViewRebound(Context context) {
		super(context);
		this.mScroller = new Scroller(getContext());
	}
	
	public ListViewRebound(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.mScroller = new Scroller(getContext());
	}

	public ListViewRebound(Context context, AttributeSet attrs,
                           int defStyle) {
		super(context, attrs, defStyle);
		this.mScroller = new Scroller(getContext());
	}

	  @Override   
      public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {   
     
          int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,   
        		  MeasureSpec.EXACTLY);   
          super.onMeasure(widthMeasureSpec, expandSpec);   
      }

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		final float x 	= ev.getX();
		final float y 	= ev.getY();
		
		switch (ev.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				mIsTouching = true;
				startVelocityTracker(y, ev); 
				break;
			case MotionEvent.ACTION_MOVE:
				int deltaY 		= (int)(mPrevScrollY - y);
				mPrevScrollY 	= y;
				//超过层的H
				if((getScrollY() < 0) || (getScrollY() > mLastMaxY)){
					deltaY /= 2;
				}
				
				if(getScrollY() <= mMaxY)
					scrollBy(0, deltaY); // 移动
				
				if(mVelocityTracker != null){
					mVelocityTracker.addMovement(ev);
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				mIsTouching 	= false;
				int velocityY 	= 0;//速度
				int scrollY		= getScrollY();//移动
				if(mVelocityTracker != null ){
					mVelocityTracker.addMovement(ev);
					mVelocityTracker.computeCurrentVelocity(800);
					velocityY = (int)mVelocityTracker.getYVelocity();
				}
				if(velocityY > 600){//layer向下拽拖
					int dis = scrollY - velocityY;
					if(dis < -150){
						dis = scrollY + 150;
					}
					snapBy(-dis, Math.abs(velocityY));
				}else if (velocityY < -600){//layer向上拽拖
//					int dis = scrollY - velocityY;
//					if(dis > (mLastMaxY + 150) ){
//						dis = mLastMaxY + 150;
//					}
					int dis = 0;
					if(scrollY < mMaxY){
						dis = mMaxY - scrollY;
					}
					
					snapBy(dis, Math.abs(velocityY));
				}
				this.computeScroll();
				
				break;
		}
		return true;
	}
	
	@Override
	public void computeScroll() {
		// TODO Auto-generated method stub
		if(mIsTouching)return;
		
		int currentY = this.getScrollY();
		if(mScroller.computeScrollOffset()){
			int toY = mScroller.getCurrY();
			scrollTo(mScroller.getCurrX(), toY);
			postInvalidate();
		}else if(currentY < 0){
			mScroller.startScroll(0, currentY, 0, -currentY, 1000);
			invalidate();
		}else if(currentY > mLastMaxY){
			mScroller.startScroll(0, currentY, 0, mLastMaxY - currentY, 1000);
			invalidate();
		}
	}
	
	
	public void snapBy(int delta, int velocity){
		int current = this.getScrollY();
		mScroller.startScroll(0, current, 0, delta, (int)(Math.abs((float)delta/velocity)*800));
//		requestLayout();
		invalidate();
	}
	
	/**
	 * 开启计速器
	 */
	private void startVelocityTracker(float y, MotionEvent ev){
		mPrevScrollY = y;
		if(mVelocityTracker == null){
			mVelocityTracker = VelocityTracker.obtain(); 
		}
		mVelocityTracker.addMovement(ev);
		
		if(!mScroller.isFinished()){
			mScroller.abortAnimation();
		}
	}
	
	public int getLastMaxY(){
		return mLastMaxY;
	}
	
	public void setLastMaxY(int y){
		this.mLastMaxY = y;
	}
	
	public int getMaxY(){
		return mMaxY;
	}
	
	public void setMaxY(int y){
		this.mMaxY = y;
	}
//	@Override
//	public boolean onInterceptTouchEvent(MotionEvent ev) {
//			return true;
//	}
}
