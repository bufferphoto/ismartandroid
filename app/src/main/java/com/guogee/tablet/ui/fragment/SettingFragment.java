package com.guogee.tablet.ui.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.guogee.pushclient.Main;
import com.guogee.pushclient.SDKUtils;
import com.guogee.tablet.MainActivity;
import com.guogee.tablet.SysData;
import com.guogee.tablet.controlService.PublicNetworkService;
import com.guogee.tablet.iSmartApplication;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.service.SenceAutoExcuteByLocationService;
import com.guogee.tablet.ui.activity.LoginActivity;
import com.guogee.tablet.ui.activity.SplashActivity;
import com.guogee.tablet.ui.widge.DelDialog;
import com.guogee.tablet.utils.Constant;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.manager.HttpManager;
import com.guogee.ismartandroid2.remoteControlService.AsyncHttpResponseHandler;
import com.guogee.ismartandroid2.remoteControlService.RemoteUserService;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.pushclient.GPushManager;
import com.guogee.sdk.ISmartManager;
import com.guogu.music.http.RemoteServerManger;
import com.guogu.music.http.imp.ICallback;
import com.p2p.push.RTPushDoorBellManager;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mt on 2017/5/22.
 */

public class SettingFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "SettingFragment";
    private RelativeLayout quitLayout;//退出
    private RelativeLayout syncLayout;//同步
    private RelativeLayout aboutLayout;//关于
    private RelativeLayout updateLayout;//版本更新
    AlertDialog.Builder builder = null;
    AlertDialog show = null;
    int mScreenWidth, mScreenHeight;
    private TextView loginNameText;
    private TextView snText;
    private iSmartApplication isapp;
    private SharedPreferences loginPreferences;
    private String userName;
    private TextView versionText;
    private ImageView versionHint;

    private Button sysLocalBtn;
    private ProgressBar updateprogress;
    private TextView synStateHint;
    private ImageView synState;
    private SysData sysData;
    private static ProgressDialog progressDialog;
    private String userPassword;

    private SharedPreferences mSceneSharedPreferences;
    private TextView location;
    private ImageView cloud;
    private TextView date;//日期
    private TextView time;//时间
    private TextView week;//星期

    private static int LOCAL_CODE = 3;

    public static SettingFragment instantiation(int position) {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.setting_layout, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isapp = (iSmartApplication) getActivity().getApplication();
        sysData = new SysData(getActivity());
        loginPreferences = getActivity().getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
        initView();
        initEvent();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;
        initUpdateReceiver();
    }

    private void initUpdateReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction("WEATHER_UPDATE_ACTION");
        getActivity().registerReceiver(receiver, filter);
        updateTime();
        updateWeather();
    }

    private void updateWeather() {
        location.setText(mSceneSharedPreferences.getString("city", getString(R.string.unknown_city)));
        cloud.setImageResource(mSceneSharedPreferences.getInt("weatherIcon", R.drawable.cloud));
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_TIME_TICK)) {
                updateTime();
                //do what you want to do ...13
            }
            if(action.equals("WEATHER_UPDATE_ACTION")){
                updateWeather();
            }
        }
    };
    /**
     * 更新时间
     */
    private void updateTime() {
        Date date = new Date();

        SimpleDateFormat format=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒 EEEE");
        format=new SimpleDateFormat("MM-dd");
        String dateStr = format.format(date);
        format=new SimpleDateFormat("EEEE");
        String weekStr = format.format(date);
        format=new SimpleDateFormat("HH:mm");
        String timeStr = format.format(date);

        this.date.setText(dateStr);//显示出日期
        this.time.setText(timeStr);//显示出时间
        this.week.setText(weekStr);
        GLog.i(TAG, "未知ss:" + location.getText());
        if (getString(R.string.unknown_city).equals(location.getText())) {
            updateWeather();
            GLog.i(TAG, "未知:" + mSceneSharedPreferences.getString("city", getString(R.string.unknown_city)));
        }
    }


    private void initEvent() {
        quitLayout.setOnClickListener(this);
        syncLayout.setOnClickListener(this);
        aboutLayout.setOnClickListener(this);
        updateLayout.setOnClickListener(this);
    }

    private void initView() {
        quitLayout = (RelativeLayout) getView().findViewById(R.id.quit_layout);
        syncLayout = (RelativeLayout) getView().findViewById(R.id.sync_layout);
        aboutLayout = (RelativeLayout) getView().findViewById(R.id.about_layout);
        updateLayout = (RelativeLayout) getView().findViewById(R.id.update_layout);
        loginNameText = (TextView) getView().findViewById(R.id.login_name);
        snText = (TextView) getView().findViewById(R.id.sn_back);
        versionText = (TextView) getView().findViewById(R.id.update_back);
        versionHint = (ImageView) getView().findViewById(R.id.versionHint);
        builder = new AlertDialog.Builder(getActivity());

        date = (TextView) getView().findViewById(R.id.date);
        time = (TextView) getView().findViewById(R.id.time);
        week = (TextView) getView().findViewById(R.id.week);

        mSceneSharedPreferences = getActivity().getSharedPreferences("weather", Context.MODE_PRIVATE);
        location = (TextView) getView().findViewById(R.id.location);
        cloud = (ImageView) getView().findViewById(R.id.cloud);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (this.getView() != null)
            this.getView().setVisibility(menuVisible ? View.VISIBLE : View.GONE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //no login
        if ((requestCode == DelDialog.RESULTCODE) && (resultCode == DelDialog.RESULTCODE)) {
            progressDialog = ProgressDialog.show(getActivity(), getResources().getString(R.string.logout_ing),
                    getResources().getString(R.string.please_wait), true, false);
            progressDialog.setCanceledOnTouchOutside(false);
            logOut();

            return;
        }


    }

    private void logOut() {
        RTPushDoorBellManager.getInstance(getActivity()).unSubscribeAll();

        RemoteUserService.LogOut(new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(Throwable error, String content) {
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (progressDialog != null) {
                            if (!progressDialog.isShowing()) {
                                GLog.d("sxx", "取消 网关");
                                return;
                            }
                        }

                        progressDialog.dismiss();
                        logoutData(true);

                        SystemUtil.toast(getActivity(), R.string.log_out_success, Toast.LENGTH_SHORT);
                    }
                });

            }

            @Override
            public void onSuccess(int statusCode, final String content) {
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (progressDialog != null) {
                            if (!progressDialog.isShowing()) {
                                GLog.d("sxx", "注销   操作被取消 ----成功");
                                return;
                            }
                        }
                        progressDialog.dismiss();

                        boolean resultflag;
                        try {
                            resultflag = new org.json.JSONObject(content).getBoolean("result");
                            logoutData(resultflag);
                            Intent intent = new Intent(getActivity(), PublicNetworkService.class);
                            getActivity().stopService(intent);
                        } catch (org.json.JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });


            }
        });
    }

    private void logoutData(boolean flag) {
        try {
            boolean resultflag = flag;
            if (!resultflag) {
                // result fase
                SystemUtil.toast(getActivity(), R.string.log_out_err, Toast.LENGTH_SHORT);
                return;
            } else {
//              	PushManager.getInstance().stopService(UserSettingActivity.this.getApplicationContext());//停止推送服务
//              	TelephonyManager tm = (TelephonyManager) isapp.getSystemService(Context.TELEPHONY_SERVICE);
//      			RemoteUserService.uploadClientId(tm.getDeviceId(), "", "Android", new AsyncHttpResponseHandler() {
//
//					@Override
//					public void onSuccess(int statusCode, String content) {
//						// TODO Auto-generated method stub
//
//					}
//
//					@Override
//					public void onFailure(Throwable error, String content) {
//						// TODO Auto-generated method stub
//
//					}
//				});
                isapp.setIsmartuser(null);
                //清空密码
                userPassword = null;
                SharedPreferences.Editor editor = loginPreferences.edit();
                editor.putString("login_password", userPassword);
                editor.commit();//提交修改
                //			loginNameText.setVisibility(View.GONE);
                SystemUtil.toast(getActivity(), R.string.log_out_success, Toast.LENGTH_SHORT);
                Intent intent = new Intent(getActivity(), SplashActivity.class);
                //	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
                startActivity(intent);

                Intent Intentservice = new Intent(getActivity(), SenceAutoExcuteByLocationService.class);  // Smither 2015/3/18登录进来 通知关闭定位Service
                getActivity().startService(Intentservice);
                GPushManager.getInstance(getActivity()).close();

                RemoteServerManger.getInstance().unregister(loginPreferences.getString("login_name", ""), new ICallback() {
                    @Override
                    public void callbackSuccess(String jsonString) {

                    }

                    @Override
                    public void callbackFail(int code, String err) {

                    }
                });
             /*   MainActivity parentActivity = (MainActivity ) getActivity();
                parentActivity.finish();*/
                getActivity().finish();
            }
        } catch (Exception e) {
            GLog.d("sxx", "log out exception" + e.toString());
            e.printStackTrace();
        }
    }


    /**
     * @param replaceTip
     * @param sureText
     * @param requestCode
     */
    private void tipUserWhetherSyncData(String replaceTip, String sureText, int requestCode) {
        Bundle bundle = new Bundle();
        bundle.putString("replaceTip", replaceTip);
        bundle.putString("sureText", sureText);
        Intent intent = new Intent(getActivity(), DelDialog.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void onResume() {
        super.onResume();
        userName = loginPreferences.getString("login_name", "");
        if (isapp.getIsmartuser() != null) {
            loginNameText.setText(userName);
            loginNameText.setVisibility(View.VISIBLE);
        } else {
            loginNameText.setVisibility(View.GONE);
        }
        if (isapp.newVersion) {
            versionHint.setVisibility(View.VISIBLE);
            versionText.setVisibility(View.GONE);
        } else {
            versionHint.setVisibility(View.GONE);
            versionText.setVisibility(View.VISIBLE);
        }
        String sn = SDKUtils.getSN(getActivity());
        if(null!=sn){
            snText.setText(sn);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.quit_layout:
                exitLogin();
                break;
            case R.id.sync_layout:
                popupSyncActivity();
                break;
            case R.id.about_layout:
                break;
            case R.id.update_layout:
                break;
        }
    }

    /**
     * 退出
     */
    private void exitLogin() {
        Bundle bundle = new Bundle();
        bundle.putString("replaceTip", getResources().getString(R.string.exit_account));
        bundle.putString("sureText", getResources().getString(R.string.ok));
        Intent intent = new Intent(getActivity(), DelDialog.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, DelDialog.RESULTCODE);
    }

    private void popupSyncActivity() {
        if (null == builder) {
            builder = new AlertDialog.Builder(getActivity());
        }
        if (null != show && show.isShowing()) {
            show.dismiss();
        }
        show = builder.show();

        LayoutInflater mLayoutInflater = LayoutInflater.from(getActivity());
        View view = mLayoutInflater.inflate(R.layout.sync_layout, null);
        updateprogress = (ProgressBar) view.findViewById(R.id.upload_progress);
        synStateHint = (TextView) view.findViewById(R.id.sysStateHint);
        synState = (ImageView) view.findViewById(R.id.synState);
        final TextView mSyncCancel = (TextView) view.findViewById(R.id.sync_cancel);
        mSyncCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
            }
        });
        sysLocalBtn = (Button) view.findViewById(R.id.sysLocalBtn);
        sysLocalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                downloadConfiguuration();
                GLog.i(TAG, "同步");
            }
        });
        show.setView(view);
        view.findFocus();

        show.getWindow().setContentView(view);
        show.getWindow().setLayout(mScreenWidth / 5 * 2, mScreenHeight / 10 * 7);//宽高   
        show.getWindow().setGravity(Gravity.CENTER);
    }

    private void changestate() {
        sysLocalBtn.setClickable(false);
        updateprogress.setVisibility(View.VISIBLE);
        synState.setVisibility(View.INVISIBLE);
        synStateHint.setText(getResources().getString(R.string.sync_now));
        synStateHint.setTextColor(getResources().getColor(R.color.sync_now));
        synStateHint.setVisibility(View.VISIBLE);
    }

    /**
     * 下载云端数据
     */
    private void downloadConfiguuration() {
        if (isapp.getIsmartuser() == null) {
            //userNoLogin();
        } else {
            changestate();
            RemoteUserService.DownloadUserConfiguration(new AsyncHttpResponseHandler() {
                @Override
                public void onFailure(Throwable error, String content) {
                    changeErrorState(error);
                }

                @Override
                public void onSuccess(int statusCode, String content) {
                    // TODO Auto-generated method stub

                    switch (statusCode) {
                        case 200:
                            try {
                                GLog.v("LZP", "Start explain Json:" + System.currentTimeMillis());
                                JSONObject value = JSONObject.parseObject(content);


                                boolean resultflag = value.getBoolean("result");
                                if (resultflag) {
                                    JSONObject returnval = value.getJSONObject("value");

                                    int versionCode = returnval.getInteger("ClientVersion");
                                    String data = returnval.getString("Configuration");
                                    JSONObject jsonObj = JSONObject.parseObject(data);
                                    new Thread(new UpdataDataDase(jsonObj, versionCode, getActivity())).start();


                                } else {
                                    int errno = value.getIntValue("errno");
                                    if (errno == HttpManager.ERROR_NOT_LOGIN) {
                                        userNoLogin();
                                    }
                                    changeErrorState(null);
//								SystemUtil.toast(SyncActivity.this, R.string.cloud_no_data, Toast.LENGTH_LONG);
                                }
                            } catch (JSONException e) {
                                changeErrorState(e);
                                e.printStackTrace();
                            }
                            break;
                        case 401:
                            isapp.setIsmartuser(null);
                            //      	userNoLogin();
                            break;
                        case 403:
                            GLog.d("syncCloudConfiguration", "403 Forbidden");
                            break;
                        default:
                            break;
                    }
                }
            });
        }
    }

    private void userNoLogin() {
        isapp.setIsmartuser(null);
        gotoActivity(LoginActivity.class);
    }

    private class UpdataDataDase implements Runnable {
        private JSONObject jsonObject;
        private Context mContext;
        private int ver;

        public UpdataDataDase(JSONObject jsonObject, int ver, Context mContext) {
            this.jsonObject = jsonObject;
            this.mContext = mContext;
            this.ver = ver;
        }

        @Override
        public void run() {
            try {
                sysData.updateDataBase(jsonObject, ver);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
//			List<GatewayInfo> gw = RoomManager.getInstance(isapp).getGateways();
//			NetworkServiceConnector.getInstance().clearGateway();
//			for (GatewayInfo gatewayInfo : gw) {
//				NetworkServiceConnector.getInstance().addGateway(gatewayInfo.getDeviceInfo().getAddr());
//			}
//			NetworkServiceConnector.getInstance().refreshGatewayStatus();
            ISmartManager.loadGateway(mContext);
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    Intent it = new Intent(Constant.UI_SCENE_CHANGE_ACTION);
                    mContext.sendBroadcast(it);
                    isapp.setRoomFragmentsModify(true);
                    changeSuccessState();
                }
            });

        }

    }

    /**
     * 修改按钮状态
     */
    private void changeState() {

        sysLocalBtn.setClickable(false);
        synState.setVisibility(View.INVISIBLE);
        updateprogress.setVisibility(View.VISIBLE);

        synStateHint.setText(getResources().getString(R.string.sync_now));
        synStateHint.setTextColor(getResources().getColor(R.color.sync_now));
        synStateHint.setVisibility(View.VISIBLE);
    }

    private void changeSuccessState() {

        sysLocalBtn.setClickable(true);
        synStateHint.setVisibility(View.INVISIBLE);
        synState.setBackgroundResource(R.drawable.zq_setting_cloud_motion7);

        synState.setVisibility(View.VISIBLE);
        updateprogress.setVisibility(View.INVISIBLE);
        //若下载本地数据，则数据更改为false
        SharedPreferences preferences = getActivity().getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
        preferences.edit().putBoolean("localDataChanged", false).commit();

        Toast.makeText(getActivity(), R.string.sync_success, Toast.LENGTH_LONG).show();


    }

    private void changeErrorState(final Throwable e) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                sysLocalBtn.setClickable(true);

                synStateHint.setText(R.string.sync_error);
                synStateHint.setTextColor(getResources().getColor(R.color.sync_error));

                updateprogress.setVisibility(View.INVISIBLE);
                synState.setBackgroundResource(R.drawable.zq_setting_cloud_erro);
                synState.setVisibility(View.VISIBLE);
                if (e != null) {
                    if (e instanceof IOException) {
                        Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_LONG).show();
                    } else if (e instanceof JSONException) {
                        Toast.makeText(getActivity(), R.string.json_error, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.sync_error, Toast.LENGTH_LONG).show();
                    }

                }
            }
        });


    }

    /**
     * go to activity
     *
     * @param cls
     */
    private void gotoActivity(Class<?> cls) {
        Intent mainIntent = new Intent(getActivity(), cls);
        startActivity(mainIntent);
        //	startActivityForResult(mainIntent, 0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(receiver);
    }
}
