package com.guogee.tablet.ui.fragment;


import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mt.mediaplay.R;
import com.guogee.tablet.utils.ImageLoaderUtil;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.tablet.view.RoundImageView;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogu.music.manager.VolumeControlManager;
import com.guogu.music.model.Music;
import com.guogu.music.play.MusicListener;
import com.guogu.music.play.PlayListener;
import com.guogu.music.play.PlayModeManager;
import com.guogu.music.play.Player;
import com.guogu.music.play.PlayerControl;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventListener;
import java.util.List;

import static android.content.Context.AUDIO_SERVICE;

public class MusicFragment extends Fragment implements PlayListener, MusicListener, View.OnClickListener, VolumeControlManager.VolumeReusltListener, PlayerControl.PauseReusltListener {

    public static final String TAG = "MusicFragment";
    private ListView mListView;
    private SeekBar musicSeekBar;
    private TextView musicNameTv;//歌名
    private TextView singerNameTv;//歌手
    private RoundImageView roundCircleIv;//歌手头像
    private ImageView singer_bg;//歌手背景
    private SharedPreferences mSceneSharedPreferences;
    private TextView location;
    private ImageView cloud;
    private TextView date;//日期
    private TextView time;//时间
    private TextView week;//星期
    private SeekBar SoundseekBar;
    private TextView tvCurrentTime, tvTotalTime, volumeView;
    private int MaxSound;
    private AudioManager audioManager;

    private ArrayList<String> datalist;
    private PlayerControl playerControl = null;
    private VolumeControlManager volumeControlManager = null;
    private ImageLoader mImageLoader;
    private DisplayImageOptions options;
    private ImageButton[] controlList = new ImageButton[4];
    private Player player;
    private PlayModeManager playManager;
    private int mode = 0;
    private boolean first = true, isPlaying = true;
    private List<Music> musics;
    private ObjectAnimator discAnimation;


    public static MusicFragment instantiation(int position) {
        MusicFragment fragment = new MusicFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_music, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        initdata();
        initView();
        initControl();
        initUpdateReceiver();
        initMedia();
        initAnimal();

        super.onViewCreated(view, savedInstanceState);
    }


    private void initMedia() {
        audioManager = (AudioManager) getActivity().getSystemService(AUDIO_SERVICE);//获取音量服务
        MaxSound = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);//获取系统音量最大值
        int currentSount = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);//获取当前音量

        GLog.i(TAG, "MaxSound:" + MaxSound + "----currentSount" + currentSount + "fds:" + formatDouble3(currentSount, MaxSound) + "%");
        volumeView.setText(formatDouble3(currentSount, MaxSound) + "%");
        SoundseekBar.setMax(MaxSound);//音量控制Bar的最大值设置为系统音量最大值
        SoundseekBar.setProgress(currentSount);//音量控制Bar的当前值设置为系统音量当前值
        SoundseekBar.setOnSeekBarChangeListener(new SeekBarListener());
        musicSeekBar.setOnSeekBarChangeListener(new SeekBarChangeEvent());
    }

    public static int formatDouble3(int a, int b) {
        float percent = (float) a / b * 100;
        return (int) percent;//返回的是String类型
    }

    private void initUpdateReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction("WEATHER_UPDATE_ACTION");
        getActivity().registerReceiver(receiver, filter);
        updateWeather();
        updateTime();
    }

    private void updateWeather() {
        location.setText(mSceneSharedPreferences.getString("city", getString(R.string.unknown_city)));
        cloud.setImageResource(mSceneSharedPreferences.getInt("weatherIcon", R.drawable.cloud));
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_TIME_TICK)) {
                updateTime();
                //do what you want to do ...13
            }
            if (action.equals("WEATHER_UPDATE_ACTION")) {
                updateWeather();
            }
        }
    };

    /**
     * 更新时间
     */
    private void updateTime() {
        Date date = new Date();

        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒 EEEE");
        format = new SimpleDateFormat("MM-dd");
        String dateStr = format.format(date);
        format = new SimpleDateFormat("EEEE");
        String weekStr = format.format(date);
        format = new SimpleDateFormat("HH:mm");
        String timeStr = format.format(date);

        this.date.setText(dateStr);//显示出日期
        this.time.setText(timeStr);//显示出时间
        this.week.setText(weekStr);
        GLog.i(TAG, "未知ss:" + location.getText());
        if (getString(R.string.unknown_city).equals(location.getText())) {
            updateWeather();
            GLog.i(TAG, "未知:" + mSceneSharedPreferences.getString("city", getString(R.string.unknown_city)));
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        playerControl.unregisterListener(this);
        playerControl.setMusicListener(null);
        playerControl.unregisterListener(this);
        volumeControlManager.unregisterVolumeListener(this);
        getActivity().unregisterReceiver(receiver);
    }

    private void initdata() {
        mImageLoader = ImageLoader.getInstance();
        options = ImageLoaderUtil.getImageOption(R.drawable.sing);
        playerControl = PlayerControl.getInstance(getActivity());
        playerControl.registerListener(this);
        playerControl.setMusicListener(this);
        playerControl.registerPauseListener(this);
        volumeControlManager = VolumeControlManager.getInstance(getActivity());
        volumeControlManager.registerVolumeListener(this);
        player = playerControl.getPlayer();
        playManager = PlayModeManager.getInstance();
        musics = playerControl.getMusics();
        datalist = new ArrayList<String>();
        String[] titleArr = getResources().getStringArray(R.array.music_array);
        if (null != titleArr) {
            int titleLength = titleArr.length;
            for (int index = 0; index < titleLength; index++) {
                datalist.add(titleArr[index]);
            }
        }
    }

    private void initView() {
        musicNameTv = (TextView) getView().findViewById(R.id.musicname);
        singerNameTv = (TextView) getView().findViewById(R.id.singer);
        roundCircleIv = (RoundImageView) getView().findViewById(R.id.roundCircle);
        singer_bg = (ImageView) getView().findViewById(R.id.singer_bg);
        musicSeekBar = (SeekBar) getView().findViewById(R.id.musicSeekBar);
        SoundseekBar = (SeekBar) getView().findViewById(R.id.soundseekbar);
        volumeView = (TextView) getView().findViewById(R.id.soundValue);
        tvCurrentTime = (TextView) getView().findViewById(R.id.tvCurrentTime);
        tvTotalTime = (TextView) getView().findViewById(R.id.tvTotalTime);

        date = (TextView) getView().findViewById(R.id.date);
        time = (TextView) getView().findViewById(R.id.time);
        week = (TextView) getView().findViewById(R.id.week);

        mSceneSharedPreferences = getActivity().getSharedPreferences("weather", Context.MODE_PRIVATE);
        location = (TextView) getView().findViewById(R.id.location);
        cloud = (ImageView) getView().findViewById(R.id.cloud);

        mListView = (ListView) getView().findViewById(R.id.listview);
        mListView.setAdapter(new Myadapter(getActivity()));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PlayerControl.getInstance(getActivity()).searchMusics(datalist.get(i));
                first = false;
            }
        });

        Music music = playerControl.getCurMusic();
        if (null != music) {
            musicNameTv.setText(music.getSongName());
            singerNameTv.setText(music.getSingerName());
            mImageLoader.displayImage(music.getPictureUrl(), roundCircleIv, options);
            //mImageLoader.displayImage(music.getPictureUrl(), singer_bg, options);
        }

        DisplayMetrics dm = getResources().getDisplayMetrics();
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) roundCircleIv.getLayoutParams();
        //获取当前控件的布局对象
        params.height = (int) (height / 3.5);//设置当前控件布局的高度
        params.width = width / 3;
        roundCircleIv.setLayoutParams(params);

    }


    private void initControl() {
        controlList[0] = (ImageButton) getView().findViewById(R.id.playMode);//播放模式
        controlList[0].setOnClickListener(this);
        controlList[1] = (ImageButton) getView().findViewById(R.id.prev);//上一曲
        controlList[1].setOnClickListener(this);
        controlList[2] = (ImageButton) getView().findViewById(R.id.IbPlayOrPause);//播放模式
        controlList[2].setOnClickListener(this);
        controlList[3] = (ImageButton) getView().findViewById(R.id.next);//下一曲
        controlList[3].setOnClickListener(this);
    }

    private void initAnimal() {
        discAnimation = ObjectAnimator.ofFloat(roundCircleIv, "rotation", 0, 360);
        discAnimation.setDuration(40000);
        //使ObjectAnimator动画匀速平滑旋转
        discAnimation.setInterpolator(new LinearInterpolator());
        //无限循环旋转
        discAnimation.setRepeatCount(ValueAnimator.INFINITE);
    }

    @Override
    public void playStatus(final int status, final int position) {
        GLog.i("  status:" + status + "   position:" + position);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (status) {
                    case Player.PLAY_PREPARE:
                        Music music = playerControl.getCurMusic();
                        if (null != music) {
                            musicNameTv.setText(music.getSongName());
                            singerNameTv.setText(music.getSingerName());
                            GLog.i(TAG, "-----url----" + music.getPictureUrl());
                            mImageLoader.displayImage(music.getPictureUrl(), roundCircleIv, options);
                            mImageLoader.displayImage(music.getPictureUrl(), singer_bg, options);
                            playing();
                        }
                        break;
                    case Player.PLAYING:
                        int duration = playerControl.getPlayer().getDuration();
                        tvTotalTime.setText(ShowTime(duration));
                        GLog.i(TAG, "ShowTime:" + duration);
                        if (duration > 0) {
                            // 计算进度（获取进度条最大刻度*当前音乐播放位置 / 当前音乐时长）
                            long pos = musicSeekBar.getMax() * position / duration;
                            musicSeekBar.setProgress((int) pos);
                            tvCurrentTime.setText(ShowTime(position));

                        }

                        break;
                    case Player.PLAY_FINISH:
//                        Toast.makeText(MainActivity.this, "播放完成", Toast.LENGTH_SHORT).show();
                        break;
                    case Player.PLAY_ERROR:

                    default:

//                        Toast.makeText(MainActivity.this, "播放错误", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    //时间显示函数,我们获得音乐信息的是以毫秒为单位的，把把转换成我们熟悉的00:00格式
    public String ShowTime(int time) {
        time /= 1000;
        int minute = time / 60;
        int hour = minute / 60;
        int second = time % 60;
        minute %= 60;
        return String.format("%02d:%02d", minute, second);
    }

    @Override
    public void musicResultSuccess(int process, List<Music> musicList) {
        musics = playerControl.getMusics();
    }

    @Override
    public void musicResultFail(int process, int error, String err) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playMode:
                mode++;
                updatePlaymode();
                break;
            case R.id.prev:
                if (discAnimation != null) {
                    discAnimation.end();
                    playerControl.playLast();
                    playing();
                }
                break;
            case R.id.IbPlayOrPause:
                if (isPlaying) {
                    controlList[2].setImageResource(R.drawable.ic_music_play_a);
                    playerControl.pause();
                    if (discAnimation != null && discAnimation.isRunning()) {
                        discAnimation.end();
                        discAnimation.cancel();
                        float valueAvatar = (float) discAnimation.getAnimatedValue();
                        discAnimation.setFloatValues(valueAvatar, 360f + valueAvatar);
                    }
                    isPlaying = false;
                    GLog.i(TAG, "------暂停------");
                } else {

                    if (!first && null != musics) {
                        playerControl.resume();
                    }
                }
                break;
            case R.id.next:
                if (discAnimation != null) {
                    discAnimation.end();
                    playerControl.playNext();
                    playing();
                }
                break;
        }
    }

    //播放时动画设置和图片切换
    private void playing() {
        if (!discAnimation.isRunning()) {//不允许动画启动多次
            discAnimation.start();
        }
        controlList[2].setImageResource(R.drawable.ic_music_play_b);
        isPlaying = true;
        GLog.i(TAG, "------播放------");
    }

    private void updatePlaymode() {
        switch (mode % 2) {
            case 0:
                controlList[0].setImageResource(R.drawable.ic_music_loop_a);
                playManager.setCurModel(PlayModeManager.PLAY_MODEL_RANDOM);
                playManager.setMusicDataModel(PlayModeManager.PLAY_MODEL_RANDOM, musics);
                SystemUtil.toast(getActivity(), getString(R.string.Shuffle_Playback), Toast.LENGTH_SHORT);
                break;
            case 1:
                controlList[0].setImageResource(R.drawable.ic_music_loop_b);
                playManager.setCurModel(PlayModeManager.PLAY_MODEL_SINGER);
                playManager.setMusicDataModel(PlayModeManager.PLAY_MODEL_SINGER, musics);
                SystemUtil.toast(getActivity(), getString(R.string.Single_cycle), Toast.LENGTH_SHORT);
                break;

        }

    }

    @Override
    public void volumeResult(int volume) {
        //GLog.i(TAG, "音量：" + volume);
        final int volumeValue = volume;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                volumeView.setText(formatDouble3(volumeValue, MaxSound) + "%");

                SoundseekBar.setProgress(volumeValue);
            }
        });

    }

    @Override
    public void pauseResult() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                controlList[2].setImageResource(R.drawable.ic_music_play_a);

                GLog.i(TAG, "-------暂停播放----");
            }
        });
        if (discAnimation != null && discAnimation.isRunning()) {
            discAnimation.end();
            discAnimation.cancel();
            float valueAvatar = (float) discAnimation.getAnimatedValue();
            discAnimation.setFloatValues(valueAvatar, 360f + valueAvatar);
        }
        isPlaying = false;
    }

    //音量进度条

    class SeekBarListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            // TODO Auto-generated method stub
            if (fromUser) {
                int SeekPosition = seekBar.getProgress();
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, SeekPosition, 0);
            }
            volumeView.setText(formatDouble3(progress, MaxSound) + "%");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // TODO Auto-generated method stub

        }

    }

    private class SeekBarChangeEvent implements SeekBar.OnSeekBarChangeListener {
        int position;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // 原本是(progress/seekBar.getMax())*player.mediaPlayer.getDuration()
            this.position = progress * playerControl.getPlayer().getDuration() / seekBar.getMax();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // seekTo()的参数是相对与影片时间的数字，而不是与seekBar.getMax()相对的数字
            //playerControl.getPlayer().seekTo(position);
            player.seekTo(position);
            GLog.i(TAG, "onStopTrackingTouch---position:" + position);
            if (player.isPlayer()) {
                playerControl.pause();
                playerControl.resume();
            }

        }

    }


    class Myadapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public Myadapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return datalist.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.item_music, null);
                holder.Music = (TextView) convertView.findViewById(R.id.music);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();//重新修改其是第几个数据项
            }
            holder.Music.setText(datalist.get(position));
            return convertView;
        }

        private class ViewHolder {
            public TextView Music;
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (this.getView() != null)
            this.getView().setVisibility(menuVisible ? View.VISIBLE : View.GONE);
    }
}
