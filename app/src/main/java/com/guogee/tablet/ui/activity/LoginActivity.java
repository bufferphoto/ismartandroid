package com.guogee.tablet.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.guogee.tablet.SysData;
import com.guogee.tablet.iSmartApplication;
import com.guogee.tablet.iSmartUser;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.service.SenceAutoExcuteByLocationService;
import com.guogee.tablet.ui.widge.DelDialog;
import com.guogee.tablet.utils.Constant;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.manager.SceneManager;
import com.guogee.ismartandroid2.remoteControlService.AsyncHttpResponseHandler;
import com.guogee.ismartandroid2.remoteControlService.RemoteUserService;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.pushclient.GPushManager;
import com.guogee.pushclient.SDKUtils;
import com.guogee.sdk.ISmartManager;
import com.guogu.music.http.RemoteServerManger;
import com.guogu.music.http.imp.ICallback;
import com.guogu.music.manager.ServiceAuthorityManger;
import com.p2p.push.RTPushDoorBellManager;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by mt on 2017/5/22.
 */

public class LoginActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";
    private String strLoginName = "";
    private String strLoginPwd = "";
    private Button mBtnLogin;
    private SharedPreferences loginPreferences;
    private ProgressDialog progressDialog;
    private iSmartApplication isapp;
    private SysData sysData;


    private final static int CREAT_DIALOG = 1;
    private final static int DIMSS_DIALOG = 2;

    private iSmartUser iUser = null;

    private ImageButton mClearName;
    private ImageButton mClearPwd;
    private EditText loginName;
    private EditText loginPwd;
    private boolean isFailed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        isapp = (iSmartApplication) getApplication();


        initView();
        initEvent();

    }

    private void initEvent() {
        mBtnLogin.setOnClickListener(this);
        mBtnLogin.setClickable(false);
    }

    private void initView() {

        mBtnLogin = (Button) findViewById(R.id.loginBtn);

        // mBtnLogin.setBackgroundResource(R.drawable.zq_public_green_btn_c);

        loginName = (EditText) findViewById(R.id.login_account);
        loginPwd = (EditText) findViewById(R.id.login_pwd);

        loginPreferences = getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
        strLoginName = loginPreferences.getString("login_name", "").trim();
        strLoginPwd = loginPreferences.getString("login_password", "").trim();
        mClearName = (ImageButton) findViewById(R.id.clear1);
        mClearPwd = (ImageButton) findViewById(R.id.clear2);
        if (!strLoginName.equals("")) {

            loginName.setText(strLoginName);
            loginName.setSelection(strLoginName.length());
            mClearName.setVisibility(View.VISIBLE);
            checkString(strLoginName, strLoginPwd);
        }
        loginName.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mClearName.setVisibility(View.GONE);
                } else if (loginName.getText().toString().trim().length() > 0) {
                    mClearName.setVisibility(View.VISIBLE);
                }

            }
        });

        loginName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s == null || s.length() == 0) {
                    loginPwd.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                strLoginName = s.toString().trim();
                if (strLoginName.length() > 0) {
                    mClearName.setVisibility(View.VISIBLE);
                } else {
                    mClearName.setVisibility(View.GONE);
                }
                checkString(strLoginName, strLoginPwd);
            }
        });

        loginPwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mClearPwd.setVisibility(View.GONE);
                } else if (loginPwd.getText().toString().trim().length() > 0) {
                    mClearPwd.setVisibility(View.VISIBLE);
                }
            }
        });

        loginPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                String tmp = s.toString();
                if (isFailed && tmp.length() < strLoginPwd.length()) {
                    isFailed = false;

                    loginPwd.setText("");
                    mClearPwd.setVisibility(View.GONE);
                    return;
                }
                strLoginPwd = s.toString().trim();
                if (strLoginPwd.length() > 0) {
                    mClearPwd.setVisibility(View.VISIBLE);
                } else {
                    mClearPwd.setVisibility(View.GONE);
                }
                checkString(strLoginName, strLoginPwd);
            }
        });

    }

    private void checkString(String user, String pwd) {
        if (!user.equals("") && !pwd.equals("")) {
            mBtnLogin.setClickable(true);
            mBtnLogin.setBackgroundResource(R.drawable.login_btn_bg_state);
        } else {
            mBtnLogin.setClickable(false);
        }
    }


    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        switch (view.getId()) {

            case R.id.loginBtn:
                loginPwd.clearFocus();
                loginName.clearFocus();

                progressDialog = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.is_requesting_data),
                        getResources().getString(R.string.please_wait), true, false);
                login();
                break;
            case R.id.clear1:
                loginName.setText("");
                break;
            case R.id.clear2:
                loginPwd.setText("");
                break;
        }
    }

    private void login() {
        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        TelephonyManager tm = (TelephonyManager) isapp.getSystemService(Context.TELEPHONY_SERVICE);
        RemoteUserService.CheckLogin(strLoginName, strLoginPwd, tm.getDeviceId(), new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(Throwable error, String content) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (progressDialog != null && progressDialog.isShowing())
                        {
                            progressDialog.dismiss();
                        }
                        isFailed = true;
                        GLog.e("login", "登陆失败");
                        SystemUtil.toast(LoginActivity.this, R.string.net_err, Toast.LENGTH_SHORT);
                    }
                });

            }

            @Override
            public void onSuccess(int statusCode, final String content) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            JSONObject resp = JSONObject.parseObject(content);
                            boolean resultflag = resp.getBoolean("result");
                            if (progressDialog != null && progressDialog.isShowing())
                            {
                                progressDialog.dismiss();
                            }

                            GLog.d("sky", "Login Status onSuccess. content = " + content);
                            if (!resultflag) {
                                // result fase
                                isFailed = true;
                                SystemUtil.toast(LoginActivity.this, R.string.login_err, Toast.LENGTH_LONG);
                                return;
                            }

                            JSONObject returnval = resp.getJSONObject("value");

                            // 是否允许登录
                            boolean allowLogin = returnval.getBoolean("AllowLogin");
                            strLoginName = returnval.getString("UserName");
                            //存储用户名和密码
                            if (allowLogin) {
                                //初始化个推
//								PushManager.getInstance().initialize(LoginActivity.this.getApplicationContext());
                                GPushManager.getInstance(LoginActivity.this).register();
                                Intent Intentservice = new Intent(LoginActivity.this, SenceAutoExcuteByLocationService.class);  // Smither 2015/3/18登录进来 通知启动定位Service
                                Intentservice.putExtra("LOGIN_REQUEST", true);
                                LoginActivity.this.startService(Intentservice);

                                SharedPreferences.Editor editor = loginPreferences.edit();
                                String oldUser = loginPreferences.getString("login_name", "");
                                boolean isChangeUser = false;
                                if (!oldUser.equalsIgnoreCase(strLoginName)) {
                                    isChangeUser = true;
                                }

                                iUser = new iSmartUser();
                                iUser.setEmail(returnval.getString("Email"));
                                iUser.setLoginID(returnval.getString("id"));
                                iUser.setPassword(strLoginPwd);
                                iUser.setUserName(strLoginName);

                                if (isChangeUser) {
                                    GLog.d("sxx", "切换了账户 同步数据");
                                    sysData = new SysData(LoginActivity.this);
                                    if (null != mHandler)
                                        mHandler.sendEmptyMessage(LoginActivity.CREAT_DIALOG);
                                    downloadConfiguration();

                                } else {
                                    GLog.d("sxx", "没有切换，登陆成??");
//									if(roomList!=null&&roomList.size()>0){   //没有切换账号,本地有数?? 跳过
                                    GLog.d("sxx", "没有切换账号,本地有数??");
                                    editor.putString("login_name", strLoginName);
                                    editor.putString("login_password", strLoginPwd);
                                    editor.commit();
                                    isapp.setIsmartuser(iUser);
                                    Intent intent = new Intent();
                                    setResult(200, intent);
                                    finish();
//									}else{   //没有数据则去同步数据
//										GLog.d("sxx","没有切换账号,本地没有数据,同步");
//										sysData=new SysData(LoginActivity.this);
//										sysData.clearAllTableData(false);
//										createDialog();
//										downloadConfiguration();
//									}
                                }

                            } else {
                                GLog.e("login", "登陆失败1");
                                Toast.makeText(getApplicationContext(), R.string.login_fail, Toast.LENGTH_LONG).show();
                            }
                            GLog.i("  strLoginName:" + strLoginName);
                            RemoteServerManger.getInstance().register(strLoginName, SDKUtils.getCID(LoginActivity.this), new ICallback() {
                                @Override
                                public void callbackSuccess(String jsonString) {
                                    GLog.i("  jsonString:" + jsonString);
                                    ServiceAuthorityManger.getInstance(getApplication()).checkVendor();
                                }

                                @Override
                                public void callbackFail(int code, String err) {
                                    GLog.i("  err:" + err);
                                }
                            });
                            RTPushDoorBellManager.getInstance(getApplicationContext()).subscribeAll();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }

        });
    }

    /**
     * go to activity
     *
     * @param cls
     */
    private void gotoActivity(Class<?> cls) {
        Intent mainIntent = new Intent(LoginActivity.this, cls);
        startActivity(mainIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == 100) && (resultCode == 200)) {
            GLog.d("sky", "Loginactivity register and login success");
            Intent intent = new Intent();
            setResult(200, intent);
            finish();
        } else {

        }
        if ((requestCode == DelDialog.RESULTCODE) && (resultCode == DelDialog.RESULTCODE)) {

            progressDialog = ProgressDialog.show(this, getResources().getString(R.string.is_requesting_data),
                    getResources().getString(R.string.please_wait), true, false);
            downloadConfiguration();

        } else {
            finish();
        }

    }

    boolean syncResult = false;

    /**
     * 同步到本地
     */
    private void downloadConfiguration() {

        sysData = new SysData(this);

        RemoteUserService.DownloadUserConfiguration(new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(Throwable error, String content) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        GLog.d("sxx", "切换了账户 同步数据fail");
                        Message msg = new Message();
                        msg.what = LoginActivity.DIMSS_DIALOG;
                        mHandler.sendMessage(msg);
                        Toast.makeText(getApplicationContext(), R.string.network_error, Toast.LENGTH_SHORT).show();
                    }
                });


            }

            @Override
            public void onSuccess(final int statusCode, final String content) {

                switch (statusCode) {
                    case 200:
                        try {
                            JSONObject value = JSONObject.parseObject(content);
                            syncResult = true;
                            boolean resultflag = value.getBoolean("result");
                            JSONObject jsonObj = null;
                            int versionCode = 0;
                            if (resultflag) {
                                JSONObject returnval = value.getJSONObject("value");
                                versionCode = returnval.getInteger("ClientVersion");
                                String conf = returnval.getString("Configuration");
                                jsonObj = JSONObject.parseObject(conf);

                            } else {//云端无数据
                                Message msg = new Message();
                                msg.what = LoginActivity.DIMSS_DIALOG;
                                mHandler.sendMessage(msg);
                                SystemUtil.toast(LoginActivity.this, R.string.cloud_no_data, Toast.LENGTH_LONG);
//												sysData.clearAllTableData(true);
                                RoomManager.getInstance(isapp).reInitData();
                                SceneManager.getInstance(isapp).reInitData();
//									Editor editor=loginPreferences.edit();
//									editor.putString("login_name", strLoginName);
//									editor.putString("login_password", strLoginPwd);
//									editor.commit();

                            }
                            UpdataDataDase(jsonObj, versionCode, getApplicationContext());
                            GLog.d("sxx", "login save");
                            RTPushDoorBellManager.getInstance(getApplicationContext()).subscribeAll();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }


                        break;
                    case 401:
                        GLog.d("sxx", "4");
                        break;
                    case 403:
                        GLog.d("sxx", "5");
                        GLog.d("syncCloudConfiguration", "没有权限 ");
                        break;
                    default:
                        break;
                }
            }
        });


    }




    public void UpdataDataDase(JSONObject jsonObject, int ver, Context mContext) {


        // TODO Auto-generated method stub
//			try{
//			 FileOutputStream outputStream = openFileOutput("Data.txt",
//	                    Activity.MODE_PRIVATE);
//			    outputStream.write(jsonObject.toJSONString().getBytes());
//			    outputStream.flush();
//			    outputStream.close();
//			}catch(Exception e){
//				GLog.d("sxx",e.toString());
//			}

        try {
            if (jsonObject != null)
                sysData.updateDataBase(jsonObject, ver);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Message msg = new Message();
        msg.what = LoginActivity.DIMSS_DIALOG;
        mHandler.sendMessage(msg);
        SharedPreferences.Editor editor = loginPreferences.edit();
        editor.putString("login_name", strLoginName);
        editor.putString("login_password", strLoginPwd);
        editor.commit();
        isapp.setIsmartuser(iUser);


        GLog.i("SceneFragment","LoginActivity:"+loginPreferences.getString("login_name", ""));

        GLog.d("sxx", "切换了账户 同步数据 successs");
        //更新数据成功之后，刷新界面
//			List<GatewayInfo> gi = RoomManager.getInstance(this).getGateways();
//			List<Room> rooms = RoomManager.getInstance(this).getRooms();
//			NetworkServiceConnector.getInstance().clearGateway();
//			for (GatewayInfo gatewayInfo : gi) {
//				for(Room room:rooms){
//					if(room.getBoxId() == gatewayInfo.getDeviceInfo().getId())
//						NetworkServiceConnector.getInstance().addGateway(gatewayInfo.getDeviceInfo().getAddr());
//					break;
//				}
//
//			}
        ISmartManager.loadGateway(mContext);
        GLog.d("sxx", "after sync data reloadgateway");
        Intent it = new Intent(Constant.UI_SCENE_CHANGE_ACTION);
        sendBroadcast(it);
        isapp.setRoomFragmentsModify(true);
        loginPreferences.edit().putBoolean("localDataChanged", false).commit();
        SystemUtil.toast(this, R.string.sync_success, Toast.LENGTH_LONG);

        Intent intent = new Intent();
        setResult(200, intent);
        finish();


    }


//	//用户没有登录
//		private void userNoLogin(){
//			Bundle bundle = new Bundle();
//			bundle.putString("replaceTip", getResources().getString(R.string.login_no_title));
//			bundle.putString("sureText", getResources().getString(R.string.login_go));
//			Intent intent = new Intent(this,DelDialog.class);
//			intent.putExtras(bundle);
//			startActivityForResult(intent, DelDialog.RESULTCODE);
//		}


    public void dimissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void createDialog() {
        progressDialog = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.is_requesting_data),
                getResources().getString(R.string.please_wait), true, false);
    }

    private Handler mHandler = new Handler() {

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LoginActivity.CREAT_DIALOG:
                    createDialog();
                    break;
                case LoginActivity.DIMSS_DIALOG:
                    dimissDialog();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (progressDialog!=null){
            progressDialog.dismiss();
            progressDialog=null;
        }
        super.onDestroy();

    }
}
