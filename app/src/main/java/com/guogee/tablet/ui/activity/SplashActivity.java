package com.guogee.tablet.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.guogee.pushclient.SDKUtils;
import com.guogee.tablet.MainActivity;
import com.guogee.tablet.iSmartApplication;
import com.guogee.tablet.iSmartUser;
import com.guogee.tablet.manager.UserDataManager;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.utils.FirVersionConstant;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.model.Room;
import com.guogee.ismartandroid2.remoteControlService.AsyncHttpResponseHandler;
import com.guogee.ismartandroid2.remoteControlService.RemoteUserService;

import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.ismartandroid2.utils.PublishHelper;
import com.guogee.pushclient.GPushManager;

import com.guogu.music.http.RemoteServerManger;
import com.guogu.music.http.imp.ICallback;
import com.guogu.music.manager.ServiceAuthorityManger;
import com.p2p.push.RTPushDoorBellManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Splash screen
 *
 * @author Huihan, 2013/1/6
 */
public class SplashActivity extends Activity {

    iSmartApplication isapp;
    int gateStatusRefreshSteps = 1;
    private final int SPLASH_DISPLAY_LENGHT = 1000;

    /**
     * 第一次启动APP
     **/
    private boolean isFirst = false;

    /**
     * 登录标志־
     *
     * @author Smither 2014/8/25
     */
    private final static int IS_LOGIN = 1;
    private final static int NOT_LOGIN = 2;
    private final static int NOT_ALLOW_LOGIN = 3;
    private static final String TAG = "SplashActivity";


    private List<Map<String, String>> roomList = null;

    private SharedPreferences mPreferences;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GLog.i(TAG, "onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen);

        isapp = (iSmartApplication) getApplication();


        roomList = new ArrayList<Map<String, String>>();
        List<Room> listRoom = RoomManager.getInstance(this).getRooms();
        for (int i = 0; i < listRoom.size(); i++) {
            roomList.add(listRoom.get(i).getModelMap());
        }


/*        // 检测设置
        checkSetting(isapp);*/

        checkAppVersion();
        // 用户登录
        checkLogin();
        // 检测版本更新
//		checkVersionUpdate();//这个接口是好的。暂时不用，用fir提供的接口
        checkFirVersionUpdate();
        // 查防盗光
//		securityLight();
        // 第一次启动
        isFirst = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//		isapp.removeActivity(this);
    }

    @Override
    protected void onStart() {
        GLog.i(TAG, "onStart");
        super.onStart();


    }


    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        GLog.i(TAG, "onNewIntent");
    }

    @Override
    protected void onResume() {
        GLog.i(TAG, "onResume");
        super.onResume();
//	
    }



    Runnable run = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub


            Intent mainIntent;

       /*     if (isFirst) {
                *//** 判断检测到的盒子是否已经绑定过，如果 "绑定过" 直接跳转到主界面上，否则进入让用户选择盒子的界面 **//*
               GLog.i(TAG,"没有绑定盒子");

            } else {*/
                GPushManager.getInstance(SplashActivity.this).register();
                mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
           /* }*/

            SplashActivity.this.finish();
        }
    };


    /**
     * - * function接收Handler信息 - * @author Smither 2014、8、25 -
     */
    private Handler mHandler = new Handler() {

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SplashActivity.IS_LOGIN:
                    new Handler().postDelayed(run, SPLASH_DISPLAY_LENGHT);
                    GLog.d("sky", "splashActivity register and login success");
                    break;
                case SplashActivity.NOT_LOGIN:
                    GLog.e("login", "登陆失败2");
                    GLog.d("sky", "NOT_LOGIN");
                    Toast.makeText(getApplicationContext(), R.string.login_fail, Toast.LENGTH_SHORT).show();
                    login();
                    break;
                case SplashActivity.NOT_ALLOW_LOGIN:
//				new Handler()
//						.postDelayed(intentRunnable, SPLASH_DISPLAY_LENGHT);
                    GLog.d("sky", "NOT_ALLOW_LOGIN");
                    login();
                    break;
                default:
                    new Handler().postDelayed(run, SPLASH_DISPLAY_LENGHT);
                    break;
            }
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        GLog.d("sky", "-----resultCode----" +resultCode);

        switch (resultCode) {
            case 200:
                GLog.d("sky", "splashActivity register and login success");
                new Handler().postDelayed(run, 100);
                break;
            default:
//			Toast.makeText(getApplicationContext(), R.string.login_no_title,
//					6000).show();
                //	new Handler().postDelayed(intentRunnable, 10);
                break;
        }

    }


    /**
     * the function must be use in here. close the app
     *
     * @author Huihan, 2013/1/6
     */
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        //android.os.Process.killProcess(android.os.Process.myPid());
        finish();
    }


/*    private void checkSetting(iSmartApplication app) {

        if (app == null)
            app = (iSmartApplication) getApplication();
        // 震动提示
        boolean state = getSharedPreferences("CONFIG", Context.MODE_PRIVATE)
                .getBoolean(SettingItemActivity.VIBRATOR, false);
        app.enableVibrator = !state;

        // 摇一摇
        boolean shake = getSharedPreferences("CONFIG", Context.MODE_PRIVATE)
                .getBoolean(SettingItemActivity.SHAKE, false);
        app.enableShake = !shake;
    }*/

    private boolean checkVersionUpdate() {

        RemoteUserService.CheckVersion(iSmartApplication.getApp().getResources().getString(R.string.app_name), new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(Throwable error, String content) {
                GLog.e(TAG, "checkVersionUpdate");
            }

            @Override
            public void onSuccess(int statusCode, String content) {
                // TODO Auto-generated method stub
                try {
                    GLog.v(TAG, "content:" + content);
                    boolean resultflag = new JSONObject(content)
                            .getBoolean("result");
                    if (resultflag) {

                        SharedPreferences mPreferences = getSharedPreferences(
                                "CONFIG", Context.MODE_PRIVATE);
                /*		JSONArray jsonArray = new JSONObject(content)
                                .getJSONArray("value");*/
                        // 只有一个
                        JSONObject returnval = new JSONObject(content).getJSONObject("value");
                        //		String client = returnval.getString("client");
                        String apkname = returnval.getString("apkName");
                        String appname = returnval.getString("appName");
                        String url = returnval.getString("downloadUrl");
                        String verName = returnval.getString("verName");
                        int verCode = returnval.getInt("verCode");
                        String descShort = returnval.getString("description");
                        String updateDescrptionUrl = returnval
                                .getString("updateDescrptionUrl");

                        Editor editor = mPreferences.edit();
                        editor.putString("apkname", apkname);
                        editor.putString("appname", appname);
                        editor.putString("url", url);
                        editor.putString("description", descShort);
                        editor.putString("verName", verName);
                        editor.putString("updateDescrptionUrl",
                                updateDescrptionUrl);
                        editor.putInt("verCode", verCode);
                        editor.commit();// 提交修改
//							if (info.versionCode >= verCode) {// 不是最新版本
//								isapp.newVersion = false;
//								return;
//							}
//							isapp.newVersion = true;

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        return true;
    }

    private boolean checkFirVersionUpdate() {
        if (PublishHelper.getBuildType() == PublishHelper.BuildType.Test) {
            return false;
        }
        RemoteUserService.CheckFirVersion(FirVersionConstant.getAppID(this), FirVersionConstant.TOKEN, new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(Throwable error, String content) {
                GLog.e(TAG, "checkVersionUpdate");
            }

            @Override
            public void onSuccess(int statusCode, String content) {
                try {
                    GLog.v(TAG, "content:" + content);
                    JSONObject jsonObject = new JSONObject(content);
                    String appname = jsonObject.getString("name");
                    String url = jsonObject.getString("install_url");
                    String verName = jsonObject.getString("versionShort");
                    int verCode = jsonObject.getInt("version");
                    String descShort = jsonObject.getString("changelog");
                    SharedPreferences mPreferences = getSharedPreferences(
                            "CONFIG", Context.MODE_PRIVATE);
                    Editor editor = mPreferences.edit();
                    editor.putString("appname", appname);
                    editor.putString("url", url);
                    editor.putString("description", descShort);
                    editor.putString("verName", verName);
                    editor.putInt("verCode", verCode);
                    editor.commit();// 提交修改
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return true;
    }

    // 用户登录
    private void checkLogin() {
        mPreferences = getSharedPreferences(UserDataManager.SP_NAME,
                Context.MODE_PRIVATE);
        final String userName = mPreferences.getString(UserDataManager.LOGIN_NAME, "");
        final String userPwd = mPreferences.getString(UserDataManager.LOGIN_PASSWD, "");
        final int lastVersion = mPreferences.getInt(UserDataManager.LAST_VERSION, 0);
        PackageManager manager = this.getPackageManager();
        int currentVersion = 0;
        try {
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            currentVersion = info.versionCode;
            if (currentVersion > lastVersion) {
                mPreferences.edit().putInt(UserDataManager.LAST_VERSION, currentVersion).commit();
                if (currentVersion == 33) {//重构的版本
                    mPreferences.edit().putString(UserDataManager.LOGIN_NAME, "").commit();
                    login();
                    GLog.d("sky", "NOT_ALLOW_LOGIN");
                    return;
                }


            }
        } catch (NameNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        // 网络。用户名和密都要为真
        if ((userName == null) || (userPwd == null) || ("".equals(userName) || ("".equals(userPwd)))) {
            GLog.d(TAG, "checklogin null");
            Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
            SplashActivity.this.startActivityForResult(intent, 1);
            return;
        }
        TelephonyManager tm = (TelephonyManager) isapp.getSystemService(Context.TELEPHONY_SERVICE);
        RemoteUserService.CheckLogin(userName, userPwd, tm.getDeviceId(),
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onFailure(Throwable error, String content) {
                        // Log.e("checkLogin==>fail", error.getMessage());
//						
                        GLog.d(TAG, "login fail");
                        if (userName != null && userPwd != null) {
                            iSmartUser iUser = new iSmartUser();
                            iUser.setPassword(userPwd);
                            iUser.setUserName(userName);
                            isapp.setIsmartuser(iUser);
                            Message msg = new Message();
                            msg.what = SplashActivity.IS_LOGIN;
                            mHandler.sendMessage(msg);
                        } else {
                            Message msg = new Message();
                            msg.what = SplashActivity.NOT_LOGIN;
                            mHandler.sendMessage(msg);

                        }

                    }

                    @Override
                    public void onSuccess(int statusCode, String content) {
                        try {
                            boolean resultflag = new JSONObject(content)
                                    .getBoolean("result");

                            if (!resultflag) {
                                // result fase
                                Message msg = new Message();
                                msg.what = SplashActivity.NOT_LOGIN;
                                mHandler.sendMessage(msg);
                                GLog.d(TAG, "result false");
//								loginView();
                                return;
                            } else {

                            }

                            JSONObject returnval = new JSONObject(content)
                                    .getJSONObject("value");

                            // 是否允许登录
                            boolean allowLogin = returnval
                                    .getBoolean("AllowLogin");
                            if (allowLogin) {
                                RTPushDoorBellManager.getInstance(getApplicationContext()).subscribeAll();
                                GLog.i("  userName:"  + userName);
                                RemoteServerManger.getInstance().register(userName, SDKUtils.getCID(SplashActivity.this), new ICallback() {
                                    @Override
                                    public void callbackSuccess(String jsonString) {
                                        GLog.i("  jsonString:"  + jsonString);
                                        ServiceAuthorityManger.getInstance(getApplication()).checkVendor();
                                    }

                                    @Override
                                    public void callbackFail(int code, String err) {
                                        GLog.i("  err:"  + err);
                                    }
                                });
                                // 记录用户
                                if (roomList != null && roomList.size() > 0) { //本地有数据
                                    GLog.d(TAG, "splash 本地有数据");
                                    iSmartUser iUser = new iSmartUser();
                                    iUser.setEmail(returnval.getString("Email"));
                                    iUser.setLoginID(returnval.getString("id"));
                                    iUser.setPassword(userPwd);
                                    iUser.setUserName(userName);
                                    isapp.setIsmartuser(iUser);
                                    Message msg = new Message();
                                    msg.what = SplashActivity.IS_LOGIN;
                                    mHandler.sendMessage(msg);
                                } else {  //没有
                                    GLog.d(TAG, "splash 本地无数据");
                                    Editor editor = mPreferences.edit();
                                    editor.putString("login_password", null);
                                    Message msg = new Message();
                                    msg.what = SplashActivity.NOT_LOGIN;
                                    mHandler.sendMessage(msg);
//									loginView();

                                }
                                GLog.d(TAG, "splashActivity login");

                            } else {
                                // Toast.makeText(getBaseContext(),
                                // R.string.login_fail, 6000).show();
                                Message msg = new Message();
                                msg.what = SplashActivity.NOT_ALLOW_LOGIN;
                                mHandler.sendMessage(msg);
                                Editor editor = mPreferences.edit();
                                editor.putString("login_password", null);
                                editor.commit();
//								loginView();
                                GLog.d(TAG, "splashActivity login fail");
                                return;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Editor editor = mPreferences.edit();
                            editor.putString("login_password", null);
//							loginView();
                            Message msg = new Message();
                            msg.what = SplashActivity.NOT_LOGIN;
                            mHandler.sendMessage(msg);
                        }
                    }

                });

    }

    public void login() {
        Intent intent;
        intent = new Intent(SplashActivity.this, LoginActivity.class);
        SplashActivity.this.startActivityForResult(intent, 5);
    }

    public void loginView() {

        Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
        SplashActivity.this.startActivityForResult(intent, 1);
    }


    public void checkAppVersion() {
        String locale = getResources().getConfiguration().locale.getCountry();
        if (locale.equals("CN")) {
            isapp.AppVersion = "CN";
        } else {
            isapp.AppVersion = "EN";
        }
    }

}
