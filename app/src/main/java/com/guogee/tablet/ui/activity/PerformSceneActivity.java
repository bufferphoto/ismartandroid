package com.guogee.tablet.ui.activity;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.guogee.tablet.manager.SceneActionManager;
import com.guogee.tablet.ui.widge.BaseSwipeListViewListener;
import com.guogee.ismartandroid2.device.Aircondition;
import com.guogee.ismartandroid2.device.RGBLight;
import com.guogee.ismartandroid2.device.RGBYWLightControl;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.model.GatewayInfo;
import com.guogee.ismartandroid2.model.IRKeyTag;
import com.guogee.ismartandroid2.model.Room;
import com.guogee.ismartandroid2.model.Scene;
import com.guogee.ismartandroid2.model.SceneAction;
import com.guogee.ismartandroid2.networkingProtocol.DeviceCMD;
import com.guogee.ismartandroid2.networkingProtocol.RGBLightCMD;
import com.guogee.ismartandroid2.networkingProtocol.DeviceType;
import com.guogee.sdk.scene.ActionData;
import com.guogee.sdk.scene.ActionListener;
import com.guogee.sdk.scene.SceneAdapter;
import com.guogee.sdk.scene.SceneRunner;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.ui.widge.CustomeImageView;
import com.guogee.tablet.ui.widge.DragSortController;
import com.guogee.tablet.ui.widge.DragSortListView;
import com.guogee.tablet.ui.widge.MenuHorizontalScrollView;
import com.guogee.tablet.ui.widge.MenuHorizontalScrollView.MenuHorizontalScrollViewListener;
import com.guogee.ismartandroid2.utils.ConvertUtils;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.tablet.utils.ImageUtil;
import com.guogee.tablet.utils.SystemUtil;


/**
 * 场景执行
 *
 * @author xieguangwei
 * @ClassName: PerformSceneActivity.java
 * @Description: TODO
 * @date 2015年12月22日
 */
public class PerformSceneActivity extends BaseActivity implements
        ActionListener, SceneAdapter, DragSortListView.DropListener {
    private static final String TAG = "PerformSceneActivity";
    private SceneRunner mRunner;
    private List<ActionView> sceneDataList;
    private TextView sceneTip;
    private DragSortListView mSwipeListView;
    private SwipeAdapter mAdapter;
    private Map<Integer, Integer> dataMap;//动作执行结果

    private Scene mScene;

    private boolean startNow = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        GLog.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setFinishOnTouchOutside(true);
        setContentView(R.layout.scene_result);
        setActivityWidthAndHeight();
        mScene = (Scene) getIntent().getExtras().getSerializable("scene");
        GLog.i(TAG, "getId:" + mScene.getId());
        dataMap = (Map<Integer, Integer>) getIntent().getExtras().getSerializable("devicesData");
        if (dataMap != null) {

        }
        GLog.i("mtmt", "-----PerformSceneActivity--->SceneName:" + mScene.getName());

        mRunner = new SceneRunner();
        mRunner.setListener(this);
        mRunner.setAdapter(this);
        startNow = getIntent().getExtras().getBoolean("startNow", false);

        init();

        runSceneAction();
    }

    private void setActivityWidthAndHeight() {
        WindowManager m = getWindowManager();
        Display d = m.getDefaultDisplay(); // 为获取屏幕宽、高
        WindowManager.LayoutParams p = getWindow().getAttributes();
        p.height = (int) (d.getHeight() * 0.7); // 高度设置为屏幕的0.3
        p.width = (int) (d.getWidth() * 0.3); // 宽度设置为屏幕的0.7
        getWindow().setAttributes(p);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GLog.i(TAG, "onStart");
        checkVisibeState();
    }


    @Override
    protected void onStop() {
        super.onStop();

//		for (int i = 0; i < sceneDataList.size(); i++) {
//			if(sceneDataList.get(i).editing){
//				mSwipeListView.closeAnimate(i);
//			}
//		}
    }

    @Override
    public void onResume() {
        super.onResume();

        if (startNow) {
            startNow = false;
        }
        if (sceneDataList.size() > 0) {
            //updatePerformSceneBtn(true);
        } else {
            //updatePerformSceneBtn(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    private void runSceneAction() {
        mAdapter.notifyDataSetChanged();
        updatePerformSceneBtn(false);
        mSwipeListView.setDragEnabled(false);
        new Thread(new ActionExecuteRunable()).start();

    }


    class ActionExecuteRunable implements Runnable {

        @Override
        public void run() {
            // TODO Auto-generated method stub

            mRunner.execute(false);

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        sceneDataList.clear();
        sceneDataList.addAll(SceneActionManager.getInstance(this).loadActions(mScene));
        mAdapter.notifyDataSetChanged();
    }

    /**
     * init
     */
    private void init() {

        initView();
    }


    /**
     * 初始化View
     */
    private void initView() {
        sceneDataList = SceneActionManager.getInstance(this).loadActions(mScene);
        GLog.i(TAG, "action count:" + sceneDataList.size());
        if (dataMap != null) {
            for (int i = 0; i < sceneDataList.size(); i++) {
                sceneDataList.get(i).status.setSendState(ActionData.STATUS_FAILED);
            }
            for (int i = 0; i < sceneDataList.size(); i++) {
                int id = sceneDataList.get(i).action.getActionId();
                if (dataMap.containsKey(id)) {
                    sceneDataList.get(i).status.setSendState(dataMap.get(id));
                }
            }
        }
        sceneTip = (TextView) findViewById(R.id.scene_tip);
        mSwipeListView = (DragSortListView) findViewById(R.id.example_lv_list);
        mAdapter = new SwipeAdapter(this, R.layout.package_row, sceneDataList, mSwipeListView);
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        mSwipeListView.setAdapter(mAdapter);
        mSwipeListView.setDropListener(this);
        DragSortController c = new DragSortController(mSwipeListView);
        mSwipeListView.setFloatViewManager(c);
        mSwipeListView.setOnTouchListener(c);
    }

    /**
     * 检查显示
     */
    private void checkVisibeState() {
        if (sceneDataList.size() > 0) {
            mSwipeListView.setVisibility(View.VISIBLE);
            sceneTip.setVisibility(View.GONE);
        } else {
            mSwipeListView.setVisibility(View.GONE);
            sceneTip.setVisibility(View.VISIBLE);
        }

        //updatePerformSceneBtn(checkCanBeSend(sceneDataList));
    }

    /**
     * 检查是否有可发送的
     *
     * @param deviceDatas
     * @return
     */
    private boolean checkCanBeSend(List<ActionView> deviceDatas) {
        boolean result = true;

        for (ActionView deviceData : deviceDatas) {
            if (deviceData.editing) {
                return false;
            }
        }


        return result;
    }

    /**
     * 更新btn
     *
     * @param canBeSend
     */
    private void updatePerformSceneBtn(boolean canBeSend) {
        if(canBeSend){
            PerformSceneActivity.this.finish();
        }
    }

    class TestBaseSwipeListViewListener extends BaseSwipeListViewListener {

        @Override
        public void onDismiss(int[] reverseSortedPositions) {
            ActionView deviceData = sceneDataList.remove(reverseSortedPositions[0]);
            mScene.removeAction(PerformSceneActivity.this, deviceData.action, null);


            checkVisibeState();
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onOpened(int position, boolean toRight) {
            // TODO Auto-generated method stub
            super.onOpened(position, toRight);
            if (sceneDataList != null) {
                sceneDataList.get(position).editing = true;
                updatePerformSceneBtn(false);
            }
            ;

        }

        @Override
        public void onClosed(int position, boolean fromRight) {
            // TODO Auto-generated method stub
            super.onClosed(position, fromRight);
            if (sceneDataList != null) {
                sceneDataList.get(position).editing = false;
                checkVisibeState();
            }

        }
    }

    private class DragController extends DragSortController {

        private SwipeAdapter mAdapter;
        DragSortListView mDslv;

        public DragController(DragSortListView dslv, SwipeAdapter adapter) {
            super(dslv, R.id.hsv, DragSortController.ON_DOWN, 0);
            // setRemoveEnabled(false);
            mDslv = dslv;
            mAdapter = adapter;

        }

        @Override
        public int startDragPosition(MotionEvent ev) {
            int res = super.dragHandleHitPosition(ev);
            int width = mDslv.getWidth();

            if ((int) ev.getX() > width / 2) {
                return res;
            } else {
                return DragSortController.MISS;
            }
        }

        @Override
        public View onCreateFloatView(int position) {

            View v = mAdapter.getView(position, null, mDslv);
            return v;
        }

        @Override
        public void onDragFloatView(View floatView, Point floatPoint, Point touchPoint) {
            final int first = mDslv.getFirstVisiblePosition();
            final int lvDivHeight = mDslv.getDividerHeight();

            View div = mDslv.getChildAt(0 - first);
            if (div != null) {
                final int limit = div.getBottom() + lvDivHeight;
                if (floatPoint.y <= limit) {
                    floatPoint.y = limit;
                }
            }
        }

        @Override
        public void onDestroyFloatView(View floatView) {
            //do nothing; block super from crashing
        }

    }

    public class SwipeAdapter extends ArrayAdapter<ActionView> implements MenuHorizontalScrollViewListener {

        private LayoutInflater mInflater;
        private DragSortListView mSwipeListView;
        private List<ActionView> dataList;
        private int mScreenWidth;
        private MenuHorizontalScrollView preScrollView;


        public SwipeAdapter(Context context, int textViewResourceId,
                            List<ActionView> dataList,
                            DragSortListView mSwipeListView) {
            super(context, textViewResourceId, dataList);

            this.dataList = dataList;
            this.mSwipeListView = mSwipeListView;
            this.mInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            mScreenWidth = (int) dm.widthPixels / 10 * 3;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            ActionView deviceData = dataList.get(position);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.package_row, parent, false);
                holder = new ViewHolder();
                holder.layout = (MenuHorizontalScrollView) convertView.findViewById(R.id.hsv);
                holder.mSceneImg = (CustomeImageView) convertView.findViewById(R.id.scene_img);
                holder.mDeviceText = (TextView) convertView.findViewById(R.id.deviceName);
                holder.mRoomText = (TextView) convertView.findViewById(R.id.roomName);
                holder.mBackDelete = (Button) convertView.findViewById(R.id.del_btn);
                holder.mState = (ImageView) convertView.findViewById(R.id.perform_scene_state);
                holder.mProgressBar = (ProgressBar) convertView.findViewById(R.id.progressBar1);
                holder.mOvertime = (TextView) convertView.findViewById(R.id.over_time);
                holder.mActionState = (TextView) convertView.findViewById(R.id.action_state);
                holder.mActionImgState = (ImageView) convertView.findViewById(R.id.action_state_img);
                View content = convertView.findViewById(R.id.ll_content);
                LayoutParams lp = content.getLayoutParams();
                lp.width = mScreenWidth;
                holder.layout.setMenuId(R.id.del_btn);
                holder.layout.setListener(this);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            holder.mBackDelete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActionView av = dataList.remove(position);
                    mScene.removeAction(PerformSceneActivity.this, av.action, null);
                    notifyDataSetChanged();
                }
            });
            if (mRunner.isRunning())
                holder.mBackDelete.setEnabled(false);
            else
                holder.mBackDelete.setEnabled(true);
            holder.layout.scrollTo(0, 0);
            holder.mDeviceText.setText(deviceData.deviceInfo.getName());
            Room room = RoomManager.getInstance(PerformSceneActivity.this).getRoomById(deviceData.deviceInfo.getRoomId());
            String roomName = room.getName();
            if (roomName.contains("guogee_")) {
                roomName = replaceString(roomName);
            }
            holder.mRoomText.setText(roomName);
            GLog.i(TAG, "devicetype:" + deviceData.action.getDevType());
            if (deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SWITCH_ONE ||//多路开关
                    deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SWITCH_TWO ||
                    deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SWITCH_THREE ||
                    deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SWITCH_FOUR ||
                    deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SWITCH_ONES ||//多路开关
                    deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SWITCH_TWOS ||
                    deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SWITCH_THREES ||
                    deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SWITCH_FOURS) {

                holder.mSceneImg.setImageResource(ImageUtil.getDeviceIconByType(deviceData.deviceInfo.getRctype()));
                holder.mDeviceText.setText(deviceData.deviceInfo.getName().split("&&")[Integer.parseInt(deviceData.action.getData())]);

            } else if (deviceData.action.getDevType() == DeviceType.DEVICE_SECURITY) {
                holder.mDeviceText.setText(R.string.smart_security);
                holder.mSceneImg.setImageResource(ImageUtil.getDeviceIconByType(DeviceType.SECURITY));

                GatewayInfo boxes = room.getGateway(PerformSceneActivity.this);
                if (boxes != null) {
                    holder.mRoomText.setText(boxes.getDeviceInfo().getName());
                }


            } else {
                holder.mSceneImg.setImageResource(ImageUtil.getDeviceIconByType(deviceData.deviceInfo.getRctype()));


            }


            if (deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_RGBLIGHT &&
                    deviceData.action.getCmd() == RGBLightCMD.SETCOLOR) {
                holder.mActionState.setVisibility(View.GONE);
                holder.mActionImgState.setVisibility(View.VISIBLE);

                byte[] data = ConvertUtils.boxAddrStringToByteArray(deviceData.action.getData());
                if (deviceData.deviceInfo.getRctype().equals(DeviceType.LIGHT_FALG)) {
                    int[] color = RGBLight.calcColor(data);
                    holder.mActionImgState.setBackgroundColor(Color.rgb(color[0], color[1], color[2]));
                } else if (deviceData.deviceInfo.getRctype().equals(DeviceType.YWLIGHT_FALG) || deviceData.deviceInfo.getRctype().equals(DeviceType.YWLIGHTCONTROL_FALG)) { //黄白灯
                    int yellownum = ((data[4] & 0xff) * 256 + (data[5] & 0xff));
                    int whitenum = ((data[6] & 0xff) * 256 + (data[7] & 0xff));
                    int total = yellownum + whitenum;
                    float rate = total / 512.0F;
                    float whiteperc = (float) whitenum / total;
                    int blue = (int) (yellownum / rate);
                    int alf = Math.round(rate * 255);
                    int color = Color.argb(alf, 0xFF, 0xF1, 255 * (int) (1 - blue / 512.0F));
                    holder.mActionImgState.setBackgroundColor(color);
                } else if (deviceData.deviceInfo.getRctype().equals(DeviceType.LIGHT_CONTROL_2_4g_FIVE) || deviceData.deviceInfo.getRctype().equals(DeviceType.LIGHT_CONTROL_2_4g_TWO)) {
                    int[] colors = RGBYWLightControl.calcColor(data);
                    int red = colors[0];
                    int green = colors[1];
                    int blue = colors[2];
                    int yellownum = colors[3];
                    int whitenum = colors[4];

                    int color = Color.WHITE;
                    if (red == 0 && green == 0 && blue == 0) {
                        float point = (yellownum / (float) 255.0);
                        if (yellownum < 5) {
                            color = Color.WHITE;
                        } else {
                            int blue1 = (int) ((255 - 165) * point);
                            int green1 = 0xF1;
                            int red1 = 255;
                            color = Color.argb((int) (255 * point), red1, green1, 255 - blue1);
                        }
                    } else {
                        color = Color.rgb(red, green, blue);
                    }
                    holder.mActionImgState.setBackgroundColor(color);
                }
            } else {
                holder.mActionState.setVisibility(View.VISIBLE);
                holder.mActionImgState.setVisibility(View.GONE);
                if (deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_AIRCOND) {  //空调特殊处理
                    String str = "";
                    int key = Integer.parseInt(deviceData.action.getData());
                    switch (key) {
                        case IRKeyTag.POWER_OFF:
                            str = getResources().getString(R.string.close);
                            break;
                        default:
                            if (key > 200) {
                                str = key - 200 + "℃";
                            } else {
                                str = (key - 100) + "℃";
                            }

                            break;
                    }
                    holder.mActionState.setText(str);
                } else if (deviceData.deviceInfo.getDevicetype() == DeviceType.DEVICE_SECURITY) {
                    if (deviceData.action.getCmd() == DeviceCMD.TURN_ON) {
                        holder.mActionState.setText(R.string.set_up_defence);
                    } else {
                        holder.mActionState.setText(R.string.remove_defence);
                    }
                } else {
                    holder.mActionState.setText(replaceString(deviceData.action.getActionName()));
                }

            }
            if (deviceData.notStudy || deviceData.noIRBox) {

                if (deviceData.noIRBox) {
                    holder.mProgressBar.setVisibility(View.INVISIBLE);
                    holder.mOvertime.setVisibility(View.INVISIBLE);
                    holder.mState.setBackgroundResource(R.drawable.zq_setting_erro_icon);
                    holder.mState.setVisibility(View.VISIBLE);
                } else if (deviceData.notStudy) {
                    holder.mState.setVisibility(View.INVISIBLE);
                    holder.mProgressBar.setVisibility(View.INVISIBLE);
                    holder.mOvertime.setText(R.string.did_not_learn);
                    holder.mOvertime.setVisibility(View.VISIBLE);
                }
            } else {
                stateSeletor(holder, deviceData.status.getSendState());
            }
            GLog.i(TAG, "action order:" + deviceData.action.getOrders());

            return convertView;
        }

        /**
         * 状态选 择器
         *
         * @param holder
         * @param state
         */
        private void stateSeletor(ViewHolder holder, int state) {

            switch (state) {
                case ActionData.STATUS_SENDING:
                    holder.mOvertime.setVisibility(View.INVISIBLE);
                    holder.mState.setVisibility(View.INVISIBLE);
                    holder.mProgressBar.setVisibility(View.VISIBLE);
                    break;
                case ActionData.STATUS_SUCCESS:
                case ActionData.STATUS_FAILED:
                    holder.mProgressBar.setVisibility(View.INVISIBLE);
                    holder.mOvertime.setVisibility(View.INVISIBLE);
                    holder.mState.setBackgroundResource((state == ActionData.STATUS_SUCCESS) ? R.drawable.zq_setting_ok_icon : R.drawable.zq_setting_erro_icon);
                    holder.mState.setVisibility(View.VISIBLE);
                    break;
                case ActionData.STATUS_INIT:
                default:
                    holder.mState.setVisibility(View.INVISIBLE);
                    holder.mProgressBar.setVisibility(View.INVISIBLE);
                    holder.mOvertime.setVisibility(View.INVISIBLE);
                    break;
            }
        }


        class ViewHolder {
            MenuHorizontalScrollView layout;
            TextView mDeviceText;
            TextView mRoomText;
            Button mBackDelete;
            CustomeImageView mSceneImg;
            ImageView mState;
            ProgressBar mProgressBar;
            TextView mOvertime;
            TextView mActionState;
            ImageView mActionImgState;
        }


        @Override
        public void onShowMenu(MenuHorizontalScrollView view) {

            if (preScrollView != null && preScrollView != view) {
                preScrollView.hideMenu();
            }
            preScrollView = view;
        }
    }


    /**
     * 替换，房间和场景有些是数据库中
     *
     * @return
     */
    private String replaceString(String name) {


        return SystemUtil.getStringByName(this, name);
    }


    public static class ActionView {
        public Device deviceInfo;
        public SceneAction action;
        public ActionData status;
        public boolean editing;
        public String cmdData;
        public boolean notStudy;
        public boolean noIRBox;
    }


    @Override
    public void onActionStateChange(final ActionData action) {
        GLog.i(TAG, "onActionStateChange,state:" + action.getSendState() + "---:" + action.getDeviceType() + "---:" + action.getData());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
                saveResult(action);
                if (!mRunner.isRunning()) {
                    updatePerformSceneBtn(true);
                    mSwipeListView.setDragEnabled(true);
                }
            }
        });


    }

    private void saveResult(ActionData action) {
        if (action.getDeviceType() == DeviceType.DEVICE_AIRCOND && action.getSendState() == 2) {//空调执行成功
            String mac = action.getDeviceMac().toLowerCase(Locale.CHINA);
            SharedPreferences mSharedPreferences = getSharedPreferences("CONFIG", MODE_PRIVATE);
            int key = Integer.valueOf(action.getData());
            if (key == IRKeyTag.POWER_OFF) {
                mSharedPreferences.edit().putInt(mac + "airPowerStatus", Aircondition.AirconditionStatus.POWER_OFF).commit();
            } else {
                if ((key > 215 && key < 228)) {//制冷温度
                    mSharedPreferences.edit().putInt(mac + "airTemperature", (key - 200)).commit();
                    mSharedPreferences.edit().putInt(mac + "airMode", Aircondition.AirconditionStatus.MODE_COLD).commit();
                    mSharedPreferences.edit().putInt(mac + "airPowerStatus", Aircondition.AirconditionStatus.POWER_ON).commit();
                } else if ((key > 115 && key < 128)) {//制热温度
                    mSharedPreferences.edit().putInt(mac + "airTemperature", (key - 100)).commit();
                    mSharedPreferences.edit().putInt(mac + "airMode", Aircondition.AirconditionStatus.MODE_WARM).commit();
                    mSharedPreferences.edit().putInt(mac + "airPowerStatus", Aircondition.AirconditionStatus.POWER_ON).commit();
                }
            }
            GLog.i(TAG, "ir key:" + key + " mac:" + mac + " state:" + mSharedPreferences.getInt(mac + "airPowerStatus", 0));
        }
    }

    @Override
    public int getActionCount() {
        return sceneDataList.size();
    }

    @Override
    public ActionData getActionData(int i) {
        return sceneDataList.get(i).status;
    }

    @Override
    public void drop(int from, int to) {
//        GLog.i(TAG, "-------from =" + from + "-----to= " + to);
        if (!mRunner.isRunning()) {
            ActionView av = sceneDataList.remove(from);
            sceneDataList.add(to, av);

            for (int i = 0; i < sceneDataList.size(); i++) {
                SceneAction sceneAction = sceneDataList.get(i).action;
                sceneAction.setOrders(i + 1);
                mScene.updateAction(PerformSceneActivity.this, sceneAction, null);
            }
            mAdapter.notifyDataSetChanged();
        }
    }
}

