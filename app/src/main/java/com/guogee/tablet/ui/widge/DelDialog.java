package com.guogee.tablet.ui.widge;


import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.mt.mediaplay.R;

/**
 * Created by mt on 2017/5/22.
 */

public class DelDialog extends Activity {
    private Button btnOK = null;
    private Button btnCancle = null;
    private TextView tipText = null;
    public static String EXTRA_IS_EXIT = "isExit";
    public static int RESULTCODE = 1;
    protected static boolean isExit;
    private String title = "";
    private String sureText = "";
    private String cancleText="";
    private boolean finishNow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.del_dialog);

        Bundle bundle=getIntent().getExtras();
        /** 获取Bundle的信息 **/
        if(bundle!=null){
            title    = bundle.getString("replaceTip");
            sureText = bundle.getString("sureText");
            finishNow = bundle.getBoolean("finishNow",false);
            cancleText=bundle.getString("cancleText");
        }

        init();
        if (finishNow) {
            setResult(RESULTCODE);
            finish();
        }
    }

    private void init() {
        btnOK = (Button) findViewById(R.id.btn_sure);
        btnCancle = (Button) findViewById(R.id.btn_cancel);

        btnOK.setOnClickListener(mEventListner);
        btnCancle.setOnClickListener(mEventListner);

        tipText = (TextView) findViewById(R.id.tip_text);
        if(title!=null){
            tipText.setText(title);
        }
        if(sureText != null){
            btnOK.setText(sureText);
        }
        if(cancleText!=null){
            btnCancle.setText(cancleText);
        }
    }

    private android.view.View.OnClickListener mEventListner = new android.view.View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_sure:
                    if(title!=null){
                        setResult(RESULTCODE);
                        Log.d("sky","coming deldialog");
                    }
                    finish();
                    break;
                case R.id.btn_cancel:
                    finish();
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void finish() {
        overridePendingTransition(0, 0);
        super.finish();

    }
}
