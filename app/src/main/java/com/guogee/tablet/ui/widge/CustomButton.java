package com.guogee.tablet.ui.widge;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.tablet.utils.LruCacheManager;
import com.example.mt.mediaplay.R;

public class CustomButton extends ImageButton
{

/*	private Bitmap normalResource;
	private Bitmap pressResource;*/
	private int normalResource;
	private int pressResource;
	private Context context;
	private int paintWidth;
	private int paintHeight;
	private boolean isPressed = false;
	private boolean isFirst = true;
	
	public interface CustomerOnClickListener{
		public void onClick(View v);
	}

	public CustomButton(Context context) {
		  super(context);
		 }
	
	public CustomButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.context = context;
		TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
		normalResource = t.getResourceId(R.styleable.CustomButton_backgroundRssource, R.drawable.zq_public_switch_btn_c);
		pressResource = t.getResourceId(R.styleable.CustomButton_backgroundPressResource, R.drawable.zq_public_switch_btn_d);
		Bitmap bitmap = LruCacheManager.getInstance(context).get(pressResource+"");
		paintWidth = bitmap.getWidth();
		paintHeight = bitmap.getHeight();
		t.recycle();
	}
	
	 public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
		  super(context, attrs, defStyleAttr);
		  this.context = context;
			TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
			normalResource = t.getResourceId(R.styleable.CustomButton_backgroundRssource, R.drawable.zq_public_switch_btn_c);
			pressResource = t.getResourceId(R.styleable.CustomButton_backgroundPressResource, R.drawable.zq_public_switch_btn_d);
			Bitmap bitmap = LruCacheManager.getInstance(context).get(pressResource+"");
			paintWidth = bitmap.getWidth();
			paintHeight = bitmap.getHeight();
			t.recycle();
	}
	 
	 public void setImageResource(int resourceId,int pressResourceId){
		normalResource = resourceId;
		pressResource = pressResourceId;
		this.invalidate();
	 }

	@Override
	protected void onDraw(Canvas canvas)
	{
		// TODO Auto-generated method stub
	//	super.onDraw(canvas);
		Paint mPaint = new Paint();
		mPaint.setFlags(Paint.FILTER_BITMAP_FLAG);
		if (isPressed) {
			canvas.drawBitmap(LruCacheManager.getInstance(context).get(pressResource+""), 0, 0, mPaint);
		}else {
			canvas.drawBitmap(LruCacheManager.getInstance(context).get(normalResource+""), 0, 0, mPaint);
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		// TODO Auto-generated method stub
	//	super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		//设置该View的宽高为图片的宽高
		setMeasuredDimension(paintWidth, paintHeight);  
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{		
		// TODO Auto-generated method stubevent		
		int action = event.getAction();
		GLog.v("CustomButton", "onTouchEvent:"+action);
		switch(action){
		case MotionEvent.ACTION_DOWN:
			if (!isPressed) {
				GLog.v("CustomButton", "isPressed:"+true);
				isPressed = true;
				this.invalidate();
				
			}	
			super.onTouchEvent(event);
			break;
		case MotionEvent.ACTION_OUTSIDE:
			if (isPressed) {
				GLog.v("CustomButton", "点击区域无效");
				isPressed = false;
				this.invalidate();
			}	
			super.onTouchEvent(event);
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			if (isPressed) {
				GLog.v("CustomButton", "isPressed:"+false);
				isPressed = false;
				this.invalidate();
			}
			super.onTouchEvent(event);
			break;
		}	
		return true;
		
	}
}
