package com.guogee.tablet.ui.widge;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.guogee.tablet.iSmartApplication;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.utils.GLog;


public class CustomDialog extends Activity {

    protected static final String TAG = "CustomDialog";
    public static final String ATTR_TARGET = "target";
    public static final int TARGET_LOCK_HISTORY = 1;
    public static final int RT_DOORBELL = 2;
    public static final int LOCK_CONFIG_CLEAR = 3;
    private int mTarget;
    private Button btnOK = null;
    private Button btnCancle = null;
    private TextView tipText = null;
    public static String EXTRA_IS_EXIT = "isExit";
    public static int RESULTCODE = 1;
    protected static boolean isExit;
    private String title = "";
    private String sureText = "";
    private String cancleText = "";
    private LinearLayout fliptiptext;
    private View line;
    private TextView headTitleView;
    private String headTitle;
    private boolean isUpdateVersion = false;
    private boolean isNeedIntentPowerControl = false;
    private boolean isNeedIntentAlermHistory = false;
    iSmartApplication isapp;
    private SharedPreferences prefs;
    private String mData;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GLog.i(TAG, " ---  onCreate-----");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.customer_dialog);
        isapp = (iSmartApplication) getApplication();
        prefs = getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        initView();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        GLog.i(TAG, " ---  onNewIntent-----");
        bundle = intent.getExtras();
    }


    @Override
    protected void onResume() {
        super.onResume();
        GLog.i(TAG, " ---  onResume-----");
        initData();
    }

    private void initView(){
        btnOK = (Button) findViewById(R.id.btn_sure);
        btnCancle = (Button) findViewById(R.id.btn_cancel);

        btnOK.setOnClickListener(mEventListner);
        btnCancle.setOnClickListener(mEventListner);

        fliptiptext = (LinearLayout) findViewById(R.id.button_view);

        tipText = (TextView) findViewById(R.id.tip_text);
        line = (View) findViewById(R.id.line);
        headTitleView = (TextView) findViewById(R.id.tip_text_title);
    }

    private void initData(){
        /** 获取Bundle的信息 **/
        if (bundle != null) {
            title = bundle.getString("replaceTip");
            sureText = bundle.getString("sureText");
            cancleText = bundle.getString("cancleText");
            headTitle = bundle.getString("headTitle");
            isUpdateVersion = bundle.getBoolean("isUpdateVersion");
            mData = bundle.getString("data");
            isNeedIntentPowerControl = bundle.getBoolean("isNeedIntentPowerControl", false);
            isNeedIntentAlermHistory = bundle.getBoolean("isNeedIntentAlermHistory", false);
            mTarget = bundle.getInt(ATTR_TARGET);

            GLog.i(TAG, " ---  title:" + title);
        }


        if (headTitle != null) {
            headTitleView.setText(headTitle);
        }
        if (title != null) {
            tipText.setText(title);
        }
        if (sureText == null || cancleText == null) {
            line.setVisibility(View.GONE);
        }
        if (sureText != null) {
            //	fliptiptext.setBackgroundResource(R.drawable.zq_public_top_bg);
            //	btnOK.setVisibility(View.GONE);
            btnOK.setText(sureText);
        } else {
            btnOK.setVisibility(View.GONE);
        }
        if (cancleText != null) {
            //		fliptiptext.setBackgroundResource(R.drawable.zq_public_top_bg);
            //		btnCancle.setVisibility(View.GONE);
            btnCancle.setText(cancleText);
        } else {
            btnCancle.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener mEventListner = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_sure:
                    if (title != null) {
                        setResult(RESULTCODE);
                        GLog.d("sky", "coming deldialog");
                    }
                    if (isUpdateVersion) {//更新
//					if(NetworkHelper.checkNetWork(isapp.getApplicationContext()) == 0 ){
//						Toast.makeText(getApplicationContext(), R.string.net_err, Toast.LENGTH_LONG).show();
//					}else {
//						int status = ISmartDownloadManager.queryDownloadStatus(isapp, prefs);
//						if(DownloadManager.ERROR_UNKNOWN == status){
//							Toast.makeText(getApplicationContext(), R.string.enable_download, Toast.LENGTH_LONG).show();
//						}
//						else if (DownloadManager.STATUS_RUNNING != status) {
//						if (DownloadManager.STATUS_RUNNING != ISmartDownloadManager.queryDownloadStatus(isapp, prefs)) {
//							//若是没有下载中,下载
//							ISmartDownloadManager.downloadManager(isapp, prefs);
//							Toast.makeText(getApplicationContext(), R.string.downloading, Toast.LENGTH_LONG).show();
//						}					
//					}								
//				 }
                        GLog.i(TAG, "install apk:" + mData);
                        SystemUtil.installApp(isapp, mData);
                    }
                   /* else if (isNeedIntentPowerControl) {
                        Intent intent = new Intent(CustomDialog.this,
                                PowerCaculatorActivity.class);
                        Bundle bundDevice = new Bundle();
                        bundDevice.putSerializable("deviceinfo", getIntent().getExtras().getSerializable("deviceinfo"));
                        intent.putExtras(bundDevice);
                        startActivity(intent);
                    } else if (isNeedIntentAlermHistory) {
                        Intent intent = new Intent(CustomDialog.this, SecurityAlermHistoryActivity.class);
                        startActivity(intent);
                    } else if (mTarget == TARGET_LOCK_HISTORY) {
                        Intent intent = new Intent(CustomDialog.this, SmartLockAlarmHistoryActivity.class);
                        startActivity(intent);
                    } else if (mTarget == RT_DOORBELL) {
                        Intent intent = new Intent(CustomDialog.this, DoorBellRTActivity.class);
                        Bundle bundDevice = new Bundle();
                        bundDevice.putSerializable("device", getIntent().getExtras().getSerializable("device"));
                        intent.putExtras(bundDevice);
                        startActivity(intent);
                    }*/
                    else if(mTarget == LOCK_CONFIG_CLEAR){
                        setResult(1);
                    }
                    finish();
                    break;
                case R.id.btn_cancel:
                    finish();
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
    }

}
