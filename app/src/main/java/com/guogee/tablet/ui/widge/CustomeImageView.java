package com.guogee.tablet.ui.widge;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.example.mt.mediaplay.R;
import com.guogee.tablet.utils.LruCacheManager;

public class CustomeImageView extends ImageView {
    private int normalResource = 0;
    private String imgPath;
    private Bitmap bitmap;
    private Context context;
    private int paintWidth;
    private int paintHeight;
    private int width;
    private int height;
    private Rect rect = new Rect(0, 0, width, height);
    Paint mPaint;

    public interface CustomerOnClickListener {
        public void onClick(View v);
    }

    public CustomeImageView(Context context) {
        super(context);
    }

    public CustomeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mPaint = new Paint();
        this.context = context;
        TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
        normalResource = t.getResourceId(R.styleable.CustomButton_backgroundRssource, R.drawable.zq_public_switch_btn_c);

        t.recycle();
    }

    public CustomeImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        mPaint = new Paint();

        TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
        normalResource = t.getResourceId(R.styleable.CustomButton_backgroundRssource, R.drawable.zq_public_switch_btn_c);
        Bitmap bitmap = getBitmap();
        paintWidth = bitmap.getWidth();
        paintHeight = bitmap.getHeight();
        t.recycle();
    }

    public void setImageResource(int resourceId, int pressResourceId) {
        normalResource = resourceId;
        bitmap = null;
        this.invalidate();
    }

    public void setImageResource(int resourceId) {
        normalResource = resourceId;
        bitmap = null;
        this.invalidate();
    }


    public void setImagePath(String path) {
        imgPath = path;
        normalResource = 0;
        bitmap = getBitmap();
        paintWidth = bitmap.getWidth();
        paintHeight = bitmap.getHeight();
//		 invalidate();
//		 GLog.i("", "onDraw,paintWidth:"+paintWidth);
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {

        mPaint.setFlags(Paint.FILTER_BITMAP_FLAG);

        if (bitmap == null || bitmap.isRecycled()) {

            canvas.drawBitmap(getBitmap(), null, rect, mPaint);
        } else {
            canvas.drawBitmap(bitmap, null, rect, mPaint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Bitmap bitmap = getBitmap();
        paintWidth = bitmap.getWidth();
        paintHeight = bitmap.getHeight();
        ScaleType st = this.getScaleType();
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) getLayoutParams();
        int lpw = lp.width;
        int lph = lp.height;
        int mode = MeasureSpec.getMode(widthMeasureSpec);
//		GLog.i("", "CustomeImageView,width mode:"+mode);
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);
        switch (mode) {
            case MeasureSpec.AT_MOST:
                if (lpw == LayoutParams.WRAP_CONTENT && paintWidth < w) {
                    width = paintWidth;
                } else {
                    width = w;
                }
                break;
            case MeasureSpec.UNSPECIFIED:
                width = paintWidth;
                break;
            case MeasureSpec.EXACTLY:
                width = w;
                break;
            default:
                break;
        }
        mode = MeasureSpec.getMode(heightMeasureSpec);
//		GLog.i("", "CustomeImageView,height mode:"+mode);

        switch (mode) {
            case MeasureSpec.AT_MOST:
                if (lph == LayoutParams.WRAP_CONTENT && paintHeight < h) {
                    height = paintHeight;
                } else {
                    height = h;
                }
                break;
            case MeasureSpec.UNSPECIFIED:
                if (width < paintWidth) {
                    height = (int) ((float) paintHeight / paintWidth * width);
                } else
                    height = paintHeight;
                break;
            case MeasureSpec.EXACTLY:
                height = h;
                break;
            default:

                break;
        }
        if (st == ScaleType.CENTER_CROP) {
            float sx = (float) width / paintWidth;
            float sy = (float) height / paintHeight;
            if (sx > 1) {
                paintHeight *= sx;
                paintWidth = width;

            }
        }

        int left = (width - paintWidth) / 2;
        int top = (height - paintHeight) / 2;

        int right = left + paintWidth;
        int bottom = top + paintHeight;
        if (paintWidth > width) {
            left = 0;
            top = 0;
            right = width;
            bottom = (int) ((float) paintHeight / paintWidth * width);
        }
        rect.set(left, top, right, bottom);
//		GLog.i("", "CustomeImageView,height :"+height + " width:"+ width + " paintw:"+paintWidth + " painth:"+paintHeight);
//		GLog.i("", "CustomeImageView,left:" + left+" top:"+top + " right:" + right + " bottom:" + bottom);
        setMeasuredDimension(width, height);

    }

    private Bitmap getBitmap() {
        String path = "";
        if (normalResource != 0) {
            path = normalResource + "";
        } else {
            path = imgPath;
        }
//		GLog.i("", "onDraw,path:"+path);
        return LruCacheManager.getInstance(context).get(path);
    }
}
