package com.guogee.tablet.camera;

import android.app.Activity;

import com.ipcamer.demo.WifiBean;
import com.tutk.IOTC.Camera;

import java.util.Calendar;


public abstract class AbstractCamera {
	public abstract void setCallBackInterface(CameraCallBackInterface callBackInterface);//设置回调
	public abstract void initCameraParams(Activity activity);//初始化摄像头
	public abstract void connectCamera(String uid, String username, String password);//连接摄像头
	public abstract void freeCamera(String strDID);//释放摄像头
	public abstract void startCameraVideo(String strDID, int streamType);//开始视频流接收
	public abstract void setControDirection(int direction);//摄像头移动方向
	public abstract void pauseCamera();//暂停
	public abstract void freeRegisterIOTCListener();//释放回调
	
	public abstract void setVdioFlip(int videoFlip);//设置图像翻转
	public abstract void startListening();//开启监听
	public abstract void startSpeaking();//开启语音
	public abstract void stopListening();//停止监听
	public abstract void stopSpeaking();//停止说话
	public abstract void setRecoringMode(int mode);//设置录像模式
	
	public abstract Camera getCamera();//
	
	public abstract void getCameraSystemParams();//过去摄像头系统参数
	
	public abstract void formateSDCard();//格式化SD卡
	public abstract void setCameraRecordingModel(int model);//设置录像模式
	public abstract void setCameraEventSensitive(int sensitive);//设置移动侦测事件敏感度
	public abstract void getCameraEventAlertSetting();//获取当前摄像头移动侦测事件敏感度
	public abstract void getCameraRecoringSetting();//获取当前摄像头录像模式
	public abstract void scanWIFI();//扫描WIFI
	public abstract void rebootCamera(String did);//重启摄像头
	public abstract void setWIFI(WifiBean wifiBean);
	public abstract void setCameraVideoQuality(int videoQuality);
	public abstract void getCurrentWIFI();
	public abstract void setTime(String did, int now, int tz,
                                 int ntp_enable, String ntp_svr);//TOTK 无法实现
	public abstract void getTime(String did);
	public abstract void setCameraPassword(String did, String username, String oldPwd, String newPwd);
	
	public abstract void searchEventVideoList(Calendar calendar);
}
