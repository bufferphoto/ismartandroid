package com.guogee.tablet.camera;

import android.graphics.Bitmap;

import com.tutk.IOTC.Camera;

public interface CameraCallBackInterface {
	public void callBackVideoBitmap(Bitmap bitmap);
	public void callBackConnectionInfo(int type);
	public void callBackIOCtrlData(Camera camera, int avChannel, int avIOCtrlMsgType, byte[] data);
	public void callBackWifiParams(String did, int enable, String ssid,
                                   int channel, int mode, int authtype, int encryp, int keyformat,
                                   int defkey, String key1, String key2, String key3, String key4,
                                   int key1_bits, int key2_bits, int key3_bits, int key4_bits,
                                   String wpa_psk);
	public void callBackWifiScanResult(String did, String ssid, String mac,
                                       int security, int dbm0, int dbm1, int mode, int channel, int bEnd, byte[] ssidByte);
	public void callBackSetSystemParamsResult(String did, int paramType,
                                              int result);
	public void callBackPPPPMsgNotifyData(String did, int type, int param);
	public void callBackDatetimeParams(String did, int now, int tz,
                                       int ntp_enable, String ntp_svr);
}
