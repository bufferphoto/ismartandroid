package com.guogee.tablet.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.guogee.tablet.iSmartApplication;
import com.guogee.sdk.db.DBTable;


public class DbHelper extends SQLiteOpenHelper {
    private volatile static DbHelper mInstance;
    public static String name = "iSmartDB.db";
    private static int version = 27;

    public DbHelper(Context context) {
        super(context, name, null, version);
        // TODO Auto-generated constructor stub
        SQLiteDatabase db = context.openOrCreateDatabase(name, Context.MODE_PRIVATE, null);
        db.close();
    }

    public static DbHelper getInstance() {
        if (mInstance == null) {
            synchronized (DbHelper.class) {
                if (mInstance == null) {
                    mInstance = new DbHelper(iSmartApplication.getApp());
                }
            }
        }
        return mInstance;
    }

    public static DbHelper getInstance(Context ctx) {
        if (mInstance == null) {
            synchronized (DbHelper.class) {
                if (mInstance == null) {
                    mInstance = new DbHelper(ctx);
                }
            }
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        ///////////////////////////////////////////////////////////////
        /*
         * 自定义表
		 */

        db.execSQL(CustomCollection.CREATE_SQL);

        ////////////////// color database /////////////////////

        String sql40 = "insert into colors (red, green, blue, white, perc, orders) values (239,255,226,0,1,0)";
        String sql41 = "insert into colors (red, green, blue, white, perc, orders) values (236,108,135,0,1,1)";
        String sql42 = "insert into colors (red, green, blue, white, perc, orders) values (44,248,150,0,1,2)";
        String sql43 = "insert into colors (red, green, blue, white, perc, orders) values (99,195,66,0,1,3)";

        String sql44 = "insert into colors (red, green, blue, white, perc, orders) values (137,248,44,0,1,4)";
        String sql45 = "insert into colors (red, green, blue, white, perc, orders) values (236,207,108,0,1,5)";
        String sql46 = "insert into colors (red, green, blue, white, perc, orders) values (248,126,44,0,1,6)";
        String sql47 = "insert into colors (red, green, blue, white, perc, orders) values (240,72,56,0,1,7)";

        db.execSQL(ColorsCollection.CREATE_SQL);
        db.execSQL(sql40);
        db.execSQL(sql41);
        db.execSQL(sql42);

        db.execSQL(sql43);
        db.execSQL(sql44);
        db.execSQL(sql45);
        db.execSQL(sql46);
        db.execSQL(sql47);


        ///////////////// time database ///////////////////////

        db.execSQL(TimesCollection.CREATE_SQL);
        ///////////////// time database ///////////////////////


        /**
         * @author Smither
         * @function Create Camera Table
         * @date 2014/9/9
         */

        db.execSQL(CameraColelction.CREATE_SQL);


        /**
         * 红外码缓存
         */

        db.execSQL(IrCodeCacheCollection.CREATE_SQL);

        /**
         * 红外码品牌缓存
         */

        db.execSQL(IrCodesBrandCacheCollection.CREATE_SQL);

        /**
         * @data 2015.4.16
         * 安防报警记录表
         */
        db.execSQL(SafetyDeviceAlarmHistoryCollection.CREATE_SQL);

        db.execSQL(SmartLockCollections.CREATE_SQL);
        db.execSQL(SmartLockAlarmHistoryTable.CREATE_SQL);
        db.execSQL(LockUserIDHelperCollection.CREATE_SQL);
        db.execSQL(SmartWallSwitchHelperCollection.CREATE_SQL);
        db.execSQL(DoorBellHelperCollection.CREATE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int verOld, int verNew) {
        switch (verOld) {
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                db.execSQL("drop table IF EXISTS " + CustomCollection.TABLE_NAME);
                db.execSQL(CustomCollection.CREATE_SQL);

                db.execSQL("drop table IF EXISTS " + CameraColelction.TABLE_NAME);
                db.execSQL(CameraColelction.CREATE_SQL);
            case 22:
                db.execSQL("drop table IF EXISTS " + SafetyDeviceAlarmHistoryCollection.TABLE_NAME);
                db.execSQL(SafetyDeviceAlarmHistoryCollection.CREATE_SQL);
                db.execSQL(SmartLockCollections.CREATE_SQL);
            case 23:
                db.execSQL(SmartLockAlarmHistoryTable.CREATE_SQL);
            case 24:
                db.execSQL(LockUserIDHelperCollection.CREATE_SQL);
            case 25:
                db.execSQL(SmartWallSwitchHelperCollection.CREATE_SQL);
            case 26:
                db.execSQL(DoorBellHelperCollection.CREATE_SQL);
            default:
                break;
        }
    }

    /**
     * @author Smither
     * @date 2015/3/30
     */
    public static class SqlCondition {
        public static final String AND = "and";
        public static final String OR = "or";
        public static final String DISTINCT = "DISTINCT";
    }

    public static class CustomCollection {
        public static final String TABLE_NAME = "custom";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String DEVICE_ID = "deviceId";
        public static final String TAG = "tag";
        public static final String X = "x";
        public static final String Y = "y";
        public static final String TYPE = "type";
        public static String CREATE_SQL = "CREATE TABLE IF NOT EXISTS custom ( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, " +
                "deviceId TEXT, " +
                "tag INTEGER NOT NULL," + //红外key
                "x INTEGER NOT NULL," +    // 水平第几格   	1为起点    ，水平6格
                "y INTEGER NOT NULL," +//垂直第几格  		1为起点
                "type INTEGER NOT NULL)";// 0 自带的， 1，自定义
    }


    public static class CameraColelction {
        public static final String TABLE_NAME = "camera";
        public static final String ID = "id";
        public static final String CAMERA_ID = "cameraId";
        public static final String LOGIN_NAME = "loginName";
        public static final String LOGIN_PWD = "loginPwd";
        public static final String RESERVE = "reserve";
        public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS camera ( "
                + "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                + "cameraId TEXT NOT NULL,"
                + "loginName TEXT NOT NULL,"
                + "loginPwd TEXT NOT NULL, " +
                "reserve TEXT)";
    }

    public static class IrCodeCacheCollection {
        public static final String TABLE_NAME = "IRCodesCache";
        public static final String CODE_ID = "CodeID";
        public static final String MODEL_ID = "ModelID";
        public static final String DISPLAY_NAME = "DisplayName";
        public static final String KEY_NAME = "KeyName";
        public static final String CODE_DATA = "CodeData";
        public static final String ENABLE = "Enable";
        public static final String CODE_LEN = "CodeLen";
        public static final String DEVICE_TYPE = "DeviceType";
        public static final String BRAND_ID = "brandId";
        public static final String IR_REMOTE_TYPE = "iremoteType";
        public static final String CREATE_SQL = "CREATE TABLE IRCodesCache (" +
                "CodeID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "ModelID INTEGER NOT NULL," +
                "DisplayName TEXT NOT NULL," +
                "KeyName INTEGER NOT NULL," +
                "brandId INTEGER NOT NULL," +
                "CodeData TEXT NOT NULL," +
                "Enable INTEGER NOT NULL," +
                "iremoteType TEXT, " +
                "CodeLen INTEGER NULL DEFAULT NULL," +
                "DeviceType TEXT NOT NULL)";
    }

    public static class IrCodesBrandCacheCollection {
        public static final String TABLE_NAME = "IRCodesBrandCache";
        public static final String CODE_ID = "CodeID";
        public static final String DISPLAY_NAME = "DisplayName";
        public static final String DEVICE_TYPE = "DeviceType";
        public static final String LANGUAGE = "language";
        public static final String IR_REMOTE_TYPE = "iremoteType";
        public static final String CREATE_SQL = "CREATE TABLE IRCodesBrandCache (" +
                "CodeID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "DisplayName TEXT NOT NULL," +
                "language TEXT, " +
                "iremoteType TEXT, " +
                "DeviceType TEXT NOT NULL)";
    }

//	public static class SchemaCollection{
//		public static final String TABLE_NAME = "schema";
//		public static final String ID = "id";
//		public static final String SCHEMA_NAME = "schemaName";
//		public static final String SCENCE_ID = "sceneId";
//		public static final String GATWAY_ID = "gatewayId";
//		public static final String DISTANCE = "distance";
//		public static final String TURN_ON = "trunOn";
//		public static final String CREATE_SQL = "CREATE TABLE schema ("+
//				 "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
//				 "schemaName TEXT NOT NULL,"+
//				 "sceneId TEXT,"+
//				 "gatewayId TEXT,"+
//				 "distance INTEGER NOT NULL," +
//				 "trunOn INTEGER NOT NULL)";
//	}

    public static class ColorsCollection {
        public static final String TABLE_NAME = "colors";
        public static final String ID = "id";
        public static final String RED = "red";
        public static final String GREEN = "green";
        public static final String BLUE = "blue";
        public static final String WHITE = "white";
        public static final String PERC = "perc";
        public static final String ORDERS = "orders";
        public static String CREATE_SQL = "CREATE TABLE IF NOT EXISTS colors ( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "red INTEGER, " +
                "green INTEGER, " +
                "blue INTEGER, " +
                "white INTEGER, " +
                "perc TEXT, " +
                "orders INTEGER NOT NULL)";
    }


    public static class TimesCollection {
        public static final String TABLE_NAME = "times";
        public static final String ID = "id";
        public static final String DEV_MAC = "devMac";
        public static final String TIME_OPEN = "timeOpen";
        public static final String TIME_CLOSE = "timeClose";
        public static final String ORDERS = "orders";
        public static String CREATE_SQL = "CREATE TABLE IF NOT EXISTS times ( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "devMac TEXT, " +
                "timeOpen TEXT, " +
                "timeClose TEXT, " +
                "orders INTEGER NOT NULL)";
    }


    public static class SafetyDeviceAlarmHistoryCollection {
        public static final String TABLE_NAME = "alarmHistory";
        public static final String ID = "id";
        public static final String USER_NAME = "userName";
        public static final String DEV_MAC = "devMac";
        public static final String TIME = "time";
        public static final String IS_READ = "read";
        public static final String DEV_NAME = "devName";
        public static final String DEV_TYPE = "devType";
        public static final String ROOM = "room";
        public static final String TYPE = "type";
        public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS alarmHistory ("
                + "id INTEGER NOT NULL PRIMARY KEY,"
                + "userName TEXT,"
                + "devMac TEXT,"
                + "time TEXT,"
                + "devType TEXT,"
                + "devName TEXT,"
                + "room TEXT,"
                + "type INTEGER,"
                + "read TEXT)";
    }

    public static class SmartLockCollections {
        public static final String TABLE_NAME = "smartLock";
        public static final String ID = "id";
        public static final String DEVICE_ID = "deviceId";
        public static final String USER_ID = "userId";
        public static final String PASSWORD = "pwd";
        public static final String LASTTIME = "lastTime";
        public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS smartLock ("
                + "id INTEGER NOT NULL PRIMARY KEY,"
                + "deviceId INTEGER NOT NULL,"
                + "userId INTEGER NOT NULL,"
                + "pwd TEXT NOT NULL,"
                + "lastTime INTEGER NOT NULL)";
    }

    public static class SmartLockAlarmHistoryTable {
        public static final String TABLE_NAME = "smartLockAlarmHistory";
        public static final String ID = "id";
        public static final String USER_NAME = "userName";
        public static final String DEV_MAC = "devMac";
        public static final String TIME = "time";
        public static final String IS_READ = "read";
        public static final String DEV_NAME = "devName";
        public static final String ROOM = "room";
        public static final String TYPE = "type";
        public static final String OPEN_METHOD = "openMethod";
        public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                + ID + " INTEGER NOT NULL PRIMARY KEY,"
                + USER_NAME + " TEXT,"
                + DEV_MAC + " TEXT,"
                + TIME + " TEXT,"
                + DEV_NAME + " TEXT,"
                + ROOM + " TEXT,"
                + TYPE + " INTEGER,"
                + OPEN_METHOD + " INTEGER,"
                + IS_READ + " INTEGER)";
    }

    public static class LockUserIDHelperCollection {
        public static final String TABLE_NAME = "lockUserIdIdentify";
        public static final String ID = "id";
        public static final String DEVICE_ADDR = "deviceAddr";
        public static final String LOCK_USER_ID = "lockUserId";
        public static final String USER_NAME = "userName";
        public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                + ID + " INTEGER NOT NULL PRIMARY KEY,"
                + DEVICE_ADDR + " TEXT NOT NULL,"
                + LOCK_USER_ID + " INTEGER NOT NULL,"
                + USER_NAME + " TEXT NOT NULL)";
    }

    public static class SmartWallSwitchHelperCollection {
        public static final String TABLE_NAME = "smartWallSwitchConfig";
        public static final String ID = "id";
        public static final String DEVICE_ID = "deviceId";
        public static final String NUM = "num";
        public static final String CONFIG_MODE = "configMode";
        public static final String VALUE = "value";
        public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                + ID + " INTEGER NOT NULL PRIMARY KEY,"
                + DEVICE_ID + " INTEGER NOT NULL,"
                + CONFIG_MODE + " INTEGER NOT NULL,"
                + NUM + " INTEGER NOT NULL,"
                + VALUE + " TEXT NOT NULL,"
                + "FOREIGN KEY(" + DEVICE_ID + ") REFERENCES " + DBTable.DevicesCollection.TABLE_NAME + "(" + DBTable.DevicesCollection.ID + ") ON DELETE CASCADE ON UPDATE CASCADE)";
    }

    public static class DoorBellHelperCollection {
        public static final String TABLE_NAME = "DoorBellConfig";
        public static final String ID = "id";
        public static final String SERVER_ID = "serverId";
        public static final String DEVICE_TYPE = "deviceType";
        public static final String MAC = "mac";
        public static final String FUN = "fun";
        public static final String PATH = "path";
        public static final String TIME = "time";
        public static final String DATA = "data";
        public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                + ID + " INTEGER NOT NULL PRIMARY KEY,"
                + SERVER_ID + " TEXT NOT NULL,"
                + DEVICE_TYPE + " TEXT NOT NULL,"
                + MAC + " TEXT NOT NULL,"
                + FUN + " INTEGER NOT NULL,"
                + PATH + " TEXT NOT NULL,"
                + TIME + " TEXT NOT NULL,"
                + DATA + " TEXT NOT NULL,"
                + "FOREIGN KEY(" + MAC + ") REFERENCES " + DBTable.DevicesCollection.TABLE_NAME + "(" + DBTable.DevicesCollection.ADDRESS + ") ON DELETE CASCADE ON UPDATE CASCADE)";
    }
}
