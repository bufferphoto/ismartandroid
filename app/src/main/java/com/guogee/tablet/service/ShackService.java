package com.guogee.tablet.service;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.PowerManager;

import com.guogee.tablet.iSmartApplication;
import com.guogee.tablet.ui.activity.VoiceActivity;
import com.guogee.tablet.ui.widge.ShakeListener;
import com.guogee.ismartandroid2.utils.GLog;


import java.io.IOException;

public class ShackService extends Service
{
	
	private ShakeListener mShakeListener;
	
	private SharedPreferences spf;
	
	@Override
	public IBinder onBind(Intent intent)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onCreate()
	{
		// TODO Auto-generated method stub
		super.onCreate();
		iSmartApplication isapp = (iSmartApplication) getApplication();
		spf = getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
		mShakeListener = new ShakeListener(this, isapp);
		mShakeListener.setOnShakeListener(new ShakeListener.OnShakeListener()
		{
			
			@Override
			public void onShake()
			{
				// TODO Auto-generated method stub
				mShakeListener.stop();
				GLog.v("ShackService", "Doing something");
				PowerManager manager = (PowerManager) ShackService.this
						.getSystemService(Context.POWER_SERVICE);
				boolean isScreenOn = manager.isScreenOn();
				KeyguardManager mKeyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
				boolean flag = mKeyguardManager.inKeyguardRestrictedInputMode();
				if (isScreenOn && flag) {
					GLog.v("ShackService", "锁屏，亮屏状态");
					if (spf.getInt("times", 0) > 2) {
//						if (!spf.getBoolean("isVoiceControlActive", false)) {
//							spf.edit().putBoolean("isVoiceControlActive", true).commit();
//							GLog.v("ShackService", "智趣语音不在顶层,启动Activity");
							startAlarm();
							Intent intent = new Intent(ShackService.this,
									VoiceActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.putExtra("showSceneButton", true);
							ShackService.this.startActivity(intent);
							spf.edit().putInt("times", 0).commit();
							GLog.v("ShackService", "次数，时间置零");
//						}
					}else {
						int times = spf.getInt("times", 0);
						spf.edit().putInt("times", ++times).commit();
						GLog.v("ShackService", "次数，时间自增");
					}
					
				}
				mShakeListener.start();
			}
		});
		
	}
	
	private void startAlarm()
	{
		try {
			Uri alert = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			MediaPlayer player = new MediaPlayer();
			player.setDataSource(this, alert);
			final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			if (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) != 0) {
				player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
				player.prepare();
				
				player.start();
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
		mShakeListener.stop();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		// TODO Auto-generated method stub
		
		return START_STICKY;
	}
	
	
}
