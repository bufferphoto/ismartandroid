package com.guogee.tablet.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.guogee.tablet.iSmartApplication;
import com.guogee.tablet.manager.SceneActionManager;
import com.guogee.tablet.ui.activity.PerformSceneActivity;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.manager.DataModifyHandler;
import com.guogee.ismartandroid2.manager.MagicManager;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.manager.SceneManager;
import com.guogee.ismartandroid2.model.GatewayAttribute;
import com.guogee.ismartandroid2.model.GatewayInfo;
import com.guogee.ismartandroid2.model.LocationScene;
import com.guogee.ismartandroid2.model.LocationSceneConfig;
import com.guogee.ismartandroid2.model.Scene;
import com.guogee.ismartandroid2.utils.GConstant.NetworkType;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.sdk.android.network.NetworkManager;
import com.guogee.sdk.scene.ActionData;
import com.guogee.sdk.scene.ActionListener;
import com.guogee.sdk.scene.SceneAdapter;
import com.guogee.sdk.scene.SceneRunner;
import com.example.mt.mediaplay.R;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SenceAutoExcuteByLocationService extends Service implements ActionListener, SceneAdapter {

    private static final int CALLBACK_MSG_CONTROL_SUCCESS = 1;

    private LocationManager mLocationManager;
    private LocationClient mLocationClient = null;
    private LocationScene mLocationScene;
    private LocationSceneConfig mEnterConfig;
    private LocationSceneConfig mLeaveConfig;

    private SceneRunner mSceneRunner;
    private float preExeDistance = 0;
    private SharedPreferences spf;
    private static int DISTANCE = 100;
    //	private boolean saveGMacLocation = false;
    private WakeLock wakeLock;
    private PowerManager pm;
    private int successCount = 0;
    private int failedCount = 0;
    private int totalCount = 0;
    private static final int MAX_FREQUENCY = 30;
    private static final float MIN_FREQUENCY = 1;

    protected static final String TAG = "SenceAutoExcuteByLocationService";


    private boolean isSendNotification = false;

    private boolean isConnectedWifi = false;

    private GatewayInfo mGatewayInfo;

    private String longitude;
    private String latitude;
    private String altitude;
    private String nameCN;
    private String nameEN;

    private List<PerformSceneActivity.ActionView> mActionData;
    private Scene mScene;
    private MagicManager mMagicManger;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        mMagicManger = MagicManager.getInstance(this);
        mSceneRunner = new SceneRunner();
        mSceneRunner.setListener(this);
        mSceneRunner.setAdapter(this);
        spf = getSharedPreferences("CONFIG", Context.MODE_PRIVATE);

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mLocationClient = new LocationClient(this);
        mLocationClient.registerLocationListener(mbdLocationListener);
        initLocation();
        //Service 初始化数据
//		mMagicManger.getLocationSceneList(LocationSceneHandler);

    }

    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy
        );//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
//		option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系
        int span = 0;
        option.setScanSpan(span);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(false);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(false);//可选，默认false,设置是否使用gps
        option.setLocationNotify(false);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIsNeedLocationDescribe(false);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(false);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIgnoreKillProcess(false);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.SetIgnoreCacheException(false);//可选，默认false，设置是否收集CRASH信息，默认收集
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        mLocationClient.setLocOption(option);
    }

    private DataModifyHandler<List<LocationScene>> LocationSceneHandler = new DataModifyHandler<List<LocationScene>>() {

        @Override
        public void onResult(List<LocationScene> obj, Exception e) {
            if (e != null)
                throw new RuntimeException(e);
            if (obj.size() == 0) {
                sendUpdateTime(MAX_FREQUENCY);
                releaseLock();
                return;
            }
            mLocationScene = obj.get(0);
            mGatewayInfo = RoomManager.getInstance(SenceAutoExcuteByLocationService.this).getGateway(mLocationScene.getGatewayId());
            if (mGatewayInfo == null) {
                sendUpdateTime(MAX_FREQUENCY);
                releaseLock();
                return;
            }
            DISTANCE = mLocationScene.getDistance();
            GatewayAttribute ga = mGatewayInfo.getGatewayOtherInfo();
            if (ga != null) {
                longitude = ga.getLongitude();
                latitude = ga.getLatitude();
                altitude = ga.getAltitude();
            } else {
                sendUpdateTime(MAX_FREQUENCY);
                releaseLock();
                return;
            }

            mMagicManger.getLocationcSceneConfigList(mLocationScene, configHandler);

        }
    };

    private DataModifyHandler<List<LocationSceneConfig>> configHandler = new DataModifyHandler<List<LocationSceneConfig>>() {

        @Override
        public void onResult(List<LocationSceneConfig> obj, Exception e) {
            if (e != null)
                throw new RuntimeException(e);
            if (obj.size() == 0) {
                sendUpdateTime(MAX_FREQUENCY);
                releaseLock();
                return;
            }
            boolean isOpen = false;
            for (LocationSceneConfig locationSceneConfig : obj) {
                if (locationSceneConfig.getSchemaType() == MagicManager.LOCATION_CONFIG_TYPE_ENTER) {
                    mEnterConfig = locationSceneConfig;
                } else {
                    mLeaveConfig = locationSceneConfig;
                }
                if (locationSceneConfig.getStatus() == MagicManager.STATUS_ON)
                    isOpen = true;

            }
            if (isOpen) {

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        getLocation();
                    }
                });
            } else {
                sendUpdateTime(MAX_FREQUENCY);
                releaseLock();

            }
        }
    };

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        mLocationClient.stop();
        releaseLock();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, SenceAutoExcuteByLocationService.class.getName());
        wakeLock.acquire();
        GLog.v(TAG, "Requrire wake lock");
        if (!isUserLogin(intent)) {
            GLog.v(TAG, "用户未登录");
            this.sendUpdateTime(MAX_FREQUENCY);
            this.releaseLock();

            return START_NOT_STICKY;
        }
//		if (!isLocationProviderAvliable()) {
//			GLog.v(TAG, "用户GPS未打开");
//			this.releaseLock();
//			this.sendUpdateTime(MAX_FREQUENCY);
//			this.stopSelf();
//			return START_NOT_STICKY;
//		}

        if (!isConnectedWIFI()) {
            isConnectedWifi = false;
        } else {
            isConnectedWifi = true;//若连接上WIFI 进行一次定位查询,若不满足执行回家 或者离家场景条件,关闭Service，若满足,则执行场景后 关闭Service

        }
//		Toast.makeText(this, "getLocationSceneList", Toast.LENGTH_SHORT).show();;
//		if (intent != null
//				&& intent.getBooleanExtra("NEED_RELOAD_DATA", false)) {//防止用户未开启定位,但是绑定网关时候发送过来的请求需要通过SAVE_GMAC_LOCATION
        mMagicManger.getLocationSceneList(LocationSceneHandler);

        return START_STICKY;

//		}else{
//			if (!isUserOpen(intent)) {
//				GLog.v(TAG, "用户未打开");
//				this.releaseLock();
//				this.sendUpdateTime(MAX_FREQUENCY);
//				this.stopSelf();
//				return START_NOT_STICKY;
//			}
//		}
//
//
//		this.getLocation();
//		return START_STICKY;

    }


    private boolean isUserLogin(Intent intent) {
        final String userName = spf.getString("login_name", "");
        final String userPwd = spf.getString("login_password", "");
        if (userName != "" && userPwd != "") {
            GLog.v(TAG, "登录");

            return true;
        } else {
            GLog.v(TAG, "没登录");
            return false;
        }
    }

    private boolean isUserOpen() {
        if (mGatewayInfo == null)//无网关
            return false;
        if (mEnterConfig == null && mLeaveConfig == null) {//无配置
            return false;
        }
        if (longitude == null || "".equals(longitude) || latitude == null || "".equals(latitude)) {//无地址
            return false;
        }
        if (mEnterConfig != null && mEnterConfig.getStatus() == MagicManager.STATUS_ON)
            //如果没有一个打开,或者与场景执行的绑定网关找不到,返回Flase
            return true;
        if (mLeaveConfig == null && mLeaveConfig.getStatus() == MagicManager.STATUS_ON)
            return true;
        return false;
    }

    private boolean isLocationProviderAvliable() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!mLocationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)
                && !mLocationManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER)) {
            return false;
        } else {
            return true;
        }
    }


    private boolean isConnectedWIFI() {
        if (NetworkManager.getInstance(this).getNetworkType().equals(NetworkType.WifiConnected)) {
            GLog.v(TAG, "连接上WIFI");
            Editor edit = spf.edit();
            edit.putInt("level", 1);
            edit.commit();
            return true;
        } else
            return false;

    }

    private void releaseLock() {
        if (wakeLock != null) {
            GLog.v(TAG, "Release wake lock");
            wakeLock.release();
            wakeLock = null;
        }
    }

    private void getLocation() {
//		mLocationManager.removeUpdates(locationListener);
        // android通过criteria选择合适的地理位置服务
//		Criteria criteria = new Criteria();
//		criteria.setAccuracy(Criteria.ACCURACY_FINE);// 高精度
//		criteria.setAccuracy(Criteria.ACCURACY_COARSE);// 模糊
//		criteria.setAltitudeRequired(false);// 设置不需要获取海拔方向数据
//		criteria.setBearingRequired(false);// 设置不需要获取方位数据
//		criteria.setCostAllowed(true);// 设置允许产生资费
//		criteria.setPowerRequirement(Criteria.POWER_LOW);// 低功耗
//		String provider = "";
//		if(!isConnectedWIFI()){
//			provider = mLocationManager.getBestProvider(criteria, true);// 获取GPS信息
//			GLog.v(TAG, "getBestProvider:" + provider);
//		}
//		else {
//		String provider = LocationManager.NETWORK_PROVIDER;
//		}
//		Toast.makeText(SenceAutoExcuteByLocationService.this,"getBestProvider:"+provider, Toast.LENGTH_SHORT).show();

//		Location location = mLocationManager.getLastKnownLocation(provider);// 通过GPS获取位置
        // mLocationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER,
        // true);

        // 设置监听器，自动更新的最小时间为间隔N秒(这里的单位是微秒)或最小位移变化超过N米(这里的单位是米)
//		mLocationManager.requestLocationUpdates(provider, 30 * 1000, 1,
//				locationListener);
//
//		GpsStatus gpsStatus = mLocationManager.getGpsStatus(null); // 获取当前状态
//		// 获取默认最大卫星数
//		int maxSatellites = gpsStatus.getMaxSatellites();
//		// 获取第一次定位时间（启动到第一次定位）
//		int costTime = gpsStatus.getTimeToFirstFix();
        mLocationClient.start();
    }

    /**
     * @param location
     * @author Smither
     * @function 根据Location判断该执行哪种场景
     */
    private void chooseExcuteSence(BDLocation location) {
        GLog.v(TAG, "chooseExcuteSence");

        preExeDistance = spf.getFloat("preExeDistance", (float) 0.0);
//		 Toast.makeText(this, "chooseExcuteSence", Toast.LENGTH_SHORT).show();
        boolean isNeedFreeWeakLock = true;
        float[] currentDis = new float[1];
//		for (int i = 0; i < listOpenSchema.size(); i++) {
        Double Longitude = Double
                .valueOf(longitude);
        Double Latitude = Double.valueOf(latitude);
//			 Toast.makeText(this,
//			 "Longitude:"+Longitude+" Latitude:"+Latitude,
//			 Toast.LENGTH_SHORT).show();
        GLog.v(TAG, "gw Longitude:" + Longitude + " Latitude:"
                + Latitude + " current Longitude:" + location.getLongitude() + " latitude:" + location.getLatitude());
        Location.distanceBetween(Latitude, Longitude,
                location.getLatitude(), location.getLongitude(), currentDis);

        currentDis[0] -= location.getRadius() / 40;
        GLog.v(TAG, "currentDis:" + currentDis[0] + " preExeDistance:"
                + preExeDistance);
        if (preExeDistance == 0) {
            // preExeDistance = currentDis[0];
            Editor edit = spf.edit();
            edit.putFloat("preExeDistance", currentDis[0] == 0 ? 1 : currentDis[0]);
            GLog.v(TAG, "第一次定位");
            //		edit.putFloat("preExeDistance", 500);
            edit.commit();
        } else {
            if (preExeDistance < DISTANCE && currentDis[0] > DISTANCE + 10) {
                if (mLeaveConfig != null) {
                    // 执行离家场景
                    Editor edit = spf.edit();
                    edit.putFloat("preExeDistance", currentDis[0]);
                    edit.commit();
                    if (mLeaveConfig.getStatus() == MagicManager.STATUS_ON) {
                        GLog.v(TAG, "leave sence excute");
                        this.executeSence(mLeaveConfig.getSceneId());
                        isNeedFreeWeakLock = false;
                    }
                }
            } else if (preExeDistance > DISTANCE && currentDis[0] < DISTANCE - 10) {
                if (mEnterConfig != null) {
                    // 执行回家场景
                    Editor edit = spf.edit();
                    edit.putFloat("preExeDistance", currentDis[0]);
                    edit.commit();
                    if (mEnterConfig.getStatus() == MagicManager.STATUS_ON) {
                        GLog.v(TAG, "back sence excute");
                        this.executeSence(mEnterConfig.getSceneId());
                        isNeedFreeWeakLock = false;
                    }
                }
            }
//				 Toast.makeText(this,
//						 "currentDis:" + currentDis[0] + " preExeDistance:" + preExeDistance,
//						 Toast.LENGTH_LONG).show();

        }
//		}

        if (isConnectedWifi) {//WIFI连接上时候,只查询一次,不满足执行条件，设置最大频率,并关闭当前Service
            //		Toast.makeText(this, "WIFI下查询一次,关闭Service,返回", Toast.LENGTH_SHORT).show();
            isConnectedWifi = false;
            GLog.i(TAG, "连上wifi，设置为最大间隔" + MAX_FREQUENCY);
            this.sendUpdateTime(MAX_FREQUENCY);
            releaseLock();
            return;
        }
//		if (currentDis[0] > 300) {
        float minute = Math.abs(currentDis[0] - DISTANCE) / 500f;
        GLog.v(TAG, "频率:" + minute + "分钟");
        //		 Toast.makeText(this,"currentDis[0] > 300:"+"频率:"+minute+"分钟",Toast.LENGTH_SHORT).show();
        this.sendUpdateTime(minute > MAX_FREQUENCY ? MAX_FREQUENCY : minute);
//			if (minute > MAX_FREQUENCY) {
//				this.releaseLock();
//				return;
//			}
//		}
//		if (currentDis[0] - distance < 20) {
//			int level = spf.getInt("level", 1);
//			GLog.v(TAG, "移动距离少于20米，长时间停留某位置次数:" + level);
//		//	 Toast.makeText(this,"移动距离少于20米，长时间停留某位置次数:"+level,Toast.LENGTH_SHORT).show();
//			// this.sendUpdateTime(level*3 > MAX_FREQUENCY ? MAX_FREQUENCY :level*3);
//			Editor edit = spf.edit();
//			int levelTemp = (level / 3) + 1;
//			edit.putInt("level", ++level);
//			edit.commit();
//			this.sendUpdateTime(levelTemp * MIN_FREQUENCY > MAX_FREQUENCY ? MAX_FREQUENCY
//					: levelTemp * MIN_FREQUENCY);
//		} else {
//			GLog.v(TAG, "移动距离超过20米");
//		//	 Toast.makeText(this,"移动距离大于20",Toast.LENGTH_SHORT).show();
//			distance = currentDis[0];
//			Editor edit = spf.edit();
//			edit.putInt("level", 1);
//			edit.commit();
//			this.sendUpdateTime(MIN_FREQUENCY);
//		}

        if (isNeedFreeWeakLock) {
            this.releaseLock();
        }
    }

    private void sendUpdateTime(float minute) {
        iSmartApplication.getApp().setTimer((minute > 0.1f) ? minute : 0.1f);
    }

    /**
     * @param senceId
     * @author Smither
     * @function 执行场景
     */
    private void executeSence(final int senceId) {
        new Thread() {
            @Override
            public void run() {
                mScene = SceneManager.getInstance(SenceAutoExcuteByLocationService.this).getSceneById(senceId);
                if (mScene != null) {
                    GLog.v(TAG, "exctureSence sceneName:" + mScene.getName());
                    mActionData = SceneActionManager.getInstance(SenceAutoExcuteByLocationService.this).loadActions(mScene);
                    totalCount = mActionData.size();
                    if (totalCount == 0) {
                        releaseLock();
                        return;
                    }
                    successCount = 0;
                    failedCount = 0;
                    isSendNotification = false;
                    map = new HashMap<Integer, Integer>();

                    mSceneRunner.execute(true);
                } else {
                    GLog.v(TAG, "scene " + senceId + "not exist");
                }
            }
        }.start();


    }

    private BDLocationListener mbdLocationListener = new BDLocationListener() {

        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            mLocationClient.stop();
            int type = bdLocation.getLocType();
            if (type == BDLocation.TypeServerError || type == BDLocation.TypeNetWorkException || type == BDLocation.TypeCriteriaException) {
                GLog.i(TAG, "定位失败");
                releaseLock();
                return;
            }
            float radius = bdLocation.getRadius();
            GLog.v(TAG, "精度:" + radius);
            if (radius == 0 || radius > 20000) {
                releaseLock();
                return;
            }
            chooseExcuteSence(bdLocation);


        }
    };

    private void startWeather(){
        Intent intent = new Intent(this, AutoUpdateService.class);
        startService(intent);
    }


    /**
     * 设置回调状态
     *
     * @param seq
     * @param isOk
     */
    private Map<Integer, Integer> map;

    protected Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            /**
             * @author Smither
             * @function 若成功发送, 则发送通知
             */
            GLog.v(TAG, "Send Notification");
//			Notification notification = new Notification();
            String tickerText = getResources().getString(R.string.zq_notice); // 通知提示
            // 显示时间
//			long when = System.currentTimeMillis();
//			notification.icon = R.drawable.ic_launcher;// 设置通知的图标
//			notification.tickerText = tickerText; // 显示在状态栏中的文字
//			notification.when = when; // 设置来通知时的时间
//			notification.flags |= Notification.FLAG_AUTO_CANCEL; // 点击清除按钮或点击通知后会自动消失
//			notification.defaults = Notification.DEFAULT_ALL; // 把所有的属性设置成默认
            Intent intent = new Intent(SenceAutoExcuteByLocationService.this,
                    PerformSceneActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // 获取PendingIntent,点击时发送该Intent
            Bundle bundle = new Bundle();
            bundle.putSerializable("devicesData", (Serializable) map);
            bundle.putSerializable("scene", mScene);
            intent.putExtras(bundle);

            String text;
            if (preExeDistance < DISTANCE) {
                if (failedCount == 0) {
                    text = getResources().getString(R.string.leave_home_tip);
                } else {
                    text = getResources().getString(
                            R.string.leave_home_failed_tip);
                }
            } else {
                if (failedCount == 0) {
                    text = getResources().getString(R.string.back_home_tip);
                } else {
                    text = getResources().getString(
                            R.string.back_home_failed_tip);
                }
            }
            PendingIntent pIntent = PendingIntent.getActivity(
                    SenceAutoExcuteByLocationService.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            // 设置通知的标题和内容
//			notification.setLatestEventInfo(
//					SenceAutoExcuteByLocationService.this, getResources()
//					.getString(R.string.app_name)+getResources()
//							.getString(R.string.zq_notice), text, pIntent);
            // 发出通知
//			notificationManager.notify(R.string.app_name, notification);
            SystemUtil.sendNotification(SenceAutoExcuteByLocationService.this, pIntent, getResources()
                    .getString(R.string.app_name) + getResources()
                    .getString(R.string.zq_notice), text);
            GLog.i(TAG, text);
            if (isConnectedWifi) {//WIFI连接上时候,只查询一次,不满足执行条件，设置最大频率,并关闭当前Service
                //		Toast.makeText(SenceAutoExcuteByLocationService.this, "WIFI下查询一次,满足执行条件，发送Notifacation,关闭Service,返回", Toast.LENGTH_SHORT).show();
                isConnectedWifi = false;
                sendUpdateTime(MAX_FREQUENCY);
            }
            SenceAutoExcuteByLocationService.this.releaseLock();
        }
    };

    @Override
    public void onActionStateChange(ActionData action) {

        int status = action.getSendState();
        if (status == ActionData.STATUS_SUCCESS) {
            successCount++;

        } else if (status == ActionData.STATUS_FAILED) {
            failedCount++;
        }
        if (successCount + failedCount >= totalCount) {
            if (!isSendNotification) {
                mHandler.sendEmptyMessage(CALLBACK_MSG_CONTROL_SUCCESS);
                isSendNotification = true;
            }
        }
        map.put(action.getActionId(), action.getSendState());

    }

    @Override
    public int getActionCount() {
        if (mActionData != null) {
            return mActionData.size();
        }
        return 0;
    }

    @Override
    public ActionData getActionData(int i) {
        if (mActionData != null && i < mActionData.size()) {
            return mActionData.get(i).status;
        }
        return null;
    }

}
