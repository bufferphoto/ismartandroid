package com.guogee.tablet.service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager.WakeLock;

import com.guogee.ismartandroid2.aidl.GatewayStatus;
import com.guogee.ismartandroid2.device.DeviceFactory;
import com.guogee.ismartandroid2.device.IDevice;
import com.guogee.ismartandroid2.device.RGBLight;
import com.guogee.ismartandroid2.device.RGBYWLightControl;
import com.guogee.ismartandroid2.device.WLightControl;
import com.guogee.ismartandroid2.device.YWLight;
import com.guogee.ismartandroid2.device.YWLightControl;
import com.guogee.ismartandroid2.manager.DataModifyHandler;
import com.guogee.ismartandroid2.manager.MagicManager;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.model.Reminder;
import com.guogee.ismartandroid2.model.ReminderItem;
import com.guogee.ismartandroid2.service.NetworkServiceConnector;
import com.guogee.ismartandroid2.utils.GLog;

import java.util.List;

/**
 * 静态广播的后台服务
 * @author lunge
 *
 */
public class BackgroundService extends Service {
	
	public static final int	Background_phone = 1;
	public static final int	Background_msg = 2;
	public static final int	Backgroud_qq=3;
	public static final int	Background_webchat=4;
	public static final int	Background_mediabtn=5;
	

    public static final String ACTION_Service 	= "com.guogu.backgroundService";
	private static final String TAG = "BackgroundService";
	private MagicManager mMagicManager;
//	private DeviceDao deviceDao;
//	private Map<String, String> deviceMap;	
	
	private int businessType;

	WakeLock wakeLock;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		mMagicManager = MagicManager.getInstance(this);
		
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		if(intent == null)
			return super.onStartCommand(intent, flags, startId);;
		businessType = intent.getIntExtra("business", 0);
		

		if(businessType == Background_mediabtn){
			
			/*PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
	        wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
	        wakeLock.acquire();
	        final Intent it = new Intent("android.intent.action.MAIN");	
	        it.setClass(getApplicationContext(), MediaButtonActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        this.startActivity(it);*/
//	        InteractionUtils.vibrator(BackgroundService.this.getApplicationContext());
//	      //  List<Map<String, String>> comboMaps = deviceDao.listDeviceByType(new String[] {"RGBLIGHT"});
//	        List<DeviceModel> listModel = DeviceManager.getInstance().getDeviceByType(Constant.LIGHT_FALG);
//	        if(listModel.size()>0){
//	        	device = listModel.get(0);
//				deviceModelUnion = new DeviceModelUnion();
//				deviceModelUnion.model = deviceModel;
//	        	
//	        }

		}else{
			mMagicManager.getReminderList(new DataModifyHandler<List<Reminder>>()
			{
				
				@Override
				public void onResult(List<Reminder> obj, Exception e)
				{
					if(e!=null)
						throw new RuntimeException(e);
					if(obj.size() == 0){
						GLog.e(TAG, "======no reminder!");
						return ;
					}
					Reminder r = obj.get(0);
					if(r.getStatus() != 1){
						GLog.e(TAG, "======reminder is off!");

						return;
					}
					mMagicManager.getReminderItemList(obj.get(0), dataHandler);
					
				}
			});
			
		}


		GLog.e("tag", "onStartCommand");
		
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	private DataModifyHandler<List<ReminderItem>> dataHandler = new DataModifyHandler<List<ReminderItem>>()
	{

		@Override
		public void onResult(List<ReminderItem> obj, Exception e)
		{
			if(e!=null)
				throw new RuntimeException(e);
			GLog.i(TAG, "====start to blink...");
			for (ReminderItem reminderItem : obj) {
				Device dev = RoomManager.getInstance(BackgroundService.this).searchDevice(reminderItem.getDeviceId());
				Device gw = RoomManager.getInstance(BackgroundService.this).searchGatewayByDeviceId(reminderItem.getDeviceId());
				if(gw!= null){
					GatewayStatus gs = NetworkServiceConnector.getInstance().getGatewayStatus(gw.getAddr());
					if(gs != null && gs.getOnlineStatus() != GatewayStatus.REMOTE){
						IDevice device = DeviceFactory.buildDevice(dev, gw.getAddr());
						if(device instanceof RGBLight){
							((RGBLight)device).blink(5, 1000);
						}else if(device instanceof YWLight){
							((YWLight)device).blink(5, 1000);
						}else if(device instanceof RGBYWLightControl){
							((RGBYWLightControl) device).blink(5, 1000);
						}else if(device instanceof YWLightControl){
							((YWLightControl) device).blink(5, 1000);
						}else if(device instanceof WLightControl){
							((WLightControl) device).blink(5, 1000);
						}
					}
				}
			}
			
			
		}
	};
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		GLog.i(TAG, "onDestroy");
		
		if(wakeLock!=null)
		    wakeLock.release();
	}


	
}
