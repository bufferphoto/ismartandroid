package com.guogee.tablet.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.SystemClock;

import com.baidu.location.Address;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.guogee.tablet.broadcastreceiver.AutoUpdateReceiver;
import com.example.mt.mediaplay.R;
import com.guogee.ismartandroid2.manager.HttpManager;
import com.guogee.ismartandroid2.remoteControlService.AsyncHttpResponseHandler;
import com.guogee.ismartandroid2.utils.GLog;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by mt on 2017/6/2.
 */

public class AutoUpdateService extends Service {

    public static final String TAG = "AutoUpdateService";
    private SharedPreferences sp;
    SharedPreferences.Editor editor;
    private LocationClient mLocationClient = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        GLog.i(TAG, "onCreate");
        sp = getSharedPreferences("weather", Context.MODE_PRIVATE);
        editor = sp.edit();
        mLocationClient = new LocationClient(this.getApplication());
        mLocationClient.registerLocationListener(mbdLocationListener);
        initLocation();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mLocationClient.start();
        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        int anHour = 60 * 60 * 1000; // 这是2小时的毫秒数
        /**
         * SystemClock.elapsedRealtime()方法可以获取到系统开机至今所经历时间的毫秒数
         */
        long triggerAtTime = SystemClock.elapsedRealtime() + anHour;
        Intent i = new Intent(this, AutoUpdateReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
        Intent weatherIntent = new Intent();
        weatherIntent.setAction("WEATHER_UPDATE_ACTION");//自定义Action
        sendBroadcast(weatherIntent); //发送广播
        // ELAPSED_REALTIME_WAKEUP表示让定时任务的触发时间从系统开机算起，且会唤醒CPU
        manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtTime, pi);
        return super.onStartCommand(intent, flags, startId);
    }

    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy
        );//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系
        int span = 0;
        option.setScanSpan(span);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(false);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIsNeedLocationDescribe(false);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(false);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIgnoreKillProcess(false);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.SetIgnoreCacheException(false);//可选，默认false，设置是否收集CRASH信息，默认收集
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        mLocationClient.setLocOption(option);
    }

    private BDLocationListener mbdLocationListener = new BDLocationListener() {

        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            mLocationClient.stop();
            int type = bdLocation.getLocType();
            if (type == BDLocation.TypeServerError || type == BDLocation.TypeNetWorkException || type == BDLocation.TypeCriteriaException) {
                GLog.i(TAG, "定位失败");
                return;
            }
            Address add = bdLocation.getAddress();
            editor.putString("city", add.city);
            editor.commit();
            if (null == add.city) {//第一次获取不到值
                mLocationClient.start();
            }
            GLog.v(TAG, "精度:" + add.city);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Intent weatherIntent = new Intent();
                    weatherIntent.setAction("WEATHER_UPDATE_ACTION");//自定义Action
                    updateWeather();
                }
            }).start();
        }
    };

    /**
     * 更新天气信息
     */
    private void updateWeather() {

        String location = sp.getString("city", "");
        GLog.i(TAG, "----city:" + location);
        String WEATHER_URL_HEADER =
                "https://query.yahooapis.com/v1/public/yql?format=json&q=";
        StringBuilder url = new StringBuilder(WEATHER_URL_HEADER);
        try {
            String YahooWeatherApiText1 = "select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(86)%20where%20text=%27";
            url.append(YahooWeatherApiText1);
            url.append(URLEncoder.encode(location, "utf8"));
            url.append("%27)");
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        GLog.i(TAG, "Url is " + url.toString());
        HttpManager.getInstance().SendGetRequest(url.toString(), null, new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(Throwable throwable, String s) {
                GLog.i(TAG, " onError ");
            }

            @Override
            public void onSuccess(int i, String s) {
                try {
                    JSONObject obj = new JSONObject(s);
                    GLog.i(TAG, "obj is " + obj.toString());
//                            JSONArray weatherDescription = obj.getJSONArray("results");

                    JSONObject data = obj.getJSONObject("query").getJSONObject("results");
                    JSONObject weatherObj;
                    if (data.optJSONArray("channel") != null) {
                        weatherObj = data.getJSONArray("channel").getJSONObject(0);
                    } else {
                        weatherObj = data.getJSONObject("channel");
                    }
                    JSONObject itemJson = weatherObj.getJSONObject("item");
                    JSONObject descriptionJson = itemJson.getJSONObject("condition");

                    final String weatherStr = getDescription(descriptionJson.getInt("code"));


                    setWeatherIcon(weatherStr.toLowerCase(), editor);
                    editor.commit();
                    GLog.i(TAG, "   weatherStr:" + weatherStr);

                } catch (Exception e) {
                    e.printStackTrace();
                    GLog.i("Tag", getString(R.string.unknown_city));
                }
            }
        });
    }

    //雅虎天气code转换
    private String getDescription(int code) {
        String key = "clouds";
        switch (code) {
            case 31:
            case 32:
            case 33:
            case 34:
            case 36:
                key = "clear";
                break;
            case 8:
            case 9:
                key = "light rain";
                break;
            case 10:
            case 11:
            case 12:
            case 17:
            case 45:
            case 47:
                key = "moderate rain";
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 35:
            case 37:
            case 38:
            case 39:
            case 40:
                key = "heavy rain";
                break;
            case 5:
            case 7:
            case 13:
            case 14:
            case 18:
                key = "light snow";
                break;
            case 15:
            case 16:
            case 41:
            case 42:
            case 46:
                key = "moderate snow";
                break;
            case 43:
                key = "heavy snow";
                break;
            default:
                key = "clouds";
                break;
        }
        return key;
    }

    private void setWeatherIcon(String description, SharedPreferences.Editor editor) {
        if (description.contains("clear") || description.contains("sunny")) {
            editor.putInt("weatherIcon", R.drawable.weather_fine);
            editor.putString("weather", getString(R.string.sunny));
        } else if (description.contains("clouds")) {
            editor.putInt("weatherIcon", R.drawable.weather_cloudy);
            editor.putString("weather", getString(R.string.cloudy));
        } else if (description.contains("rain")) {// 对rain进行模糊匹配
            if (description.contains("light")) {
                editor.putInt("weatherIcon", R.drawable.weather_rain1);
                editor.putString("weather", getString(R.string.light_rain));
            } else if (description.contains("moderate")) {
                editor.putInt("weatherIcon", R.drawable.weather_rain2);
                editor.putString("weather", getString(R.string.moderate_rain));
            } else if (description.contains("heavy")) {
                editor.putInt("weatherIcon", R.drawable.weather_rain3);
                editor.putString("weather", getString(R.string.heavy_rain));
            } else {
                editor.putInt("weatherIcon", R.drawable.weather_rain1);
                editor.putString("weather", getString(R.string.light_rain));
            }
        } else if (description.contains("snow")) { // 对snow进行模糊匹配
            if (description.contains("light")) {
                editor.putInt("weatherIcon", R.drawable.weather_snow1);
                editor.putString("weather", getString(R.string.light_snow));
            } else if (description.contains("moderate")) {
                editor.putInt("weatherIcon", R.drawable.weather_snow2);
                editor.putString("weather", getString(R.string.moderate_snow));
            } else if (description.contains("heavy")) {
                editor.putInt("weatherIcon", R.drawable.weather_snow3);
                editor.putString("weather", getString(R.string.heavy_snow));
            } else {
                editor.putInt("weatherIcon", R.drawable.weather_snow1);
                editor.putString("weather", getString(R.string.light_snow));
            }
        } else {
            editor.putInt("weatherIcon", R.drawable.weather_cloudy);
            editor.putString("weather", getString(R.string.cloudy));
        }
    }

}
