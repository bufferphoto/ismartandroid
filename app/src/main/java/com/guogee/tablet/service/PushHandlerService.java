package com.guogee.tablet.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;

import com.guogee.tablet.manager.DeviceManager;
import com.guogee.tablet.manager.UserDataManager;
import com.guogee.tablet.model.DoorBell;
import com.guogee.tablet.model.SmartLockUserIdNameModel;
import com.guogee.tablet.utils.NumberUtil;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.manager.DataModifyHandler;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.music.MusicFilterSetting;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.ismartandroid2.utils.PublishHelper;
import com.guogu.music.manager.VolumeControlManager;
import com.guogu.music.play.PlayerControl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * 处理推送消息
 */
public class PushHandlerService extends Service {

    private static final String TAG = "PushHandlerService";

    public static final int PUSH_TYPE_SECURITY = 1;
    public static final int PUSH_TYPE_OUTLET = 2;
    public static final int PUSH_TYPE_LOCK = 3;
    public static final int PUSH_TYPE_IFTTT = 4;
    public static final int PUSH_TYPE_USER = 5;
    public static final int PUSH_TYPE_DEIVCE_UPDATE = 6;
    public static final int PUSH_TYPE_MINI_DOORBELL = 7;
    public final static int V0 = 1000;// 文本控制
    public final static int V1 = 1001;// 播放指定歌曲
    public final static int V2 = 1002;// 播放指定告警铃声
    public final static int V3 = 1003;// 停止播放

    private static final int MSG_PWSSWORD_CHANGE = 1;

    private Handler mHandler;
    private PlayerControl playerControl;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                int what = msg.what;
//                String txt = msg.obj.toString();
                switch (what) {
                    case MSG_PWSSWORD_CHANGE://密码改变
//                        Intent it = new Intent(PushHandlerService.this, PasswordChangedAlarmActivity.class);
//                        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(it);
                        break;
                }
            }
        };
        playerControl = PlayerControl.getInstance(this);
        acquireWakeLock();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        GLog.i("  onStartCommand  ");
        if (intent != null) {
            SharedPreferences sp = getSharedPreferences(UserDataManager.SP_NAME, MODE_PRIVATE);
            String pwd = sp.getString(UserDataManager.LOGIN_PASSWD, "");
            if (pwd.equals("") && !SystemUtil.isDebugable(this))
                return START_STICKY;
            String data = intent.getStringExtra("data");
            if (data == null)
                return START_STICKY;
            try {
                JSONObject obj = new JSONObject(data);
                int pushType = obj.getInt("pushType");
                GLog.d(TAG, "pushType:" + pushType);
                switch (pushType) {
                    case PUSH_TYPE_OUTLET:
                    case PUSH_TYPE_SECURITY:
                        handleAlarm(this, obj, pushType);
                        break;
                    case PUSH_TYPE_LOCK:
                        handlLockPush(this, obj, pushType);
                        break;
                    case PUSH_TYPE_IFTTT://联动推送
                        handleIFTTTMsg(this, obj);
                        break;
                    case PUSH_TYPE_USER://用户信息推送
                        handleUserMsg(this, obj);
                        break;
                    case PUSH_TYPE_DEIVCE_UPDATE://设备升级提醒
                        handleDeviceUpdateMsg(this, obj);
                        break;
                    case PUSH_TYPE_MINI_DOORBELL://mini门铃推送
                        handleMiniDoorMsg(this, obj);
                        break;
                    case V0:
                    case V1:
                    case V2:
                    case V3:
                        handleMusicData(obj);
                        break;
                    default:
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        GLog.i("  onDestroy  ");
        releaseWakeLock();
        playerControl.destory();
    }

    private void handleMusicData(JSONObject obj) {
        try {
            String pushType = obj.getString("pushType");
            String text = obj.getString("pushMsg");
            switch (Integer.valueOf(pushType)) {
                case V1:
                    String volum = obj.optString("pushVolum");
                    if (null != volum && !volum.isEmpty()) {
                        VolumeControlManager.getInstance(this).setVolume(Integer.valueOf(volum));
                    }
                    MusicFilterSetting.ControlKeyword controlKeyword = MusicFilterSetting.filterKeyword(text);
                    if (null != controlKeyword) {
                        playerControl.playMusic(text);
                    } else {
                        playerControl.searchMusics(text);
                    }
                    break;
                case V2:
                    playerControl.speak("给我报警");
                    break;
                case V3:
                    playerControl.pause();
                    break;
                default:
                    playerControl.playMusic(text);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
        处理门铃提示消息
     */
    private void handleMiniDoorMsg(Context context, JSONObject data) {
        try {
            String mac = data.getString("devMac");
            int fun = data.getInt("type");
            String path = data.getString("operation");
            String serverId = data.optString("pushId", "");//推送无ID传下来
            path = "http://" + PublishHelper.getControlServerIP() + path;

            GLog.d(TAG, "mac:" + mac);
            DoorBell doorbell = new DoorBell();
            doorbell.setServerId(serverId);
            doorbell.setDeviceType(1);
            doorbell.setMac(mac);
            doorbell.setPath(path);
            doorbell.setFun(fun);
            doorbell.setTime(new Date(System.currentTimeMillis()).toString());
            doorbell.setData("");
            Device device = RoomManager.getInstance(context).searchDevice(mac);
            if (null != device) {
//                String title = context.getString(R.string.zq_notice);
//                String des = context.getString(R.string.doorbell_time_tip_1);
//                if (fun == 2) {
//                    des = context.getString(R.string.doorbell_tip_2);
//                }
//                String txt = context.getString(R.string.doorbell) + "(" + device.getName() + ")" + ": " + des;
//
//                if (SystemUtil.isRunningForeground(context)) {
//                    Intent it = new Intent(this, TipDialog.class);
//                    it.putExtra("TipMessage", txt);
//                    it.putExtra("success", true);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("doorbell", doorbell);
//                    it.putExtras(bundle);
//                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    startActivity(it);
//                } else {
//                    Intent it = new Intent(this, DoorBellMiniDetailInfoActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("doorbell", doorbell);
//                    it.putExtras(bundle);
//                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    PendingIntent pi = PendingIntent.getActivity(
//                            context, 0, it, PendingIntent.FLAG_UPDATE_CURRENT);
//                    SystemUtil.sendNotification(this, pi, title, txt, R.raw.doorbell);
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
        处理设备升级提示消息
     */
    private void handleDeviceUpdateMsg(Context context, JSONObject data) {
//        try {
//            String mac = data.getString("devMac");
//            String ver = data.getString("newVersion");
//            GLog.d(TAG, "mac:" + mac);
//            GLog.d(TAG, "ver:" + ver);
//            Device device = RoomManager.getInstance(context).searchDevice(mac);
//            if (null != device) {
//                String title = context.getString(R.string.zq_notice);
//                String txt = device.getName() + context.getString(R.string.device_update_tip) + ver;
//
//                if (SystemUtil.isRunningForeground(context)) {
//                    Intent it = new Intent(this, TipDialog.class);
//                    it.putExtra("TipMessage", txt);
//                    it.putExtra("success", true);
//                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    startActivity(it);
//                } else {
//                    Intent it = new Intent(this, SplashActivity.class);
//                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    PendingIntent pi = PendingIntent.getActivity(
//                            context, 0, it, PendingIntent.FLAG_UPDATE_CURRENT);
//                    SystemUtil.sendNotification(this, pi, title, txt);
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }


    /**
     * 处理IFTTT推送
     *
     * @param context
     * @param data
     * @throws JSONException
     */
    private void handleIFTTTMsg(Context context, JSONObject data) throws JSONException {
//        int type = data.getInt("type");
//        if (type == 4) {//定时场景
//            String name = data.getString("operation");
//            int fail = data.getInt("failCount");
//            if (name.startsWith("guogee_")) {
//                name = SystemUtil.getStringByName(context, name);
//            }
//            String title = context.getString(R.string.zq_notice);
//            String txt = name + context.getString(R.string.scene_execution_completed);
//
//            Intent bit = new Intent(Constant.UI_SCENE_CHANGE_ACTION);
//            sendBroadcast(bit);
//
//            if (SystemUtil.isRunningForeground(context)) {
//                Intent it = new Intent(this, TipDialog.class);
//                it.putExtra("TipMessage", txt);
//                it.putExtra("success", true);
//                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(it);
//            } else {
//                Intent it = new Intent(this, SplashActivity.class);
//                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                PendingIntent pi = PendingIntent.getActivity(
//                        context, 0, it, PendingIntent.FLAG_UPDATE_CURRENT);
//                SystemUtil.sendNotification(this, pi, title, txt);
//            }
//        }
    }


    private void insertAlertData(Context context, JSONObject obj) throws JSONException {
//        Device device = RoomManager.getInstance(context).searchDevice(obj.getString("devMac"));
//        String devMac = obj.getString("devMac");
//        String time = obj.getString("time");
//        String isRead = SecurityAlermHistoryManager.UN_READ;
//        SafetyDeviceAlarmHistoryModel model = new SafetyDeviceAlarmHistoryModel();
//        model.setDevMac(devMac);
//        model.setRoom(RoomManager.getInstance(context).getRoomById(device.getRoomId()).getName());
//        model.setTime(time);
//        model.setRead(isRead);
//        model.setDevType(device.getRctype());
//        model.setDevName(device.getName());
//        model.setType(getAlarmType(obj.getString("type")));
//        SharedPreferences spf = context.getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
//        model.setUserName(spf.getString("login_name", ""));
//        List<SafetyDeviceAlarmHistoryModel> list = new ArrayList<SafetyDeviceAlarmHistoryModel>();
//        list.add(model);
//        SecurityAlermHistoryManager.getInstance().insertAlermHistory(list);
//        sendBroadcast(new Intent(SecurityAllDeviceActivity.NEED_REFRESH));

    }

    private void handlLockPush(Context context, JSONObject obj, int pushTyupe) throws JSONException {
//        Device device = RoomManager.getInstance(context).searchDevice(obj.getString("devMac"));
//        if (device == null)
//            return;
//        String deviceName = device.getName();
//        Room room = RoomManager.getInstance(context).getRoomById(device.getRoomId());
//        String userName = "";
//        if (!obj.isNull("openLockUser")) {
//            userName = obj.getString("openLockUser");
//        }
//        String time = obj.getString("time");
//        int type = obj.getInt("type");
//        int openMethod = 0;
//        int rawId = R.raw.alarm;
//        if (type == 4) {//开锁提示
//            openMethod = obj.getInt("openMethod");
//            rawId = 0;
//        }
//        GLog.i(TAG, "---userName=" + userName);
//        SmartLockHistory slh = new SmartLockHistory();
//        slh.setUserName(userName);
//        slh.setType(type);
//        slh.setOpenMethod(openMethod);
//        slh.setRoom(room.getName());
//        slh.setDevMac(device.getAddr());
//        slh.setDevName(deviceName);
//        slh.setTime(time);
//        DeviceManager.getInstance().addSmartLockHistory(slh, null);
//        String title = context.getResources().getString(R.string.alarm_tip);
////        if (type == 4) {
////            userName = getUserNameByOpenMethod(device.getAddr(), userName, openMethod);
////        }
//
//        String msg = buildLockMsg(this, deviceName, userName, type);
//
//        if (SystemUtil.isRunningForeground(context)) {//app在前台
//
//            Intent it = new Intent(context, CustomDialog.class);
//            it.putExtra("headTitle", title);
//            it.putExtra("replaceTip", msg);
//            it.putExtra("sureText", context.getResources().getString(R.string.check));
//            it.putExtra("cancleText", context.getResources().getString(R.string.cancle));
//            it.putExtra(CustomDialog.ATTR_TARGET, CustomDialog.TARGET_LOCK_HISTORY);
//            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
////            it.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            context.startActivity(it);
//        } else {
//            Intent it = new Intent(context, SmartLockAlarmHistoryActivity.class);
//            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            PendingIntent pIntent = PendingIntent.getActivity(
//                    context, 0, it, PendingIntent.FLAG_UPDATE_CURRENT);
//            SystemUtil.sendNotification(context, pIntent, title, msg, rawId);
//        }
    }

//    public static String buildLockMsg(Context ctx, String deviceName, String userName, int type) {
//        String msg = "";
//        switch (type) {
//            case 1:
//                msg = deviceName + ctx.getString(R.string.PICK_LOCK_ALARM_FORMAT) + "," + ctx.getString(R.string.please_note);
//                break;
//            case 2:
//                msg = deviceName + ctx.getString(R.string.TRY_SIXTIMES_ALARM_FORMAT) + "," + ctx.getString(R.string.please_note);
//                break;
//            case 3:
//                msg = deviceName + ctx.getString(R.string.LOWER_POWER_ALARM_FORMAT) + "," + ctx.getString(R.string.please_note);
//                break;
//            case 4:
//                if ("".equals(userName))
//                    msg = deviceName + ctx.getString(R.string.opened) + "," + ctx.getString(R.string.please_note);
//                else
//                    msg = userName + ctx.getString(R.string.opened) + deviceName + "," + ctx.getString(R.string.please_note);
//                break;
//            case 5:
//                msg = deviceName + ctx.getString(R.string.set_up_defence) + "," + ctx.getString(R.string.please_note);
//                break;
//            case 6:
//                msg = deviceName + ctx.getString(R.string.remove_defence) + "," + ctx.getString(R.string.please_note);
//                break;
//            case 7:
//                msg = deviceName + ctx.getString(R.string.open_by_hijack) + "," + ctx.getString(R.string.please_note);
//                break;
//            case 8:
//                msg = deviceName + ctx.getString(R.string.open_by_key) + "," + ctx.getString(R.string.please_note);
//                break;
//        }
//        return msg;
//    }

    private boolean handleAlarm(Context context, JSONObject obj, int pushType) throws JSONException {
//        Device device = RoomManager.getInstance(context).searchDevice(obj.getString("devMac"));
//        String deviceName = "";
//        String alertType = "";
//        if (device != null) {
//            deviceName = device.getName();
//            if (device.getRctype().equals(DeviceType.SMART_LOCK)) {//已过时
//                SmartLockModel slm = DeviceManager.getInstance().getSmartLockInfoSyn(device.getId());
//                if (slm != null) {
//                    long last = slm.getLastTime();
//                    if (System.currentTimeMillis() / 1000 - last < 6) {
//                        return false;
//                    }
//                }
//            }
//
//            if (!obj.isNull("type")) {
//                int id = getPowerControlAlertMsg(obj.getString("type"));
//                if (id != 0) {
//                    alertType = context.getResources().getString(id);
//                }
//            }
//        } else {
//            return false;
//        }
//
//
//        Intent intent;
//        // 获取PendingIntent,点击时发送该Intent
//        if (pushType == PUSH_TYPE_OUTLET) {
//            intent = new Intent(context,
//                    PowerCaculatorActivity.class);
//            Bundle bundDevice = new Bundle();
//            bundDevice.putSerializable("deviceinfo", device);
//            intent.putExtras(bundDevice);
//
//        } else {
//            intent = new Intent(context,
//                    SecurityAlermHistoryActivity.class);
//            insertAlertData(this, obj);
//
//        }
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        PendingIntent pIntent = PendingIntent.getActivity(
//                context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        String text = String.format(context.getResources().getString(R.string.accpet_a_alert), deviceName, alertType);
//        String title = context.getResources().getString(R.string.alert_tips);
//        if (SystemUtil.isRunningForeground(context)) {//app在前台
//            Bundle bundle = new Bundle();
//            bundle.putString("headTitle", title);
//            bundle.putString("replaceTip", text);
//            bundle.putString("sureText", context.getResources().getString(R.string.check));
//            bundle.putString("cancleText", context.getResources().getString(R.string.cancle));
//            if (pushType == PUSH_TYPE_OUTLET) {
//                bundle.putBoolean("isNeedIntentPowerControl", true);
//            } else {
//                bundle.putBoolean("isNeedIntentAlermHistory", true);
//            }
//            bundle.putSerializable("deviceinfo", device);
//            Intent intentDialog = new Intent(context, CustomDialog.class);
//            intentDialog.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intentDialog.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//            intentDialog.putExtras(bundle);
//            context.startActivity(intentDialog);
//        } else {
//            SystemUtil.sendNotification(context, pIntent, title, text, R.raw.alarm);
//        }
        return true;
    }

    private int getPowerControlAlertMsg(String type) {
        int strID = 0;

//        GLog.i(TAG, "alarm type:" + type);
//        if (type.equalsIgnoreCase("OVER_VOL_ALARM_FORMAT")) {//过压
//            strID = R.string.string_over_voltage_1;
//        } else if (type.equalsIgnoreCase("UNDER_VOL_ALARM_FORMAT")) {//欠压
//
//            strID = R.string.string_less_voltage_1;
//        } else if (type.equalsIgnoreCase("OVER_LOAD_ALARM_FORMAT")) {//过载
//
//            strID = R.string.string_heavy_voltage_1;
//        } else if (type.equalsIgnoreCase("UNDER_LOAD_ALARM_FORMAT")) {//轻载
//
//            strID = R.string.string_light_voltage_1;
//        } else if (type.equalsIgnoreCase("LOAD_LOSE_ALARM_FORMAT")) {//失效
//
//            strID = R.string.string_unuseless_1;
//        } else if (type.equalsIgnoreCase("OPEN_LOCK_ALARM_FORMAT")) {
//            strID = R.string.open;
//        }
        return strID;
    }

    private int getAlarmType(String type) {
        int t = 0;
        switch (type) {
            case "OPEN_LOCK_ALARM_FORMAT":
                t = 4;
                break;
            case "PICK_LOCK_ALARM_FORMAT":
                t = 1;
                break;
            case "TRY_SIXTIMES_ALARM_FORMAT":
                t = 2;
                break;
            case "LOWER_POWER_ALARM_FORMAT":
                t = 3;
                break;

            default:
                break;
        }

        return t;
    }

    private void handleUserMsg(Context context, JSONObject data) throws JSONException {
//        int type = data.getInt("type");
//        if (type == 1) {//修改密码
//            if (iSmartApplication.getApp().getIsmartuser() == null) {//未登陆
//                GLog.i(TAG, "未登陆");
//                return;
//            }
//
//            iSmartApplication.getApp().setIsmartuser(null);
//
//            SharedPreferences.Editor editor = getSharedPreferences("CONFIG", Context.MODE_PRIVATE).edit();
//            editor.putString("login_password", null).commit();
//
//            if (SystemUtil.isRunningForeground(PushHandlerService.this)) {
//                Message msg = mHandler.obtainMessage();
//                msg.what = MSG_PWSSWORD_CHANGE;
//                msg.obj = getString(R.string.password_change);
//                mHandler.sendMessage(msg);
//            } else {
//                Intent it = new Intent(PushHandlerService.this, SplashActivity.class);
//                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                PendingIntent pi = PendingIntent.getActivity(
//                        PushHandlerService.this, 0, it, PendingIntent.FLAG_UPDATE_CURRENT);
//                SystemUtil.sendNotification(PushHandlerService.this, pi, getString(R.string.zq_notice), getString(R.string.password_change));
//
//                Intent it1 = new Intent();
//                it1.setAction(Constant.ACTION_PASSWORD_CHANGE);
//                sendBroadcast(it1);
//            }
//
//        }
    }

    private String getUserNameByOpenMethod(String deviceAddr, String userName, int openMethod) {
        if (null == userName)
            return null;
        final String[] name = new String[1];
        name[0] = userName;
        if ((openMethod & 1) == 1 || (openMethod & 2) == 2 || (openMethod & 8) == 8 || (openMethod & 16) == 16) {
            //非APP打开替换设置的userID名字
            if (NumberUtil.isNumber(userName)) {
                int userId = Integer.valueOf(userName);
                if (userId <= 999 && userId >= 0) {
                    final byte[] lock = new byte[1];
                    synchronized (lock) {
                        DeviceManager.getInstance().getSmartLockUserIDByUserId(deviceAddr, userId, new DataModifyHandler<SmartLockUserIdNameModel>() {
                            @Override
                            public void onResult(SmartLockUserIdNameModel model, Exception e) {
                                synchronized (lock) {
                                    if (null != model) {
                                        name[0] = model.getUserName();
                                    }
                                    lock.notify();
                                }
                            }
                        });
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }
        GLog.i(TAG, "---name[0]=" + name[0]);
        return name[0];
    }

    private PowerManager.WakeLock wakeLock = null;

    /**
     * 获取电源锁，保持该服务在屏幕熄灭时仍然获取CPU时，保持运行
     */
    private void acquireWakeLock() {
        if (null == wakeLock) {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK
                    | PowerManager.ON_AFTER_RELEASE, getClass()
                    .getCanonicalName());
            if (null != wakeLock) {
                GLog.i("PM", "call acquireWakeLock");
                wakeLock.acquire();
            }
        }
    }

    // 释放设备电源锁
    private void releaseWakeLock() {
        if (null != wakeLock && wakeLock.isHeld()) {
            GLog.i("PM", "call releaseWakeLock");
            wakeLock.release();
            wakeLock = null;
        }
    }

}
