package com.guogee.tablet.service;

import java.util.List;


public interface DaoInterface<T> {
	public int insertItem(T obj);
	/**
	 * 根据列查询对象
	 * @param con 查询条件
	 * @param conJoin 查询条件拼接方式
	 * @return 对象列表
	 */
	public List<T> getItemByFeiled(List<String> con, String conJoin, String orderby);
	public List<T> getItemInFeiled(String feiled, List<String> con, String order);
	public boolean deleteItemByFeiled(List<String> con, String conJoin);
	public boolean updateItemByFeiled(T obj, List<String> con, String conJoin);
	public String getMax(String col, List<String> con, String conJoin);
	
	public List<T> selectQuery(String sql);
	public int excute(String sql);
	
	public boolean insertItemByTransaction(List<T> obj);//批量插入
}
