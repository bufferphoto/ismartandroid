/**
 *
 */
package com.guogee.tablet.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;


import com.guogee.tablet.ui.activity.PerformSceneActivity;
import com.guogee.tablet.ui.activity.VoiceActivity;
import com.guogee.ismartandroid2.utils.GLog;

import com.guogee.tablet.ui.activity.LoginActivity;

/**
 * @author xieguangwei
 * @ClassName: ActivityManager.java
 * @Description: TODO
 * @date 2015年11月3日
 */
public class ActivityManager {
    public static final String ACTIVITY_CUSTOM_EDIT = "ACTIVITY_CUSTOM_EDIT";
    public static final String ACTIVITY_LIGHTGROUP_EDIT = "ACTIVITY_LIGHTGROUP_EDIT";
    public static final String ACTIVITY_VOICE = "ACTIVITY_VOICE";
    public static final String ACTIVITY_ADD_MAGIC = "ACTIVITY_ADD_MAGIC";
    public static final String ACTIVITY_NOTIFICATION = "ACTIVITY_NOTIFICATION";
    public static final String ACTIVITY_LOCATION_SCENE = "ACTIVITY_LOCATION_SCENE";
    public static final String ACTIVITY_CONSTANT_TEMPERATURE = "ACTIVITY_CONSTANT_TEMPERATURE";
    public static final String ACTIVITY_SCENE_TIMER = "ACTIVITY_SCENE_TIMER";
    public static final String ACTIVITY_LOGIN = "ACTIVITY_LOGIN";//登陆
    public static final String ACTIVITY_SCENE_DETAIL = "ACTIVITY_SCENE_DETAIL";//场景详细
    public static final String ACTIVITY_SCENE_EDIT = "ACTIVITY_SCENE_EDIT";
    public static final String ACTIVITY_ALARM_HISTORY = "ACTIVITY_ALARM_HISTORY";
    public static final String ACTIVITY_PACKAGE_SCAN = "ACTIVITY_PACKAGE_SCAN";//套餐包
    public static final String ACTIVITY_NIGHT_HELPER = "ACTIVITY_NIGHT_HELPER";//起夜助手
    public static final String ACTIVITY_NIGHT_HELPER_DEVICE = "ACTIVITY_NIGHT_HELPER_DEVICE";//起夜助手选择设备
    public static final String ACTIVITY_CONSTANT_HUMIDITY = "ACTIVITY_CONSTANT_HUMIDITY";//恒湿
    public static final String ACTIVITY_CONSTANT_HUMIDITY_DEVICE = "ACTIVITY_CONSTANT_HUMIDITY_DEVICE";//恒湿选择设备
    public static final String ACTIVITY_SMART_LOCK_HISTORY = "ACTIVITY_SMART_LOCK_HISTORY";
    public static final String ACTIVITY_OPEN_RECORD = "ACTIVITY_OPEN_RECORD";//开锁记录
    public static final String ACTIVITY_SMARTLOCK_OPENED_MAGIC = "ACTIVITY_LOCK_OPENED_MAGIC";//门锁开启联动
    public static final String ACTIVITY_SMARTLOCK_SET_SAFTY_MAGIC = "ACTIVITY_SMARTLOCK_SET_SAFTY_MAGIC";//门锁设防联动
    public static final String ACTIVITY_WIFI_GATEWAY_CONF = "ACTIVITY_WIFI_GATEWAY_CONF";//WIFI网关配置
    public static final String ACTIVITY_PIR_LINKAGER_MAGIC = "ACTIVITY_PIR_LINKAGER_MAGIC";//门锁开启联动
    public static final String ACTIVITY_LOCK_CONFIG_USER_ID = "ACTIVITY_LOCK_CONFIG_USER_ID";//配置锁用户userId
    private static final String TAG = "ActivityManager";

    public static void gotoActivity(Activity activity, String name, Intent intent) {
        GLog.i(TAG, "goto activity " + name);
        if (intent == null)
            intent = new Intent();
        setIntent(activity, name, intent);

        activity.startActivity(intent);
    }

    public static void gotoActivityForResult(Activity activity, String name, Intent intent, int code) {
        GLog.i(TAG, "goto activity " + name);
        if (intent == null)
            intent = new Intent();
        setIntent(activity, name, intent);

        activity.startActivityForResult(intent, code);

    }

    public static void gotoActivityForResult(Fragment activity, String name, Intent intent, int code) {
        GLog.i(TAG, "goto activity " + name);
        if (intent == null)
            intent = new Intent();
        setIntent(activity.getContext(), name, intent);

        activity.startActivityForResult(intent, code);

    }

    private static void setIntent(Context activity, String name, Intent intent) {

        if (ACTIVITY_LOGIN.equals(name)) {
            intent.setClass(activity, LoginActivity.class);
        }else if (ACTIVITY_VOICE.equals(name)) {//语音
            intent.setClass(activity, VoiceActivity.class);
        } else if (ACTIVITY_SCENE_DETAIL.equals(name)) {
            intent.setClass(activity, PerformSceneActivity.class);
        }
    }
}
