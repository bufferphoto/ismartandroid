package com.guogee.tablet.manager;

/**
 * Created by xieguangwei on 16/3/8.
 */
public class UserDataManager {

    public static final String SP_NAME = "CONFIG";

    public static final String LOGIN_NAME = "login_name";

    public static final String LOGIN_PASSWD = "login_password";

    public static final String LAST_VERSION = "lastVersion";

    public static final String SAVE_SMART_LOCK_PWD = "save_lock_pwd";
}
