/**
 *
 */
package com.guogee.tablet.manager;


import com.guogee.tablet.dao.DAOFactory;
import com.guogee.tablet.db.DbHelper;
import com.guogee.tablet.model.SmartLockHistory;
import com.guogee.tablet.model.SmartLockModel;
import com.guogee.tablet.model.SmartLockUserIdNameModel;
import com.guogee.tablet.service.DaoInterface;
import com.guogee.ismartandroid2.manager.DataModifyHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author xieguangwei
 * @ClassName: DeviceManager.java
 * @Description: 设备管理类
 * @date 2015年11月4日
 */
public class DeviceManager {
    private static volatile DeviceManager mInstance;
    private DaoInterface<SmartLockModel> mSmartLockDao;
    private DaoInterface<SmartLockHistory> mSmartLockHistoryDao;
    private DaoInterface<SmartLockUserIdNameModel> mSmartLockUserIdNameModeDao;

    private ExecutorService mPool;

    private DeviceManager() {
        mSmartLockDao = DAOFactory.getDao(DAOFactory.DaoType.SmartLock);
        mSmartLockHistoryDao = DAOFactory.getDao(DAOFactory.DaoType.SmartLockHistory);
        mSmartLockUserIdNameModeDao = DAOFactory.getDao(DAOFactory.DaoType.SmartLockUserID);
        mPool = Executors.newFixedThreadPool(1);
    }

    public static final DeviceManager getInstance() {
        if (mInstance == null) {
            synchronized (DeviceManager.class) {
                if (mInstance == null) {
                    mInstance = new DeviceManager();
                }
            }
        }

        return mInstance;
    }

    public void getSmartLockInfoList(final DataModifyHandler<List<SmartLockModel>> handler) {
        if (handler == null) {
            return;
        }
        mPool.execute(new Runnable() {

            @Override
            public void run() {
                List<SmartLockModel> list = mSmartLockDao.getItemByFeiled(null, null, null);
                handler.onResult(list, null);

            }
        });
    }

    /*
     * 查找密码锁信息
     */
    public void getSmartLockInfo(final int deviceId, final DataModifyHandler<SmartLockModel> handler) {
        if (handler == null)
            return;
        mPool.execute(new Runnable() {

            @Override
            public void run() {
                List<String> con = new ArrayList<String>();
                con.add(DbHelper.SmartLockCollections.DEVICE_ID + "=" + deviceId);
                List<SmartLockModel> list = mSmartLockDao.getItemByFeiled(con, null, null);
                SmartLockModel slm = null;
                if (list.size() > 0)
                    slm = list.get(0);
                handler.onResult(slm, null);

            }
        });
    }

    /*
     * 查找密码锁信息
     */
    @Deprecated
    public SmartLockModel getSmartLockInfoSyn(final int deviceId) {

        List<String> con = new ArrayList<String>();
        con.add(DbHelper.SmartLockCollections.DEVICE_ID + "=" + deviceId);
        List<SmartLockModel> list = mSmartLockDao.getItemByFeiled(con, null, null);
        SmartLockModel slm = null;
        if (list.size() > 0)
            slm = list.get(0);
        return slm;
    }

    /**
     * 添加智能锁信息
     *
     * @param slm
     * @param handler
     */
    public void addSmartLockInfo(final SmartLockModel slm, final DataModifyHandler<SmartLockModel> handler) {
        if (slm == null)
            return;
        mPool.execute(new Runnable() {

            @Override
            public void run() {
                slm.setId(mSmartLockDao.insertItem(slm));
                if (handler != null) {
                    handler.onResult(slm, null);

                }
            }
        });
    }

    /**
     * 更新智能锁信息
     *
     * @param slm
     * @param handler
     */
    public void updateSmartLockInfo(final SmartLockModel slm, final DataModifyHandler<SmartLockModel> handler) {
        if (slm == null)
            return;
        mPool.execute(new Runnable() {

            @Override
            public void run() {
                List<String> con = new ArrayList<String>();
                con.add(DbHelper.SmartLockCollections.ID + "=" + slm.getId());
                mSmartLockDao.updateItemByFeiled(slm, con, null);
                if (handler != null)
                    handler.onResult(slm, null);

            }
        });
    }

    /**
     * 删除智能锁信息
     *
     * @param slm
     * @param handler
     */
    public void deleteSmartLockInfo(final SmartLockModel slm, final DataModifyHandler<SmartLockModel> handler) {
        if (slm == null)
            return;
        mPool.execute(new Runnable() {

            @Override
            public void run() {
                List<String> con = new ArrayList<String>();
                con.add(DbHelper.SmartLockCollections.ID + "=" + slm.getId());
                mSmartLockDao.deleteItemByFeiled(con, null);
                if (handler != null)
                    handler.onResult(slm, null);

            }
        });
    }

    public void addSmartLockHistory(final SmartLockHistory slh, final DataModifyHandler<SmartLockHistory> handler) {
        if (slh == null)
            return;
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                slh.setId(mSmartLockHistoryDao.insertItem(slh));
                if (handler != null)
                    handler.onResult(slh, null);
            }
        });
    }

    public void getSmartLockHistory(final DataModifyHandler<List<SmartLockHistory>> handler) {
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                List<SmartLockHistory> list = mSmartLockHistoryDao.getItemByFeiled(null, null, DbHelper.SmartLockAlarmHistoryTable.ID + " DESC");
                handler.onResult(list, null);
            }
        });
    }

    public void updateSmartLockHistory(final SmartLockHistory slh) {
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                List<String> con = new ArrayList<String>();
                con.add(DbHelper.SmartLockAlarmHistoryTable.ID + "=" + slh.getId());
                mSmartLockHistoryDao.updateItemByFeiled(slh, con, null);
            }
        });
    }

    public void getAllSmartLockUserID(final DataModifyHandler<List<SmartLockUserIdNameModel>> handler) {
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                List<SmartLockUserIdNameModel> list = mSmartLockUserIdNameModeDao.getItemByFeiled(null, null, DbHelper.LockUserIDHelperCollection.LOCK_USER_ID + " ASC");
                if (handler != null)
                    handler.onResult(list, null);
            }
        });
    }

    public void getAllSmartLockUserIDByDeviceAddr(final String deviceAddr, final DataModifyHandler<List<SmartLockUserIdNameModel>> handler) {
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                List<String> con = new ArrayList<String>();
                con.add(DbHelper.LockUserIDHelperCollection.DEVICE_ADDR + " = '" + deviceAddr + "'");
                List<SmartLockUserIdNameModel> list = mSmartLockUserIdNameModeDao.getItemByFeiled(con, null, DbHelper.LockUserIDHelperCollection.LOCK_USER_ID + " ASC");
                if (handler != null)
                    handler.onResult(list, null);
            }
        });
    }

    public void querySmartLockUserIdName(final String deviceAddr, final String userName , final DataModifyHandler<SmartLockUserIdNameModel> handler){
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                List<String> con = new ArrayList<String>();
                con.add(DbHelper.LockUserIDHelperCollection.DEVICE_ADDR + " = '" + deviceAddr + "'");
                con.add(" and ");
                con.add(DbHelper.LockUserIDHelperCollection.USER_NAME + " = '" + userName + "'");
                List<SmartLockUserIdNameModel> model = mSmartLockUserIdNameModeDao.getItemByFeiled(con, null, null);
                if (null != handler) {
                    if (null != model && model.size() > 0) {
                        handler.onResult(model.get(0), null);
                    } else {
                        handler.onResult(null, null);
                    }
                }
            }
        });
    }

    public void getSmartLockUserIDByUserId(final String deviceAddr, final int lockUserId, final DataModifyHandler<SmartLockUserIdNameModel> handler) {
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                List<String> con = new ArrayList<String>();
                con.add(DbHelper.LockUserIDHelperCollection.DEVICE_ADDR + " = '" + deviceAddr + "'");
                con.add(" and ");
                con.add(DbHelper.LockUserIDHelperCollection.LOCK_USER_ID + " = " + lockUserId);
                List<SmartLockUserIdNameModel> model = mSmartLockUserIdNameModeDao.getItemByFeiled(con, null, null);
                if (null != handler) {
                    if (null != model && model.size() > 0) {
                        handler.onResult(model.get(0), null);
                    } else {
                        handler.onResult(null, null);
                    }
                }
            }
        });
    }

    public void addOrUpdateSmartLockUserIdName(final SmartLockUserIdNameModel model, final DataModifyHandler<SmartLockUserIdNameModel> handler) {
        if (null == model) {
            return;
        }
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                if (model.getId() <= 0) {
                    //add
                    model.setId(mSmartLockUserIdNameModeDao.insertItem(model));
                } else {
                    //update
                    List<String> con1 = new ArrayList<String>();
                    con1.add(DbHelper.LockUserIDHelperCollection.ID + "=" + model.getId());
                    mSmartLockUserIdNameModeDao.updateItemByFeiled(model, con1, null);
                }
                if (handler != null)
                    handler.onResult(model, null);
            }
        });
    }


    public void deleteSmartLockUserIdName(final SmartLockUserIdNameModel model, final DataModifyHandler<SmartLockUserIdNameModel> handler) {
        if (null == model) {
            return;
        }
        mPool.execute(new Runnable() {
            @Override
            public void run() {
                List<String> con = new ArrayList<String>();
                con.add(DbHelper.LockUserIDHelperCollection.ID + "=" + model.getId());
                mSmartLockUserIdNameModeDao.deleteItemByFeiled(con, null);
                if (handler != null)
                    handler.onResult(model, null);
            }
        });
    }

}
