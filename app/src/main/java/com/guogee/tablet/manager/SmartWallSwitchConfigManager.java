package com.guogee.tablet.manager;

import com.guogee.tablet.dao.AbstractDao;
import com.guogee.tablet.dao.DAOFactory;
import com.guogee.tablet.db.DbHelper;
import com.guogee.tablet.model.SmartWallSwitchConfigModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/9/13.
 */
public class SmartWallSwitchConfigManager {
    private volatile static SmartWallSwitchConfigManager instance;
    private AbstractDao<SmartWallSwitchConfigModel> dao;

    private SmartWallSwitchConfigManager() {
        dao = DAOFactory.getDao(DAOFactory.DaoType.SmartWallSwitchConfig);
    }

    public static SmartWallSwitchConfigManager getInstance() {
        if (instance == null) {
            synchronized (TimerManager.class) {
                if (instance == null) {
                    instance = new SmartWallSwitchConfigManager();
                }
            }
        }
        return instance;
    }

    public List<SmartWallSwitchConfigModel> querySmartWallSwitchConfig(int deviceId) {
        List<String> listValue = new ArrayList<String>();
        listValue.add(DbHelper.SmartWallSwitchHelperCollection.DEVICE_ID + " = " + deviceId);
        return dao.getItemByFeiled(listValue, DbHelper.SqlCondition.AND, null);
    }

    public void addOrUpdateSmartWallSwitchConfig(SmartWallSwitchConfigModel model) {
        if (model.getId() <= 0) {
            //插入
            dao.insertItem(model);
        } else {
            //更新
            List<String> listValue = new ArrayList<String>();
            listValue.add(DbHelper.SmartWallSwitchHelperCollection.DEVICE_ID + " = " + model.getDeviceId());
            listValue.add(DbHelper.SmartWallSwitchHelperCollection.NUM + " = " + model.getNum());
            dao.updateItemByFeiled(model, listValue, DbHelper.SqlCondition.AND);
        }
    }

//    public boolean updateSmartWallSwitchConfig(SmartWallSwitchConfigModel model) {
//        List<String> listValue = new ArrayList<String>();
//        listValue.add(DbHelper.SmartWallSwitchHelperCollection.DEVICE_ID + " = " + model.getDeviceId());
//        listValue.add(DbHelper.SmartWallSwitchHelperCollection.INDEX + " = " + model.getIndex());
//        return dao.updateItemByFeiled(model, listValue, DbHelper.SqlCondition.AND);
//    }

    public boolean deleteSmartWallSwitchConfig(int deviceId) {
        List<String> listValue = new ArrayList<String>();
        listValue.add(DbHelper.SmartWallSwitchHelperCollection.DEVICE_ID + " = " + deviceId);
        return dao.deleteItemByFeiled(listValue, DbHelper.SqlCondition.AND);
    }

    public List<SmartWallSwitchConfigModel> getAllSmartWallSwitchConfigs() {
        return dao.getItemByFeiled(null, null, null);
    }
}
