package com.guogee.tablet.manager;

import com.guogee.tablet.dao.AbstractDao;
import com.guogee.tablet.dao.DAOFactory;
import com.guogee.tablet.db.DbHelper;
import com.guogee.tablet.model.CameraModel;


import java.util.ArrayList;
import java.util.List;

public class CameraManager {
	private volatile static CameraManager mInstance;
	
	private AbstractDao<CameraModel> dao;
	public CameraManager(){
		dao = DAOFactory.getDao(DAOFactory.DaoType.Camera);
	}
	
	public static CameraManager getInstance(){
		synchronized (CameraManager.class) {
			if (mInstance == null) {
				mInstance = new CameraManager();
				return mInstance; 
			}else {
				return mInstance;
			}
		}
	}
	
	public boolean addCamera(CameraModel model){
		deleteCameraByCameraID(model.getCameraID());
		dao.insertItem(model);
		return true;
	}
	
	
	public boolean deleteCameraByCameraID(String cameraID){
		List<String> con = new ArrayList<String>();
		con.add(DbHelper.CameraColelction.CAMERA_ID +" = '"+cameraID+"'");
		return dao.deleteItemByFeiled(con, DbHelper.SqlCondition.AND);
	}
	
	public boolean deleteAllCamera(){
		return dao.deleteItemByFeiled(null, null);
	}
	
	public boolean updateCameraByCamaraID(CameraModel model,String cameraID){
		List<String> con = new ArrayList<String>();
		con.add(DbHelper.CameraColelction.CAMERA_ID +" = '"+cameraID+"'");
		return dao.updateItemByFeiled(model, con, DbHelper.SqlCondition.AND);
	}
	
	public List<CameraModel> getAllCamera(){
		return dao.getItemByFeiled(null, null, null);
	}
	
	public CameraModel getCameraByCameraID(String cameraId){
		List<String> con = new ArrayList<String>();
		con.add(DbHelper.CameraColelction.CAMERA_ID +" = '"+cameraId+"'");
		List<CameraModel> list  = dao.getItemByFeiled(con, DbHelper.SqlCondition.AND, null);
		if (list != null & list.size() > 0) {
			return list.get(0);
		}else {
			return null;
		}
	}
	
	public boolean updateCameraName(CameraModel mode,String cameraID){
		List<String> con = new ArrayList<String>();
		con.add(DbHelper.CameraColelction.CAMERA_ID +" = '"+cameraID+"'");
		return dao.updateItemByFeiled(mode, con, DbHelper.SqlCondition.AND);
	}
	
}
