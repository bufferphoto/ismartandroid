/**
 *
 */
package com.guogee.tablet.manager;

import android.content.Context;

import com.guogee.ismartandroid2.manager.DataModifyHandler;
import com.guogee.ismartandroid2.manager.KeyManager;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.manager.SceneManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.model.GatewayInfo;
import com.guogee.ismartandroid2.model.IRKey;
import com.guogee.ismartandroid2.model.Room;
import com.guogee.ismartandroid2.model.Scene;
import com.guogee.ismartandroid2.model.SceneAction;
import com.guogee.ismartandroid2.networkingProtocol.DeviceType;
import com.guogee.tablet.utils.Encoder;
import com.guogee.sdk.scene.ActionData;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.model.SmartLockModel;
import com.guogee.tablet.ui.activity.PerformSceneActivity.ActionView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xieguangwei
 * @ClassName: SceneActionManager.java
 * @Description: TODO
 * @date 2015年12月3日
 */
public class SceneActionManager {
    private volatile static SceneActionManager mInstance;
    private Context mContext;

    private SceneActionManager(Context ctx) {
        mContext = ctx;
    }

    public static SceneActionManager getInstance(Context ctx) {

        if (mInstance == null) {
            synchronized (SceneActionManager.class) {
                if (mInstance == null) {
                    mInstance = new SceneActionManager(ctx);
                }
            }
        }

        return mInstance;
    }

    public List<ActionView> loadActions(Scene scene) {

        List<SceneAction> seceneActions = scene.getActions(mContext);
        List<ActionView> sceneDataList = new ArrayList<ActionView>();

        for (SceneAction sceneAction : seceneActions) {
            int deviceType = sceneAction.getDevType();

            String gw = null;
            Device gwd = RoomManager.getInstance(mContext).searchGatewayByDeviceId(sceneAction.getDevId());
            if (gwd != null) {
                gw = gwd.getAddr();
            }
            ActionData data = new ActionData(sceneAction.getSceneId(), sceneAction.getActionId(), sceneAction.getDevType(), sceneAction.getDevVer(), sceneAction.getDevMac(),
                    sceneAction.getCmd(), sceneAction.getData(), false, gw);
            final ActionView av = new ActionView();
            av.action = sceneAction;
            av.deviceInfo = RoomManager.getInstance(mContext).searchDevice(sceneAction.getDevId());
            av.status = data;
            av.cmdData = data.getData();
            if (deviceType == DeviceType.DEVICE_SECURITY) {//安防
                Device dev = new Device();
                String datas = sceneAction.getData();
                if ("".equals(datas)) {
                    dev.setRoomId(av.deviceInfo.getRoomId());
                } else {
                    dev.setRoomId(Integer.parseInt(sceneAction.getData()));

                }
                Room room = RoomManager.getInstance(mContext).getRoomById(dev.getRoomId());
                if (room == null) {
                    SceneManager.getInstance(mContext).getSceneById(sceneAction.getSceneId()).removeAction(mContext, sceneAction, null);
                }
                GatewayInfo gi = room.getGateway(mContext);
                if (gi != null)
                    data.setGatewayMac(gi.getDeviceInfo().getAddr());
                dev.setDevicetype(sceneAction.getDevType());
                av.deviceInfo = dev;
            } else if (DeviceType.DEVICE_AIRCOND == deviceType ||
                    DeviceType.DEVICE_DVD == deviceType ||
                    DeviceType.DEVICE_FAN == deviceType ||
                    DeviceType.DEVICE_TV == deviceType ||
                    DeviceType.DEVICE_TVBox == deviceType ||
                    DeviceType.DEVICE_IPTV == deviceType ||
                    DeviceType.DEVICE_PROJECTOR == deviceType) {
                int cmd = Integer.parseInt(sceneAction.getData());
                Device device = RoomManager.getInstance(mContext).searchDevice(sceneAction.getDevId());
                List<Device> devices = RoomManager.getInstance(mContext).getRoomById(device.getRoomId()).getDeviceByType(mContext, DeviceType.IR_BOX);
                if (devices.size() > 0) {
                    Device irBox = devices.get(0);
                    IRKey key = KeyManager.getInstance(mContext).getIRKey(sceneAction.getDevId(), cmd);
                    if (key == null || !KeyManager.getInstance(mContext).checkKeyMatch(irBox.getDevicetype(), irBox.getVer(), key.getCodeType())) {//未学习
                        av.notStudy = true;
                    } else {
                        data.setIrKey(key);
                    }
                    data.setIrboxMac(irBox.getAddr());
                } else {
                    av.noIRBox = true;
                }
            } else if (DeviceType.DEVICE_SMART_LOCK == deviceType) {
                synchronized (av) {
                    DeviceManager.getInstance().getSmartLockInfo(sceneAction.getDevId(), new DataModifyHandler<SmartLockModel>() {
                        @Override
                        public void onResult(SmartLockModel smartLockModel, Exception e) {
                            synchronized (av) {
                                try {
                                    if (smartLockModel != null && !smartLockModel.getPwd().equals("")) {
                                        av.status.setPwd(Encoder.decryptDES(smartLockModel.getPwd()));
                                        av.status.setUserId(smartLockModel.getUserId());
                                    } else {
                                        av.status.setPwd("1");
                                        av.status.setUserId(1000);
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                } finally {
                                    av.notify();
                                }
                            }
                        }
                    });
                    try {
                        av.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else if (deviceType == DeviceType.DEVICE_CURTAIN_OPENCLOSE_DOUBLE || deviceType == DeviceType.DEVICE_CURTAIN_ROLL_DOUBLE) {
                String[] addrs = av.deviceInfo.getAddr().split("&");
                Device dev = new Device();
                if (addrs.length == 2) {
                    if (addrs[0].equalsIgnoreCase(sceneAction.getDevMac())) {
                        dev.setName(av.deviceInfo.getName() + "(" + mContext.getString(R.string.curtain_cloth_string) + ")");
                    } else if (addrs[1].equalsIgnoreCase(sceneAction.getDevMac())) {
                        dev.setName(av.deviceInfo.getName() + "(" + mContext.getString(R.string.curtain_sha_string) + ")");
                    } else {
                        dev.setName(av.deviceInfo.getName());
                    }
                    dev.setRctype(av.deviceInfo.getRctype());
                    dev.setDevicetype(av.deviceInfo.getDevicetype());
                    dev.setVer(av.deviceInfo.getVer());
                    dev.setAddr(sceneAction.getDevMac());
                    dev.setRoomId(av.deviceInfo.getRoomId());
                    dev.setOrders(av.deviceInfo.getOrders());
                }
                av.deviceInfo = dev;
            }

            if (sceneDataList.size() == 0)
                sceneDataList.add(av);
            else {
                for (int i = sceneDataList.size() - 1; i >= 0; i--) {
                    ActionView actionView = sceneDataList.get(i);
                    if (av.action.getOrders() >= actionView.action.getOrders()) {
                        sceneDataList.add(i + 1, av);
                        break;
                    } else if (i == 0) {
                        sceneDataList.add(0, av);
                    }
                }
            }

        }
        return sceneDataList;
    }

    //删除场景里面的智能墙面开关的某一个本地
    public void deleteActionView(int num) {
        List<Scene> scenes = SceneManager.getInstance(mContext).getScenes();
        for (Scene scene : scenes) {
            List<ActionView> sceneDataList = SceneActionManager.getInstance(mContext).loadActions(scene);
            if (null != sceneDataList && sceneDataList.size() > 0) {
                for (ActionView actionView : sceneDataList) {
                    Device device = actionView.deviceInfo;
                    if (null != device && null != device.getRctype()) {
                        if (device.getRctype().equals(DeviceType.SMART_SWITCH_ONE_FALG) ||//多路智能墙面开关
                                device.getRctype().equals(DeviceType.SMART_SWITCH_ONES_FALG) ||
                                device.getRctype().equals(DeviceType.SMART_SWITCH_TWO_FALG) ||
                                device.getRctype().equals(DeviceType.SMART_SWITCH_TWOS_FALG) ||
                                device.getRctype().equals(DeviceType.SMART_SWITCH_THREE_FALG) ||//多路智能墙面开关
                                device.getRctype().equals(DeviceType.SMART_SWITCH_THREES_FALG) ||
                                device.getRctype().equals(DeviceType.SMART_SWITCH_FOUR_FALG) ||
                                device.getRctype().equals(DeviceType.SMART_SWITCH_FOURS_FALG)) {

                            if (num == Integer.parseInt(actionView.action.getData())) {
                                scene.removeAction(mContext, actionView.action, null);
                            }
                        }
                    }
                }
            }
        }
    }


}
