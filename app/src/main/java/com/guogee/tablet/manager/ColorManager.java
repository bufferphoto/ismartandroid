package com.guogee.tablet.manager;

import com.guogee.tablet.dao.AbstractDao;
import com.guogee.tablet.dao.DAOFactory;
import com.guogee.tablet.dao.DAOFactory.DaoType;
import com.guogee.tablet.db.DbHelper.ColorsCollection;
import com.guogee.tablet.db.DbHelper.SqlCondition;
import com.guogee.tablet.model.ColorModel;

import java.util.ArrayList;
import java.util.List;

public class ColorManager {
private volatile static ColorManager mInstance;
	
	private AbstractDao<ColorModel> dao;
	public ColorManager(){
		dao = DAOFactory.getDao(DaoType.Color);	
	}
	
	public static ColorManager getInstance(){
		synchronized (ColorManager.class) {
			if (mInstance == null) {
				mInstance = new ColorManager();
				return mInstance; 
			}else {
				return mInstance;
			}
		}
	}
	
	public boolean addColor(ColorModel model){
		dao.insertItem(model);
		return true;
	}
	
	public boolean deleteColorById(int id){
		List<String> con = new ArrayList<String>();
		con.add(ColorsCollection.ID +" = "+id);
		return dao.deleteItemByFeiled(con, SqlCondition.AND);
	}
	
	public boolean updateColorById(ColorModel model,int id){
		List<String> con = new ArrayList<String>();
		con.add(ColorsCollection.ID +" = "+id);
		return dao.updateItemByFeiled(model, con, SqlCondition.AND);
	}
	
	public List<ColorModel> getAllColor(){
		return dao.getItemByFeiled(null, null, null);
	}
	
	public boolean deleteAllColor(){
		return dao.deleteItemByFeiled(null, null);
	}
}
