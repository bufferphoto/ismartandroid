package com.guogee.tablet.manager;



import com.guogee.tablet.dao.AbstractDao;
import com.guogee.tablet.dao.DAOFactory;
import com.guogee.tablet.db.DbHelper.CustomCollection;
import com.guogee.tablet.db.DbHelper.SqlCondition;
import com.guogee.tablet.model.CustomModel;

import java.util.ArrayList;
import java.util.List;

public class CustomManager {
	private volatile static CustomManager mInstance;
	
	private AbstractDao<CustomModel> dao;
	public CustomManager(){
		dao = DAOFactory.getDao(DAOFactory.DaoType.Custom);
	}
	
	public static CustomManager getInstance(){
		synchronized (CustomManager.class) {
			if (mInstance == null) {
				mInstance = new CustomManager();
				return mInstance; 
			}else {
				return mInstance;
			}
		}
	}
	
	public boolean addCustom(CustomModel model){
		dao.insertItem(model);
		return true;
	}
	
	public boolean deleteCustomeByNameAddress(String name, int devId){
		List<String> con = new ArrayList<String>();
		con.add(CustomCollection.NAME +" = '"+name+"'");
		con.add(CustomCollection.DEVICE_ID +" = "+devId);
		return dao.deleteItemByFeiled(con, SqlCondition.AND);
	}
	
	public boolean deleteCustomByAddress(int devId){
		List<String> con = new ArrayList<String>();
		con.add(CustomCollection.DEVICE_ID +" = "+devId);
		return dao.deleteItemByFeiled(con, SqlCondition.AND);
	}

	/**
	 * 通过按钮的设备id和按钮名称更新按钮
	 * @param model
	 * @return
	 */
	public boolean updateCustomByAddress(CustomModel model){
		List<String> con = new ArrayList<String>();
		con.add(CustomCollection.NAME +" = '"+model.getName()+"'");
		con.add(CustomCollection.DEVICE_ID +" = "+model.getDeviceId());
		return dao.updateItemByFeiled(model, con, SqlCondition.AND);
	}
	
	public List<CustomModel> getCustomByAddress(int devId){
		List<String> con = new ArrayList<String>();
		con.add(CustomCollection.DEVICE_ID +" = "+devId);
		return dao.getItemByFeiled(con, SqlCondition.AND,null);
	}
	
	public CustomModel getCustomByNameAddress(String name, int devId){
		List<String> con = new ArrayList<String>();
		con.add(CustomCollection.NAME +" = '"+name+"'");
		con.add(CustomCollection.DEVICE_ID +" = "+devId);
		List<CustomModel> list = dao.getItemByFeiled(con, SqlCondition.AND, null);
		if (list == null || list.size() < 1) {
			return null;
		}else {
			return list.get(0);
		}	
	}
	
	public List<CustomModel> getAllCustom(){
		return dao.getItemByFeiled(null, null, null);
	}
	
}
