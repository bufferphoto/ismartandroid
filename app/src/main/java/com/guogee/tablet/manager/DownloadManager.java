/**
 * 
 */
package com.guogee.tablet.manager;

import com.guogee.ismartandroid2.utils.GLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 *@ClassName:     DownloadManager.java
 * @Description:   下载管理器
 * @author xieguangwei
 * @date 2015年12月11日
 * 
 */
public class DownloadManager
{
	private static final String TAG = "DownloadManager";
	private static DownloadManager mInstance = new DownloadManager();
	private Thread mDownloadThread;
	private List<DownloadListener> listeners = new ArrayList<DownloadListener>();
	private boolean stop;
	private long netFileLenght;
	private long localFileLenght;
	
	private DownloadManager(){
		
	}
	
	public static DownloadManager getInstance(){
		return mInstance;
	}
	
	public void addDownloadListener(DownloadListener listener){
		listeners.add(listener);
	}
	public void removeDownloadListenr(DownloadListener listener){
		listeners.remove(listener);
	}
	public void stop(){
		stop = true;
	}
	public void download(final String url,final String path){
		stop = false;
		if(mDownloadThread == null || !mDownloadThread.isAlive()){
			mDownloadThread = new Thread(){
				@Override
				public void run(){
					netFileLenght = getNetFileSize(url);
		            localFileLenght = getLocalFileSize(path);
					if (localFileLenght >= netFileLenght) {
						GLog.i(TAG, "下载已完成");
						for (DownloadListener downloadListener : listeners) {
							downloadListener.onDownloadComplete(path);
						}
			            return;
					}
					GLog.i(TAG,"netFileLenght : " + netFileLenght + " localFileLenght : " + localFileLenght);
					final HttpClient httpClient = new DefaultHttpClient();
					httpClient.getParams().setIntParameter("http.socket.timeout", 5000);
					final HttpGet httpGet = new HttpGet(url);
					httpGet.addHeader("Range", "bytes=" + localFileLenght + "-");
					try {
						final HttpResponse response = httpClient.execute(httpGet);
						final int code = response.getStatusLine().getStatusCode();
						final HttpEntity entity = response.getEntity();
						if (entity != null && code < 400) {
							File file = new File(path);
							RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
							randomAccessFile.seek(localFileLenght);
							InputStream inputStream = entity.getContent();
							int b = 0;
							final byte buffer[] = new byte[1024];
							while ((b = inputStream.read(buffer)) != -1&&!stop) {
								randomAccessFile.write(buffer, 0, b);
								localFileLenght+=b;
//								GLog.i(TAG, "====已下载:"+localFileLenght+" 总长度："+netFileLenght);

							}
							randomAccessFile.close();
							inputStream.close();
							httpClient.getConnectionManager().shutdown();
							if(localFileLenght == netFileLenght){
								GLog.i(TAG, "下载完成");
								for (DownloadListener downloadListener : listeners) {
									downloadListener.onDownloadComplete(path);
								}
							}
						}
					} catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						
					}
					
				}
			};
			mDownloadThread.start();
			
		}
	}
	
	public float getDownloadPersent(){
		if(netFileLenght == 0)
			return 0;
		else 
			return (float)localFileLenght/netFileLenght;
	}
	
	public static Long getLocalFileSize(String fileName) {       
		File file = new File(fileName);
		return file.length();
	}
	
	public static Long getNetFileSize(String url) {
         Long count = -1L;
         final HttpClient httpClient = new DefaultHttpClient();
         httpClient.getParams().setIntParameter("http.socket.timeout", 5000);
         final HttpGet httpGet = new HttpGet(url);
         HttpResponse response = null;
         try {
             response = httpClient.execute(httpGet);
             final int code = response.getStatusLine().getStatusCode();
             final HttpEntity entity = response.getEntity();
             if (entity != null && code == 200) {
                 count = entity.getContentLength();
             }
         } catch (ClientProtocolException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             httpClient.getConnectionManager().shutdown();
         }
         return count;
     }
	
	
	public static interface DownloadListener{
		void onDownloadComplete(String path);;
	}
}
