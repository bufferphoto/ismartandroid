package com.guogee.tablet.manager;

import com.guogee.tablet.dao.AbstractDao;
import com.guogee.tablet.dao.DAOFactory;
import com.guogee.tablet.db.DbHelper.SqlCondition;
import com.guogee.tablet.db.DbHelper.TimesCollection;
import com.guogee.tablet.model.TimesModel;

import java.util.ArrayList;
import java.util.List;

public class TimerManager {
	private volatile static TimerManager instance;
	private AbstractDao<TimesModel> dao;
    private TimerManager(){
    	dao = DAOFactory.getDao(DAOFactory.DaoType.Times);
    }
    
    public static TimerManager getInstance(){
    	if(instance == null){
    		synchronized(TimerManager.class){
    			if(instance == null){
    				instance = new TimerManager();
    			}
    		}
    	}
    	return instance;
    }
    
    public boolean addTimes(TimesModel model){
    	dao.insertItem(model);
    	return true;
    }
    
    public boolean deleteTimesByDeviceMac(String deviceMac){
    	List<String> listValue = new ArrayList<String>();
    	listValue.add(TimesCollection.DEV_MAC +" = '"+deviceMac+"'");
    	return dao.deleteItemByFeiled(listValue, SqlCondition.AND);
    }
    
    public boolean updateTimesByDeviceMac(TimesModel model,String deviceMac){
    	List<String> listValue = new ArrayList<String>();
    	listValue.add(TimesCollection.DEV_MAC +" = '"+deviceMac+"'");
    	return dao.updateItemByFeiled(model, listValue, SqlCondition.AND);
    }
    
    public boolean updateTimesById(TimesModel model,int id){
    	List<String> listValue = new ArrayList<String>();
    	listValue.add(TimesCollection.ID +" = "+id);
    	return dao.updateItemByFeiled(model, listValue, SqlCondition.AND);
    }
    
    public List<TimesModel> getTimesByDeviceMac(String deviceMac){
    	List<String> listValue = new ArrayList<String>();
    	listValue.add(TimesCollection.DEV_MAC +" = '"+deviceMac+"'");
    	return dao.getItemByFeiled(listValue, SqlCondition.AND, null);
    }
    
    public List<TimesModel> getAllTimes(){
    	return dao.getItemByFeiled(null, null, null);
    }
}
