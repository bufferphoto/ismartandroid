package com.guogee.tablet.interaction;

import android.app.Activity;

import com.guogee.tablet.utils.SceneActionUtil;
import com.guogee.tablet.manager.SmartWallSwitchConfigManager;
import com.guogee.tablet.model.SmartWallSwitchConfigModel;
import com.guogee.tablet.ui.widge.RemindListDialog;
import com.guogee.ismartandroid2.aidl.GatewayStatus;
import com.guogee.ismartandroid2.device.DeviceFactory;
import com.guogee.ismartandroid2.device.DeviceListener;
import com.guogee.ismartandroid2.device.SmartDevice;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.manager.SceneManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.model.GatewayInfo;
import com.guogee.ismartandroid2.model.Room;
import com.guogee.ismartandroid2.model.Scene;
import com.guogee.ismartandroid2.networkingProtocol.DeviceType;
import com.guogee.ismartandroid2.response.Status;
import com.guogee.ismartandroid2.service.NetworkServiceConnector;
import com.guogee.ismartandroid2.utils.GLog;

import com.example.mt.mediaplay.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/5/23.
 */
public class AddRoomActionToSceneControl implements DeviceListener<Status> {

    private Activity context;
    private Scene mScene;

    private List<String> remindText;
    private List<Device> deviceList;
    private RemindListDialog remindListDialog;
    private int count = 0;

    public AddRoomActionToSceneControl(Activity context) {
        this.context = context;
        deviceList = new ArrayList<>();
        remindText = new ArrayList<>();
        remindListDialog = new RemindListDialog(context);
    }

    /**
     * @param roomList
     * @param sceneName
     * @param isClearAction 是否清楚原有的场景动作
     */
    public void saveAction(List<Room> roomList, String sceneName, boolean isClearAction) {
        mScene = isExistSceneName(sceneName);
        if (null == mScene) {
            mScene = SceneActionUtil.createScene(context, sceneName);
            if (null == mScene) {
                return;
            }
        }
        if (isClearAction) {
            mScene.removeAllAction(context, null);
        }
        queryDeviceList(roomList);
    }

    private Scene isExistSceneName(String sceneName) {
        List<Scene> sceneList = SceneManager.getInstance(context).getScenes();
        for (Scene model : sceneList) {
            GLog.i(" sceneName:" + model.getName());
            if (sceneName.equalsIgnoreCase(model.getName())) {
                return model;
            }
        }
        return null;
    }

    private void queryDeviceList(List<Room> roomList) {
        List<Room> rooms = new ArrayList<>();
        for (Room room : roomList) {
            GatewayInfo gatewayInfo = RoomManager.getInstance(context).getGateway(room.getBoxId());
            if (null != gatewayInfo) {
                GatewayStatus gatewayStatus = NetworkServiceConnector.getInstance().getGatewayStatus(gatewayInfo.getDeviceInfo().getAddr());
                if (null != gatewayStatus) {
                    GLog.d("getOnlineStatus:" + gatewayStatus.getOnlineStatus());
                    int status = gatewayStatus.getOnlineStatus();
                    if (status == GatewayStatus.LAN || status == GatewayStatus.REMOTE) {
                        rooms.add(room);
                    }
                }
            }
        }

        //网关在线的房间
        deviceList.clear();
        for (Room room : rooms) {
            deviceList.addAll(SceneActionUtil.loadCurrentRoomQueryDevice(context, room));
        }

        remindText.clear();
        remindListDialog.setData(remindText);
        count = 0;
        for (Device device : deviceList) {
            queryStatus(device);
        }
    }

    private void queryStatus(Device dev) {
        Room room = RoomManager.getInstance(context).getRoomById(dev.getRoomId());
        GatewayInfo gatewayInfo = room.getGateway(context);
        if (null != gatewayInfo && null != gatewayInfo.getDeviceInfo()) {
            SmartDevice<Status> device = (SmartDevice<Status>) DeviceFactory.buildDevice(dev, gatewayInfo.getDeviceInfo().getAddr());
            device.setListener(this);
            int seq = device.queryStatus();
        }
    }

    @Override
    public void onSuccess(Status status) {
        GLog.i(" onSuccess getDeviceType:" + status.getDeviceType() + "   mac:" + status.getMac());
        refreshDeviceStatus(status, true);
        List<Device> deviceList = getDevice(status.getMac());
        for (Device device : deviceList) {
            showDialog(device, true);
            SceneActionUtil.saveAction(context, mScene, device, status);
        }
    }

    @Override
    public void onFail(Status status) {
        GLog.i(" onFail getDeviceType:" + status.getDeviceType() + "   mac:" + status.getMac());
        refreshDeviceStatus(status, false);
        List<Device> deviceList = getDevice(status.getMac());
        for (Device device : deviceList) {
            showDialog(device, false);
        }
    }

    private void showDialog(final Device device, final boolean isSuccess) {
        GLog.i("   isFinishing:" + context.isFinishing());
        if (context.isFinishing()) {
            return;
        }
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null == device) {
                    return;
                }
                String tip = context.getString(R.string.add);
                tip += "  " + device.getName();
                if (isSuccess) {
                    tip += "  " + context.getString(R.string.successfully);
                } else {
                    tip += "  " + context.getString(R.string.failed) + "   " + context.getString(R.string.ERROR_PPCS_DEVICE_NOT_ONLINE);
                }
                remindText.add(tip);
                remindListDialog.setData(remindText);
                if (!remindListDialog.isShowing()) {
                    remindListDialog.show();
                }
                GLog.i(" count:" + count);
                count = count + 1;
                GLog.i(" count:" + count + " size:" + deviceList.size());
                if (deviceList.size() == count) {
                    GLog.i(" fisish ");
                }
            }
        });
    }

    private void refreshDeviceStatus(Status status, boolean isOk) {
        Device device = RoomManager.getInstance(context).searchDevice(status.getMac());
        if (null != device) {
            if (device.getRctype().equals(DeviceType.SWITCH_ONE_FALG) || device.getRctype().equals(DeviceType.SWITCH_ONES_FALG)
                    || device.getRctype().equals(DeviceType.SWITCH_TWO_FALG) || device.getRctype().equals(DeviceType.SWITCH_TWOS_FALG)
                    || device.getRctype().equals(DeviceType.SWITCH_THREE_FALG) || device.getRctype().equals(DeviceType.SWITCH_THREES_FALG)
                    || device.getRctype().equals(DeviceType.SWITCH_FOUR_FALG) || device.getRctype().equals(DeviceType.SWITCH_FOURS_FALG)) {//墙面开关
                List<Device> devices = getDevice(status.getMac());
                for (Device device1 : devices) {
                    deviceList.remove(device1);
                }
                String[] names = device.getName().split("&&");
                for (int i = 0; i < names.length; i++) {
                    Device model = new Device();
                    model.setName(names[i]);
                    model.setId(device.getId());
                    model.setSystemid(device.getSystemid());
                    model.setVisible(device.getVisible());
                    model.setAddr(device.getAddr());
                    model.setVer(device.getVer());
                    model.setOrders(device.getOrders());
                    model.setDevicetype(device.getDevicetype());
                    model.setRctype(device.getRctype());
                    model.setRoomId(device.getRoomId());
                    model.switchNum = i;
                    if (isOk) {
                        model.deviceStatus = 0;  //0 正常   1 正在发送数据   2 设备失联
                        model.isOpen = status.isOn(i); //设备是否开启 状态
                    } else {
                        model.deviceStatus = 2;  //0 正常   1 正在发送数据   2 设备失联
                        model.isOpen = false; //设备是否开启 状态
                    }
                    deviceList.add(model);
                }
            } else if (device.getRctype().equals(DeviceType.SMART_SWITCH_ONE_FALG) || device.getRctype().equals(DeviceType.SMART_SWITCH_ONES_FALG)
                    || device.getRctype().equals(DeviceType.SMART_SWITCH_TWO_FALG) || device.getRctype().equals(DeviceType.SMART_SWITCH_TWOS_FALG)
                    || device.getRctype().equals(DeviceType.SMART_SWITCH_THREE_FALG) || device.getRctype().equals(DeviceType.SMART_SWITCH_THREES_FALG)
                    || device.getRctype().equals(DeviceType.SMART_SWITCH_FOUR_FALG) || device.getRctype().equals(DeviceType.SMART_SWITCH_FOURS_FALG)) {//智能墙面开关
                List<Device> devices = getDevice(status.getMac());
                for (Device device1 : devices) {
                    deviceList.remove(device1);
                }
                List<SmartWallSwitchConfigModel> models = SmartWallSwitchConfigManager.getInstance().querySmartWallSwitchConfig(device.getId());
                //只显示本地开关，命名的
                for (int i = 0; i < models.size(); i++) {
                    SmartWallSwitchConfigModel configModel = models.get(i);
                    GLog.i("getValue:" + configModel.getValue());
                    if (configModel.getConfigMode() == 1) {
                        Device model = new Device();
                        model.setName(configModel.getValue());
                        model.setId(device.getId());
                        model.setSystemid(device.getSystemid());
                        model.setVisible(device.getVisible());
                        model.setAddr(device.getAddr());
                        model.setVer(device.getVer());
                        model.setOrders(device.getOrders());
                        model.setDevicetype(device.getDevicetype());
                        model.setRctype(device.getRctype());
                        model.setRoomId(device.getRoomId());
                        model.switchNum = i;
                        if (isOk) {
                            model.deviceStatus = 0;  //0 正常   1 正在发送数据   2 设备失联
                            model.isOpen = status.isOn(i); //设备是否开启 状态
                        } else {
                            model.deviceStatus = 2;  //0 正常   1 正在发送数据   2 设备失联
                            model.isOpen = false; //设备是否开启 状态
                        }
                        deviceList.add(model);
                    }
                }
            }
        }
    }

    private List<Device> getDevice(String mac) {
        List<Device> deviceList = new ArrayList<>();
        for (Device device : this.deviceList) {
            if (device.getAddr().equalsIgnoreCase(mac)) {
                deviceList.add(device);
            }
        }
        return deviceList;
    }
}
