package com.guogee.tablet.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.android.volley.toolbox.ImageLoader;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.ui.activity.PerformSceneActivity;
import com.guogee.tablet.ui.widget.FancyCoverFlowAdapter;
import com.guogee.tablet.utils.LruCacheManager;
import com.guogee.tablet.utils.ViewUtil;
import com.guogee.ismartandroid2.manager.SceneManager;
import com.guogee.ismartandroid2.utils.GLog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/29.
 */
public class GallayAdapter extends FancyCoverFlowAdapter {

    private Context context;
    private List<SceneItem> list = new ArrayList<>();
    private Bitmap bitmap;

    public GallayAdapter(Context context, List<SceneItem> list) {
        this.context = context;
        if (null != list) {
            this.list = list;
        }
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.zq_scene_icon4_bg);
    }

    public void setData(List<SceneItem> list) {
        if (null != list) {
            this.list = list;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return list.size();
        //return Integer.MAX_VALUE;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
//        return position % list.size();
        return position;
    }

    @Override
    public View getCoverFlowItem(final int position, View reusableView, ViewGroup parent) {
        RelativeLayout relativeLayout = (RelativeLayout) reusableView;
        if (null == relativeLayout) {
            relativeLayout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.gallay_item_layout, null);
        }

        GLog.i("mtmt", "length" + list.size());

        Integer resId = list.get(position % list.size()).getListItemBg();
        Bitmap bitmap1 = LruCacheManager.getInstance(context).get(resId + "");
        ImageView mSceneIcon = (ImageView) relativeLayout.findViewById(R.id.scene_icon);
        mSceneIcon.setImageResource(list.get(position % list.size()).getListItemIcon());
        Button mAction = (Button) relativeLayout.findViewById(R.id.mSceneAction);
        mAction.setText(list.get(position % list.size()).getListItemTitle().toString());

//        relativeLayout.setBackgroundResource(resId);
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            relativeLayout.setBackground( new BitmapDrawable(bitmap1));
        }else{
            relativeLayout.setBackgroundResource(resId);
        }

//        imageView.setImageBitmap(bitmap);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        // Bitmap bitmap = decodeResource(context.getResources(), R.drawable.zq_scene_icon4_bg);
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        GLog.i("mtmt", "----width:" + width + "---height:" + height + "---Density:" + displayMetrics.density);
        //Gallery.LayoutParams layoutParams = new Gallery.LayoutParams(ViewUtil.Dp2Px(context, width / 192 * 18), ViewUtil.Dp2Px(context, height / 12 * 3));
        Gallery.LayoutParams layoutParams = new Gallery.LayoutParams(width, height);
        relativeLayout.setLayoutParams(layoutParams);


//        ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.imageview);

        return relativeLayout;
    }

}
