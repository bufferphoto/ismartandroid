package com.guogee.tablet.adapter;

/**
 * 场景 listview item 对象
 *
 * @author john-deng
 */
public class SceneItem {

    private int id;

    private String listItemTitle;

    private int listItemIcon;

    private boolean isCheck;

    private int listItemCbIcon;

    private int listItemBg;


    public SceneItem() {

    }

    public SceneItem(int id, String listItemTitle, int listItemIcon) {
        this.id = id;
        this.listItemTitle = listItemTitle;
        this.listItemIcon = listItemIcon;
    }

    public SceneItem(int id, String listItemTitle, int listItemIcon, int listItemBg) {
        this.id = id;
        this.listItemTitle = listItemTitle;
        this.listItemIcon = listItemIcon;
        this.listItemBg = listItemBg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setListItemTitle(String listItemTitle) {
        this.listItemTitle = listItemTitle;
    }

    public String getListItemTitle() {
        return listItemTitle;
    }

    public void setListItemIcon(int listItemIcon) {
        this.listItemIcon = listItemIcon;
    }

    public int getListItemIcon() {
        return listItemIcon;
    }

    public void setCheckMode(boolean check) {
        isCheck = check;
    }

    public boolean getCheckMode() {
        return isCheck;
    }

    public int getListItemCbIcon() {
        return listItemCbIcon;
    }

    public void setListItemCbIcon(int listItemCbIcon) {
        this.listItemCbIcon = listItemCbIcon;
    }

    public int getListItemBg() {
        return listItemBg;
    }

    public void setListItemBg(int listItemBg) {
        this.listItemBg = listItemBg;
    }

}
