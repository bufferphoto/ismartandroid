package com.guogee.tablet.adapter;

import com.guogee.ismartandroid2.model.Device;

public class VoiceCmdData {
	private int seq;//为了防止Packet的Seq丢失
	public int getSeq()
	{
		return seq;
	}

	public void setSeq(int seq)
	{
		this.seq = seq;
	}
	private Device deviceModel;
//	private Map<String,String> toDevice;
	private StringBuffer result = new StringBuffer();
	private String cmdData; // 场景专用
	private byte device_type;
	private byte device_version;
	private boolean isLeft = false;
	private boolean isOk   = false;
	public static boolean isChinese = false;
	//要进行二次发命令的动作，先发了查询包，
	// 二次命令才是实际包
	// 如灯光调暗，先发查询包，在设置动作，动作值对应动作组中的index
	private int secondaryAction = -1;   
	
	public VoiceCmdData(String result){
		this.result.append(result);
	}
	
	public VoiceCmdData(Device toDevice, String result){
		this.deviceModel 		= toDevice;
		this.device_version = (byte)deviceModel.getVer();
		this.device_type	= (byte)deviceModel.getDevicetype();
		this.result.append(result);
		if(!isChinese){
			this.result.append(" ");
		}
	}
	
	public void setCmdData(String cmd){
		this.cmdData = cmd;
	}
	
	public String getCmdData(){
		return  cmdData;
	}
	
	public byte getDeviceVersion(){
		return this.device_version;
	}
	
	public void setDeviceVersion(String version){
		this.device_version=(byte)Integer.parseInt(version);
	}
	
	public Device getDeviceModel(){
		return this.deviceModel;
	}
	
	public void setDeviceModel(Device deviceModel){
		this.deviceModel = deviceModel;
	}
	
	public void addResult(String str){
		this.result.append(str);
		if(!isChinese){
			this.result.append(" ");
		}
	}
	
	public void setResult(String str){
		this.result = new StringBuffer(str);
	}
	
	public String getResult(){
		String str = this.result.toString();
		
		return str.replace(".*", "");
	}
	
	public void setDeviceType(byte type){
		this.device_type = type;
	}
	
	public byte getDeviceType(){
		return this.device_type;
	}
	
	public void setIsLeft(boolean isLeft){
		this.isLeft = isLeft;
	}
	public boolean getIsLeft(){
		return isLeft;
	}
	public void setIsOk(boolean isOk){
		this.isOk = isOk;
	}
	public boolean getIsOk(){
		return isOk;
	}
	/**
	 * 要进行二次发命令的动作，先发了查询包，
	 * 二次命令才是实际包
	 * 如灯光调暗，先发查询包，在设置动作，动作值对应动作组中的index
	 * @param action
	 */ 
	public void setSecondaryAction(int action){
		this.secondaryAction = action;
	}
	/**
	 * 要进行二次发命令的动作，先发了查询包，
	 * 二次命令才是实际包
	 * 如灯光调暗，先发查询包，在设置动作，动作值对应动作组中的index
	 * @param action
	 */ 
	public int getSecondaryAction(){
		return secondaryAction;
	}
	
}
