package com.guogee.tablet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mt.mediaplay.R;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by mt on 2017/5/26.
 */

public class AdapterDirectPop extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context context;
    private List<SceneItem> mlist = new ArrayList<>();

    public AdapterDirectPop(Context mContext, List<SceneItem> mlist) {
        if (null != mlist) {
            this.mlist = mlist;
        }
        this.context = mContext;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_scene, null);
            holder.SceneTitle = (TextView) convertView.findViewById(R.id.scene_title);
            holder.CheckScene = (ImageView) convertView.findViewById(R.id.scene_cb);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();//重新修改其是第几个数据项
        }
        holder.SceneTitle.setText(mlist.get(position).getListItemTitle());
        holder.CheckScene.setImageResource(mlist.get(position).getListItemCbIcon());
        SceneItem item = mlist.get(position);
        if (item.getCheckMode()) {
            holder.CheckScene.setImageResource(R.drawable.scene_check_btn_b);
            holder.CheckScene.setVisibility(View.VISIBLE);
        } else {
            holder.CheckScene.setImageResource(R.drawable.scene_check_btn_a);
            holder.CheckScene.setVisibility(View.VISIBLE);

        }
        return convertView;
    }

    public class ViewHolder {
        public TextView SceneTitle;
        public ImageView CheckScene;
    }

}
