package com.guogee.tablet.adapter;

public class SecurityLightItem {

	private String startTime;
	
	private String endTime;
	
	
	public SecurityLightItem(String startTime, String endTime){
		this.startTime 	= startTime;
		this.endTime	= endTime;
	}
	
	public String getStartTime(){
		return startTime;
	}
	
	public String getEndTime(){
		return endTime;
	}
}
