package com.guogee.tablet.adapter;

import android.view.View;

import com.example.mt.mediaplay.R;


public class VoiceItemAdapter {

	private String content;
	private boolean isLeft;
	private boolean isOk;
	private int bgIdOk 		= R.drawable.zq_sound_input_box_b;
	private int bgIdFail 	= R.drawable.zq_sound_input_box_c;
	private View convertView;
	
	public VoiceItemAdapter(String content, boolean isLeft, boolean isOk){
		this.content 	= content;
		this.isLeft		= isLeft;
		this.isOk		= isOk;
	}
	
	public void setConvertView(View view){
		this.convertView = view;
	}
	
	public View getConvertView(){
		return this.convertView;
	}
	
	public String getContent(){
		return content;
	}
	public boolean getIsLeft(){
		return isLeft;
	}
	public int getBgId(){
		if(isOk){
			return bgIdOk;
		}else{
			return bgIdFail;
		}
	}
}
