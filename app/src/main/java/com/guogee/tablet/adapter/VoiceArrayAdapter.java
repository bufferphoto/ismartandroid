package com.guogee.tablet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.example.mt.mediaplay.R;

import java.util.List;


public class VoiceArrayAdapter extends ArrayAdapter<VoiceItemAdapter>{

	private LayoutInflater mInflater;
	private int resourceLeft;
	private int resourceRight;
	private List<VoiceItemAdapter> list;
	
	public VoiceArrayAdapter(Context context, int resourceLeft, int resourceRight, List<VoiceItemAdapter> objects) {
		super(context, resourceLeft, resourceRight, objects);
		this.mInflater 		= LayoutInflater.from(context);
		this.resourceLeft 	= resourceLeft;
		this.resourceRight 	= resourceRight;
		this.list			= objects;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		VoiceItemAdapter item = list.get(position);
		//if(convertView == null){
			int resource;
			if(item.getIsLeft()){
				resource = resourceLeft;
			}else{
				resource = resourceRight;
			}
			convertView = mInflater.inflate(resource, parent, false);
		//}
		
		TextView textView = (TextView)convertView.findViewById(R.id.text_content);
//		if(item.getIsLeft()){
//			textView.setBackgroundResource(item.getBgId());
//		}
		textView.setText(item.getContent());
		
		return convertView;
	}
}
