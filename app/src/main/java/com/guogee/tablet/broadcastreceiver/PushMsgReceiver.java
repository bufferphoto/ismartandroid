package com.guogee.tablet.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.guogee.tablet.service.PushHandlerService;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.remoteControlService.AsyncHttpResponseHandler;
import com.guogee.ismartandroid2.remoteControlService.RemoteUserService;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.pushclient.GPushManager;
import com.guogu.music.manager.ServiceAuthorityManger;

public class PushMsgReceiver extends BroadcastReceiver {

    private static final String TAG = "PushMsgReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        GLog.d(TAG, "onReceive() action=" + bundle.getInt(GPushManager.BROADCAST_CMD));
        switch (bundle.getInt(GPushManager.BROADCAST_CMD)) {

            case GPushManager.CMD_PUSH_TO_CLIENT:
                // 获取透传数据
                String payload = bundle.getString(GPushManager.BROADCAST_PLAYLOAD);

                if (payload != null) {

                    GLog.d(TAG, "Got Payload:" + payload);
                    Intent intent1 = new Intent();
                    intent1.setClass(context, PushHandlerService.class);
                    intent1.putExtra("data", payload);
                    context.startService(intent1);
                }
                break;
            case GPushManager.CMD_REGISTER:
                // 获取ClientID(CID)
                String cid = bundle.getString(GPushManager.BROADCAST_CLIENT_ID);
                GLog.v(TAG, "cid:" + cid);
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                RemoteUserService.uploadClientId(tm.getDeviceId(), cid, "Android", SystemUtil.getAppVersionCode(context),
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, String content) {

                            }

                            @Override
                            public void onFailure(Throwable error, String content) {

                            }
                        });
                ServiceAuthorityManger.getInstance(context).checkVendor();
                break;

            default:
                break;
        }
    }


}
