package com.guogee.tablet.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.guogee.tablet.service.BackgroundService;
import com.guogee.ismartandroid2.utils.GLog;


import java.util.Date;

public class MediaButtonBroadcastReceiver extends BroadcastReceiver {

	private static long lastTime;
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		GLog.v("xgw"," call MediaButtonBroadcastReceiver:"+action);

		if(action.equals("android.intent.action.MEDIA_BUTTON"))
		{
			
			 
			 long now = new Date().getTime();
			 if(now - lastTime < 500){
				 return;
			 }
			 GLog.v("xgw"," call MediaButtonBroadcastReceiver,now:"+now+",last:"+lastTime);

			 lastTime = now;
	         //KeyEvent event = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);  
	         
	         //if ((event != null)&& (event.getKeyCode() == KeyEvent.KEYCODE_HEADSETHOOK)) {  
	        	 GLog.d("xgw", "begin to start service.");
	        	 Intent service = new Intent(context,BackgroundService.class);
	     		 service.putExtra("business", BackgroundService.Background_mediabtn);
	             service.setAction(BackgroundService.ACTION_Service);  
	             context.startService(service);
	        	 
	         //}  
		}else if(action.equals("android.intent.action.HEADSET_PLUG"))
		{
			 if (intent.getIntExtra("state", 0) == 0){      
				 GLog.d("xgw", "headset unplug.");
             }  
             else if (intent.getIntExtra("state", 0) == 1){  
            	 GLog.d("xgw", "headset plug");
             }
		}
		
		  
         
         

	}

}
