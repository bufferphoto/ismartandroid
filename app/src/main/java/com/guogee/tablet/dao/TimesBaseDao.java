package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.db.DbHelper.TimesCollection;
import com.guogee.tablet.model.TimesModel;

public class TimesBaseDao extends AbstractDao<TimesModel>{

	public TimesBaseDao() {
		super();
		tableName = TimesCollection.TABLE_NAME;
	}


	@Override
	public TimesModel parseItem(Cursor cursor) {
		// TODO Auto-generated method stub
		TimesModel model = new TimesModel();
		model.setId(cursor.getInt(cursor.getColumnIndex(TimesCollection.ID)));
		model.setDevMac(cursor.getString(cursor.getColumnIndex(TimesCollection.DEV_MAC)));
		model.setOrders(cursor.getInt(cursor.getColumnIndex(TimesCollection.ORDERS)));
		model.setTimeClose(cursor.getString(cursor.getColumnIndex(TimesCollection.TIME_CLOSE)));
		model.setTimeOpen(cursor.getString(cursor.getColumnIndex(TimesCollection.TIME_OPEN)));
		return model;
	}

}
