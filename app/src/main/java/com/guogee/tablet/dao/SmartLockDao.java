/**
 * 
 */
package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.model.SmartLockModel;
import com.guogee.tablet.db.DbHelper;


/**
 *@ClassName:     SmartLockDao.java
 * @Description:   TODO
 * @author xieguangwei
 * @date 2016年1月11日
 * 
 */
public class SmartLockDao extends AbstractDao<SmartLockModel>
{

	public SmartLockDao(){
		tableName = DbHelper.SmartLockCollections.TABLE_NAME;
	}
	@Override
	public SmartLockModel parseItem(Cursor cursor)
	{
		SmartLockModel alm = new SmartLockModel();
		
		alm.setDeviceId(cursor.getInt(cursor.getColumnIndex(DbHelper.SmartLockCollections.DEVICE_ID)));
		alm.setId(cursor.getInt(cursor.getColumnIndex(DbHelper.SmartLockCollections.ID)));
		alm.setLastTime(cursor.getLong(cursor.getColumnIndex(DbHelper.SmartLockCollections.LASTTIME)));
		alm.setPwd(cursor.getString(cursor.getColumnIndex(DbHelper.SmartLockCollections.PASSWORD)));
		alm.setUserId(cursor.getInt(cursor.getColumnIndex(DbHelper.SmartLockCollections.USER_ID)));
		
		return alm;
	}
	
}
