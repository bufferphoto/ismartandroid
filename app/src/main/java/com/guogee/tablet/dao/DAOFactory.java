package com.guogee.tablet.dao;


import com.guogee.ismartandroid2.model.BaseModel;

public class DAOFactory {

    public enum DaoType {
        Camera,
        Custom,
        IRCodesBrandCache,
        IRCodesCache,
        Times,
        SafetyAlarm,
        Color,
        SmartLock,
        SmartLockHistory,
        SmartLockUserID,
        SmartWallSwitchConfig,
        DoorbellConfig,
    }

    public static <T extends BaseModel> AbstractDao<T> getDao(DaoType type) {
        switch (type) {

            case Camera:
                return (AbstractDao<T>) new CameraBaseDao();
            case Custom:
                return (AbstractDao<T>) new CustomBaseDao();
            case IRCodesBrandCache:
                return (AbstractDao<T>) new IRCodesBrandCacheBaseDao();
            case IRCodesCache:
                return (AbstractDao<T>) new IRCodesCacheBaseDao();

            case Times:
                return (AbstractDao<T>) new TimesBaseDao();
            case SafetyAlarm:
                return (AbstractDao<T>) new SafetyDeviceAlarmHistoryBaseDao();
            case Color:
                return (AbstractDao<T>) new ColorsBaseDao();
            case SmartLock:
                return (AbstractDao<T>) new SmartLockDao();
            case SmartLockHistory:
                return (AbstractDao<T>) new SmartLockHistoryDao();
            case SmartLockUserID:
                return (AbstractDao<T>) new SmartLockUserIdNameDao();
            case SmartWallSwitchConfig:
                return (AbstractDao<T>) new SmartWallSwitchConfigDao();
            case DoorbellConfig:
                return (AbstractDao<T>) new DoorBellDao();
            default:
                return null;
        }
    }
}
