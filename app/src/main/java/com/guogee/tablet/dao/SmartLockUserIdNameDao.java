package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.db.DbHelper;
import com.guogee.tablet.model.SmartLockUserIdNameModel;

/**
 * Created by Administrator on 2016/7/28.
 */
public class SmartLockUserIdNameDao extends AbstractDao<SmartLockUserIdNameModel> {

    public SmartLockUserIdNameDao() {
        tableName = DbHelper.LockUserIDHelperCollection.TABLE_NAME;
    }

    @Override
    public SmartLockUserIdNameModel parseItem(Cursor cursor) {
        SmartLockUserIdNameModel slh = new SmartLockUserIdNameModel();
        slh.setId(cursor.getInt(cursor.getColumnIndex(DbHelper.LockUserIDHelperCollection.ID)));
        slh.setDeviceAddr(cursor.getString(cursor.getColumnIndex(DbHelper.LockUserIDHelperCollection.DEVICE_ADDR)));
        slh.setUserName(cursor.getString(cursor.getColumnIndex(DbHelper.LockUserIDHelperCollection.USER_NAME)));
        slh.setLockUserId(cursor.getInt(cursor.getColumnIndex(DbHelper.LockUserIDHelperCollection.LOCK_USER_ID)));
        return slh;
    }
}
