package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.model.CameraModel;
import com.guogee.tablet.db.DbHelper;

public class CameraBaseDao extends AbstractDao<CameraModel>{

	public CameraBaseDao() {
		super();
		tableName = DbHelper.CameraColelction.TABLE_NAME;
	}

	@Override
	public CameraModel parseItem(Cursor cursor) {
		// TODO Auto-generated method stub
		CameraModel model = new CameraModel();
		model.setId(cursor.getInt(cursor.getColumnIndex(DbHelper.CameraColelction.ID)));
		model.setCameraID(cursor.getString(cursor.getColumnIndex(DbHelper.CameraColelction.CAMERA_ID)));
		model.setLoginName(cursor.getString(cursor.getColumnIndex(DbHelper.CameraColelction.LOGIN_NAME)));
		model.setLoginPwd(cursor.getString(cursor.getColumnIndex(DbHelper.CameraColelction.LOGIN_PWD)));
		model.setReserve(cursor.getString(cursor.getColumnIndex(DbHelper.CameraColelction.RESERVE)));
		return model;
	}

	

}
