package com.guogee.tablet.dao;


import android.database.Cursor;

import com.guogee.tablet.model.ColorModel;
import com.guogee.tablet.db.DbHelper;

public class ColorsBaseDao extends AbstractDao<ColorModel>{

	public ColorsBaseDao() {
		super();
		tableName = DbHelper.ColorsCollection.TABLE_NAME;
	}


	@Override
	public ColorModel parseItem(Cursor cursor) {
		// TODO Auto-generated method stub
		ColorModel model = new ColorModel();
		model.setId(cursor.getInt(cursor.getColumnIndex(DbHelper.ColorsCollection.ID)));
		model.setBlue(cursor.getInt(cursor.getColumnIndex(DbHelper.ColorsCollection.BLUE)));
		model.setGreen(cursor.getInt(cursor.getColumnIndex(DbHelper.ColorsCollection.GREEN)));
		model.setOrders(cursor.getInt(cursor.getColumnIndex(DbHelper.ColorsCollection.ORDERS)));
		model.setPerc(cursor.getString(cursor.getColumnIndex(DbHelper.ColorsCollection.PERC)));
		model.setRed(cursor.getInt(cursor.getColumnIndex(DbHelper.ColorsCollection.RED)));
		model.setWhite(cursor.getInt(cursor.getColumnIndex(DbHelper.ColorsCollection.WHITE)));
		return model;
	}

}
