package com.guogee.tablet.dao;


import android.database.Cursor;

import com.guogee.tablet.db.DbHelper.CustomCollection;
import com.guogee.tablet.model.CustomModel;

public class CustomBaseDao extends AbstractDao<CustomModel>{

	public CustomBaseDao() {
		super();
		tableName = CustomCollection.TABLE_NAME;
	}

	@Override
	public CustomModel parseItem(Cursor cursor) {
		// TODO Auto-generated method stub
		CustomModel model = new CustomModel();
		model.setId(cursor.getInt(cursor.getColumnIndex(CustomCollection.ID)));
		model.setName(cursor.getString(cursor.getColumnIndex(CustomCollection.NAME)));
		model.setDeviceId(cursor.getInt(cursor.getColumnIndex(CustomCollection.DEVICE_ID)));
		model.setTag(cursor.getInt(cursor.getColumnIndex(CustomCollection.TAG)));
		model.setX(cursor.getInt(cursor.getColumnIndex(CustomCollection.X)));
		model.setY(cursor.getInt(cursor.getColumnIndex(CustomCollection.Y)));
		model.setType(cursor.getInt(cursor.getColumnIndex(CustomCollection.TYPE)));
		return model;
	}

}
