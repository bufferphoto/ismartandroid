package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.db.DbHelper.SmartLockAlarmHistoryTable;
import com.guogee.tablet.model.SmartLockHistory;

/**
 * Created by xieguangwei on 16/3/2.
 */
public class SmartLockHistoryDao extends AbstractDao<SmartLockHistory> {
    public SmartLockHistoryDao(){
        tableName = SmartLockAlarmHistoryTable.TABLE_NAME;
    }
    @Override
    public SmartLockHistory parseItem(Cursor cursor) {
        SmartLockHistory slh = new SmartLockHistory();
        slh.setTime(cursor.getString(cursor.getColumnIndex(SmartLockAlarmHistoryTable.TIME)));
        slh.setDevMac(cursor.getString(cursor.getColumnIndex(SmartLockAlarmHistoryTable.DEV_MAC)));
        slh.setDevName(cursor.getString(cursor.getColumnIndex(SmartLockAlarmHistoryTable.DEV_NAME)));
        slh.setId(cursor.getInt(cursor.getColumnIndex(SmartLockAlarmHistoryTable.ID)));
        slh.setOpenMethod(cursor.getInt(cursor.getColumnIndex(SmartLockAlarmHistoryTable.OPEN_METHOD)));
        slh.setRead(cursor.getInt(cursor.getColumnIndex(SmartLockAlarmHistoryTable.IS_READ)));
        slh.setRoom(cursor.getString(cursor.getColumnIndex(SmartLockAlarmHistoryTable.ROOM)));
        slh.setType(cursor.getInt(cursor.getColumnIndex(SmartLockAlarmHistoryTable.TYPE)));
        slh.setUserName(cursor.getString(cursor.getColumnIndex(SmartLockAlarmHistoryTable.USER_NAME)));

        return slh;
    }
}
