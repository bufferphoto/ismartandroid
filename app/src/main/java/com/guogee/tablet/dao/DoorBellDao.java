package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.model.DoorBell;
import com.guogee.tablet.db.DbHelper;

/**
 * Created by Administrator on 2017/4/25.
 */
public class DoorBellDao extends AbstractDao<DoorBell> {

    public DoorBellDao() {
        tableName = DbHelper.DoorBellHelperCollection.TABLE_NAME;
    }

    @Override
    public DoorBell parseItem(Cursor cursor) {
        DoorBell doorbell = new DoorBell();
        doorbell.setId(cursor.getInt(cursor.getColumnIndex(DbHelper.DoorBellHelperCollection.ID)));
        doorbell.setServerId(cursor.getString(cursor.getColumnIndex(DbHelper.DoorBellHelperCollection.SERVER_ID)));
        doorbell.setDeviceType(cursor.getInt(cursor.getColumnIndex(DbHelper.DoorBellHelperCollection.DEVICE_TYPE)));
        doorbell.setMac(cursor.getString(cursor.getColumnIndex(DbHelper.DoorBellHelperCollection.MAC)));
        doorbell.setFun(cursor.getInt(cursor.getColumnIndex(DbHelper.DoorBellHelperCollection.FUN)));
        doorbell.setPath(cursor.getString(cursor.getColumnIndex(DbHelper.DoorBellHelperCollection.PATH)));
        doorbell.setTime(cursor.getString(cursor.getColumnIndex(DbHelper.DoorBellHelperCollection.TIME)));
        doorbell.setData(cursor.getString(cursor.getColumnIndex(DbHelper.DoorBellHelperCollection.DATA)));
        return doorbell;
    }
}
