package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.model.IRCodesBrandCacheModel;
import com.guogee.tablet.db.DbHelper;

public class IRCodesBrandCacheBaseDao extends AbstractDao<IRCodesBrandCacheModel>{

	public IRCodesBrandCacheBaseDao() {
		super();
		tableName = DbHelper.IrCodesBrandCacheCollection.TABLE_NAME;
	}

	
	@Override
	public IRCodesBrandCacheModel parseItem(Cursor cursor) {
		// TODO Auto-generated method stub
		IRCodesBrandCacheModel model = new IRCodesBrandCacheModel();
		model.setCodeID(cursor.getInt(cursor.getColumnIndex(DbHelper.IrCodesBrandCacheCollection.CODE_ID)));
		model.setDeviceType(cursor.getString(cursor.getColumnIndex(DbHelper.IrCodesBrandCacheCollection.DEVICE_TYPE)));
		model.setDisplayName(cursor.getString(cursor.getColumnIndex(DbHelper.IrCodesBrandCacheCollection.DISPLAY_NAME)));
		model.setLanguage(cursor.getString(cursor.getColumnIndex(DbHelper.IrCodesBrandCacheCollection.LANGUAGE)));
		model.setIremoteType(cursor.getString(cursor.getColumnIndex(DbHelper.IrCodesBrandCacheCollection.IR_REMOTE_TYPE)));
		return model;
	}

}
