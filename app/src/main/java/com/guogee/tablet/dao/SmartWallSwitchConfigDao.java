package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.db.DbHelper;
import com.guogee.tablet.model.SmartWallSwitchConfigModel;

/**
 * Created by Administrator on 2016/9/13.
 */
public class SmartWallSwitchConfigDao extends AbstractDao<SmartWallSwitchConfigModel> {

    public SmartWallSwitchConfigDao() {
        tableName = DbHelper.SmartWallSwitchHelperCollection.TABLE_NAME;
    }

    @Override
    public SmartWallSwitchConfigModel parseItem(Cursor cursor) {
        SmartWallSwitchConfigModel model = new SmartWallSwitchConfigModel();

        model.setId(cursor.getInt(cursor.getColumnIndex(DbHelper.SmartWallSwitchHelperCollection.ID)));
        model.setDeviceId(cursor.getInt(cursor.getColumnIndex(DbHelper.SmartWallSwitchHelperCollection.DEVICE_ID)));
        model.setNum(cursor.getInt(cursor.getColumnIndex(DbHelper.SmartWallSwitchHelperCollection.NUM)));
        model.setConfigMode(cursor.getInt(cursor.getColumnIndex(DbHelper.SmartWallSwitchHelperCollection.CONFIG_MODE)));
        model.setValue(cursor.getString(cursor.getColumnIndex(DbHelper.SmartWallSwitchHelperCollection.VALUE)));

        return model;
    }

}
