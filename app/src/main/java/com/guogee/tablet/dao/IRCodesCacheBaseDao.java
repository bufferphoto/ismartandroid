package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.db.DbHelper.IrCodeCacheCollection;
import com.guogee.tablet.model.IRCodesCacheModel;

public class IRCodesCacheBaseDao extends AbstractDao<IRCodesCacheModel>{

	public IRCodesCacheBaseDao() {
		super();
		tableName = IrCodeCacheCollection.TABLE_NAME;
	}


	@Override
	public IRCodesCacheModel parseItem(Cursor cursor) {
		// TODO Auto-generated method stub
		IRCodesCacheModel model = new IRCodesCacheModel();
		model.setCodeID(cursor.getInt(cursor.getColumnIndex(IrCodeCacheCollection.CODE_ID)));
		model.setCodeData(cursor.getString(cursor.getColumnIndex(IrCodeCacheCollection.CODE_DATA)));
		model.setCodeLen(cursor.getInt(cursor.getColumnIndex(IrCodeCacheCollection.CODE_LEN)));
		model.setDeviceType(cursor.getString(cursor.getColumnIndex(IrCodeCacheCollection.DEVICE_TYPE)));
		model.setDisplayName(cursor.getString(cursor.getColumnIndex(IrCodeCacheCollection.DISPLAY_NAME)));
		model.setEnable(cursor.getInt(cursor.getColumnIndex(IrCodeCacheCollection.ENABLE)));
		model.setKeyName(cursor.getInt(cursor.getColumnIndex(IrCodeCacheCollection.KEY_NAME)));
		model.setModelID(cursor.getInt(cursor.getColumnIndex(IrCodeCacheCollection.MODEL_ID)));
		model.setBrandId(cursor.getInt(cursor.getColumnIndex(IrCodeCacheCollection.BRAND_ID)));
		model.setIremoteType(cursor.getString(cursor.getColumnIndex(IrCodeCacheCollection.IR_REMOTE_TYPE)));
		model.DownloadStatu = 0;
		model.TestStatu = 0;
		return model;
	}

}
