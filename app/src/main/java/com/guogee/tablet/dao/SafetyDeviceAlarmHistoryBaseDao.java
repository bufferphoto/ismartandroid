package com.guogee.tablet.dao;

import android.database.Cursor;

import com.guogee.tablet.db.DbHelper.SafetyDeviceAlarmHistoryCollection;
import com.guogee.tablet.model.SafetyDeviceAlarmHistoryModel;

public class SafetyDeviceAlarmHistoryBaseDao extends AbstractDao<SafetyDeviceAlarmHistoryModel>{

	public SafetyDeviceAlarmHistoryBaseDao() {
		super();
		tableName  = SafetyDeviceAlarmHistoryCollection.TABLE_NAME;
	}

	@Override
	public SafetyDeviceAlarmHistoryModel parseItem(Cursor cursor) {
		// TODO Auto-generated method stub
		SafetyDeviceAlarmHistoryModel model = new SafetyDeviceAlarmHistoryModel();
		model.setId(cursor.getInt(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.ID)));
		model.setDevMac(cursor.getString(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.DEV_MAC)));
		model.setTime(cursor.getString(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.TIME)));
		model.setUserName(cursor.getString(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.USER_NAME)));
		model.setRead(cursor.getString(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.IS_READ)));
		model.setDevName(cursor.getString(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.DEV_NAME)));
		model.setDevType(cursor.getString(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.DEV_TYPE)));
		model.setRoom(cursor.getString(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.ROOM)));
		model.setType(cursor.getInt(cursor.getColumnIndex(SafetyDeviceAlarmHistoryCollection.TYPE)));
		return model;
	}

}
