package com.guogee.tablet.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.guogee.tablet.db.DbHelper;
import com.guogee.ismartandroid2.model.BaseModel;
import com.guogee.tablet.service.DaoInterface;
import com.guogee.ismartandroid2.utils.GLog;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 *@ClassName:     AbstractDao.java
 * @Description:   TODO
 * @author xieguangwei
 * @date 2015年10月17日
 *todo 使用读写锁提升效率
 */
public abstract class AbstractDao<T extends BaseModel> implements DaoInterface<T> {

	private static final String TAG = "AbstractDao";
	private DbHelper helper = null;
	protected String tableName;
	private SQLiteDatabase db;
	public AbstractDao(){
		helper =  DbHelper.getInstance();
		db = helper.getWritableDatabase();
	}
	
	public abstract T parseItem(Cursor cursor);
	
	@Override
	public List<T> getItemInFeiled(String feiled, List<String> con, String order){
		String excuteSql = "select * from "+tableName+rebuildInCondotion(feiled,con);
		if (con != null) {
			excuteSql += " COLLATE NOCASE";
		}	
		if(order != null)
			excuteSql += " order by " + order;
		GLog.v(TAG, "excuteSql:"+excuteSql);
		return selectQuery(excuteSql);	
	}
	
	
	
	@Override

	public List<T> getItemByFeiled(List<String> con, String conJoin, String orderby){
		
		String excuteSql = "select * from "+tableName + rebuildingCondition(con, conJoin);
		if (con != null) {
			excuteSql += " COLLATE NOCASE";
		}
		if(orderby != null)
			excuteSql += " order by " + orderby;
		GLog.v(TAG, "excuteSql:"+excuteSql);
		return selectQuery(excuteSql);	
	}
	@Override
	public boolean deleteItemByFeiled(List<String> con, String conJoin){
		
		String excuteSql = "delete from "+tableName + rebuildingCondition(con, conJoin);
		if (con != null) {
			excuteSql += " COLLATE NOCASE";
		}
		GLog.v(TAG, "excuteSql:"+excuteSql);
		excute(excuteSql);
		return true;
	}
	
	public List<String> getSingleColByField(String col, List<String> con, String conJoin, String orderby){
		String excuteSql = "select " + col +" from "+tableName + rebuildingCondition(con, conJoin);
		if (con != null) {
			excuteSql += " COLLATE NOCASE";
		}
		if(orderby != null)
			excuteSql += " order by " + orderby;
		GLog.v(TAG, "excuteSql:"+excuteSql);
		return selectSingle(excuteSql);
	}
	
	public int insertItem(T obj){	
		return excute(rebuildInsertSql(obj));
	}
	
	protected String rebuildInsertSql(T obj){
		Map<String, Object> objMap = obj.getDBMap();
		Set<String> keys = objMap.keySet();
		Collection<Object> values = objMap.values();
		StringBuilder nameCause = new StringBuilder(tableName +"(");
		for (Iterator<String> it = keys.iterator(); it.hasNext();) {
			nameCause.append(it.next());
			if(it.hasNext())
				nameCause.append(",");
		}
		nameCause.append(") ");
		StringBuilder valCause = new StringBuilder(" values(");
		for(Iterator<Object> it = values.iterator(); it.hasNext();){
			Object val = it.next();
			if(val instanceof String) {
				String va = val.toString();
				va = replaceSpecialCharacter(va);
				valCause.append("'" + va + "'");
			}else {
				valCause.append(val.toString());
			}
			if (it.hasNext()) {
				valCause.append(",");
			}
		}
		valCause.append(")");
		String sql = "insert into " + nameCause.toString() + valCause.toString();
		return sql;
	}
	
	@Override
	public boolean insertItemByTransaction(List<T> objs){
		List<String> sqlList = new ArrayList<String>();
		for (T obj : objs) {
			sqlList.add(rebuildInsertSql(obj));
		}
		synchronized (helper) {
			if (!db.isOpen()) {
				db = helper.getWritableDatabase();
			}		
			db.beginTransaction();
			try {
				for (String sql : sqlList) {
					db.execSQL(sql);
				}
				// 设置事务标志为成功，当结束事务时就会提交事务
				db.setTransactionSuccessful();
			} catch (Exception e) {
				e.printStackTrace();
				Log.v(TAG, "insertIrCodeCache Exception:" + e.toString());
				throw e;
			} finally {
				// 结束事务
				db.endTransaction();
				db.close();
			}
			return true;
		}		
	}
	@Override
	public boolean updateItemByFeiled(T obj, List<String> condition, String queryCondition){
		
		StringBuilder sql = new StringBuilder("update " + tableName+ " set ") ;
		Map<String, Object> objMap = obj.getDBMap();
		Set<String> keys = objMap.keySet();
		keys.remove("id");
		for(Iterator<String> it = keys.iterator(); it.hasNext();){
			String key = it.next();
//			if(key.equals("id")){
//				continue;
//			}
			Object value = objMap.get(key);
			sql.append(key + "=");
			if(value instanceof String) {
				String va = value.toString();
				va = replaceSpecialCharacter(va);
				sql.append("'" + va + "'");
			}else {
				sql.append(value.toString());
			}
			if (it.hasNext()) {
				sql.append(",");
			}
		}
		if (condition != null) {
			sql.append(" where ");
			for (int i = 0; i < condition.size(); i++) {
				sql.append(condition.get(i));
				if(i < condition.size() - 1)
					sql.append(" "+queryCondition+" ");
			}
				sql.append(" COLLATE NOCASE");
		}
		
		excute(sql.toString());
		return true;
	}
	
	public String getMax(String col, List<String> con, String conJoin){
		String excuteSql = "select max(" + col +") from "+tableName + rebuildingCondition(con, conJoin);
		List<String> list = selectSingle(excuteSql);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
	
	protected List<String> selectSingle(String sql){
		GLog.v(TAG, "select single:"+sql);
		Cursor cursor = null;
		List<String> result = new ArrayList<String>();
		
			synchronized (helper) {
				try{
				if (!db.isOpen()) {
					db = helper.getWritableDatabase();
				}
				cursor = db.rawQuery(sql,null);
			    while(cursor.moveToNext()){
			    	result.add( cursor.getString(0));
			    }
			}catch(Exception e){
				e.printStackTrace();
				throw e;
			}
			finally{
				if (cursor != null) {
					cursor.close();
				}
				if (db != null)
				{
					db.close();
				}
			} 
			return result;
			}		
	}
	public List<T> selectQuery(String sql){
		GLog.v(TAG, "selectQuery:"+sql);

		List<T> list = new ArrayList<T>();
		Cursor cursor = null;
		synchronized (helper) {
			try{
				if (!db.isOpen()) {
					db = helper.getWritableDatabase();
				}
				cursor = db.rawQuery(sql,null);
				while(cursor.moveToNext()){
					T d = parseItem(cursor);		
					list.add(d);
				}			
			}catch(Exception e){
				e.printStackTrace();
				throw e;
			}
			finally{
				if (cursor != null) {
					cursor.close();
				}
				if (db != null)
				{
					db.close();
				}
			} 
			return list;
		}
		
	}
	
	public int excute(String sql){
		int id=0;
		synchronized (helper) {
			Cursor cursor = null;

			try{
				GLog.v(TAG, "excuteSql:"+sql);
				if (!db.isOpen()) {
					db = helper.getWritableDatabase();
				}
				db.execSQL(sql);
				if (sql.indexOf("insert") != -1) {//若是插入操作，则获取最后一条ID
					cursor = db.rawQuery("select last_insert_rowid() from "+tableName,null);
					if(cursor.moveToFirst())  
						id = cursor.getInt(0);
				}
			}catch(Exception e){
				e.printStackTrace();
				throw e;
			}
			finally{
				if (db != null)
					db.close();
				if(cursor != null)
					cursor.close();
			}

			
			return id;
		}

	}; 
	
	/**
	 * @author Smither
	 */
	protected String rebuildingCondition(List<String> con, String conJoin){
		StringBuilder condition = new StringBuilder("");
		if(con == null || con.size() == 0){
			return condition.toString();
		}
		condition.append(" where ");
		for (int i = 0; i < con.size(); i++) {
			condition.append(con.get(i));
			GLog.v(TAG, "conJoin:"+conJoin +" con:"+con.size());
			if(conJoin != null && i!= con.size()-1){
				condition.append(" "+conJoin+" ");
			}
		}
		return condition.toString();
	}
	
	protected String rebuildInCondotion(String feiled, List<String> con){
		String str = "";
		if (con == null || con.size() == 0) {
			return str;
		}	
		for(int i = 0; i < con.size(); i++){
			if (str != "") {
				str = str+","+"'"+con.get(i)+"'";
			}else {
				str = "'"+con.get(i)+"'";
			}			
		}
		if (feiled != null) {
			str = " where " +feiled+" in (" +str;
		}
		str += " )";
		return str;
	}

	private String replaceSpecialCharacter(String value) {
		value = value.replace("\'", "");
		return value;
	}
}
