package com.guogee.tablet;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.guogee.tablet.db.DbHelper;
import com.guogee.tablet.manager.CameraManager;
import com.guogee.tablet.manager.ColorManager;
import com.guogee.tablet.db.DbHelper.CameraColelction;
import com.guogee.tablet.db.DbHelper.CustomCollection;
import com.guogee.tablet.manager.CustomManager;
import com.guogee.tablet.manager.DeviceManager;
import com.guogee.tablet.manager.SmartWallSwitchConfigManager;
import com.guogee.tablet.model.CameraModel;
import com.guogee.tablet.model.ColorModel;
import com.guogee.tablet.model.CustomModel;
import com.guogee.tablet.model.SmartLockModel;
import com.guogee.tablet.model.SmartLockUserIdNameModel;
import com.guogee.tablet.model.SmartWallSwitchConfigModel;
import com.guogee.ismartandroid2.manager.DataModifyHandler;
import com.guogee.ismartandroid2.manager.DataSync;
import com.guogee.ismartandroid2.utils.GLog;

import java.util.ArrayList;
import java.util.List;

public class SysData {

    public static final String TAG = "SysData";
    private Context context;
    private DataSync ds;

    public SysData(Context cx) {
        this.context = cx;
        ds = new DataSync(cx);
    }


    /**
     * @return
     * @author Smither
     * @update 2014/8/25
     */
    public JSONArray getCustomsData() {

        JSONArray result = new JSONArray();
        List<CustomModel> customList = CustomManager.getInstance().getAllCustom();

        for (CustomModel model : customList) {
            JSONObject tempMap = new JSONObject();
            tempMap.put("id", model.getId());
            tempMap.put("deviceId", model.getDeviceId());

            tempMap.put("iconName", model.getName());

            tempMap.put("xIndex", model.getX());

            tempMap.put("yIndex", model.getY());

            tempMap.put("iconTag", model.getTag());

            tempMap.put("type", model.getType());

            result.add(tempMap);
        }
        return result;
    }


    /**
     * @return 防止数据过大，造成内存溢出，因而换sax读取方式JSON
     * @author Smither
     * @update 2014/8/25
     */
    public JSONArray getColorsData() {

        JSONArray returnDevice = new JSONArray();
        List<ColorModel> colorList = ColorManager.getInstance().getAllColor();

        for (ColorModel model : colorList) {
            JSONObject tempMap = new JSONObject();

            tempMap.put("id", model.getId());
            int value = model.getRed();
            tempMap.put("red", value);

            value = model.getGreen();
            tempMap.put("green", value);

            value = model.getBlue();
            tempMap.put("blue", value);

            value = model.getWhite();
            tempMap.put("white", value);

            String str = model.getPerc();
            tempMap.put("perc", str);

            value = model.getOrders();
            tempMap.put("orders", value);

            returnDevice.add(tempMap);
        }
        return returnDevice;
    }

    public JSONArray getCameraData() {

        JSONArray returnDevice = new JSONArray();
        List<CameraModel> listCamera = CameraManager.getInstance().getAllCamera();
        for (CameraModel model : listCamera) {
            JSONObject tempMap = new JSONObject();
            tempMap.put("id", model.getId());
            String value = model.getCameraID();
            tempMap.put("cameraId", value);

            value = model.getLoginName();
            tempMap.put("loginName", value);

            value = model.getLoginPwd();
            tempMap.put("loginPwd", value);

            value = model.getReserve();
            tempMap.put("reserve", value);

            returnDevice.add(tempMap);
        }
        return returnDevice;
    }

    public JSONArray getSmartLockData() throws InterruptedException {
        final JSONArray data = new JSONArray();
        synchronized (data) {
            DeviceManager.getInstance().getSmartLockInfoList(new DataModifyHandler<List<SmartLockModel>>() {
                @Override
                public void onResult(List<SmartLockModel> smartLockModels, Exception e) {
                    if (e != null)
                        throw new RuntimeException(e);
                    synchronized (data) {
                        for (SmartLockModel slm : smartLockModels) {
                            JSONObject tmp = new JSONObject();
                            tmp.put("id", slm.getId());
                            tmp.put("deviceId", slm.getDeviceId());
                            tmp.put("userId", slm.getUserId());
                            tmp.put("pwd", slm.getPwd());
                            tmp.put("lastTime", slm.getLastTime());
                            data.add(tmp);
                        }
                        data.notify();
                    }
                }
            });
            data.wait();
        }
        return data;

    }

    public JSONArray getLockUserIdData() throws InterruptedException {
        final JSONArray data = new JSONArray();
        synchronized (data) {
            DeviceManager.getInstance().getAllSmartLockUserID(new DataModifyHandler<List<SmartLockUserIdNameModel>>() {
                @Override
                public void onResult(List<SmartLockUserIdNameModel> smartLockUserIdNameModels, Exception e) {
                    if (e != null)
                        throw new RuntimeException(e);
                    synchronized (data) {
                        for (SmartLockUserIdNameModel slm : smartLockUserIdNameModels) {
                            JSONObject tmp = new JSONObject();
                            tmp.put("id", slm.getId());
                            tmp.put("devMac", slm.getDeviceAddr());
                            tmp.put("lockId", slm.getLockUserId());
                            tmp.put("userName", slm.getUserName());
                            data.add(tmp);
                        }
                        data.notify();
                    }
                }
            });
            data.wait();
        }
        return data;

    }

    public JSONArray getSmartWallSwitchConfig() throws InterruptedException {
        final JSONArray data = new JSONArray();
        List<SmartWallSwitchConfigModel> models = SmartWallSwitchConfigManager.getInstance().getAllSmartWallSwitchConfigs();
        if (null != models && models.size() > 0) {
            for (SmartWallSwitchConfigModel model : models) {
                JSONObject tmp = new JSONObject();
                tmp.put("id", model.getId());
                tmp.put("deviceId", model.getDeviceId());
                tmp.put("num", model.getNum());
                tmp.put("configMode", model.getConfigMode());
                tmp.put("value", model.getValue());
                data.add(tmp);
            }
        }
        return data;

    }

    /**
     * @return
     * @author Smither
     * @update 2014/8/25
     */
    public JSONObject getAllData() throws InterruptedException {

        JSONObject resultJs = new JSONObject();

        resultJs.put("RGBLightColor", getColorsData());

        resultJs.put("custom", getCustomsData());
        resultJs.put("camera", getCameraData());
        resultJs.put("smartLock", getSmartLockData());
        resultJs.put("lockUserIdIdentify", getLockUserIdData());
        resultJs.put("smartWallSwitchConfig", getSmartWallSwitchConfig());
        return resultJs;
    }

    /**
     * @return
     * @throws Exception
     * @author Smither
     * @update 2014/8/25
     */
    public String getJSONData() throws Exception {

        JSONObject jsonobj = getAllData();
        DataSync ds = new DataSync(context);
        JSONObject jo = ds.getAllData();
        jsonobj.putAll(jo);
        return jsonobj.toString();
    }

    public void updateDataBase(JSONObject sysData, int ver) throws Exception {
        ds.updateDataBase(sysData, ver);

        JSONArray customdata = sysData.getJSONArray("custom");
        updateCustomDataBaseWithTransaction(customdata, ver);

        JSONArray cameraData = sysData.getJSONArray("camera");
        updateCameraDataBaseWithTransaction(cameraData, ver);

        JSONArray smartLockData = sysData.getJSONArray("smartLock");
        updateSmartLockDatabaseWithTransaction(smartLockData, ver);

        if (sysData.containsKey("lockUserIdIdentify")) {
            JSONArray lockUserIdentifyData = sysData.getJSONArray("lockUserIdIdentify");
            updateLockUserIdentifyDatabaseWithTransaction(lockUserIdentifyData, ver);
        }

        if (sysData.containsKey("smartWallSwitchConfig")) {
            JSONArray smartWallSwitchConfigData = sysData.getJSONArray("smartWallSwitchConfig");
            updateSmartWallSwitchConfigDatabaseWithTransaction(smartWallSwitchConfigData, ver);
        }
    }

    private void updateSmartWallSwitchConfigDatabaseWithTransaction(JSONArray smartWallSwitchConfigData, int ver) {
        GLog.i(TAG, "smartWallSwitchConfigData:" + smartWallSwitchConfigData + " ver:" + ver);
        if (smartWallSwitchConfigData == null || ver < 40)
            return;
        List<ContentValues> insertList = new ArrayList<ContentValues>();
        for (int i = 0; i < smartWallSwitchConfigData.size(); i++) {
            JSONObject data = smartWallSwitchConfigData.getJSONObject(i);
            ContentValues value = new ContentValues();
            value.put(DbHelper.SmartWallSwitchHelperCollection.ID, data.getIntValue("id"));
            value.put(DbHelper.SmartWallSwitchHelperCollection.DEVICE_ID, data.getString("deviceId"));
            value.put(DbHelper.SmartWallSwitchHelperCollection.NUM, data.getIntValue("num"));
            value.put(DbHelper.SmartWallSwitchHelperCollection.CONFIG_MODE, data.getString("configMode"));
            value.put(DbHelper.SmartWallSwitchHelperCollection.VALUE, data.getString("value"));
            insertList.add(value);
        }
        clearAndInsertWithTransaction(DbHelper.SmartWallSwitchHelperCollection.TABLE_NAME, insertList);
    }

    private void updateLockUserIdentifyDatabaseWithTransaction(JSONArray lockUserIdentifyData, int ver) {
        GLog.i(TAG, "lockUserIdentifyData:" + lockUserIdentifyData + " ver:" + ver);
        if (lockUserIdentifyData == null || ver < 39)
            return;
        List<ContentValues> insertList = new ArrayList<ContentValues>();
        for (int i = 0; i < lockUserIdentifyData.size(); i++) {
            JSONObject data = lockUserIdentifyData.getJSONObject(i);
            ContentValues value = new ContentValues();
            value.put(DbHelper.LockUserIDHelperCollection.ID, data.getIntValue("id"));
            value.put(DbHelper.LockUserIDHelperCollection.DEVICE_ADDR, data.getString("devMac"));
            value.put(DbHelper.LockUserIDHelperCollection.LOCK_USER_ID, data.getIntValue("lockId"));
            value.put(DbHelper.LockUserIDHelperCollection.USER_NAME, data.getString("userName"));
            insertList.add(value);
        }
        clearAndInsertWithTransaction(DbHelper.LockUserIDHelperCollection.TABLE_NAME, insertList);
    }

    private void updateSmartLockDatabaseWithTransaction(JSONArray smartlockData, int ver) {
        GLog.i(TAG, "smartlock:" + smartlockData + " ver:" + ver);
        if (smartlockData == null || ver < 39)
            return;
        List<ContentValues> insertList = new ArrayList<ContentValues>();
        for (int i = 0; i < smartlockData.size(); i++) {
            JSONObject data = smartlockData.getJSONObject(i);
            ContentValues value = new ContentValues();
            value.put(DbHelper.SmartLockCollections.ID, data.getIntValue("id"));
            value.put(DbHelper.SmartLockCollections.DEVICE_ID, data.getIntValue("deviceId"));
            value.put(DbHelper.SmartLockCollections.USER_ID, data.getIntValue("userId"));
            value.put(DbHelper.SmartLockCollections.PASSWORD, data.getString("pwd"));
            value.put(DbHelper.SmartLockCollections.LASTTIME, data.getLongValue("lastTime"));
            insertList.add(value);
        }
        clearAndInsertWithTransaction(DbHelper.SmartLockCollections.TABLE_NAME, insertList);
    }

    private void updateCameraDataBaseWithTransaction(JSONArray cameraData, int ver) {
        List<ContentValues> insertList = new ArrayList<ContentValues>();

        if (cameraData != null && ver >= 35) {
            for (int i = 0; i < cameraData.size(); i++) {
                JSONObject cameraTempdata = cameraData.getJSONObject(i);

                ContentValues value = new ContentValues();
                value.put(CameraColelction.ID, cameraTempdata.getString("id"));

                String cameraId = cameraTempdata.getString("cameraId");
                if (cameraId == null || cameraId.equals(""))
                    continue;
                value.put(CameraColelction.CAMERA_ID, cameraId);
                value.put(CameraColelction.LOGIN_NAME, cameraTempdata.getString("loginName"));
                value.put(CameraColelction.LOGIN_PWD, cameraTempdata.getString("loginPwd"));
                value.put(CameraColelction.RESERVE, cameraTempdata.getString("reserve"));
                insertList.add(value);
            }
        }

        clearAndInsertWithTransaction(CameraColelction.TABLE_NAME, insertList);

    }

    /**
     * @param customdata
     * @author Sky
     * 事务更新自定义数据
     */
    private void updateCustomDataBaseWithTransaction(JSONArray customdata, int ver) {
        List<ContentValues> insertList = new ArrayList<ContentValues>();

        if (customdata != null && ver >= 35) {
            for (int i = 0; i < customdata.size(); i++) {

                JSONObject deviceTempdata = customdata.getJSONObject(i);
                ContentValues value = new ContentValues();
                value.put(CustomCollection.ID, deviceTempdata.getIntValue("id"));
                value.put(CustomCollection.DEVICE_ID, deviceTempdata.getIntValue("deviceId"));
                value.put(CustomCollection.NAME, deviceTempdata.getString("iconName"));
                value.put(CustomCollection.X, deviceTempdata.getIntValue("xIndex"));
                value.put(CustomCollection.Y, deviceTempdata.getIntValue("yIndex"));
                value.put(CustomCollection.TAG, deviceTempdata.getIntValue("iconTag"));
                value.put(CustomCollection.TYPE, deviceTempdata.getIntValue("type"));
                insertList.add(value);
                GLog.i(TAG, "insert into Custom, device id:" + deviceTempdata.getString("deviceId") +
                        "tag:" + deviceTempdata.getString("iconTag"));

            }
        }

        clearAndInsertWithTransaction(CustomCollection.TABLE_NAME, insertList);

    }

    /**
     * @param tableName
     * @param data
     * @author Sky
     * 事务插入公共方法
     */
    private void clearAndInsertWithTransaction(String tableName,
                                               List<ContentValues> data) {
        DbHelper db = new DbHelper(context);
        SQLiteDatabase database = db.getWritableDatabase();
        database.beginTransaction();


        try {
            database.delete(tableName, null, null);
            for (int i = 0; i < data.size(); i++) {
                database.insert(tableName, null, data.get(i));
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
            if (database != null) {
                database.close();
            }
        }
    }


}

