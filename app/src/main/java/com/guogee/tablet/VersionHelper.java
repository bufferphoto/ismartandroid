package com.guogee.tablet;

import android.content.Context;

import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.service.NetworkServiceConnector;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.ismartandroid2.utils.PublishHelper;

import com.guogee.pushclient.GPushManager;

/**
 * Created by xieguangwei on 16/1/21.
 */
public class VersionHelper {


    public static void setUp(Context context) {
        boolean isDebug = SystemUtil.isDebugable(context);
        isDebug=true;
        if (isDebug) {
            PublishHelper.setBuildType(PublishHelper.BuildType.Test);
        } else {
            PublishHelper.setBuildType(PublishHelper.BuildType.Release);
        }
        GLog.setDebug(isDebug);
        NetworkServiceConnector.getInstance().setNetworkDebug(false);//是否打印udp数据包
        GPushManager.getInstance(context).setDebug(isDebug);//推送服务器

    }
}
