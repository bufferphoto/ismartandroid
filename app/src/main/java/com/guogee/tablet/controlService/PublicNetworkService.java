package com.guogee.tablet.controlService;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.PowerManager;

import com.guogee.tablet.MainActivity;
import com.example.mt.mediaplay.R;
import com.guogee.ismartandroid2.service.NetworkServiceConnector;
import com.guogee.ismartandroid2.utils.GLog;




public class PublicNetworkService extends Service {

    private PowerManager.WakeLock wakeLock;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        SharedPreferences preferences = getSharedPreferences("CONFIG",
                Context.MODE_PRIVATE);
        final String userName = preferences.getString("login_name", "");
        final String userPwd = preferences.getString("login_password", "");
        if (!userName.equals("") && !userPwd.equals("")) {
            Notification notification = new Notification();
            notification.icon = R.drawable.ic_launcher;
            //设置内容和点击事件
            Intent intent = new Intent(this, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
            String title = getResources().getString(R.string.app_name) + " " + getResources().getString(R.string.running_title);
            notification.setLatestEventInfo(this, title, getResources().getString(R.string.running_tip), contentIntent);
            notification.tickerText = title;
            notification.icon = R.drawable.ic_launcher;
            notification.flags = Notification.FLAG_FOREGROUND_SERVICE; //设置为点击后自动取消
            startForeground(1235, notification);
        }
    }

    private void acquire() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "com.task.TalkMessageService");
        wakeLock.acquire();
    }

    private void release() {
        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        GLog.d("PublicNetworkService", "control service onStartCommand");
//		if(intent != null){
//			boolean stop = intent.getBooleanExtra("stop",false);
//			if(stop) {
//				stopSelf();
//				return START_NOT_STICKY;
//			}
//		}

        NetworkServiceConnector.getInstance();
        return START_STICKY;
    }


    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
        return super.onUnbind(intent);
    }


    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }
}
