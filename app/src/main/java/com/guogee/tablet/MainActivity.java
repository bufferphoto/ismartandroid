package com.guogee.tablet;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.guogee.tablet.controlService.PublicNetworkService;
import com.guogee.tablet.manager.ActivityManager;
import com.guogee.tablet.manager.DownloadManager;
import com.guogee.tablet.service.AutoUpdateService;
import com.guogee.tablet.ui.activity.BaseActivity;
import com.guogee.tablet.ui.fragment.HomeContentfragment;
import com.guogee.tablet.ui.fragment.SceneFragment;
import com.guogee.tablet.ui.fragment.SettingFragment;
import com.example.mt.mediaplay.R;
import com.guogee.tablet.ui.fragment.MusicFragment;
import com.guogee.tablet.ui.widge.CustomDialog;
import com.guogee.tablet.utils.Constant;
import com.guogee.ismartandroid2.device.DeviceListener;
import com.guogee.ismartandroid2.device.Gateway;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.networkingProtocol.DeviceType;
import com.guogee.ismartandroid2.response.GatewayResponse;
import com.guogee.ismartandroid2.utils.GConstant;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.sdk.android.network.NetworkManager;
import com.iflytek.cloud.SpeechUtility;
import com.umeng.analytics.MobclickAgent;

import java.io.File;

import de.greenrobot.event.EventBus;

/**
 * Created by mt on 2017/5/22.
 */

public class MainActivity extends BaseActivity implements OnCheckedChangeListener, View.OnClickListener,DownloadManager.DownloadListener,NetworkManager.NetworkListener , DeviceListener<GatewayResponse> {

    private static final String TAG = "MainActivity";
    private RadioButton tabHome;
    private RadioButton tabScene;
    private RadioButton tabSetting;
    private RadioButton tabMagic;
    private RadioButton tabMusic;
    private RadioButton tabVoice;
    private FrameLayout mContainer;
    private int currentTabId;

    private DownloadManager mDownloadManager;
    private NetworkManager mNetworkManager;
    private iSmartApplication isapp;
    private ProgressDialog progressDialog;
    private String mAppPath;
    SharedPreferences spf;
    private boolean hasShowUpdate;
    private boolean showToast;

    private boolean isAlreadIntentAddBoxPage = false;

    /**
     * 登录标志־
     *
     * @author Smither 2014/8/25
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isapp = (iSmartApplication) getApplication();

        initView();
        MobclickAgent.openActivityDurationTrack(false);
        MobclickAgent.updateOnlineConfig(this);
        registerLanguageChange();
        spf = getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
        mDownloadManager = DownloadManager.getInstance();
        mDownloadManager.addDownloadListener(this);
        mNetworkManager = NetworkManager.getInstance(this);
        mNetworkManager.addListener(this);

        startService(new Intent(this, PublicNetworkService.class));

        SpeechUtility.createUtility(isapp, "appid=" + getString(R.string.app_id));
        Intent intent = new Intent(this, AutoUpdateService.class);
        startService(intent);
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        MobclickAgent.onPageStart(TAG);
        MobclickAgent.onResume(this);

        if (isapp.isJustOpen) {
            this.RefreshGateOnlineStatus(null, false);
            isapp.isJustOpen = false;
        }
        GLog.d(TAG, "onresume");
        getNewVersion();
        tabHome.setChecked(false);
        tabMagic.setChecked(false);
        tabScene.setChecked(false);
        tabMusic.setChecked(false);
        tabSetting.setChecked(false);
        tabVoice.setChecked(false);
        switch (currentTabId) {
            case R.id.radio_scene:
                tabScene.setChecked(true);
                break;
            case R.id.radio_setting:
                tabSetting.setChecked(true);
                break;
            case R.id.radio_magic:
                tabMagic.setChecked(true);
                break;
            case R.id.radio_music:
                tabMusic.setChecked(true);
                break;
            case R.id.radio_home:
                tabHome.setChecked(true);
            default:
                tabScene.setChecked(true);
                break;
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }



    private void initView() {
        tabHome = (RadioButton) findViewById(R.id.radio_home);
        tabScene = (RadioButton) findViewById(R.id.radio_scene);
        tabSetting = (RadioButton) findViewById(R.id.radio_setting);
        tabMagic = (RadioButton) findViewById(R.id.radio_magic);
        tabMusic = (RadioButton) findViewById(R.id.radio_music);
        tabVoice = (RadioButton) findViewById(R.id.btn_voice);
        mContainer = (FrameLayout) findViewById(R.id.container);

        tabHome.setOnCheckedChangeListener(this);
        tabScene.setOnCheckedChangeListener(this);
        tabSetting.setOnCheckedChangeListener(this);
        tabMusic.setOnCheckedChangeListener(this);
        tabMagic.setOnCheckedChangeListener(this);

        tabHome.setOnClickListener(this);
        tabScene.setOnClickListener(this);
        tabSetting.setOnClickListener(this);
        tabMusic.setOnClickListener(this);
        tabMagic.setOnClickListener(this);
        tabVoice.setOnClickListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            Fragment fragment = (Fragment) mFragmentPagerAdapter
                    .instantiateItem(mContainer, buttonView.getId());
            Log.i(TAG, "----buttonView----tId=" + buttonView.getId());
            mFragmentPagerAdapter.setPrimaryItem(mContainer, 0, fragment);
            Log.i(TAG, "----onCheckedChanged----fragment=" + fragment);
            mFragmentPagerAdapter.finishUpdate(mContainer);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_voice:
                ActivityManager.gotoActivity(this, ActivityManager.ACTIVITY_VOICE, null);
                overridePendingTransition(R.anim.voice_activity_enter_anim, R.anim.home_activity_exit_anim);
                break;
            case R.id.radio_magic:
                currentTabId = R.id.radio_magic;
                tabHome.setChecked(false);
                tabScene.setChecked(false);
                tabMusic.setChecked(false);
                tabSetting.setChecked(false);
                tabVoice.setChecked(false);
                break;
            case R.id.radio_scene:
                currentTabId = R.id.radio_scene;
                tabHome.setChecked(false);
                tabMagic.setChecked(false);
                tabMusic.setChecked(false);
                tabSetting.setChecked(false);
                tabVoice.setChecked(false);
                break;
            case R.id.radio_setting:
                currentTabId = R.id.radio_setting;
                tabHome.setChecked(false);
                tabMagic.setChecked(false);
                tabScene.setChecked(false);
                tabMusic.setChecked(false);
                tabVoice.setChecked(false);
                break;
            case R.id.radio_music:
                currentTabId = R.id.radio_music;
                tabHome.setChecked(false);
                tabMagic.setChecked(false);
                tabScene.setChecked(false);
                tabSetting.setChecked(false);
                tabVoice.setChecked(false);
                break;
            case R.id.radio_home:
                currentTabId = R.id.radio_home;
                tabMagic.setChecked(false);
                tabScene.setChecked(false);
                tabMusic.setChecked(false);
                tabSetting.setChecked(false);
                tabVoice.setChecked(false);
                break;
        }
    }


    private FragmentPagerAdapter mFragmentPagerAdapter = new FragmentPagerAdapter(
            getSupportFragmentManager()) {

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case R.id.radio_scene:
                    return SceneFragment.instantiation(2);
                case R.id.radio_setting:
                    return SettingFragment.instantiation(3);
                case R.id.radio_music:
                    return MusicFragment.instantiation(4);
                case R.id.radio_magic:
                    // return Fragment.instantiate(MainActivity.this, "com.guogu.ismartandroid2.ui.activity.magic.MagicListFragment");

                case R.id.radio_home:
                    return HomeContentfragment.instantiation(1);
                default:
                    return SceneFragment.instantiation(2);
            }
        }

        @Override
        public int getCount() {
            return 5;
        }
    };

    //tableID改变
    public void onEventMainThread(Bundle bundle) {
        if (null != bundle) {
            int tabId = bundle.getInt("TabId", -1);
            Log.i(TAG, "----onStart----tabId=" + tabId);
            if (tabId > -1) {
                currentTabId = tabId;
            }
        }
    }

    @Override
    public void onDestroy() {
        GLog.i(TAG, "main activity ondestroy.");
        super.onDestroy();

        GLog.i("AutoUpdateService","destory");
        Intent intent = new Intent(this, AutoUpdateService.class);
        stopService(intent);
        unregisterReceiver(languageReceiver);
        mDownloadManager.removeDownloadListenr(this);
        mNetworkManager.removeListener(this);
        //	isapp.removeActivity(this);
        EventBus.getDefault().unregister(this);

    }

    private long firstime = 0;

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            long secondtime = System.currentTimeMillis();
            if (secondtime - firstime > 3000) {
                Toast.makeText(MainActivity.this, getResources().getString(R.string.exit_tip), Toast.LENGTH_SHORT).show();
                firstime = secondtime;
                return true;
            } else {
//				Intent startMain = new Intent(Intent.ACTION_MAIN);
//                startMain.addCategory(Intent.CATEGORY_HOME);
//                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(startMain);
                this.moveTaskToBack(true);
                return true;

            }

        }
        return super.onKeyUp(keyCode, event);
    }

    /**
     * 刷新网关在线状态
     *
     * @return
     */
    public boolean RefreshGateOnlineStatus(ProgressDialog progressDialog, boolean showToast) {
        this.progressDialog = progressDialog;
        this.showToast = showToast;
        isAlreadIntentAddBoxPage = false;
        // 发送局域网验证
        sentAckToBox();

        return true;
    }

    private void noNewGateway() {
        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                return;
            }
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        if (this.showToast) {

            //		startActivityForResult(intent, 1);
            //		Toast.makeText(this, getResources().getString(R.string.no_check_network),Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessage(1);
            this.showToast = false;
        }

    }

    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Bundle bundle = new Bundle();
                    bundle.putString("headTitle", getResources().getString(R.string.zq_nitoce_title));
                    bundle.putString(
                            "replaceTip",
                            getResources().getString(
                                    R.string.no_check_network));
                    bundle.putString("cancleText", getResources().getString(R.string.ok));
                    Intent intent = new Intent(getApplicationContext(), CustomDialog.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 显示更新APK
     */
    private void showUpdateDialog() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isUpdateVersion", true);
        SharedPreferences mPreferences = getSharedPreferences(
                "CONFIG", Context.MODE_PRIVATE);
        String s = mPreferences.getString("description", getResources().getString(R.string.zq_has_new_version));
//    	char[] str = s.toCharArray();
//    	 StringBuilder sb = new StringBuilder();
//    	for (int i = 0; i < str.length; i++) {
//    		switch (str[i]) {
//			case '\\':
//				sb.append("\\\\");
//				break;
//			case 'n':
//				break;
//			default:
//				sb.append(str[i]);
//				break;
//			}
//		}
//    	GLog.v("LZP", sb.toString().replace("\\\\", "\n"));
        bundle.putString(
                "replaceTip", s.replace("\\n", "\n"));
        bundle.putString("cancleText", getResources().getString(R.string.next_time));
        bundle.putString("sureText", getResources().getString(R.string.update_now));
        bundle.putString("headTitle", getResources().getString(R.string.zq_has_new));
        bundle.putString("data", mAppPath);
        Intent intent = new Intent(getApplicationContext(), CustomDialog.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }



    /**
     * wifi 状态发送ACK 指令UDP 广播，查询盒子是否存在
     */
    private void sentAckToBox() {

        GLog.d("", "发送查找网关请求");
        Gateway gw = new Gateway();
        gw.setListener(this);
        gw.searchGatewayInLan();
    }


    /**
     * 判断盒子是否存在于已绑定列表
     *
     * @param gateaddr
     * @return
     */
    private boolean isBoxInBinddingList(String gateaddr) {

        Device gateway = RoomManager.getInstance(this).searchDevice(gateaddr);
        if (gateway != null) {
            return true;
        } else {
            return false;
        }

    }


    /**
     * 获取盒子信息成功
     *
     * @param gateaddr
     */
    private void GetBoxSuccessed(String gateaddr, String gateipaddr, int gateport, String deviceType, int deviceVersion) {
        GLog.d("sxx", "GetBoxSuccessed enter.");

        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                return;
            }
        }

        if (!isBoxInBinddingList(gateaddr)) {
            GLog.d("GetBoxSuccessed",
                    "isBoxInBinddingList(gateaddr) = false");


            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
            GLog.v("LZP", "isAlreadIntentAddBoxPage:" + isAlreadIntentAddBoxPage);
            if (!isAlreadIntentAddBoxPage) {
                isAlreadIntentAddBoxPage = true;
                Bundle bundle = new Bundle();
                bundle.putString("gateaddr", gateaddr);
                bundle.putString("gateipaddr", gateipaddr);
                bundle.putString("deviceType", deviceType);
                bundle.putInt("deviceVersion", deviceVersion);
                bundle.putInt("gateport", gateport);
                //bundle.putInt("bindBoxSize", bindBoxSize);
              /* ---------mt-------------- final Intent mainIntent = new Intent(this, AddBoxActivity.class);
                mainIntent.putExtras(bundle);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(mainIntent);
                    }
                }, 400);*/
            }
        }
        // 如果盒子在绑定列表中，设置盒子状态
        else {
            GLog.d("sxx", "GetBoxSuccessed  noNewGateway");
            this.noNewGateway();
        }

    }


    /**
     * add by sky 监听系统语言设置
     */
    private void registerLanguageChange() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_LOCALE_CHANGED);
        filter.addAction(Constant.ACTION_PASSWORD_CHANGE);
        registerReceiver(languageReceiver, filter);

    }

    private BroadcastReceiver languageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            GLog.i(TAG, "ACTION_LOCALE_CHANGED");

		 	 /*Intent it = new Intent(getApplicationContext(), MainActivity.class);
             it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		     it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		     startActivity(it);*/

            finish();
        }
    };


    @Override
    public void onSuccess(GatewayResponse obj) {
        String src_mac = obj.getMac();
        String gateipaddr = obj.getIp();
        int gateport = obj.getPort();
        // 获取盒子信息成功
        String deviceType;
        switch (obj.getVersion()) {
            case 0x10:
                deviceType = DeviceType.GATEWAY_PLUG_FLAG;
                break;
            case 0x20:
                onFail(null);
                return;
            case 0x30:
                deviceType = DeviceType.GATEWAY_INTEL_BOX;
                break;
            case 0x00:
            default:
                deviceType = DeviceType.GATEWAY_FALG;
                break;
        }
        //GetBoxSuccessed(src_mac, gateipaddr, gateport, deviceType, obj.getVersion());

    }


    @Override
    public void onFail(GatewayResponse obj) {
        noNewGateway();

    }


    @Override
    public void onDownloadComplete(String path) {
        if (!hasShowUpdate) {
            hasShowUpdate = true;
            GLog.i(TAG, "onDownloadComplete");
            isapp.newVersion = true;
            spf.edit().putString("updatePath", path).commit();
            showUpdateDialog();

        }

    }

    private void getNewVersion() {
        if (mNetworkManager.getNetworkType() == GConstant.NetworkType.WifiConnected) {

            try {
                int ver = spf.getInt("verCode", 0);
                PackageManager manager = getApplication()
                        .getPackageManager();
                PackageInfo info = manager.getPackageInfo(
                        getApplication().getPackageName(), 0);
                GLog.i(TAG, "versionCode:" + info.versionCode);
                if (ver > info.versionCode) {
//					mAppPath = Environment.getExternalStorageDirectory().getPath() +File.separator+ "iSmartAndroid"+File.separator+"ismart"+ver+".apk";
                    File cache = getExternalCacheDir();
                    if (cache == null)
                        return;
                    mAppPath = cache.getAbsolutePath() + File.separator + getString(R.string.app_name) + "-" + ver + ".apk";

                    String url = spf.getString("url", "");
                    mDownloadManager.download(url, mAppPath);
                }

            } catch (PackageManager.NameNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }


    }


    @Override
    public void onNetworkStateChange(GConstant.NetworkType state) {
        if (state == GConstant.NetworkType.MobileConnected || state == GConstant.NetworkType.NoNetwork) {
            mDownloadManager.stop();
        } else {
            getNewVersion();
        }
    }



}
