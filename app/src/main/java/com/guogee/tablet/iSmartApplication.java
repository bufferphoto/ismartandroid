package com.guogee.tablet;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;

import com.guogee.tablet.adapter.SecurityLightItem;
import com.guogee.tablet.broadcastreceiver.MediaButtonBroadcastReceiver;
import com.guogee.tablet.camera.AbstractCamera;
import com.guogee.tablet.controlService.KeepAliveReceiver;
import com.guogee.tablet.utils.Constant;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.model.Room;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.tablet.utils.ImageLoaderUtil;
import com.guogee.pushclient.GPushManager;
import com.guogee.sdk.ISmartManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class iSmartApplication extends Application {
	
	
	private static final String TAG = "iSmartApplication";
	private static iSmartApplication app;
	/*
	 * 登陆用户
	 */
	private iSmartUser ismartuser;

	/*
	 * 已开启的防盗光列
	 */
	public Map<String, SecurityLightItem> securityMap = new HashMap<String, SecurityLightItem>();
	
	/*
	 * 已选中的房间
	 */
	private Room currentRoom;
	/*
	 * 已选择红外遥控设备对应控制指令列表
	 */
	private List<Map<String, String>> deviceKeys;

	/*
	 * 震动提示标识
	 */
	public boolean enableVibrator = true;
	/**
	 * 摇一摇
	 */
	public boolean enableShake = true;
	/*
	 * 新版本
	 */
	public boolean newVersion = false;

	public boolean isJustOpen = true;//是否刚打开app
	

	
	/*
	 * app语言版本
	 */
	public static String AppVersion="EN";
	
	/**
	 * 环境检测仪标准
	 */
	public static int enviromentStandard = 0;

	/**
	 * Camera 全屏时候保存对当前Camera的引用
	 * Smither 2014/12/12
	 */
	private AbstractCamera camera;
	
	public AbstractCamera getCamera() {
		return camera;
	}
	public void setCamera(AbstractCamera camera) {
		this.camera = camera;
	}
	/**
	 * 房间对应的实例修改了
	 * @param dl
	 */
	private boolean roomFragmentsModify = true;
	/**
	 *  房间对应的实例修改了
	 * @param modify
	 */
	public void setRoomFragmentsModify(boolean modify){
		this.roomFragmentsModify = modify;
	}
	/**
	 * 房间对应的实例修改了
	 * @return
	 */
	public boolean getRoomFragmentsModify(){
		return roomFragmentsModify;
	}
	

	
	//////////////////////////////////////////////////////////////////////////////
	
	public iSmartApplication() {  
        
	}
	
	public static iSmartApplication getApp(){
		return app;
	}
	
	@Override
	public void onCreate() {
		GLog.i(TAG, "onCreate");
		String processName = SystemUtil.getProcessName(this,android.os.Process.myPid());
		GLog.i(TAG, "processName:"+processName);
		if(processName.contains(":remote"))//多进程
			return;

//		if (!PublishHelper.isRelease()) {
//			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll()
//           
//            .build());
//			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll()
//           
//            .build());
//		}
		super.onCreate();
		VersionHelper.setUp(this);
		ISmartManager.loadGateway(this);
		
		app = this;
		ISmartManager.init(this);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH){
			ComponentName mediaButtonReceiver = new ComponentName(getPackageName(), MediaButtonBroadcastReceiver.class.getName());

			AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

	        audioManager.registerMediaButtonEventReceiver(mediaButtonReceiver);
	        
		}
		MediaButtonBroadcastReceiver mReceiver = new MediaButtonBroadcastReceiver();
		IntentFilter intentFilter = new IntentFilter();  
        intentFilter.addAction("android.intent.action.HEADSET_PLUG");
		this.registerReceiver(mReceiver, intentFilter);
		
		SharedPreferences spf = getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
		enviromentStandard = spf.getInt("EnviromentDataStandard", 0);
		setTimer(3);
		//Smither 错误信息上传
		CrashHandler crashHandler = CrashHandler.getInstance();
		crashHandler.init(this,this);
		initPush();

		String cachePath = getCacheDir().getAbsolutePath() + "/CacheImage";
		// 初始化ImageLoader
		ImageLoaderUtil.initImageLoader(this, cachePath);
	}



	private void initPush(){

		GPushManager.getInstance(this);

	}
	
	public void setTimer(float time){
		
		Intent it = new Intent(this,KeepAliveReceiver.class);
		it.setAction(Constant.ACTION_KEEP_ALIVE);
		PendingIntent pi = PendingIntent.getBroadcast(iSmartApplication.getApp(), 78787878, it, PendingIntent.FLAG_UPDATE_CURRENT);
		int interval = (int) (60*1000*time);
		long firstime=System.currentTimeMillis()+interval;
	    AlarmManager am=(AlarmManager)getSystemService(ALARM_SERVICE);
	    am.setRepeating(AlarmManager.RTC_WAKEUP, firstime, interval, pi);
	}
	
	/*
	 * 获取已登陆用户
	 */
	public iSmartUser getIsmartuser() {
		return ismartuser;
	}

	/*
	 * 设置已登陆用户
	 */
	public void setIsmartuser(iSmartUser ismartuser) {
		this.ismartuser = ismartuser;
	}
	

	
	/*
	 * 获取已选中设备控制指令列表
	 */
	public List<Map<String, String>> getDeviceKeys() {
		return deviceKeys;
	}

	/*
	 * 设置已选中设备控制指令列表
	 */
	public void setDeviceKeys(List<Map<String, String>> deviceKeys) {
		this.deviceKeys = deviceKeys;
	}


	/**
	 * 获得当前的房间
	 * @return
	 */
	public Room getCurrentRoom() {
		return currentRoom;
	}
	/**
	 * 设置当前的房间
	 * @return
	 */
	public void setCurrentRoom(Room currentRoom) {
		SharedPreferences spf = getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
		spf.edit().putInt("currentRoom", currentRoom.getId()).commit();
		this.currentRoom = currentRoom;
	}

	private Activity addboxobj;
	
	public void setBoxActivity(Activity obj){
		addboxobj = obj;
	}
	public void finishBoxActivity(){
		addboxobj.finish();
	}

	@Override
	public void onTerminate() {
		// 程序终止的时候执行
		GLog.d(TAG, "onTerminate");
		super.onTerminate();
	}
	/************************************end *****************************************************/

}
