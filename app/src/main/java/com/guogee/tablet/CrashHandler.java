package com.guogee.tablet;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Looper;
import android.util.Log;

import com.example.mt.mediaplay.R;
import com.guogee.tablet.utils.SystemUtil;
import com.guogee.ismartandroid2.remoteControlService.AsyncHttpResponseHandler;
import com.guogee.ismartandroid2.remoteControlService.RemoteDeviceControlService;
import com.guogee.ismartandroid2.utils.GLog;
import com.guogee.ismartandroid2.utils.PublishHelper;
import com.guogee.ismartandroid2.utils.PublishHelper.BuildType;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * UncaughtException处理类,当程序发生Uncaught异常的时候,由该类来接管程序,并记录发送错误报告.
 * 
 * @author Smither 2014/6/25
 * 
 */
public class CrashHandler implements UncaughtExceptionHandler {
	private static final String TAG = "CrashHandler";
	private Thread.UncaughtExceptionHandler mDefaultHandler;// 系统默认的UncaughtException处理类
	private static CrashHandler INSTANCE = new CrashHandler();// CrashHandler实例
	private Context mContext;// 程序的Context对象
	private Map<String, String> info = new HashMap<String, String>();// 用来存储设备信息和异常信息
	private SimpleDateFormat format = new SimpleDateFormat(
			"yyyy-MM-dd-HH-mm-ss");// 用于格式化日期,作为日志文件名的一部分
	private volatile static boolean sended = true;
	private iSmartApplication isapp;

	/** 保证只有一个CrashHandler实例 */
	private CrashHandler() {

	}

	/** 获取CrashHandler实例 ,单例模式 */
	public static CrashHandler getInstance() {
		return INSTANCE;
	}

	/**
	 * 初始化
	 *
	 * @param context
	 */
	public void init(Context context, iSmartApplication isapp) {
		mContext = context;
		this.isapp = isapp;
		mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();// 获取系统默认的UncaughtException处理器
		Thread.setDefaultUncaughtExceptionHandler(this);// 设置该CrashHandler为程序的默认处理器
	}

	/**
	 * 当UncaughtException发生时会转入该重写的方法来处理
	 */
	public void uncaughtException(Thread thread, Throwable ex) {
		GLog.v(TAG, "uncaughtException");
		if (!handleException(ex) && mDefaultHandler != null) {
		// 如果自定义的没有处理则让系统默认的异常处理器来处理
			mDefaultHandler.uncaughtException(thread, ex);
			} else {
				try {
					while (!sended){
						Thread.sleep(200);// 如果处理了，让程序继续运行3秒再退出，保证文件保存并上传到服务器
						GLog.i(TAG, "sending...");
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// 退出程序
				android.os.Process.killProcess(android.os.Process.myPid());
				System.exit(0); // kill off the crashed app
			}
	}

	private void sendErrorMessage(String sb){

		SharedPreferences spf = mContext.getSharedPreferences("CONFIG", Context.MODE_PRIVATE);
		long lastSend = spf.getLong("lastSendCrashTime", 0);
		long current = System.currentTimeMillis();
		if(current - lastSend < 6000){
			return;
		}
		sended =false;
		Editor edit = spf.edit();
		edit.putLong("lastSendCrashTime", current).commit();

		GLog.i(TAG, "send crash email,last send time:"+ lastSend);

		Map<String, String> params = new HashMap<String, String>();
		params.put("model", android.os.Build.BRAND+android.os.Build.MODEL);
		params.put("sysVersion", android.os.Build.VERSION.SDK);
		
		String username = spf.getString("login_name", "");
		if (username == "" || username == null) {
			username = "default";
		}
		params.put("userName", username);
		params.put("appName",
				isapp.getResources().getString(R.string.app_name));
		params.put("appVersion",
				SystemUtil.getAppVersionName(mContext));
		params.put("error", sb.toString());
		RemoteDeviceControlService.getinstance().uploadErrorMessage(params,
					new AsyncHttpResponseHandler() {

						@Override
						public void onSuccess(int statusCode, String content) {
							// TODO Auto-generated method stub
							GLog.v(TAG, "onSuccess:" + content);
							sended = true;
						}

						@Override
						public void onFailure(Throwable error, String content) {
							// TODO Auto-generated method stub
							GLog.v(TAG, "onFailure:" + content);
							sended = true;
						}
					});
	}

	/**
	 * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.
	 * 
	 * @param ex
	 *            异常信息
	 * @return true:如果处理了该异常信息;否则返回false.
	 */
	public boolean handleException(Throwable ex) {
		if (ex == null)
			return false;
		new Thread() {
			public void run() {
				Looper.prepare();
		//		Toast.makeText(mContext,mContext.getResources().getString(R.string.reboot_tip), 0).show();
				Looper.loop();
			}
		}.start();
		// 收集设备参数信息
		collectDeviceInfo(mContext);
		// 保存日志文件	
		ex.printStackTrace();
		String error = saveCrashInfo2File(ex);
		if (PublishHelper.getBuildType() == BuildType.Test || PublishHelper.getBuildType() == BuildType.Release) {
			sendErrorMessage(error);
		}	
		return true;
	}

	/**
	 * 收集设备参数信息
	 * 
	 * @param context
	 */
	public void collectDeviceInfo(Context context) {
		try {
			PackageManager pm = context.getPackageManager();// 获得包管理器
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(),
					PackageManager.GET_ACTIVITIES);// 得到该应用的信息，即主Activity
			if (pi != null) {
				String versionName = SystemUtil.getAppVersionName(context);
				int versionCode = SystemUtil.getAppVersionCode(context);
				info.put("versionName", versionName);
				info.put("versionCode", ""+versionCode);
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		Field[] fields = Build.class.getDeclaredFields();// 反射机制
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				info.put(field.getName(), field.get("").toString());
				Log.d(TAG, field.getName() + ":" + field.get(""));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	private String saveCrashInfo2File(Throwable ex) {
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<String, String> entry : info.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			sb.append(key + "=" + value + "\r\n");
		}
		Writer writer = new StringWriter();
		PrintWriter pw = new PrintWriter(writer);
		ex.printStackTrace(pw);
		Throwable cause = ex.getCause();
		// 循环着把所有的异常信息写入writer中
		while (cause != null) {
			cause.printStackTrace(pw);
			cause = cause.getCause();
		}
		pw.close();// 记得关闭
		String result = writer.toString();
		sb.append(result);
		return sb.toString();
	}
}
