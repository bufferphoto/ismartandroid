package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

public class CustomModel extends BaseModel {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -3694880627150264907L;
	private int id = -1;
	private String name;
	private int deviceId;
	private int tag = -1;
	private int x = -1;
	private int y = -1;
	private int type = -1;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	public int getTag() {
		return tag;
	}
	public void setTag(int tag) {
		this.tag = tag;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
