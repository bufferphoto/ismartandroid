package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

public class DoorBell extends BaseModel {

    private int id;
    private String serverId;//服务器数据库中字段的唯一id
    private int deviceType;
    private String mac;
    private int fun;
    private String path;
    private String time;
    private String data;// json字符串、保留字段

    public DoorBell() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getMac() {
        return mac;
    }

    public void setFun(int fun) {
        this.fun = fun;
    }

    public int getFun() {
        return fun;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

}
