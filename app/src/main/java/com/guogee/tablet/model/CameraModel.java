package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

public class CameraModel extends BaseModel {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -5437006224439814911L;
	private int id;
	private String cameraId;
	private String loginName;
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	private String loginPwd;
	private String reserve;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCameraID() {
		return cameraId;
	}
	public void setCameraID(String cameraID) {
		this.cameraId = cameraID;
	}
	public String getLoginPwd() {
		return loginPwd;
	}
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}
	public String getReserve() {
		return reserve;
	}
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
}
