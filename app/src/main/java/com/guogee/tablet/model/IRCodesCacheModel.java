package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

public class IRCodesCacheModel extends BaseModel {
		public int TestStatu = -1;
		public int DownloadStatu = -1;
		private int CodeID = -1;
		private int ModelID = -1;
		private String DisplayName;
		private int KeyName = -1;
		private String CodeData;
		private int Enable = -1;
		private int CodeLen = -1;
		private String DeviceType;
		private String iremoteType;
		private int brandId = -1;
		public int getBrandId() {
			return brandId;
		}
		public void setBrandId(int brandId) {
			this.brandId = brandId;
		}
		public String getIremoteType() {
			return iremoteType;
		}
		public void setIremoteType(String iremoteType) {
			this.iremoteType = iremoteType;
		}
		public int getCodeID() {
			return CodeID;
		}
		public void setCodeID(int codeID) {
			CodeID = codeID;
		}
		public int getModelID() {
			return ModelID;
		}
		public void setModelID(int modelID) {
			ModelID = modelID;
		}
		public String getDisplayName() {
			return DisplayName;
		}
		public void setDisplayName(String displayName) {
			DisplayName = displayName;
		}
		public int getKeyName() {
			return KeyName;
		}
		public void setKeyName(int keyName) {
			KeyName = keyName;
		}
		public String getCodeData() {
			return CodeData;
		}
		public void setCodeData(String codeData) {
			CodeData = codeData;
		}
		public int getEnable() {
			return Enable;
		}
		public void setEnable(int enable) {
			Enable = enable;
		}
		public int getCodeLen() {
			return CodeLen;
		}
		public void setCodeLen(int codeLen) {
			CodeLen = codeLen;
		}
		public String getDeviceType() {
			return DeviceType;
		}
		public void setDeviceType(String deviceType) {
			DeviceType = deviceType;
		}
}
