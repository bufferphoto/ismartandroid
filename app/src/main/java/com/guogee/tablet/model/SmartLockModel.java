/**
 * 
 */
package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

/**
 *@ClassName:     SmartLockModel.java
 * @Description:   TODO
 * @author xieguangwei
 * @date 2016年1月11日
 * 
 */
public class SmartLockModel extends BaseModel
{
	private int id;
	private int deviceId;
	private int userId;
	private String pwd;
	private long lastTime;
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getDeviceId()
	{
		return deviceId;
	}
	public void setDeviceId(int deviceId)
	{
		this.deviceId = deviceId;
	}
	public int getUserId()
	{
		return userId;
	}
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public String getPwd()
	{
		return pwd;
	}
	public void setPwd(String pwd)
	{
		this.pwd = pwd;
	}
	public long getLastTime()
	{
		return lastTime;
	}
	public void setLastTime(long lastTime)
	{
		this.lastTime = lastTime;
	}
	
	
	
}
