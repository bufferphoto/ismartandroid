package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

/**
 * Created by xieguangwei on 16/3/2.
 */
public class SmartLockHistory extends BaseModel {

    private int id;
    private String devMac;
    private String userName;
    private String time;
    private int read;
    private String devName;
    private String room;
    private int type;
    private int openMethod;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevMac() {
        return devMac;
    }

    public void setDevMac(String devMac) {
        this.devMac = devMac;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOpenMethod() {
        return openMethod;
    }

    public void setOpenMethod(int openMethod) {
        this.openMethod = openMethod;
    }
}
