package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

public class IRCodesBrandCacheModel extends BaseModel {
	private int CodeID = -1;
	private String DisplayName;
	private String DeviceType;
	private String language;
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getIremoteType() {
		return iremoteType;
	}
	public void setIremoteType(String iremoteType) {
		this.iremoteType = iremoteType;
	}
	private String iremoteType;
	
	public int getCodeID() {
		return CodeID;
	}
	public void setCodeID(int codeID) {
		CodeID = codeID;
	}
	public String getDisplayName() {
		return DisplayName;
	}
	public void setDisplayName(String displayName) {
		DisplayName = displayName;
	}
	public String getDeviceType() {
		return DeviceType;
	}
	public void setDeviceType(String deviceType) {
		DeviceType = deviceType;
	}
}
