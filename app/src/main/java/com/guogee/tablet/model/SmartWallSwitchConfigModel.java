package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

/**
 * Created by Administrator on 2016/9/13.
 */
public class SmartWallSwitchConfigModel extends BaseModel {

    private int id;
    private int deviceId;
    private int num;
    private int configMode;
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getConfigMode() {
        return configMode;
    }

    public void setConfigMode(int configMode) {
        this.configMode = configMode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
