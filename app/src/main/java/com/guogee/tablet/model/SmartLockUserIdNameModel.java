package com.guogee.tablet.model;

import com.guogee.ismartandroid2.model.BaseModel;

/**
 * Created by Administrator on 2016/7/28.
 */
public class SmartLockUserIdNameModel extends BaseModel {

    private int id;
    private String deviceAddr;
    private int lockUserId;
    private String userName;//ID对应设置的锁名

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceAddr() {
        return deviceAddr;
    }

    public void setDeviceAddr(String devMac) {
        this.deviceAddr = devMac;
    }

    public int getLockUserId() {
        return lockUserId;
    }

    public void setLockUserId(int lockUserId) {
        this.lockUserId = lockUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
