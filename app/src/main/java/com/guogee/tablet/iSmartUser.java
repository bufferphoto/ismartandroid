package com.guogee.tablet;

public class iSmartUser {
	private String LoginID;
	private String Email;
	private String UserName;
	private String Password;
	private String DeviceIDs;

	public String getLoginID() {
		return LoginID;
	}

	public void setLoginID(String loginID) {
		LoginID = loginID;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String UserName) {
		this.UserName = UserName;
	}
	
	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getDeviceIDs() {
		return DeviceIDs;
	}

	public void setDeviceIDs(String deviceIDs) {
		DeviceIDs = deviceIDs;
	}
	
}
