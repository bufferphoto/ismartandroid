package com.p2p.rtdoobell;

import java.io.Serializable;

public class AVIOAudioPacket implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final int HEAD_LENGTH = 12;

	private short head;// 同步头 0x5AA5
	private short length;// 音频数据长度
	private int tsnap;// 时间戳
	private byte format;// 音频编码类型 0 - PCM 1 - G711A
	private byte reservel1;// 保留字段
	private short reservel2;// 保留字段
	private byte[] data;// 数据

	public void setHead(short head) {
		this.head = head;
	}

	public short getHead() {
		return head;
	}

	public void setLength(short length) {
		this.length = length;
	}

	public short getLength() {
		return length;
	}

	public void setTsnap(int tsnap) {
		this.tsnap = tsnap;
	}

	public int getTsnap() {
		return tsnap;
	}

	public void setFormat(byte format) {
		this.format = format;
	}

	public byte getFormat() {
		return format;
	}

	public void setReservel1(byte reservel1) {
		this.reservel1 = reservel1;
	}

	public byte getReservel1() {
		return reservel1;
	}

	public void setReservel2(short reservel2) {
		this.reservel2 = reservel2;
	}

	public short getReservel2() {
		return reservel2;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public byte[] getData() {
		return data;
	}

}
