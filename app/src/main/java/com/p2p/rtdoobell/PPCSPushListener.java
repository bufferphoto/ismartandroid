package com.p2p.rtdoobell;

public interface PPCSPushListener {

	public void pushMsg(AVIOCmdPacket avioCmdPacket);
}
