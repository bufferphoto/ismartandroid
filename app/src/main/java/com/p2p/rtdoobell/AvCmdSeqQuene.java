package com.p2p.rtdoobell;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AvCmdSeqQuene {

    private LinkedList<AvCmdSeq> mAvCmdSeqList;

    public AvCmdSeqQuene() {
        mAvCmdSeqList = new LinkedList<AvCmdSeq>();
    }

    /*
     * 返回执行seq
     */
    public synchronized int regPPCSCmdListener(PPCSCmdListener listener) {
        AvCmdSeq mAvCmdSeq = getAvCmdSeq(listener);
        if (null != mAvCmdSeq) {
            return mAvCmdSeq.getSeq();
        } else {
            mAvCmdSeq = new AvCmdSeq(mAvCmdSeqList.size() + 1, listener);
            mAvCmdSeqList.addLast(mAvCmdSeq);
            return mAvCmdSeq.getSeq();
        }
    }

    public synchronized void unregPPCSCmdListener(PPCSCmdListener listener) {
        AvCmdSeq mAvCmdSeq = getAvCmdSeq(listener);
        if (null != mAvCmdSeq) {
            mAvCmdSeqList.remove(mAvCmdSeq);
        }
    }

    public List<AvCmdSeq> getAvCmdSeqAllList() {
        return mAvCmdSeqList;
    }

    /*
     * 获取命令监听list
     */
    public List<AvCmdSeq> getAvCmdSeqList(int avCmd) {
        List<AvCmdSeq> acAvCmdSeqs = null;
        if (mAvCmdSeqList != null && mAvCmdSeqList.size() > 0) {
            acAvCmdSeqs = new ArrayList<AvCmdSeq>();
            for (int i = 0; i < mAvCmdSeqList.size(); i++) {
                AvCmdSeq avCmdSeq = mAvCmdSeqList.get(i);
                if (null != avCmdSeq.isExistCmd(avCmd)) {
                    acAvCmdSeqs.add(avCmdSeq);
                }
            }
        }
        return acAvCmdSeqs;
    }

    public AvCmdSeq getAvCmdSeq(int seq) {
        if (mAvCmdSeqList != null && mAvCmdSeqList.size() > 0) {
            for (int i = 0; i < mAvCmdSeqList.size(); i++) {
                AvCmdSeq avCmdSeq = mAvCmdSeqList.get(i);
                if (seq == avCmdSeq.getSeq()) {
                    return avCmdSeq;
                }
            }
        }
        return null;
    }

    private AvCmdSeq getAvCmdSeq(PPCSCmdListener listener) {
        if (mAvCmdSeqList != null && mAvCmdSeqList.size() > 0) {
            for (int i = 0; i < mAvCmdSeqList.size(); i++) {
                AvCmdSeq avCmdSeq = mAvCmdSeqList.get(i);
                if (null != listener && listener.equals(avCmdSeq.getListener())) {
                    return avCmdSeq;
                }
            }
        }
        return null;
    }

    public void removeTimeOutCmd() {
        if (mAvCmdSeqList != null && mAvCmdSeqList.size() > 0) {
            for (int i = 0; i < mAvCmdSeqList.size(); i++) {
                AvCmdSeq avCmdSeq = mAvCmdSeqList.get(i);
                avCmdSeq.removeCMDTimeOut();
            }
        }
    }

    public void removeErrorCmd(int errorCode) {
        if (mAvCmdSeqList != null && mAvCmdSeqList.size() > 0) {
            for (int i = 0; i < mAvCmdSeqList.size(); i++) {
                AvCmdSeq avCmdSeq = mAvCmdSeqList.get(i);
                avCmdSeq.removeErrorOut(errorCode);
            }
        }
    }
}
