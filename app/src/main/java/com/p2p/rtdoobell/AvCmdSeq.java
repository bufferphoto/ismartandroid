package com.p2p.rtdoobell;

import com.p2p.pppp_api.PPCS_APIs;
import com.p2p.util.DoorBellUtil;

import java.util.ArrayList;
import java.util.List;

public class AvCmdSeq {

    private int seq; // 区分对象调用
    private PPCSCmdListener cmdListener;// 对象监听
    private List<AVIOCMD> cmdList;

    public class AVIOCMD {
        public int cmdCode;// 发送命令
        public long cmdSendTime;// 命令发送的时间
        public byte[] cmdData;// 发送数据
    }

    public AvCmdSeq(int seq, PPCSCmdListener cmdListener) {
        this.seq = seq;
        this.cmdListener = cmdListener;
        cmdList = new ArrayList<AVIOCMD>();
    }

    public int getSeq() {
        return seq;
    }

    public PPCSCmdListener getListener() {
        return cmdListener;
    }

    public List<AVIOCMD> getAvIoCmds() {
        return cmdList;
    }

    public void addCMD(int cmdCode, byte[] data) {
        long time = System.currentTimeMillis();
        AVIOCMD cmd = isExistCmd(cmdCode, data);
        if (null != cmd) {
            // if (time - cmd.cmdSendTime >= 5000) {
            cmd.cmdSendTime = time;
            cmd.cmdData = data;
            // }
        } else {
            cmd = new AVIOCMD();
            cmd.cmdCode = cmdCode;
            cmd.cmdSendTime = time;
            cmd.cmdData = data;
            cmdList.add(cmd);
        }
    }

    public void addCMD(int cmdCode, byte[] data, long time) {
        AVIOCMD cmd = isExistCmd(cmdCode, data);
        if (null != cmd) {
            // if (time - cmd.cmdSendTime >= 5000) {
            cmd.cmdSendTime = time;
            cmd.cmdData = data;
            // }
        } else {
            cmd = new AVIOCMD();
            cmd.cmdCode = cmdCode;
            cmd.cmdSendTime = time;
            cmd.cmdData = data;
            cmdList.add(cmd);
        }
    }

    public void removeCMD(int cmdCode) {
        AVIOCMD cmd = isExistCmd(cmdCode);
        if (null != cmd) {
            cmdList.remove(cmd);
        }
    }

    public void removeCMD(int cmdCode, byte[] data) {
        AVIOCMD cmd = isExistCmd(cmdCode, data);
        if (null != cmd) {
            cmdList.remove(cmd);
        }
    }

    /*
     * 移除错误，返回指令码
     */
    public List<Integer> removeErrorOut(int errorCode) {
        long time = System.currentTimeMillis();
        List<Integer> cmds = new ArrayList<Integer>();
        for (int i = 0; i < cmdList.size(); i++) {
            AVIOCMD cmd = cmdList.get(i);
//            GLog.i("olife", "    cmd.cmdSendTime:" + cmd.cmdSendTime);
            if (cmd.cmdSendTime > 0 && (time - cmd.cmdSendTime < AVCmdConstant.CMD_DELAY_TIME)) {
                cmdList.remove(cmd);
                if (null != cmdListener) {
                    cmdListener.ppcsCmdCallBcakFail(cmd.cmdCode, errorCode);
                }
                cmds.add(cmd.cmdCode);
            }
        }
        return cmds;
    }

    /*
     * 移除超时指令，返回超时指令码
     */
    public List<Integer> removeCMDTimeOut() {
        long time = System.currentTimeMillis();
        List<Integer> cmds = new ArrayList<Integer>();
        for (int i = 0; i < cmdList.size(); i++) {
            AVIOCMD cmd = cmdList.get(i);
//            GLog.i("olife", "  timeout  cmd.cmdSendTime:" + cmd.cmdSendTime);
            if (cmd.cmdSendTime > 0 && (time - cmd.cmdSendTime >= AVCmdConstant.CMD_DELAY_TIME)) {
                cmdList.remove(cmd);
                if (null != cmdListener) {
                    cmdListener.ppcsCmdCallBcakFail(cmd.cmdCode, PPCS_APIs.ERROR_PPCS_TIME_OUT);
                }
                cmds.add(cmd.cmdCode);
            }
        }
        return cmds;
    }

    public AVIOCMD isExistCmd(int cmdCode, byte[] data) {
        for (int i = 0; i < cmdList.size(); i++) {
            AVIOCMD cmd = cmdList.get(i);
            if (cmd.cmdCode == cmdCode) {
                if (null == data && cmd.cmdData == null) {
                    return cmd;
                }
                if (null != data && null != cmd.cmdData) {
                    if (DoorBellUtil.byteArrayToHexString2(cmd.cmdData).equalsIgnoreCase(DoorBellUtil.byteArrayToHexString2(cmd.cmdData))) {
                        return cmd;
                    }
                }
            }
        }
        return null;
    }

    public AVIOCMD isExistCmd(int cmdCode) {
        for (int i = 0; i < cmdList.size(); i++) {
            AVIOCMD cmd = cmdList.get(i);
            if (cmd.cmdCode == cmdCode) {
                return cmd;
            }
        }
        return null;
    }

}
