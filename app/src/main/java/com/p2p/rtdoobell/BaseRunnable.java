package com.p2p.rtdoobell;

public abstract class BaseRunnable implements Runnable {
    protected boolean isRunning = true;

    public boolean isRunning() {
        return isRunning;
    }

    public void stop() {
        isRunning = false;
    }

}
