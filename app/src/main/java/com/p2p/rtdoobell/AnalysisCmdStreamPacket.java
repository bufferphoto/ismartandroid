package com.p2p.rtdoobell;

import com.guogee.ismartandroid2.utils.GLog;
import com.p2p.util.DoorBellUtil;

public class AnalysisCmdStreamPacket {

    private static final String TAG = AnalysisCmdStreamPacket.class.getSimpleName();

    public static byte[] unpackPacket(AVIOCmdPacket streamPacket) {
        int length = AVIOCmdPacket.HEAD_LENGTH;
        int cmd_length = 0;
        byte[] cmdData = streamPacket.getCmdData();
        if (null != cmdData) {
            length += cmdData.length;
            cmd_length += cmdData.length;
        }
        byte[] data = new byte[length];

        System.arraycopy(
                DoorBellUtil.int2byte(
                        streamPacket.getSyncCode() == 1 ? AVCmdConstant.SYNC_MODEL_QA : AVCmdConstant.SYNC_MODEL_PUSH),
                0, data, 0, 4); // 长度4

        System.arraycopy(DoorBellUtil.short2byte(streamPacket.getCmdCode()), 0, data, 4, 2); // 长度2

        System.arraycopy(DoorBellUtil.int2byte(cmd_length), 0, data, 12, 4); // 长度4

        if (null != cmdData) {
            System.arraycopy(cmdData, 0, data, 16, cmdData.length); // 长度2
        }

        GLog.i(TAG, "unpackPacket data:" + DoorBellUtil.byteArrayToHexString2(data));
        return data;
    }

    public static AVIOCmdPacket assemblePacket(byte[] data) {
        GLog.i(TAG, "assemblePacket data:" + DoorBellUtil.byteArrayToHexString2(data));

        AVIOCmdPacket streamPacket = null;
        if (null != data && data.length >= AVIOCmdPacket.HEAD_LENGTH) {
            streamPacket = new AVIOCmdPacket();
            byte[] syncCode = new byte[4];
            System.arraycopy(data, 0, syncCode, 0, 4); // 长度4
            GLog.i(TAG, "unpackPacket  Util.byte2int(syncCode):" + DoorBellUtil.byte2int(syncCode));
            streamPacket.setSyncCode(DoorBellUtil.byte2int(syncCode) == AVCmdConstant.SYNC_MODEL_QA ? 1 : 2);

            byte[] cmdCode = new byte[2];
            System.arraycopy(data, 4, cmdCode, 0, 2); // 长度2
            streamPacket.setCmdCode(DoorBellUtil.byte2Short(cmdCode));
            GLog.i(TAG, "cmdCode:" + streamPacket.getCmdCode());

            byte[] resevedW1 = new byte[2];
            System.arraycopy(data, 6, resevedW1, 0, 2); // 长度2
            streamPacket.setResevedW1(DoorBellUtil.byte2Short(resevedW1));
            GLog.i(TAG, "resevedW1:" + streamPacket.getResevedW1());

            byte[] resevedW2 = new byte[4];
            System.arraycopy(data, 8, resevedW2, 0, 4); // 长度4
            streamPacket.setResevedW2(DoorBellUtil.byte2int(resevedW2));
            GLog.i(TAG, "getResevedW2:" + streamPacket.getResevedW2());

            byte[] cmdLength = new byte[4];
            System.arraycopy(data, 12, cmdLength, 0, 4); // 长度4
            streamPacket.setCmdLength(DoorBellUtil.byte2int(cmdLength));
            GLog.i(TAG, "getCmdLength:" + streamPacket.getCmdLength());

//            if (streamPacket.getCmdLength() > 0) {
//                byte[] cmdData = new byte[streamPacket.getCmdLength()];
//                System.arraycopy(data, 16, cmdData, 0, cmdData.length); // 长度4
//                streamPacket.setCmdData(cmdData);
//            }

        }
        return streamPacket;
    }

}
