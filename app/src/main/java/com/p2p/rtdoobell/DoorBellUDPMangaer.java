package com.p2p.rtdoobell;

import com.guogee.ismartandroid2.utils.GLog;
import com.p2p.util.DoorBellUtil;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DoorBellUDPMangaer {
	// 发送数据包间隔，单位毫秒
	private final int SendPktInterval = 3000;
	// 发送数据包次数
	private final int SendPktTimes = 5;

	private final static String TAG = DoorBellUDPMangaer.class.getSimpleName();
	private final static int LOCAL_PORT = 5001;
	private ExecutorService mSendService;
	private ExecutorService mRecvFilterService;
	private DatagramSocket mSocket;
	private ReceiverThread mRecvThread;
	private Timer mSendTimer;

	private volatile static DoorBellUDPMangaer mInstance;

	private DoorBellUDPMangaer() {
		GLog.i(TAG, "create RobotUDPManager");

		try {
			mSocket = new DatagramSocket(LOCAL_PORT);
			mSendService = Executors.newSingleThreadExecutor();
			mRecvFilterService = Executors.newSingleThreadExecutor();
			mRecvThread = new ReceiverThread();
			mRecvThread.start();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public static DoorBellUDPMangaer getInstance() {
		if (mInstance == null) {
			synchronized (DoorBellUDPMangaer.class) {
				if (mInstance == null) {
					mInstance = new DoorBellUDPMangaer();
				}
			}
		}
		return mInstance;
	}

	public void sendPacket() {
		if (mSendTimer == null) {
			mSendTimer = new Timer();
			mSendTimer.schedule(new SendTask(), 0, SendPktInterval);
		}
	}

	public void stopSendPacket() {
		if (null != mSendTimer) {
			mSendTimer.cancel();
			mSendTimer = null;
		}
	}

	private class SendTask extends TimerTask {

		@Override
		public void run() {
			mSendService.execute(new PktSender());
		}
	}

	private class PktSender implements Runnable {
		public void run() {
			try {
				byte[] data = new byte[8];
				System.arraycopy(DoorBellUtil.int2byte(AVCmdConstant.SYNC_MODEL_QA), 0, data, 0, 4);
				data[6] = 0x2d;
				data[7] = 0x5a;
				InetAddress dest = InetAddress.getByName("255.255.255.255");
				DatagramPacket pkt = new DatagramPacket(data, data.length, dest, LOCAL_PORT);
				mSocket.send(pkt);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class ReceiveFilter implements Runnable {
		private DatagramPacket mPkt;

		ReceiveFilter(DatagramPacket pkt) {
			mPkt = pkt;
		}

		public void run() {
			String srcIP = mPkt.getAddress().getHostAddress();
			String localIP = mSocket.getLocalAddress().getHostAddress();
			int srcPort = mPkt.getPort();
			// GLog.d(TAG, "receive from " + srcIP + ",local IP " + localIP);
			// GLog.d(TAG, "receive:" + Util.bytesToHexString(mPkt.getData(),
			// mPkt.getLength()));
			// GLog.d(TAG, "revice from host " + srcIP + "port " +
			// mPkt.getPort());
			if (srcIP.equals(localIP))
				return;
			byte[] data = mPkt.getData();
			AVIODevSearchPacket searchPacket = AnalysisDevSearchPacket.assemblePacket(data);
			if (null != searchPacket) {
				if (null != listener) {
					listener.doorbellSearchResult(searchPacket);
				}
			}
		}
	}

	private class ReceiverThread extends Thread {
		byte[] buf = new byte[1024]; // 接收 buf

		@Override
		public void run() {
			while (true) {
				DatagramPacket recvpkt = new DatagramPacket(buf, buf.length);
				try {
					mSocket.receive(recvpkt);

					mRecvFilterService.execute(new ReceiveFilter(recvpkt));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void setDoorbellSearchListener(DoorbellSearchListener listener) {
		this.listener = listener;
	}

	private DoorbellSearchListener listener;

	public interface DoorbellSearchListener {
		public void doorbellSearchResult(AVIODevSearchPacket searchPacket);
	}
}
