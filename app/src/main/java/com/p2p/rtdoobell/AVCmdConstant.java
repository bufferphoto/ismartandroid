package com.p2p.rtdoobell;

public class AVCmdConstant {

	public final static int SYNC_MODEL_QA = 0x55AA55AA;// 同步码 APP<->设备
	// 0x55AA55AA (问答式 )
	public final static int SYNC_MODEL_PUSH = 0x55BB55BB;// 设备->APP
	// 0x55BB55BB(推送式)
	public final static int AUDIO_HEAD = 0x5AA5;//音频头
	public final static int CMD_DELAY_TIME = 10000;//10s
	
	

	public static final byte CHANNEL_PPCS_CMD = 0;// 信令通道
	public static final byte CHANNEL_PPCS_REAL_VIDEO = 1;// 实时视频流
	public static final byte CHANNEL_PPCS_REAL_AUDIO = 2;// 实时音频流
	public static final byte CHANNEL_PPCS_RECORD_VIDEO = 3;// 录制回放视频流
	public static final byte CHANNEL_PPCS_RECORD_AUDIO = 4;// 录制回放音频流

	// 命令字(APP->设备 问答式)
	public static final byte xMP2P_CMD_LOGIN = 1; // 设备登陆
	public static final byte xMP2P_CMD_OPENVIDEO = 2; // 开启视频流
	public static final byte xMP2P_CMD_CLOSEVIDEO = 3; // 关闭视频流
	public static final byte xMP2P_CMD_OPENAUDIO = 4; // 开启音频流
	public static final byte xMP2P_CMD_CLOSEAUDIO = 5; // 关闭音频流
	public static final byte xMP2P_CMD_OPENTALK = 6; // 开启对讲
	public static final byte xMP2P_CMD_CLOSETALK = 7; // 关闭对讲
	public static final byte xMP2P_CMD_DEVINFO = 10; // 获取设备信息
	public static final byte xMP2P_CMD_GET_BATTERY = 11; // 获取电池电量
	public static final byte xMP2P_CMD_SDINFO = 12; // 获取SD卡信息
	public static final byte xMP2P_CMD_DOORBELLINFO = 13; // 获取门铃状态
	public static final byte xMP2P_CMD_RECORDDATE_SEARCH = 14; // 搜索存在录像的日期
	public static final byte xMP2P_CMD_RECORDLIST_SEARCH = 15; // 搜索指定日期的录像
	public static final byte xMP2P_CMD_RECORD_PLAY = 20; // 录像回放
	public static final byte xMP2P_CMD_RECORD_PLAY_CTRL = 21; // 回放控制
	public static final byte xMP2P_CMD_CHANGEPASSWD = 30; // 修改密码

	// 命令字(设备->APP 推送式)
	public static final byte xMP2P_MSG_VOLTAGE = 0; // 电池电压(例:80代表0.8V)锂电池返回电池电量
	public static final byte xMP2P_MSG_LOW_POWER = 1; // 电量低
	public static final byte xMP2P_MSG_RECORD_PLAYEND = 10;// 回放结束

	// 视频编码格式
	public static final byte VIDEO_CODEC_H264 = 1;
	public static final byte VIDEO_CODEC_MJPEG = 2;
	public static final byte VIDEO_CODEC_H265 = 3;

	// 音频编码格式
	public static final byte AUDIO_CODEC_PCM = 1;
	public static final byte AUDIO_CODEC_G711A = 2;
	public static final byte AUDIO_CODEC_G711U = 3;
	public static final byte AUDIO_CODEC_AAC = 4;
	public static final byte AUDIO_CODEC_OPUS = 5;

	// 录像回放控制类型
	public static final byte RECORD_CTRL_CONTINUE = 0; // 继续回放
	public static final byte RECORD_CTRL_PAUSE = 1;// 暂停回放
	public static final byte RECORD_CTRL_STOP = 2;// 停止回放

}
