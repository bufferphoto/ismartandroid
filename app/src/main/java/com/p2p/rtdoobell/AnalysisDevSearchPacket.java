package com.p2p.rtdoobell;

import com.guogee.ismartandroid2.utils.GLog;
import com.p2p.util.DoorBellUtil;

import java.io.UnsupportedEncodingException;

public class AnalysisDevSearchPacket {

	private static final String TAG = AnalysisDevSearchPacket.class.getSimpleName();

	public static AVIODevSearchPacket assemblePacket(byte[] data) {
		// GLog.i(TAG, "unpackPacket data:" + Util.byteArrayToHexString2(data));

		AVIODevSearchPacket streamPacket = null;
		if (null != data && data.length >= AVIOCmdPacket.HEAD_LENGTH) {
			streamPacket = new AVIODevSearchPacket();
			byte[] sign = new byte[4];
			System.arraycopy(data, 0, sign, 0, 4); // 长度4
			streamPacket.sign = DoorBellUtil.byte2int(sign);
			// GLog.i(TAG, "unpackPacket sign:" + streamPacket.sign);
			if (streamPacket.sign != AVCmdConstant.SYNC_MODEL_QA) {
				GLog.i(TAG, "error data");
				return null;
			}

			byte[] len = new byte[2];
			System.arraycopy(data, 4, len, 0, 2); // 长度2
			streamPacket.len = DoorBellUtil.byte2Short(len);
			// GLog.i(TAG, "len:" + streamPacket.len);

			if (streamPacket.len >= 84) {
				try {
					byte[] cmdData = new byte[streamPacket.len];
					System.arraycopy(data, 8, cmdData, 0, cmdData.length);
					byte[] uuid = new byte[32];
					System.arraycopy(cmdData, 0, uuid, 0, uuid.length);
					streamPacket.devInfo.uuid = new String(uuid, "UTF-8");
					streamPacket.devInfo.uuid = streamPacket.devInfo.uuid.replace("\0", "");
					// GLog.i(TAG, "uuid:" + streamPacket.devInfo.uuid);
					byte[] ip = new byte[16];
					System.arraycopy(cmdData, 32, ip, 0, ip.length);
					streamPacket.devInfo.ip = new String(ip, "UTF-8");
					streamPacket.devInfo.ip = streamPacket.devInfo.ip.replace("\0", "");
					// GLog.i(TAG, "ip:" + streamPacket.devInfo.ip);
					byte[] devmod = new byte[16];
					System.arraycopy(cmdData, 48, devmod, 0, devmod.length);
					streamPacket.devInfo.devMod = new String(devmod, "UTF-8");
					streamPacket.devInfo.devMod = streamPacket.devInfo.devMod.replace("\0", "");
					// GLog.i(TAG, "devMod:" + streamPacket.devInfo.devMod);
					byte[] sofrver = new byte[16];
					System.arraycopy(cmdData, 64, sofrver, 0, sofrver.length);
					streamPacket.devInfo.sofrVer = new String(sofrver, "UTF-8");
					streamPacket.devInfo.sofrVer = streamPacket.devInfo.sofrVer.replace("\0", "");
					// GLog.i(TAG, "sofrVer:" + streamPacket.devInfo.sofrVer);
					byte[] doornum = new byte[4];
					System.arraycopy(cmdData, 80, doornum, 0, doornum.length);
					streamPacket.devInfo.doornum = DoorBellUtil.byte2int(doornum);
					// GLog.i(TAG, "doornum:" + streamPacket.devInfo.doornum);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}

		}
		return streamPacket;
	}

}
