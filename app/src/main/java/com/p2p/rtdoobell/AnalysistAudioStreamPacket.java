package com.p2p.rtdoobell;

import com.guogee.ismartandroid2.utils.GLog;
import com.p2p.util.DoorBellUtil;

public class AnalysistAudioStreamPacket {

	private static final String TAG = AnalysistAudioStreamPacket.class.getSimpleName();

	/*
	 * 加上包头
	 */
	public static byte[] assemblePacket(byte[] g711_data) {
		if (null != g711_data) {
			short length = (short) g711_data.length;
			byte[] packet = new byte[length + AVIOAudioPacket.HEAD_LENGTH];
			short head = AVCmdConstant.AUDIO_HEAD;
			// short head = 0x5BB5;
			// int time = (int) (System.currentTimeMillis() / 1000);
			System.arraycopy(DoorBellUtil.short2byte(head), 0, packet, 0, 2);
			System.arraycopy(DoorBellUtil.short2byte(length), 0, packet, 2, 2);
			// System.arraycopy(Util.int2byte(time), 0, packet, 4, 4);
			packet[8] = 1;
			System.arraycopy(g711_data, 0, packet, 12, g711_data.length);

			return packet;
		}
		return null;
	}

	public static AVIOAudioPacket unpackPacket(byte[] data) {
		// GLog.i(TAG, "unpackPacket data:" + Util.byteArrayToHexString2(data));
		AVIOAudioPacket streamPacket = null;
		if (null != data && data.length == AVIOAudioPacket.HEAD_LENGTH) {
			streamPacket = new AVIOAudioPacket();
			byte[] head = new byte[2];
			System.arraycopy(data, 0, head, 0, 2); // 长度2
			// GLog.i(TAG, "head data:" + Util.byteArrayToHexString2(head));
			streamPacket.setHead(DoorBellUtil.byte2Short(head));
			// GLog.i(TAG, "head:" + streamPacket.getHead());
			if (streamPacket.getHead() != AVCmdConstant.AUDIO_HEAD) {
				GLog.i(TAG, "error data");
				return null;
			}

			byte[] length = new byte[2];
			System.arraycopy(data, 2, length, 0, 2); // 长度2
			streamPacket.setLength(DoorBellUtil.byte2Short(length));
			// GLog.i(TAG, "Length:" + streamPacket.getLength());

			byte[] tsnap = new byte[4];
			System.arraycopy(data, 4, tsnap, 0, 4); // 长度4
			streamPacket.setTsnap(DoorBellUtil.byte2int(tsnap));
			// GLog.i(TAG, "tsnap:" + streamPacket.getTsnap());

			streamPacket.setFormat(data[8]);
			// GLog.i(TAG, "Format:" + (streamPacket.getFormat()));
			streamPacket.setReservel1(data[9]);

			byte[] reservel2 = new byte[2];
			System.arraycopy(data, 10, reservel2, 0, 2); // 长度2
			streamPacket.setReservel2(DoorBellUtil.byte2Short(reservel2));

			// GLog.i(TAG, "resver2:" + streamPacket.getReservel2());
		}
		return streamPacket;
	}
}
