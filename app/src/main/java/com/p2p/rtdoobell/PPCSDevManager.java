package com.p2p.rtdoobell;

import android.content.Context;

import com.alibaba.fastjson.JSONObject;
import com.guogee.ismartandroid2.utils.GLog;
import com.p2p.pppp_api.PPCS_APIs;
import com.p2p.pppp_api.st_PPCS_Session;
import com.p2p.util.DoorBellUtil;
import com.xmitech.sdk.H264Frame;
import com.xmitech.sdk.XmMovieViewController;
import com.xmitech.sdk.interfaces.AVFilterListener;
import com.yuv.display.GLFrameSurface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PPCSDevManager implements AVFilterListener, PPCSCmdListener {

    private static final String TAG = PPCSDevManager.class.getSimpleName();
    private volatile static PPCSDevManager mInstance;

    public static final int MAX_SIZE_BUF = 131075; // 128*1024;
    public static final int SIZE_BUF = 1024;
    public static final int MAX_FRAMEBUF = 1600000;

    private int m_handleSession = -1;
    private String mDevUID = "";

    //    private PpcsCmdSendThread mPpcsCmdSendThread = null;
    private PpcsCmdRecvThread mPpcsCmdRecvThread = null;// 接收通道0命令的返回数据
    private VideoRecvThread mVideoRecvThread = null;
    private AudioRecvThread mAudioRecvThread = null;
    private VideoRecvThread mVideoRecordRecvThread = null;// 录像视频
    private AudioRecvThread mAudioRecordRecvThread = null;// 录像音频
    private ExecutorService mSendService = Executors.newFixedThreadPool(4);
    private ExecutorService mSendCmdService = Executors.newSingleThreadExecutor();

    private AvCmdSeqQuene avCmdSeqQuene = null;
    //    private AvCmdSeq avCmdAll;//记录所有命令
    private List<PPCSPushListener> ppcsPushListenerList;

    private XmMovieViewController mController;
    private Map<Context, XmMovieViewController> mapControl = new HashMap<>();

    private PPCSDevManager() {
        ppcsPushListenerList = new ArrayList<>();
        avCmdSeqQuene = new AvCmdSeqQuene();
//        avCmdAll = new AvCmdSeq(0, this);
    }

    public static PPCSDevManager getInstance() {
        if (mInstance == null) {
            synchronized (PPCSDevManager.class) {
                mInstance = new PPCSDevManager();
            }
        }
        return mInstance;
    }

    public int regPPCSCmdListener(PPCSCmdListener listener) {
        return avCmdSeqQuene.regPPCSCmdListener(listener);
    }

    public void unregPPCSCmdListener(PPCSCmdListener listener) {
        avCmdSeqQuene.unregPPCSCmdListener(listener);
    }

    public void regPPCSPushListener(PPCSPushListener ppcsPushListener) {
        ppcsPushListenerList.add(ppcsPushListener);
    }

    public void unregPPCSPushListener(PPCSPushListener ppcsPushListener) {
        if (ppcsPushListenerList.contains(ppcsPushListener))
            ppcsPushListenerList.remove(ppcsPushListener);
    }

    /*
     * 设置音视频控制view
     */
    public void setXmMovieViewController(Context context, GLFrameSurface glSurfaceView) {
        if (null == mapControl.get(context)) {
            XmMovieViewController mController = new XmMovieViewController(this, glSurfaceView, context);
            mController.initAudioRecordAndTrack();
            mapControl.put(context, mController);
        }
        mController = mapControl.get(context);
        GLog.i(TAG, "  mController:" + mController);
    }

    public String getAPIVersion() {
        int n = PPCS_APIs.PPCS_GetAPIVersion();
        String apiver = String.format("%d.%d.%d.%d", (n >> 24) & 0xff, (n >> 16) & 0xff, (n >> 8) & 0xff, n & 0xff);
        return apiver;
    }

    public int initPPCS() {
        String strPara = "EBGAEIBIKHJJGFJKEOGCFAEPHPMAHONDGJFPBKCPAJJMLFKBDBAGCJPBGOLKIKLKAJMJKFDOOFMOBECEJIMM";
        return PPCS_APIs.PPCS_Initialize(strPara.getBytes());
    }

    public int deInitPPCS() {
        if (null != mapControl) {
            if (null != mapControl.values() && mapControl.values().size() > 0) {
                for (XmMovieViewController xmMovieViewController : mapControl.values()) {
                    GLog.i(TAG, "  xmMovieViewController:" + xmMovieViewController);
                    if (null != xmMovieViewController) {
                        xmMovieViewController.releaseAudio();
                        xmMovieViewController = null;
                    }
                }
            }
            mapControl.clear();
        }

        avCmdSeqQuene.getAvCmdSeqAllList().clear();
//        avCmdAll.getAvIoCmds().clear();

        return PPCS_APIs.PPCS_DeInitialize();
    }

    public int connectDev(String devUID) {
        if (devUID == null || devUID.length() == 0) {
            return PPCS_APIs.ER_ANDROID_NULL;
        }

        this.mDevUID = devUID;

        if (m_handleSession < 0) {
            m_handleSession = PPCS_APIs.PPCS_Connect(mDevUID, (byte) 1, 0);
            if (m_handleSession < 0) {
                return m_handleSession;
            }
        }

        if (null != mPpcsCmdRecvThread && mPpcsCmdRecvThread.isRunning()) {
            mPpcsCmdRecvThread.stop();
            mPpcsCmdRecvThread = null;
        }

//        if (null != mPpcsCmdSendThread) {
//            mPpcsCmdSendThread.stop();
//            mPpcsCmdSendThread = null;
//        }
//        mPpcsCmdSendThread = new PpcsCmdSendThread();
//        mSendCmdService.execute(mPpcsCmdSendThread);

        if (null != mPpcsCmdRecvThread)
            mPpcsCmdRecvThread.stop();
        mPpcsCmdRecvThread = null;

        mPpcsCmdRecvThread = new PpcsCmdRecvThread();
        mSendService.execute(mPpcsCmdRecvThread);

        return PPCS_APIs.ERROR_PPCS_SUCCESSFUL;
    }

    public boolean isConnected() {
        return (m_handleSession >= 0);
    }

    public int checkPPCS(st_PPCS_Session SInfo) {
        if (null == SInfo) {
            SInfo = new st_PPCS_Session();
        }
        int ret = PPCS_APIs.PPCS_Check(m_handleSession, SInfo);
        if (ret == PPCS_APIs.ERROR_PPCS_SUCCESSFUL) {
            String str;
            str = String.format("  ----Session Ready: -%s----", (SInfo.getMode() == 0) ? "P2P" : "RLY");
            System.out.println(str);
            str = String.format("  Socket: %d", SInfo.getSkt());
            System.out.println(str);
            str = String.format("  Remote Addr: %s:%d", SInfo.getRemoteIP(), SInfo.getRemotePort());
            System.out.println(str);
            str = String.format("  My Lan Addr: %s:%d", SInfo.getMyLocalIP(), SInfo.getMyLocalPort());
            System.out.println(str);
            str = String.format("  My Wan Addr: %s:%d", SInfo.getMyWanIP(), SInfo.getMyWanPort());
            System.out.println(str);
            str = String.format("  Connection time: %d", SInfo.getConnectTime());
            System.out.println(str);
            str = String.format("  DID: %s", SInfo.getDID());
            System.out.println(str);
            str = String.format("  I am : %s", (SInfo.getCorD() == 0) ? "Client" : "Device");
            System.out.println(str);
        }
        GLog.i(TAG, "ret:" + ret);
        return ret;
    }

    public int disConnectDev(int seq) {
        int nRet = PPCS_APIs.ER_ANDROID_NULL;

        if (m_handleSession >= 0) {

            closeTalk(seq);
            closeAudio(seq);
            closeVideo(seq);

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

//            if (null != mPpcsCmdSendThread)
//                mPpcsCmdSendThread.stop();
//            mPpcsCmdSendThread = null;
            // 停止接收0通道数据线程
            if (null != mPpcsCmdRecvThread)
                mPpcsCmdRecvThread.stop();
            mPpcsCmdRecvThread = null;

            nRet = PPCS_APIs.PPCS_Close(m_handleSession);
            m_handleSession = -1;
        }

        return nRet;
    }

    /*
     * 打开视频
     */
    public int startVideo(int seq) {
        if (null != mVideoRecvThread && mVideoRecvThread.isRunning()) {
            mVideoRecvThread.stop();
            mVideoRecvThread = null;
        }

        int ret = startVideoStreamCmd(seq);
        if (ret >= 0) {
            mVideoRecvThread = new VideoRecvThread(AVCmdConstant.CHANNEL_PPCS_REAL_VIDEO);
            mSendService.execute(mVideoRecvThread);
            if (null != mController) {
                mController.initAudioRecordAndTrack();
                mController.play();
            }
        }
        return ret;
    }

    /*
     * 关闭视频
     */
    public int closeVideo(int seq) {
        if (null != mVideoRecvThread)
            mVideoRecvThread.stop();
        mVideoRecvThread = null;

        int ret = closeVideoStreamCmd(seq);
        if (ret >= 0) {
            if (null != mController) {
                mController.stop();
//                mController.releaseAudio();
            }
        }
        return ret;
    }

    /*
     * 打开音频
     */
    public int startAudio(int seq) {
        if (null != mAudioRecvThread && mAudioRecvThread.isRunning()) {
            mAudioRecvThread.stop();
            mAudioRecvThread = null;
        }

        int ret = startAudioStreamCmd(seq);
        if (ret >= 0) {
            mAudioRecvThread = new AudioRecvThread(AVCmdConstant.CHANNEL_PPCS_REAL_AUDIO);
            mSendService.execute(mAudioRecvThread);
            if (null != mController && !mController.isEnableAudio()) {
                mController.enableAudio(true);
            }
        }
        return ret;
    }

    /*
     * 关闭音频
     */
    public int closeAudio(int seq) {
        if (null != mAudioRecvThread)
            mAudioRecvThread.stop();
        mAudioRecvThread = null;

        int ret = closeAudioStreamCmd(seq);
        if (ret >= 0) {
            if (null != mController && mController.isEnableAudio()) {
                mController.enableAudio(false);
            }
        }
        return ret;
    }

    /*
     * 对讲
     */
    public int startTalk(int seq) {
        int ret = startRecordStreamCmd(seq);
        if (ret >= 0) {
            if (null != mController) {
                GLog.i(TAG, " isAECAailable:" + mController.isAECAailable());
                GLog.i(TAG, " isNSAvailable:" + mController.isNSAvailable());
                GLog.i(TAG, " isTalkback:" + mController.isTalkback());
                mController.talkback(true);
            }
        }
        return ret;
    }

    /*
     * 关闭对讲
     */
    public int closeTalk(int seq) {
        int ret = closeRecordStreamCmd(seq);
        if (ret >= 0) {
            if (null != mController && mController.isTalkback()) {
                mController.talkback(false);
            }
        }
        return ret;
    }

    /*
     * fileName:文件路径+名字.png
     */
    public boolean screenShotImage(String fileName) {
        if (null != mController)
            return mController.getBitmap(fileName);
        return false;
    }

    /*
     * 录制视频 fileName:文件路径+名字.mp4
     *
     * @param Width 录制的视频的分辨率宽度
     *
     * @param Height 录制的视频的分辨率高度
     *
     * @param Rate 录制的视频的帧率
     */
    public void startRecordToMP4(String fileName, int width, int height, int Rate) {
        if (null != mController)
            mController.startRecordToMP4(fileName, width, height, Rate);
    }

    /*
     * 停止录制
     */
    public void stopRecordToMP4() {
        if (null != mController)
            mController.stopRecordToMP4();
    }

    /*
     * 打开录像视频
     */
    public int playRecordVideo(int seq, String date, String filename) {
        if (null != mVideoRecordRecvThread && mVideoRecordRecvThread.isRunning())
            mVideoRecordRecvThread.stop();
        mVideoRecordRecvThread = null;

        if (null != mAudioRecordRecvThread && mAudioRecordRecvThread.isRunning())
            mAudioRecordRecvThread.stop();
        mAudioRecordRecvThread = null;

        int ret = recordPlayCmd(seq, date, filename);
        if (ret >= 0) {
            mVideoRecordRecvThread = new VideoRecvThread(AVCmdConstant.CHANNEL_PPCS_RECORD_VIDEO);
            mSendService.execute(mVideoRecordRecvThread);

            if (null != mController) {
                mController.initAudioRecordAndTrack();
                mController.play();
            }

            mAudioRecordRecvThread = new AudioRecvThread(AVCmdConstant.CHANNEL_PPCS_RECORD_AUDIO);
            mSendService.execute(mAudioRecordRecvThread);
            if (null != mController && !mController.isEnableAudio()) {
                mController.enableAudio(true);
            }
        }
        return ret;
    }

    public int ctlRecordVideo(int seq, int token, int ctrlcmd) {
        if (ctrlcmd == 2) {
            return closeRecordVideo(seq, token, ctrlcmd);
        } else {
            return recordPlayCtlCmd(seq, token, ctrlcmd);
        }
    }

    /*
     * 关闭录像视频
     */
    private int closeRecordVideo(int seq, int token, int ctrlcmd) {
        int ret = recordPlayCtlCmd(seq, token, ctrlcmd);
        if (ret >= 0) {

            if (null != mVideoRecordRecvThread)
                mVideoRecordRecvThread.stop();
            mVideoRecordRecvThread = null;

            if (null != mAudioRecordRecvThread)
                mAudioRecordRecvThread.stop();
            mAudioRecordRecvThread = null;

            if (null != mController && mController.isEnableAudio()) {
                mController.enableAudio(false);
            }

            if (null != mController) {
                mController.stop();
//                mController.releaseAudio();
            }
        }
        return ret;
    }

    /*
     * 设备登录
     */
    public int loginDevice(int seq, String userName, String userPwd) {
        int random = 123456;
        String auth = DoorBellUtil.stringToMD5(userName + ":" + random + ":" + userPwd);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", userName);
        jsonObject.put("random", random);
        jsonObject.put("auth", auth);

        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_LOGIN, jsonObject.toJSONString().getBytes());
    }

    /*
     * 修改设备登陆密码
     */
    public int resetDevicePwd(int seq, String userName, String userOldPwd, String userNewPwd) {
        int random = 123456;
        String auth = DoorBellUtil.stringToMD5(userName + ":" + random + ":" + userOldPwd);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", userName);
        jsonObject.put("random", random);
        jsonObject.put("auth", auth);
        jsonObject.put("newpass", userNewPwd);

        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_CHANGEPASSWD, jsonObject.toJSONString().getBytes());
    }

    /*
     * 开启视频流
     */
    private int startVideoStreamCmd(int seq) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_OPENVIDEO, jsonObject.toJSONString().getBytes());
    }

    /*
     * 关闭视频流
     */
    private int closeVideoStreamCmd(int seq) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_CLOSEVIDEO, jsonObject.toJSONString().getBytes());
    }

    /*
     * 开启音频流
     */
    private int startAudioStreamCmd(int seq) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_OPENAUDIO, jsonObject.toJSONString().getBytes());
    }

    /*
     * 关闭音频流
     */
    private int closeAudioStreamCmd(int seq) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_CLOSEAUDIO, jsonObject.toJSONString().getBytes());
    }

    /*
     * 开启对讲
     */
    private int startRecordStreamCmd(int seq) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_OPENTALK, jsonObject.toJSONString().getBytes());
    }

    /*
     * 关闭对讲
     */
    private int closeRecordStreamCmd(int seq) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_CLOSETALK, jsonObject.toJSONString().getBytes());
    }

    /*
     * 获取设备信息
     */
    public int getDeviceInfo(int seq) {
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_DEVINFO, null);
    }

    /*
     * 获取电池电量(命令发送成功后由设备主动推送电量信息)
     */
    public int getBatteryCmd(int seq) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_GET_BATTERY, jsonObject.toJSONString().getBytes());
    }

    /*
     * 获取SD卡信息
     */
    public int getSDCardInfo(int seq) {
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_SDINFO, null);
    }

    /*
     * 获取门铃联网状态
     */
    public int getDoorBellInfo(int seq) {
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_DOORBELLINFO, null);
    }

    /*
     * 搜索存在录像的日期
     */
    public int getRecordDateInfo(int seq) {
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_RECORDDATE_SEARCH, null);
    }

    /*
     * 搜索指定日期的录像
     *
     * data -> 日期
     *
     * type -> 0 所有录像 1 全时录像(计划) 2 报警录像
     *
     */
    public int getRecordList(int seq, String date, int type) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        jsonObject.put("date", date);
        jsonObject.put("type", type);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_RECORDLIST_SEARCH, jsonObject.toJSONString().getBytes());
    }

    /*
     * 回放指定录像文件
     *
     * data -> 日期
     *
     * filename -> 文件名
     *
     */
    public int recordPlayCmd(int seq, String date, String filename) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("channel", 0);
        jsonObject.put("dirname", date);
        jsonObject.put("filename", filename);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_RECORD_PLAY, jsonObject.toJSONString().getBytes());
    }

    /*
     * 控制录像回放状态
     *
     * token -> 控制口令
     *
     * ctrlcmd -> 控制命令字
     * RECORD_CTRL_CONTINUE=0,			//继续回放
        RECORD_CTRL_PAUSE,				//暂停回放
        RECORD_CTRL_STOP				//停止回放
     *
     */
    public int recordPlayCtlCmd(int seq, int token, int ctrlcmd) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", token);
        jsonObject.put("ctrlcmd", ctrlcmd);
        return sendIOWriteCtrl(seq, 1, AVCmdConstant.xMP2P_CMD_RECORD_PLAY_CTRL, jsonObject.toJSONString().getBytes());
    }

    public int writeRecord(byte[] data) {
        return PPCS_APIs.PPCS_Write(m_handleSession, AVCmdConstant.CHANNEL_PPCS_REAL_AUDIO, data, data.length);
    }

    /*
     *
     * seq -> 区别发送实现回调的对象
     *
     * syncCode -> 1:APP<->设备 0x55AA55AA (问答式) 2: 设备->APP 0x55BB55BB (推送式)
     *
     * cmdCode ->
     *
     * cmdData ->json数据转换成的byte
     *
     */
    private int sendIOWriteCtrl(int seq, int syncCode, short cmdCode, byte[] cmdData) {

        AVIOCmdPacket streamPacket = new AVIOCmdPacket(syncCode, cmdCode, cmdData);

        byte[] data = AnalysisCmdStreamPacket.unpackPacket(streamPacket);

        AvCmdSeq avCmdSeq = avCmdSeqQuene.getAvCmdSeq(seq);
        if (null != avCmdSeq)
            avCmdSeq.addCMD(cmdCode, data, System.currentTimeMillis());

        return PPCS_APIs.PPCS_Write(m_handleSession, AVCmdConstant.CHANNEL_PPCS_CMD, data, data.length);
//        avCmdAll.addCMD(cmdCode, data, 0);
//        return 0;
    }

    private boolean isSendCmd = true;
    private Lock lock = new ReentrantLock();

    @Override
    public void ppcsCmdCallBcakSuccess(AVIOCmdPacket packet) {
        GLog.i(TAG, "  ---ppcsCmdCallBcakSuccess---  " + packet.getCmdCode());
        isSendCmd = true;
    }

    @Override
    public void ppcsCmdCallBcakFail(int cmdCode, int errorRet) {
        GLog.i(TAG, "  ---ppcsCmdCallBcakFail---  " + cmdCode);
        isSendCmd = true;
    }

//    private class PpcsCmdSendThread extends BaseRunnable {
//
//        public PpcsCmdSendThread() {
//            isSendCmd = true;
//        }
//
//        @Override
//        public void run() {
//            while (isRunning) {
//                GLog.i(TAG, " isSendCmd:" + isSendCmd);
//                if (isSendCmd) {
//                    List<AvCmdSeq.AVIOCMD> cmdList = avCmdAll.getAvIoCmds();
//                    if (null != cmdList && cmdList.size() > 0) {
//                        GLog.i(TAG, "  --- cmdList.size(): " + cmdList.size());
//                        AvCmdSeq.AVIOCMD cmd = cmdList.get(0);
//                        cmd.cmdSendTime = System.currentTimeMillis();
//
//                        List<AvCmdSeq> acCmdSeqList = avCmdSeqQuene.getAvCmdSeqList(cmd.cmdCode);
//                        if (null != acCmdSeqList) {
//                            for (AvCmdSeq acCmdSeq : acCmdSeqList) {
//                                AvCmdSeq.AVIOCMD avCmd = acCmdSeq.isExistCmd(cmd.cmdCode, cmd.cmdData);
//                                if (null != avCmd) {
//                                    avCmd.cmdSendTime = cmd.cmdSendTime;
//                                }
//                            }
//                        }
//
//                        isSendCmd = false;
//                        GLog.i(TAG, "send data:" + DoorBellUtil.byteArrayToHexString2(cmd.cmdData));
//                        PPCS_APIs.PPCS_Write(m_handleSession, AVCmdConstant.CHANNEL_PPCS_CMD, cmd.cmdData, cmd.cmdData.length);
//                        GLog.i(TAG, "  send cmd:" + cmd.cmdCode + " isSendCmd:" + isSendCmd);
//                    }
//                }
//
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

    private class PpcsCmdRecvThread extends BaseRunnable {
        private final String TAG = PpcsCmdRecvThread.class.getSimpleName();
        private int nCurStreamIOType = 0;
        private int nRet = 0;
        private byte[] pAVStreamHeadData;
        //        private byte[] pAVStreamData;
        private int[] nRecvSize;
        private AVIOCmdPacket streamPacket = null;

        public PpcsCmdRecvThread() {
            pAVStreamHeadData = new byte[AVIOCmdPacket.HEAD_LENGTH];
//            pAVStreamData = new byte[MAX_SIZE_BUF];
            nRecvSize = new int[1];
        }

        @Override
        public void run() {
            while (isRunning) {
                streamPacket = null;
                nRet = recieveCmdData(nRecvSize);
                if (nRecvSize[0] > 0 && null != streamPacket && null != streamPacket.getCmdData()) {
                    if (streamPacket.getSyncCode() == 1) {
//                        avCmdAll.removeCMD(streamPacket.getCmdCode());
//                        avCmdAll.getListener().ppcsCmdCallBcakSuccess(streamPacket);

                        List<AvCmdSeq> avCmdSeqs = avCmdSeqQuene.getAvCmdSeqList(streamPacket.getCmdCode());
                        if (null != avCmdSeqs) {
                            for (AvCmdSeq avCmdSeq : avCmdSeqs) {
                                avCmdSeq.removeCMD(streamPacket.getCmdCode());
                                avCmdSeq.getListener().ppcsCmdCallBcakSuccess(streamPacket);
                            }
                        }
                    } else {
                        for (PPCSPushListener ppcsPushListener : ppcsPushListenerList) {
                            ppcsPushListener.pushMsg(streamPacket);
                        }
                    }
                } else if (nRet < 0 && nRet != PPCS_APIs.ERROR_PPCS_TIME_OUT) {
                    // 通知
//                    avCmdAll.removeErrorOut(nRet);
                    avCmdSeqQuene.removeErrorCmd(nRet);

                    if (nRet == PPCS_APIs.ERROR_PPCS_SESSION_CLOSED_TIMEOUT) {
//                        GLog.i(TAG, "PpcsCmdRecvThread recv" + "ThreadRecvIOCtrl: Session TimeOUT!");
                    } else if (nRet == PPCS_APIs.ERROR_PPCS_SESSION_CLOSED_REMOTE) {
//                        GLog.i(TAG, "PpcsCmdRecvThread recv" + "ThreadRecvIOCtrl: Session Remote Close!");
                    } else if (nRet == PPCS_APIs.ERROR_PPCS_SESSION_CLOSED_CALLED) {
//                        GLog.i(TAG, "PpcsCmdRecvThread recv" + "ThreadRecvIOCtrl: myself called PPCS_Close!");
                    }
                } else {
//                    avCmdAll.removeCMDTimeOut();
                    avCmdSeqQuene.removeTimeOutCmd();
                }
            }
        }


        private int recieveCmdData(int[] nRecvSize) {
            nRecvSize[0] = AVIOCmdPacket.HEAD_LENGTH;
            nRet = PPCS_APIs.PPCS_Read(m_handleSession, AVCmdConstant.CHANNEL_PPCS_CMD, pAVStreamHeadData, nRecvSize,
                    5000);
//            GLog.i(TAG, "nRet:" + nRet + " nRecvSize[0]:" + nRecvSize[0]);
            if (nRecvSize[0] > 0) {
                streamPacket = AnalysisCmdStreamPacket.assemblePacket(pAVStreamHeadData);
                if (null != streamPacket) {
                    nRecvSize[0] = streamPacket.getCmdLength();
                    if (nRecvSize[0] > 0) {
                        byte[] pAVStreamData = new byte[nRecvSize[0]];
                        nRet = PPCS_APIs.PPCS_Read(m_handleSession, AVCmdConstant.CHANNEL_PPCS_CMD, pAVStreamData, nRecvSize,
                                5000);
//                        GLog.i(TAG, "data nRet:" + nRet + " nRecvSize[0]:" + nRecvSize[0]);
                        if (nRecvSize[0] > 0) {
                            streamPacket.setCmdData(pAVStreamData);
                        }
                    }
                }
            }
            GLog.i(TAG, "nRet:" + nRet + " nRecvSize[0]:" + nRecvSize[0]);
            return nRet;
        }

    }

    private class VideoRecvThread extends BaseRunnable {
        private final String TAG = VideoRecvThread.class.getSimpleName();
        private int nCurStreamIOType = 0;
        private int nRet = 0;
        private byte[] pAVStreamHeadData;
        private byte[] pAVStreamData;
        private int[] nRecvSize;
        private byte[] imageFrame;
        private int frameSize = 0;
        private byte videoType = AVCmdConstant.CHANNEL_PPCS_RECORD_VIDEO;

        public VideoRecvThread(byte videoType) {
            imageFrame = new byte[MAX_SIZE_BUF];
            nRecvSize = new int[1];
            pAVStreamHeadData = new byte[AVIOVideoPacket.HEAD_LENGTH];
            pAVStreamData = new byte[SIZE_BUF];
            this.videoType = videoType;
        }

        @Override
        public void run() {
            while (isRunning) {
                nRecvSize[0] = AVIOVideoPacket.HEAD_LENGTH;
                nRet = PPCS_APIs.PPCS_Read(m_handleSession, videoType, pAVStreamHeadData, nRecvSize, 5000);
                // GLog.i(TAG, " recv" + " nRet:" + nRet + " nRecvSize:" +
                // nRecvSize[0]);
                if (nRecvSize[0] > 0) {
                    AVIOVideoPacket avioVideoPacket = AnalysistVideoStreamPacket.unpackPacket(pAVStreamHeadData);
                    if (null != avioVideoPacket) {
                        nRecvSize[0] = avioVideoPacket.getDataSize();
                        nRet = PPCS_APIs.PPCS_Read(m_handleSession, videoType, pAVStreamData, nRecvSize, 5000);
                        // GLog.i(TAG,
                        // "nRet:" + nRet + " length:" + pAVStreamData.length +
                        // " nRecvSize[0]:" + nRecvSize[0]);
                        if (nRecvSize[0] > 0) {
                            byte[] data = new byte[nRecvSize[0]];
                            System.arraycopy(pAVStreamData, 0, data, 0, data.length);
                            avioVideoPacket.setData(data);
                            // GLog.i(TAG, " aCurPackage:" +
                            // avioVideoPacket.getCurPackage() + " total:"
                            // + avioVideoPacket.getPackageCnt());
                            if (avioVideoPacket.getCurPackage() == avioVideoPacket.getPackageCnt()) {
                                System.arraycopy(avioVideoPacket.getData(), 0, imageFrame, frameSize,
                                        avioVideoPacket.getData().length);
                                frameSize += avioVideoPacket.getDataSize();
                                if (null != mController) {
                                    byte[] imagFile = new byte[frameSize];
                                    System.arraycopy(imageFrame, 0, imagFile, 0, frameSize);
                                    H264Frame frame = new H264Frame();
                                    frame.h264 = imagFile;
                                    frame.frameType = avioVideoPacket.getFType();
                                    // GLog.i(TAG, " one image " + " nRecvSize:"
                                    // + imagFile.length);
                                    mController.InputMediaH264(frame);
                                }
                                // myVideoData(avioVideoPacket, imageFrame,
                                // frameSize);
                                frameSize = 0;
                            } else {
                                System.arraycopy(avioVideoPacket.getData(), 0, imageFrame, frameSize,
                                        avioVideoPacket.getData().length);
                                frameSize += avioVideoPacket.getDataSize();
                            }
                            data = null;
                            avioVideoPacket = null;
                        }
                    }
                } else {

                }
            }

        }
    }

    private class AudioRecvThread extends BaseRunnable {
        private final String TAG = AudioRecvThread.class.getSimpleName();
        private int nRet = 0;
        private byte[] pAVStreamHeadData;
        private byte[] pAVStreamData;
        private int[] nRecvSize;
        private byte videoType = AVCmdConstant.CHANNEL_PPCS_REAL_AUDIO;

        public AudioRecvThread(byte videoType) {
            nRecvSize = new int[1];
            pAVStreamHeadData = new byte[AVIOAudioPacket.HEAD_LENGTH];
            this.videoType = videoType;
        }

        @Override
        public void run() {
            while (isRunning) {
                nRecvSize[0] = AVIOAudioPacket.HEAD_LENGTH;
                nRet = PPCS_APIs.PPCS_Read(m_handleSession, videoType, pAVStreamHeadData, nRecvSize, 5000);
                // GLog.i(TAG, " recv" + " nRet:" + nRet + " nRecvSize:" +
                // nRecvSize[0]);
                if (nRecvSize[0] > 0) {
                    AVIOAudioPacket avioAudioPacket = AnalysistAudioStreamPacket.unpackPacket(pAVStreamHeadData);
                    if (null != avioAudioPacket) {
                        nRecvSize[0] = avioAudioPacket.getLength();
                        pAVStreamData = null;
                        pAVStreamData = new byte[nRecvSize[0]];
                        nRet = PPCS_APIs.PPCS_Read(m_handleSession, videoType, pAVStreamData, nRecvSize, 5000);
//                        GLog.i(TAG, "nRet:" + nRet + " length:" + pAVStreamData.length);
                        if (nRecvSize[0] > 0) {
                            avioAudioPacket.setData(pAVStreamData);
                            if (null != mController)
                                mController.InputMediaG711(pAVStreamData);
                            // if (null != ppcsVideoListener) {
                            // ppcsVideoListener.avAudioStream(avioAudioPacket);
                            // }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void audioFramelen(byte[] g711_data) {
        // GLog.i(TAG, "audioFramelen :" + g711_data.length + "");
        // GLog.i(TAG, "audioFramelen data:" +
        // Util.byteArrayToHexString2(g711_data));
        byte[] data = AnalysistAudioStreamPacket.assemblePacket(g711_data);
        if (null != data) {
            writeRecord(data);
        }
    }
}
