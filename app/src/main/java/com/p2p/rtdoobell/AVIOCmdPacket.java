package com.p2p.rtdoobell;

import java.io.Serializable;

public class AVIOCmdPacket implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public final static int HEAD_LENGTH = 16;
	
	private int sync_code;// 1->问答 2->推送式
	private short cmd_code;// 信令码
	private short reseved_w1;// 保留
	private int reseved_d2;// 保留
	private int cmd_length;// 信令内容数据长度
	private byte[] cmdData;// json数据

	public AVIOCmdPacket() {

	}

	public AVIOCmdPacket(int sync_code, short cmd_code, byte[] cmdData) {
		this.sync_code = sync_code;
		this.cmd_code = cmd_code;
		this.cmdData = cmdData;
	}

	public void setSyncCode(int sync_code) {
		this.sync_code = sync_code;
	}

	public int getSyncCode() {
		return sync_code;
	}

	public void setCmdCode(short cmd_code) {
		this.cmd_code = cmd_code;
	}

	public short getCmdCode() {
		return cmd_code;
	}

	public void setResevedW1(short reseved_w1) {
		this.reseved_w1 = reseved_w1;
	}

	public short getResevedW1() {
		return reseved_w1;
	}

	public void setResevedW2(int reseved_d2) {
		this.reseved_d2 = reseved_d2;
	}

	public int getResevedW2() {
		return reseved_d2;
	}

	public void setCmdLength(int cmd_length) {
		this.cmd_length = cmd_length;
	}

	public int getCmdLength() {
		return cmd_length;
	}

	public void setCmdData(byte[] cmdData) {
		this.cmdData = cmdData;
	}

	public byte[] getCmdData() {
		return cmdData;
	}
}
