package com.p2p.rtdoobell;

import java.io.Serializable;

public class AVIOVideoPacket implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final int HEAD_LENGTH = 24;

	private int sign;// 同步头，固定值0x55AA55AA(4 byte)
	private byte s_type;// 流类型(1byte) 1-video
	private byte e_type;// 编码类型(1byte) 1-H264 2-MJPEG
	private byte f_type;// 视频帧类型(1byte) 1-I帧 2-P帧 3-B帧
	private byte f_rate;// 视频帧率类型(1byte)
	private short width;// 宽度(2byte)
	private short height;// 高度(2byte)
	private short packageCnt;// 当前帧总包数(2byte)
	private short curPackage;// 当前包包号，从1开始(2byte)
	private int tsnap;// 时间戳(us)(4byte)
	private short dataSize;// 负载数据有效长度 <=1000(2byte)
	private short reserve;// 0 保留位(2byte)
	private byte[] data;

	public AVIOVideoPacket() {

	}

	public void setSign(int sign) {
		this.sign = sign;
	}

	public int getSign() {
		return sign;
	}

	public void setSType(byte s_type) {
		this.s_type = s_type;
	}

	public byte getSType() {
		return s_type;
	}

	public void setEType(byte e_type) {
		this.e_type = e_type;
	}

	public byte getEType() {
		return e_type;
	}

	public void setFType(byte f_type) {
		this.f_type = f_type;
	}

	public byte getFType() {
		return f_type;
	}

	public void setFRate(byte f_rate) {
		this.f_rate = f_rate;
	}

	public byte getFRate() {
		return f_rate;
	}

	public void setWidth(short width) {
		this.width = width;
	}

	public short getWidth() {
		return width;
	}

	public void setHeight(short height) {
		this.height = height;
	}

	public short getHeight() {
		return height;
	}

	public void setPackageCnt(short packageCnt) {
		this.packageCnt = packageCnt;
	}

	public short getPackageCnt() {
		return packageCnt;
	}

	public void setCurPackage(short curPackage) {
		this.curPackage = curPackage;
	}

	public short getCurPackage() {
		return curPackage;
	}

	public void setTsnap(int tsnap) {
		this.tsnap = tsnap;
	}

	public int getTsnap() {
		return tsnap;
	}

	public void setDataSize(short dataSize) {
		this.dataSize = dataSize;
	}

	public short getDataSize() {
		return dataSize;
	}

	public void setReserve(short reserve) {
		this.reserve = reserve;
	}

	public short getReserve() {
		return reserve;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public byte[] getData() {
		return data;
	}

}
