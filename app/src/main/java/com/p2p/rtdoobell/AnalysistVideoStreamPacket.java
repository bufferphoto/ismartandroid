package com.p2p.rtdoobell;

import com.p2p.util.DoorBellUtil;

public class AnalysistVideoStreamPacket {

	private static final String TAG = AnalysistVideoStreamPacket.class.getSimpleName();

	public static AVIOVideoPacket unpackPacket(byte[] data) {
		// GLog.i(TAG, "unpackPacket data:" + Util.byteArrayToHexString2(data));
		AVIOVideoPacket streamPacket = null;
		if (null != data && data.length == AVIOVideoPacket.HEAD_LENGTH) {
			byte[] byte4 = new byte[4];
			byte[] byte2 = new byte[2];

			streamPacket = new AVIOVideoPacket();
			System.arraycopy(data, 0, byte4, 0, 4); // 长度4
			streamPacket.setSign(DoorBellUtil.byte2int(byte4));
			// GLog.i(TAG, "sign:" + streamPacket.getSign());
			if (streamPacket.getSign() != AVCmdConstant.SYNC_MODEL_QA) {
				// GLog.i(TAG, "error data");
				return null;
			}

			streamPacket.setSType(data[4]);
			streamPacket.setEType(data[5]);
			streamPacket.setFType(data[6]);
			// GLog.i(TAG, "FType:" + streamPacket.getFType());
			streamPacket.setFRate(data[7]);

			System.arraycopy(data, 8, byte2, 0, 2); // 长度2
			streamPacket.setWidth(DoorBellUtil.byte2Short(byte2));
			// GLog.i(TAG, "with:" + streamPacket.getWidth());

			System.arraycopy(data, 10, byte2, 0, 2); // 长度2
			streamPacket.setHeight(DoorBellUtil.byte2Short(byte2));
			// GLog.i(TAG, "hight:" + streamPacket.getHeight());

			System.arraycopy(data, 12, byte2, 0, 2); // 长度2
			streamPacket.setPackageCnt(DoorBellUtil.byte2Short(byte2));
			// GLog.i(TAG, "packageCnt:" + streamPacket.getPackageCnt());

			System.arraycopy(data, 14, byte2, 0, 2); // 长度2
			streamPacket.setCurPackage(DoorBellUtil.byte2Short(byte2));
			// GLog.i(TAG, "CurPackage:" + streamPacket.getCurPackage());

			System.arraycopy(data, 16, byte4, 0, 4); // 长度4
			streamPacket.setTsnap(DoorBellUtil.byte2int(byte4));
			// GLog.i(TAG, "tsnap:" + streamPacket.getTsnap());

			System.arraycopy(data, 20, byte2, 0, 2); // 长度2
			streamPacket.setDataSize(DoorBellUtil.byte2Short(byte2));
			// GLog.i(TAG, "dataSize:" + streamPacket.getDataSize());

			System.arraycopy(data, 22, byte2, 0, 2); // 长度2
			streamPacket.setReserve(DoorBellUtil.byte2Short(byte2));
			// GLog.i(TAG, "resver:" + streamPacket.getReserve());
		}
		return streamPacket;
	}
}
