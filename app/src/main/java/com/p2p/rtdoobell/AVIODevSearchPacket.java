package com.p2p.rtdoobell;

import java.io.Serializable;

public class AVIODevSearchPacket implements Serializable {

    private static final long serialVersionUID = 1L;

    public final static int HEAD_LENGTH = 8;

    public int sign;// 0x55aa55aa
    public short len;// 负载数据长度
    public byte maincmd;// 主命令0x2d
    public byte assisitcmd;// 辅助命令0x5a
    public DevInfo devInfo;

    public AVIODevSearchPacket() {
        devInfo = new DevInfo();
    }

    public DevInfo getDevList() {
        return devInfo;
    }

    public class DevInfo {
        public String uuid;
        public String ip;
        public String devMod;
        public String sofrVer;
        public int doornum;
    }
}
