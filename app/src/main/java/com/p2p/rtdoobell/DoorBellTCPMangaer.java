package com.p2p.rtdoobell;

import com.guogee.ismartandroid2.utils.GLog;
import com.p2p.util.DoorBellUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DoorBellTCPMangaer {

    private final static String TAG = DoorBellTCPMangaer.class.getSimpleName();
    private volatile static DoorBellTCPMangaer mInstance;
    private final static int TCP_LOCAL_PORT = 22270;
    public static final int FILED = 0;
    public static final int CONNECTED_FALED = 1;
    public static final int CMD_SUCCESS = 2;
    public static final int CMD_FAILED = 3;

    private ExecutorService mSendService;
    private ExecutorService mRecieveService;

    private Socket scoket;
    private OutputStream osSend;
    private String ip = "127.0.0.1";

    private DoorBellTCPMangaer() {
        mSendService = Executors.newSingleThreadExecutor();
        mRecieveService = Executors.newSingleThreadExecutor();
    }

    public static DoorBellTCPMangaer getInstance() {
        if (mInstance == null) {
            synchronized (DoorBellTCPMangaer.class) {
                if (mInstance == null) {
                    mInstance = new DoorBellTCPMangaer();
                }
            }
        }
        return mInstance;
    }

    public void sendData(String ip, String wifiName, String wifiPwd) {
        mSendService.execute(new SendTCPTask(ip, wifiName, wifiPwd));
    }

    // 用于链接服务器端 线程中执行
    private boolean connectToServer(String ip) {
        try {
            scoket = new Socket(ip, TCP_LOCAL_PORT);
            GLog.v(TAG, "Connected Host:" + ip);
            return true;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    // 断开与服务器端的链接
    private void disConnectToServer() {
        try {
            if (scoket != null && !scoket.isClosed()) {
                scoket.close();
            }
            if (osSend != null) {
                osSend.flush();
                osSend.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class SendTCPTask implements Runnable {
        private final int DATA_LENGTH = 80;
        private final int TAG = 0x11AA22BB;
        private final int CMD_TYPE = 0x100;
        private final int LENGTH = 64;
        private final int RESULT = 0;
        private String ip = "127.0.0.1";
        private String wifiName;
        private String wifiPwd;

        public SendTCPTask(String ip, String wifiName, String wifiPwd) {
            this.ip = ip;
            this.wifiName = wifiName;
            this.wifiPwd = wifiPwd;
        }

        @Override
        public void run() {
            try {
                if (connectToServer(ip)) {
                    mRecieveService.execute(new Reciever());
                    osSend = scoket.getOutputStream();
                    byte[] buffer = new byte[DATA_LENGTH];
                    // 组装头
                    System.arraycopy(DoorBellUtil.int2byte(TAG), 0, buffer, 0, 4);
                    System.arraycopy(DoorBellUtil.int2byte(CMD_TYPE), 0, buffer, 4, 4);
                    System.arraycopy(DoorBellUtil.int2byte(LENGTH), 0, buffer, 8, 4);
                    System.arraycopy(DoorBellUtil.int2byte(RESULT), 0, buffer, 12, 4);
                    // 组装data
                    System.arraycopy(wifiName.getBytes(), 0, buffer, 16, wifiName.getBytes().length);
                    System.arraycopy(wifiPwd.getBytes(), 0, buffer, 48, wifiPwd.getBytes().length);
                    // GLog.i("SendTCPTask", "buffer.length:" + buffer.length);
                    osSend.write(buffer, 0, buffer.length);
                    osSend.flush();
                } else {
                    listenerAndClose(CONNECTED_FALED);
                }
            } catch (IOException e) {
                e.printStackTrace();
                listenerAndClose(FILED);
            } catch (Exception e) {
                e.printStackTrace();
                listenerAndClose(FILED);
            }
        }
    }

    private class Reciever implements Runnable {
        private final int DATA_LENGTH = 80;
        private final int TAG = 0x11AA22BB;
        private final int CMD_TYPE = 0x101;
        private final int LENGTH = 32;
        private final int RESULT = 0;// 0成功。非0失败

        @Override
        public void run() {
            InputStream is = null;
            try {
                byte[] data = new byte[1024];
                is = scoket.getInputStream();
                int read = 0;
                while ((read = is.read(data)) != -1) {
                    // GLog.i("Reciever", "receive data:" + read);
                    if (read > 16) {
                        byte[] result = new byte[4];
                        System.arraycopy(data, 12, result, 0, 4);
                        if (DoorBellUtil.byte2int(result) == 0) {// 成功
                            if (read >= 48) {
                                byte[] uuid = new byte[32];
                                System.arraycopy(data, 16, uuid, 0, uuid.length);
                                String devUuid = new String(uuid, "UTF-8");
                                // GLog.i("Reciever", "devUuid:" + devUuid);
                                devUuid = devUuid.replace("\0", "");
                                listenerAndClose(CMD_SUCCESS, devUuid);
                            } else {
                                listenerAndClose(CMD_FAILED);
                            }
                        } else {// 失败
                            listenerAndClose(CMD_FAILED);
                        }
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                listenerAndClose(CMD_FAILED);
            } finally {
                // GLog.i("Reciever", "isFinish:");
                if (null != is) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void listenerAndClose(int status) {
        listenerAndClose(status, "");
    }

    private void listenerAndClose(int status, String uuid) {
        disConnectToServer();
        GLog.i(TAG, "  status:" + status + "  uuid:" + uuid);
        if (null != listener) {
            listener.configWifiResult(status, uuid);
        }
    }

    public void setConfigWifiListener(ConfigWifiListener listener) {
        this.listener = listener;
    }

    private ConfigWifiListener listener;

    public interface ConfigWifiListener {
        public void configWifiResult(int status, String uuid);
    }

}
