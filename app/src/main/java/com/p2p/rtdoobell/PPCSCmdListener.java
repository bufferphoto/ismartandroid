package com.p2p.rtdoobell;

public interface PPCSCmdListener {

	public void ppcsCmdCallBcakSuccess(AVIOCmdPacket packet);

	public void ppcsCmdCallBcakFail(int cmdCode, int errorRet);
}
