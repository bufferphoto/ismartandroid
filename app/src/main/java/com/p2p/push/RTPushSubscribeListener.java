package com.p2p.push;

/**
 * Created by Administrator on 2017/4/13.
 */
public interface RTPushSubscribeListener {
    /*
     *   step : 0(NDT_PPCS_Initialize)
             *   1(DPS_Initialize)
             *   2(DPS_TokenAcquire)
             *   3(WiPN_Query)
             *   4(WiPN_ChkSubscribe)
             *   5(WiPN_Subscribe)
             *   6(WiPN_UnSubscribe)
     * status:命令执行状态
     *   ret:错误值
     */
    public void rtPushSubscribeResult(RTPushDoorBellManager.SubscribleStep step, boolean status, int ret);
}
