package com.p2p.push;

import android.content.Context;
import android.content.SharedPreferences;

import com.dps.ppcs_api.DPS_API;
import com.guogee.ismartandroid2.manager.RoomManager;
import com.guogee.ismartandroid2.model.Device;
import com.guogee.ismartandroid2.networkingProtocol.DeviceType;
import com.guogee.ismartandroid2.utils.GLog;
import com.ndt.ppcs_api.NDT_API;
import com.ndt.ppcs_api.st_NDT_NetInfo;
import com.p2p.rtdoobell.BaseRunnable;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Administrator on 2017/4/11.
 */
public class RTPushDoorBellManager {

    private final static String TAG = RTPushDoorBellManager.class.getSimpleName();

    public static enum SubscribleStep {
        NDT_PPCS_Initialize, DPS_Initialize, DPS_TokenAcquire, WiPN_Query, WiPN_ChkSubscribe, WiPN_Subscribe, WiPN_UnSubscribe
    }

    private volatile static RTPushDoorBellManager mInstance;
    //// NDT Parameters:
    private final String aes128key = "0123456789ABCDEF"; //default value
    private final String initString = "EBGAEIBIKHJJGFJKEOGCFAEPHPMAHONDGJFPBKCPAJJMLFKBDBAGCJPBGOLKIKLKAJMJKFDOOFMOBECEJIMM";
    //// DPS Parameters:
    private final String dps_server = "120.25.220.150";
    private final int dps_port = 32750;
    private final String dps_key = "fpt@@CS2-Network";
    private final String gEncDecKey = "WiPN@CS2-Network";//Encryption and decryption KEY
    private final int mode = 1;
    private final String[] QueryDID = {"PPCS-014143-SBKHR", "PPCS-014144-RVDKK"};

    private Context context;
    private ExecutorService mSendCmdService;
    private CheckSubscribeThread checkSubscribeThread = null;
    private SubscribeThread subscribeThread = null;
    private UnSubscribeThread unSubscribeThread = null;
    private WiPN_StringEncDec iPNStringEncDec;
    // The subscriber name of APP
    //    private final String pkName = "DPS_Demo";
    public static final String pkName = "guogee";
    //EventCH:0~0xffffffff   Must be consistent with the device side
    private long gEventCH = 0;
    private Map<String, PushVo> map = null;

    private RTPushDoorBellManager(Context context) {
        this.context = context;
        iPNStringEncDec = new WiPN_StringEncDec();
        mSendCmdService = Executors.newSingleThreadExecutor();
        map = new HashMap<>();
        GLog.i(TAG, "  pkName:" + pkName);
    }

    public static RTPushDoorBellManager getInstance(Context context) {
        if (mInstance == null) {
            synchronized (RTPushDoorBellManager.class) {
                mInstance = new RTPushDoorBellManager(context.getApplicationContext());
            }
        }
        return mInstance;
    }

    public String getAPIVersion() {
        int[] Version = new int[1];
        return NDT_API.NDT_PPCS_GetAPIVersion(Version);
    }

    public int NDT_PPCS_Initialize() {
        return NDT_API.NDT_PPCS_Initialize(initString, 0, null, aes128key);
    }

    public int DPS_Initialize() {
        return DPS_API.DPS_Initialize(dps_server, dps_port, dps_key, 0);
    }

    public int NDT_PPCS_DeInitialize() {
        return NDT_API.NDT_PPCS_DeInitialize();
    }

    public int DPS_DeInitialize() {
        return DPS_API.DPS_DeInitialize();
    }

    public void subscribeAll() {
        List<Device> devices = RoomManager.getInstance(context).getDeviceByType(DeviceType.RT_DOORBELL);
        if (null != devices && devices.size() > 0) {
            for (Device device : devices) {
                GLog.i(TAG, "  subscribe device Addr():" + device.getAddr());
                subscribe(device.getAddr());
            }
        }
    }

    public void unSubscribeAll() {
        List<Device> devices = RoomManager.getInstance(context).getDeviceByType(DeviceType.RT_DOORBELL);
        if (null != devices && devices.size() > 0) {
            for (Device device : devices) {
                unSubscribe(device.getAddr());
            }
        }
    }

    public int ckeckSubscribe(String DID) {
        if (null == DID || DID.length() == 0) {
            return -1;
        }
//        if (null != checkSubscribeThread)
//            checkSubscribeThread.stop();
//        checkSubscribeThread = null;

//        checkSubscribeThread = new CheckSubscribeThread(DID);
        mSendCmdService.execute(new CheckSubscribeThread(DID));
        return 0;
    }

    public int subscribe(String DID) {
        if (null == DID || DID.length() == 0) {
            return -1;
        }
//        if (null != subscribeThread)
//            subscribeThread.stop();
//        subscribeThread = null;
//
//        subscribeThread = new SubscribeThread(DID);
        mSendCmdService.execute(new SubscribeThread(DID));
        return 0;
    }

    public int unSubscribe(String DID) {
        if (null == DID || DID.length() == 0) {
            return -1;
        }
//        if (null != unSubscribeThread)
//            unSubscribeThread.stop();
//        unSubscribeThread = null;
//
//        unSubscribeThread = new UnSubscribeThread(DID);
        mSendCmdService.execute(new UnSubscribeThread(DID));
        return 0;
    }

    public void NetworkDetect() {
        GLog.i(TAG, "NDT_NetInfo:");
        st_NDT_NetInfo NetInfo = new st_NDT_NetInfo();
        NDT_API.NDT_PPCS_NetworkDetect(NetInfo, 3000); //// wait for 3 sec
        GLog.i(TAG, "My Lan IP:" + NetInfo.getLanIP() + " Port=" + NetInfo.getLanPort());
        GLog.i(TAG, "My Wan IP:" + NetInfo.getWanIP() + " Port=" + NetInfo.getWanPort());
        GLog.i(TAG, "Server Hello Ack: " + (1 == NetInfo.bServerHelloAck ? "Yes" : "No"));
        if (0 == NetInfo.bServerHelloAck) {
            GLog.i(TAG, "*** Warning!! CS didn't response!!");
        }
    }

    private class CheckSubscribeThread extends BaseRunnable {
        private PushVo pushVo;

        public CheckSubscribeThread(String did) {
            pushVo = new PushVo();
            pushVo.gDID = did;
        }

        @Override
        public void run() {
            GLog.i(TAG, "   ---CheckSubscribeThread---  ");
            CheckSubscribe(pushVo, pushVo.gDID);
        }
    }

    private boolean CheckSubscribe(PushVo pushVo, String did) {
        if (null == pushVo) {
            pushVo = new PushVo();
            pushVo.gDID = did;
        }
        int ret = NDT_PPCS_Initialize();
        if (ret != NDT_API.NDT_ERROR_NoError && ret != NDT_API.NDT_ERROR_AlreadyInitialized) {
            //回调错误
            if (null != listener) {
                listener.rtPushSubscribeResult(SubscribleStep.NDT_PPCS_Initialize, false, ret);
            }
            return false;
        }
        if (null != listener) {
            listener.rtPushSubscribeResult(SubscribleStep.NDT_PPCS_Initialize, true, ret);
        }

        ret = DPS_Initialize();
        if (ret != DPS_API.ERROR_DPS_Successful && ret != DPS_API.ERROR_DPS_AlreadyInitialized) {
            //回调错误
            if (null != listener) {
                listener.rtPushSubscribeResult(SubscribleStep.DPS_Initialize, false, ret);
            }
            return false;
        }
        if (null != listener) {
            listener.rtPushSubscribeResult(SubscribleStep.DPS_Initialize, true, ret);
        }

        ret = DPS_TokenAcquire(pushVo);
        if (ret < 0) {
            //回调错误
            if (null != listener) {
                listener.rtPushSubscribeResult(SubscribleStep.DPS_TokenAcquire, false, ret);
            }
            return false;
        }
        if (null != listener) {
            listener.rtPushSubscribeResult(SubscribleStep.DPS_TokenAcquire, true, ret);
        }

        ret = WiPN_Query(pushVo, QueryDID);
        if (ret < 0 || pushVo.UTCTServerTime <= 0) {
            //回调错误
            if (null != listener) {
                listener.rtPushSubscribeResult(SubscribleStep.WiPN_Query, false, ret);
            }
            return false;
        }
        if (null != listener) {
            listener.rtPushSubscribeResult(SubscribleStep.WiPN_Query, true, ret);
        }

        boolean isSubscribe = false;
        ret = WiPN_ChkSubscribe(pushVo);
        if (ret < 0) {
            //回调错误
            if (null != listener) {
                listener.rtPushSubscribeResult(SubscribleStep.WiPN_ChkSubscribe, false, ret);
            }
        }else{
            if (null != listener) {
                listener.rtPushSubscribeResult(SubscribleStep.WiPN_ChkSubscribe, true, ret);
            }
            isSubscribe = true;
        }

        map.put(pushVo.gDID, pushVo);
        return isSubscribe;
    }

    public class SubscribeThread extends BaseRunnable {
        private PushVo pushVo;
        private String did;

        public SubscribeThread(String did) {
            pushVo = map.get(did);
            this.did = did;
        }

        @Override
        public void run() {
            GLog.i(TAG, "   ---SubscribeThread---  pushVo:" + pushVo);
            boolean isSubscrible = false;
            if (null == pushVo) {
                isSubscrible =  CheckSubscribe(pushVo, did);
            }
            pushVo = map.get(did);
            GLog.i(TAG, "   ---SubscribeThread---  pushVo:" + pushVo);
            if (null == pushVo || isSubscrible) {
                return;
            }
            int ret = WiPN_Subscribe(pushVo);
            boolean status = true;
            if (ret < 0) {
                status = false;
            }
            if (null != listener) {
                listener.rtPushSubscribeResult(SubscribleStep.WiPN_Subscribe, status, ret);
            }
        }
    }

    public class UnSubscribeThread extends BaseRunnable {
        private PushVo pushVo;

        public UnSubscribeThread(String did) {
            pushVo = map.get(did);
        }

        @Override
        public void run() {
            GLog.i(TAG, "   ---UnSubscribeThread---  pushVo:" + pushVo);
            if (null == pushVo) {
                return;
            }
            int ret = WiPN_UnSubscribe(pushVo);
            boolean status = true;
            if (ret < 0) {
                status = false;
            }
            if (null != listener) {
                map.remove(pushVo.gDID);
                listener.rtPushSubscribeResult(SubscribleStep.WiPN_UnSubscribe, status, ret);
            }
        }
    }

    public void setRTPushSubscribeListener(RTPushSubscribeListener listener) {
        this.listener = listener;
    }

    private RTPushSubscribeListener listener;

    /*
        RecvBuf 48个字节,不能为null
        token在RecvBuf返回获取方式   DPS_token = new String(Arrays.copyOf(RecvBuf, 32));
         return <0 失败 >=0成功
     */
    private int DPS_TokenAcquire(PushVo pushVo) {
        int ret = 0;
        pushVo.DPS_token = getTokenByLocal();
        GLog.i(TAG, "  pushVo.DPS_token:" + pushVo.DPS_token);
        if (null == pushVo.DPS_token || pushVo.DPS_token.isEmpty()) {
            byte[] RecvBuf = new byte[48];
            Arrays.fill(RecvBuf, (byte) 0);
            ret = DPS_API.DPS_TokenAcquire(RecvBuf, 48);
            if (ret >= 0) {
                pushVo.DPS_token = new String(Arrays.copyOf(RecvBuf, 32));
                saveTokenToLocal(pushVo.DPS_token);
                GLog.i(TAG, " pushVo.DPS_token :" + pushVo.DPS_token);
            } else {
                GLog.i(TAG, " pushVo.DPS_token  fail ");
            }
        }
        return ret;
    }

    //Query, access to relevant information from the QueryServer
    private int WiPN_Query(PushVo pushVo, String[] _QS_DID) {
        int count = 0;
        int ret = -1;
        int QueryHandle = -1;
        byte[] Enc = new byte[256];
        Arrays.fill(Enc, (byte) 0);
        String QueryCommand = "DID=" + pushVo.gDID + "&";
        iPNStringEncDec.iPN_StringEnc(gEncDecKey.getBytes(), QueryCommand.getBytes(), Enc, Enc.length);
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        int index = (random.nextInt() & Integer.MAX_VALUE) % _QS_DID.length;
        long time = System.currentTimeMillis();
        long queryTime = 20 * 1000;//查询10s
        while (true) {
            if (QueryHandle < 0) {
                if (System.currentTimeMillis() - time > queryTime) {
                    ret = QueryHandle;
                    break;
                }
                for (int i = 0; i < _QS_DID.length; i++) {
                    index = (index + 1) % _QS_DID.length;
                    GLog.i(TAG, "index:" + index);
                    QueryHandle = NDT_API.NDT_PPCS_SendTo(_QS_DID[index], Enc, Enc.length, mode);
                    GLog.i(TAG, "QueryHandle:" + QueryHandle);
                    if (QueryHandle < 0) {
                        continue;
                    } else {
                        break;
                    }
                }
            } else {
                GLog.i(TAG, "查询中...");
                GLog.i(TAG, "NDT_PPCS_SendTo() ret = " + QueryHandle);
                byte[] RecvBuf = new byte[1280];
                byte[] dest = new byte[1280];
                int[] Size1 = new int[1];
                Arrays.fill(RecvBuf, (byte) 0);
                Size1[0] = RecvBuf.length;

                ret = NDT_API.NDT_PPCS_RecvFrom(QueryHandle, RecvBuf, Size1, 10 * 1000);
                if (ret == NDT_API.NDT_ERROR_NoError) {
                    iPNStringEncDec.iPN_StringDnc(gEncDecKey.getBytes(), RecvBuf, dest, dest.length);
                    String QSResponse = new String(dest);
                    QSResponse = QSResponse.replace("\0", "");
                    Split_String(pushVo, QSResponse);
                    break;
                } else if (ret == NDT_API.NDT_ERROR_NoAckFromDevice || ret == NDT_API.NDT_ERROR_NoPushServerKnowDevice || ret == NDT_API.NDT_ERROR_TimeOut) {
                    count++;
                    if (count == 3) {
                        GLog.i(TAG, "Query Fail! ret = " + ret);
                        break;
                    }
                } else if (NDT_API.NDT_ERROR_RemoteHandleClosed == ret) {
                    GLog.i(TAG, "Query Fail! ret = " + ret);
                    break;
                } else {
                    GLog.i(TAG, "Query Fail! ret = " + ret);
                    break;
                }
            }
        }

        NDT_API.NDT_PPCS_CloseHandle(QueryHandle);
        return ret;
    }

    private int WiPN_ChkSubscribe(PushVo pushVo) {
        byte[] Enc = new byte[1280];
        byte[] RecvBuf = new byte[1280];
        byte[] dest = new byte[1280];
        Arrays.fill(Enc, (byte) 0);
        Arrays.fill(RecvBuf, (byte) 0);
        Arrays.fill(dest, (byte) 0);
        int[] size = new int[1];
        int[] size1 = new int[1];
        size[0] = Enc.length;
        size1[0] = RecvBuf.length;
        int i = 0;
        int ret = -1;
        int count = 0;

        String Command = "DID=" + pushVo.gDID + "&CH=" + gEventCH + "&AG=DPS" + "&APP=" + pkName + "&INFO=" + pushVo.DPS_token + "&ACT=ChkSubscribe&";
        GLog.i(TAG, Command);
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        int index = (random.nextInt() & Integer.MAX_VALUE) % pushVo.SubNum;
        for (i = 0; i < pushVo.SubNum; i++) {
            long curTime = System.currentTimeMillis();
            String SubCommand = Command + "UTCT=0x" + Long.toHexString(pushVo.UTCTServerTime + (curTime - pushVo.gRecvTime) / 1000) + "&";
            GLog.i(TAG, SubCommand);
            iPNStringEncDec.iPN_StringEnc(gEncDecKey.getBytes(), SubCommand.getBytes(), Enc, size[0]);
            index = (index + 1) % pushVo.SubNum;
            GLog.i(TAG, "SubDID:" + pushVo.SubDID[index]);
            int SubHandle = NDT_API.NDT_PPCS_SendTo(pushVo.SubDID[index], Enc, Enc.length, mode);
            if (SubHandle < 0) {
                GLog.i(TAG, "ChkSubscribe SendTo Fail,DID=" + pushVo.SubDID[index] + "SubHandle = " + SubHandle);
                ret = SubHandle;
                continue;
            } else {
                while (true) {
                    ret = NDT_API.NDT_PPCS_RecvFrom(SubHandle, RecvBuf, size1, 5 * 1000);
                    if (ret == NDT_API.NDT_ERROR_NoError) {
                        pushVo.gRecvTime = System.currentTimeMillis();
                        iPNStringEncDec.iPN_StringDnc(gEncDecKey.getBytes(), RecvBuf, dest, dest.length);
                        String SubServerResponse = new String(dest);
                        GLog.i(TAG, "resp:" + SubServerResponse);
                        int indexUTCT = SubServerResponse.indexOf("UTCT=");
                        int Lastindex = SubServerResponse.lastIndexOf("&");
                        String UTCT = SubServerResponse.substring(indexUTCT + 5, Lastindex);

                        pushVo.UTCTServerTime = Long.parseLong(UTCT.substring(2), 16);
                        int start = SubServerResponse.indexOf("List=");
                        int end = SubServerResponse.indexOf("&");
                        String str = SubServerResponse.substring(start + 5, end);
                        GLog.i(TAG, "str = " + str);
                        String[] DID_CHs = str.split(",");
                        ret = NDT_API.DID_ALREAD_UNSUBSCRIBLE;
                        for (String did_ch : DID_CHs) {
                            String[] v = did_ch.split(":");
                            if (v.length > 1) {
                                System.out.println(v[0] + ":" + v[1]);
                                if (v[0].equals(pushVo.gDID) && Integer.parseInt(v[1]) == gEventCH) {
                                    ret = NDT_API.DID_ALREAD_SUBSCRIBLE;
                                }
                            }
                        }
                        break;
                    } else if (ret == NDT_API.NDT_ERROR_NoAckFromDevice || ret == NDT_API.NDT_ERROR_NoPushServerKnowDevice || ret == NDT_API.NDT_ERROR_TimeOut) {
                        count++;
                        if (count == 3) {
                            break;
                        }
                    } else {
                        count = 0;
                        break;
                    }
                }
                NDT_API.NDT_PPCS_CloseHandle(SubHandle);
            }
        }
        return ret;
    }

    private int WiPN_Subscribe(PushVo pushVo) {
        byte[] Enc = new byte[1280];
        byte[] RecvBuf = new byte[1280];
        byte[] dest = new byte[1280];
        Arrays.fill(Enc, (byte) 0);
        Arrays.fill(RecvBuf, (byte) 0);
        Arrays.fill(dest, (byte) 0);
        int[] size = new int[1];
        int[] size1 = new int[1];
        size[0] = Enc.length;
        size1[0] = RecvBuf.length;
        int i = 0;
        int ret = -1;
        int count = 0;
        String Command = "";
        int gEventCH = 0;
        Command = "DID=" + pushVo.gDID + "&CH=" + gEventCH + "&AG=DPS" + "&APP=" + pkName + "&INFO=" + pushVo.DPS_token + "&ACT=Subscribe&";
        GLog.i(TAG, Command);
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        int index = (random.nextInt() & Integer.MAX_VALUE) % pushVo.SubNum;
        for (i = 0; i < pushVo.SubNum; i++) {
            long curTime = System.currentTimeMillis();
            GLog.i(TAG, pushVo.UTCTServerTime + "   " + pushVo.gRecvTime);
            String SubCommand = Command + "UTCT=0x" + Long.toHexString(pushVo.UTCTServerTime + (curTime - pushVo.gRecvTime) / 1000) + "&";
            GLog.i(TAG, SubCommand);
            iPNStringEncDec.iPN_StringEnc(gEncDecKey.getBytes(), SubCommand.getBytes(), Enc, size[0]);
            index = (index + 1) % pushVo.SubNum;
            GLog.i(TAG, "SubDID:" + pushVo.SubDID[index]);
//            int SubHandle = NDT_API.NDT_PPCS_SendTo(SERVER_DID, Enc, Enc.length, mode);
            int SubHandle = NDT_API.NDT_PPCS_SendTo(pushVo.SubDID[index], Enc, Enc.length, mode);
            if (SubHandle < 0) {
                GLog.i(TAG, "Subscribe SendTo Fail,SubHandle = " + SubHandle);
                ret = SubHandle;
                continue;
            } else {
                while (true) {
                    ret = NDT_API.NDT_PPCS_RecvFrom(SubHandle, RecvBuf, size1, 5 * 1000);
                    if (ret == NDT_API.NDT_ERROR_NoError) {
                        pushVo.gRecvTime = System.currentTimeMillis();
                        iPNStringEncDec.iPN_StringDnc(gEncDecKey.getBytes(), RecvBuf, dest, dest.length);
                        String SubServerResponse = new String(dest);
                        System.out.println(SubServerResponse);
                        int indexUTCT = SubServerResponse.indexOf("UTCT=");
                        int Lastindex = SubServerResponse.lastIndexOf("&");
                        String UTCT = SubServerResponse.substring(indexUTCT + 5, Lastindex);
                        System.out.println(UTCT);
                        pushVo.UTCTServerTime = Long.parseLong(UTCT.substring(2), 16);
                        int start = SubServerResponse.indexOf("RET=");
                        int end = SubServerResponse.indexOf("&");
                        String str = SubServerResponse.substring(start + 4, end);
                        GLog.i(TAG, "str = " + str);
                        if (str.equals("OK")) {
                            GLog.i(TAG, "DID:" + pushVo.gDID + ", EventCH:" + gEventCH + " 订阅成功.");
                            ret = NDT_API.DID_ALREAD_SUBSCRIBLE;
                            NDT_API.NDT_PPCS_CloseHandle(SubHandle);
                            return ret;
                        } else {
                            GLog.i(TAG, "DID:" + pushVo.gDID + ", EventCH:" + gEventCH + " 订阅失败！请看下面错误原因.");
                            GLog.i(TAG, SubServerResponse);
                            ret = NDT_API.DID_ALREAD_UNSUBSCRIBLE;
                        }
                        count = 0;
                        break;
                    } else if (ret == NDT_API.NDT_ERROR_NoAckFromDevice || ret == NDT_API.NDT_ERROR_NoPushServerKnowDevice || ret == NDT_API.NDT_ERROR_TimeOut) {
                        count++;
                        if (count == 3) {
                            break;
                        }
                    } else {
                        count = 0;
                        break;
                    }
                }
                NDT_API.NDT_PPCS_CloseHandle(SubHandle);
            }
        }
        if (i == pushVo.SubNum) {
            GLog.i(TAG, pushVo.gDID + "订阅失败！");
        }
        return ret;
    }

    private int WiPN_UnSubscribe(PushVo pushVo) {
        byte[] Enc = new byte[1280];
        byte[] RecvBuf = new byte[1280];
        byte[] dest = new byte[1280];
        Arrays.fill(Enc, (byte) 0);
        Arrays.fill(RecvBuf, (byte) 0);
        Arrays.fill(dest, (byte) 0);
        int[] size = new int[1];
        int[] size1 = new int[1];
        size[0] = Enc.length;
        size1[0] = RecvBuf.length;
        int i = 0;
        int ret = -1;
        int count = 0;
        String Command = "";
        int gEventCH = 0;
        Command = "DID=" + pushVo.gDID + "&CH=" + gEventCH + "&AG=DPS" + "&APP=" + pkName + "&INFO=" + pushVo.DPS_token + "&";
        GLog.i(TAG, "WiPN_UnSubscribe, Cmd:" + Command);
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        int index = (random.nextInt() & Integer.MAX_VALUE) % pushVo.SubNum;
        for (i = 0; i < pushVo.SubNum; i++) {
            long curTime = System.currentTimeMillis();
            String UnSubCommand = Command + "UTCT=0x" + Long.toHexString(pushVo.UTCTServerTime + (curTime - pushVo.gRecvTime) / 1000) + "&" + "ACT=UnSubscribe&";
            iPNStringEncDec.iPN_StringEnc(gEncDecKey.getBytes(), UnSubCommand.getBytes(), Enc, size[0]);
            index = (index + 1) % pushVo.SubNum;
            int SubHandle = NDT_API.NDT_PPCS_SendTo(pushVo.SubDID[index], Enc, Enc.length, mode);
            if (SubHandle < 0) {
                GLog.i(TAG, "UnSubscribe SendTo Fail,UnSubHandle = " + SubHandle);
                ret = SubHandle;
                continue;
            } else {
                while (true) {
                    ret = NDT_API.NDT_PPCS_RecvFrom(SubHandle, RecvBuf, size1, 5 * 1000);
                    if (ret == NDT_API.NDT_ERROR_NoError) {
                        pushVo.gRecvTime = System.currentTimeMillis();
                        iPNStringEncDec.iPN_StringDnc(gEncDecKey.getBytes(), RecvBuf, dest, dest.length);
                        String SubServerResponse = new String(dest);
                        System.out.println(SubServerResponse);
                        int indexUTCT = SubServerResponse.indexOf("UTCT=");
                        int Lastindex = SubServerResponse.lastIndexOf("&");
                        String UTCT = SubServerResponse.substring(indexUTCT + 5, Lastindex);
                        System.out.println(UTCT);
                        pushVo.UTCTServerTime = Long.parseLong(UTCT.substring(2), 16);
                        int start = SubServerResponse.indexOf("RET=");
                        int end = SubServerResponse.indexOf("&");
                        System.out.println(start);
                        System.out.println(end);
                        String str = SubServerResponse.substring(start + 4, end);
                        GLog.i(TAG, "str = " + str);
                        if (str.equals("OK")) {
                            GLog.i(TAG, "DID:" + pushVo.gDID + ", EventCH:" + gEventCH + " 取消订阅成功.");
                            NDT_API.NDT_PPCS_CloseHandle(SubHandle);
                            ret = NDT_API.DID_ALREAD_UNSUBSCRIBLE;
                            return ret;
                        } else {
                            GLog.i(TAG, "DID:" + pushVo.gDID + ", EventCH:" + gEventCH + " 取消订阅失败.");
                            GLog.i(TAG, SubServerResponse);
                            ret = NDT_API.DID_ALREAD_SUBSCRIBLE;
                        }
                        count = 0;
                        break;
                    } else if (ret == NDT_API.NDT_ERROR_NoAckFromDevice || ret == NDT_API.NDT_ERROR_NoPushServerKnowDevice || ret == NDT_API.NDT_ERROR_TimeOut) {
                        count++;
                        if (count == 3) {
                            break;
                        }
                    } else {
                        count = 0;
                        break;
                    }
                }
                NDT_API.NDT_PPCS_CloseHandle(SubHandle);
            }
        }
        if (i == pushVo.SubNum) {
            GLog.i(TAG, "取消订阅失败！");
        }
        return ret;
    }

    private final Lock _mutex = new ReentrantLock(true);

    private void Split_String(PushVo pushVo, String _QSResponse) {
        GLog.i(TAG, "Split_String:" + _QSResponse);
        int index = _QSResponse.indexOf("Subs=");
        if(index == -1){
            return;
        }
        String SubscribeServerString = _QSResponse.substring(index);
        GLog.i(TAG, "SubscribeServerString = " + SubscribeServerString);
        int startNum = SubscribeServerString.indexOf("=");
        int endNum = SubscribeServerString.indexOf(",");
        int SubNum = Integer.valueOf(SubscribeServerString.substring(startNum + 1, endNum));
        pushVo.SubNum = SubNum;
        GLog.i(TAG, "SubNum = " + SubNum);
        int indexUTCT = SubscribeServerString.indexOf("UTCT=");
        String subDidString = SubscribeServerString.substring(endNum + 1, indexUTCT - 1);
        GLog.i(TAG, "subDidString = " + subDidString);
        pushVo.SubDID = subDidString.split(",");
        GLog.i(TAG, "subDidString = " + subDidString);
        int Lastindex = SubscribeServerString.lastIndexOf("&");
        String UTCT = SubscribeServerString.substring(indexUTCT + 5, Lastindex);
        GLog.i(TAG, "UTCT:" + UTCT);
        pushVo.UTCTServerTime = Long.parseLong(UTCT.substring(2), 16);
        GLog.i(TAG, "UTCTServerTime = " + pushVo.UTCTServerTime);
        pushVo.gRecvTime = System.currentTimeMillis();
        GLog.i(TAG, "gRecvTime = " + pushVo.gRecvTime);
    }


    public String getTokenByLocal() {
        SharedPreferences settings = context.getSharedPreferences(pkName, 0);
        return settings.getString("DPS_TOKEN", "");
    }

    public void saveTokenToLocal(String token) {
        SharedPreferences settings = context.getSharedPreferences(pkName, 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString("DPS_TOKEN", token);
        PE.commit();
    }


    public class PushVo {
        public String gDID;
        public String DPS_token;
        public int SubNum;
        public String[] SubDID;
        public long UTCTServerTime;
        public long gRecvTime;
    }
}
